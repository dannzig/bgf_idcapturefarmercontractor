package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "contracting_agent")
//@SyncDescription(service_id = "1", service_name = "Contracting Agent", download_link = "/Employees/Details/RebindGridcontractingAgent", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class ContractingAgent extends SelectionData implements Serializable {


    @DynamicProperty(json_key = "natid")
    public String natid;


   @DynamicProperty(json_key = "employeeno")
    public String employeeno;






    public ContractingAgent() {
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }

}
