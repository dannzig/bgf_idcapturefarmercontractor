package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "Competition_assessment_survey_entry")

@SyncDescription(service_name = "U Competition assessment survey entry", upload_link = "/FarmersContract/Group/AddGroupCompetitionProgress", service_type = SyncDescription.service_type.Upload)
@SyncDescription(chunk_size = 100000, service_name = "D Competition assessment survey entry", download_link = "/FarmersContract/Group/MobileFarmerGroupCompetition", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class CompetitionAssessmentSurveyEntry extends SurveyEntry implements Serializable {

//    {
//            "$id": "6",
//            "id": 1,
//            "group_id": 170,
//            "achieve_score": 2.0,
//            "total_marks": 10.0,
//            "seedlings_issued": 1,
//            "seedlings_surviving": 1,
//            "transaction_no": "202764",
//            "datecomparer": 17023270044237438,
//            "date_time": "2023-12-01T00:00:00",
//            "status": true,
//            "user_id": 1,
//            "group_name": "TestBtb"
//    }

    @DynamicProperty(json_key = "group_id")
    public String group_id;

    @DynamicProperty(json_key = "group_name")
    public String group_name;

    @DynamicProperty(json_key = "group_transaction_no")
    public String group_transaction_no;

    @DynamicProperty(json_key = "achieve_score")
    public String achieve_score;

    @DynamicProperty(json_key = "date_time")
    public String date_time;

    @DynamicProperty(json_key = "status")
    public String status;


    public SurveyQuestionChoice surveyQuestionChoice = new SurveyQuestionChoice();

    public CompetitionAssessmentSurveyEntry() {

    }

    public CompetitionAssessmentSurveyEntry(String group_id,String group_transaction_no, String survey, String total_score) {
        this.group_id = group_id;
        this.group_transaction_no = group_transaction_no;
        this.survey = survey;
        this.total_score = total_score;
        this.achieve_score = total_score;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public CompetitionAssessmentSurveyEntry(String group_id, String survey, String total_score) {
        this.group_id = group_id;
        this.survey = survey;
        this.total_score = total_score;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

}
