package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.system.MultiSelectionData;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.spartautils.svars;


public class MultiCheckAdapter extends RecyclerView.Adapter<MultiCheckAdapter.view> {

    Context cntxt;
    public ArrayList<MultiSelectionData> items;
    onFilterUpdatedListener listener;
    ViewGroup parent;

    public interface onFilterUpdatedListener {

        void onSelectionUpdated(MultiSelectionData multiSelectionData, ArrayList<MultiSelectionData> selectedItems);
    }


    public MultiCheckAdapter(ArrayList<MultiSelectionData> items, @NonNull onFilterUpdatedListener listener) {

        this.items = items;
        this.listener = listener;
        listener.onSelectionUpdated(null,selectedSearchFilter());


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        this.parent = parent;
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_multi_selection_data, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CheckBox checkBox;

        int position;


        view(View itemView) {
            super(itemView);

            checkBox = itemView.findViewById(R.id.title);


        }

        void populate(int position) {
            this.position = position;
            MultiSelectionData multiSelectionData = items.get(position);

            MultiSelectionData savedSearchFilterItem = svars.workingObject(MultiSelectionData.class, multiSelectionData.sid);
            if (savedSearchFilterItem != null) {
                multiSelectionData.selected = savedSearchFilterItem.selected;
            }

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(multiSelectionData.selected==b){
                        return;
                    }

                    multiSelectionData.selected = b;
                    svars.setWorkingObject(multiSelectionData, multiSelectionData.sid);
                    try {
                        notifyDataSetChanged();
                    } catch (Exception ex) {
                    }
                    listener.onSelectionUpdated(multiSelectionData,selectedSearchFilter());
                }
            });
            checkBox.setChecked(multiSelectionData.selected);
            checkBox.setText(multiSelectionData.name);
        }



        @Override
        public void onClick(View view) {

        }
    }
    String[] tableFilter;



    ArrayList<MultiSelectionData> selectedSearchFilter() {
        ArrayList<MultiSelectionData> searchFilterItems = new ArrayList<>();
        for (MultiSelectionData multiSelectionData : items) {
            MultiSelectionData savedSearchFilterItem= svars.workingObject(MultiSelectionData.class, multiSelectionData.sid);
            if (savedSearchFilterItem!=null&&savedSearchFilterItem.selected) {
                searchFilterItems.add(savedSearchFilterItem);
            }
        }

        return searchFilterItems;
    }






}
