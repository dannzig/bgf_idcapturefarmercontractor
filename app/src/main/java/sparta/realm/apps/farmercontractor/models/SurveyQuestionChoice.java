package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "survey_question_choice")
@SyncDescription(service_name = "Survey question choice", download_link = "/FarmersContract/Questionnaire/getAllquestionandRating", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class SurveyQuestionChoice extends RealmModel implements Serializable {

//    {"$id":"6"
//    ,"id":1
//    ,"farmer_assessment_questionnaire_id":1
//    ,"rating":"BAD"
//    ,"marks_from":0.0
//    ,"marks_to":3.0
//    ,"comment":null
//    ,"datecomparer":1}

    @DynamicProperty(json_key = "farmer_assessment_questionnaire_id")
    public String question;

    @DynamicProperty(json_key = "rating")
    public String choice;

    @DynamicProperty(json_key = "comment")
    public String choice_description;

    @DynamicProperty(json_key = "rating_index")
    public String choice_index;

    @DynamicProperty(json_key = "index_display_mode")
    public String index_display_mode;

    @DynamicProperty(json_key = "marks_from")
    public String marks_from;

    @DynamicProperty(json_key = "marks_to")
    public String marks_to;

    public SurveyQuestionChoice() {


    }

    public SurveyQuestionChoice(String sid, String question, String choice, String choice_description, String choice_index, String index_display_mode, String marks_from, String marks_to) {
        this.choice = choice;
        this.choice_description = choice_description;
        this.choice_index = choice_index;
        this.index_display_mode = index_display_mode;
        this.marks_from = marks_from;
        this.marks_to = marks_to;
        this.sid = sid;
        this.question = question;
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }

}
