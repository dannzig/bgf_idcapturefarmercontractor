package sparta.realm.apps.farmercontractor.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.databinding.ActivitySeedlingProductionBinding;
import sparta.realm.apps.farmercontractor.models.AgroForestAgent;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.NurseryTypes;
import sparta.realm.apps.farmercontractor.models.SeedlingProduction;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.models.system.ValidationRules;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;

public class SeedlingsProduction extends AppCompatActivity {
    ActivitySeedlingProductionBinding binding;
    ArrayList<SeedlingProduction> lastSaved;
    Button save;
    String currentTime;
    DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySeedlingProductionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        lastSaved = Realm.databaseManager.loadObjectArray(SeedlingProduction.class, new Query().setCustomQuery("SELECT * FROM seedling_production WHERE _id IN ( SELECT max( _id ) FROM seedling_production )"));
        Log.e("Last saved", "onCreate: "+lastSaved);

        save = binding.btnSave;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveDialog();
            }
        });
        initUi();
    }

    SearchSpinner.InputListener nurseryTypeInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            NurseryTypes nurseryTypes = Realm.databaseManager.loadObject(NurseryTypes.class, new Query().setTableFilters("sid='" + input + "'"));
//            if (nurseryTypes != null) {
//                binding.nurseryInput.setInput(nurseryTypes.nursery_type);
//            }
        }
    };
    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);
            binding.agentInput.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query().setTableFilters("country='" + input + "'")), agentInputListener);

//            binding.countyInput.setInput(member.county);
//            binding.siteInput.setInput(member.site);
//            binding.agentInput.setInput(null);
//            binding.locationInput.setInput(null);
//            binding.subLocationInput.setInput(null);
//            binding.villageInput.setInput(null);

            if (input.equals("110")) {
                binding.countyInput.setTitle("County");
                binding.locationInput.setTitle("Ward");
                binding.subLocationInput.setTitle("Sub-Location");
            }
            if (input.equals("222")) {
                binding.countyInput.setTitle("District");
                binding.locationInput.setTitle("Sub-County");
                binding.subLocationInput.setTitle("Parish");
            }
        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
//            binding.locationInput.setInput(member.location);
//            binding.subLocationInput.setInput(null);
//            binding.villageInput.setInput(null);
        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
//            binding.subLocationInput.setInput(member.sub_location);
//            binding.villageInput.setInput(null);
        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
//            binding.villageInput.setInput(member.village);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("county='" + input + "'")), (sparta.realm.utils.FormTools.SearchSpinner.InputListener) siteInputListener);
//            binding.siteInput.setInput(member.site);
        }
    };

    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

        }
    };

    SearchSpinner.InputListener agentInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            AgroForestAgent nurseryAgents= Realm.databaseManager.loadObject(AgroForestAgent.class, new Query().setTableFilters("sid='" + input + "'"));
//            if (nurseryAgents != null) {
//                binding.agentInput.setInput(nurseryAgents.name);
//            }
        }
    };

    void initUi() {
        binding.nurseryInput.setDataset(NurseryTypes.class.getName(), nurseryTypeInputListener);
        binding.countryInput.setDataset(Country.class.getName(), countryInputListener);

        ValidationRules validationRules = new ValidationRules();
        validationRules.mandatory = ValidationRules.MandatoryStatus.Mandatory.ordinal() + "";
        validationRules.text_input_type = ValidationRules.TextInputType.numeric.ordinal() + "";
        binding.seedsSownInput.setValidationRules(validationRules);
        binding.seedsPrickedInput.setValidationRules(validationRules);
        binding.totalInput.setValidationRules(validationRules);
        binding.readySeedsInput.setValidationRules(validationRules);
        binding.rejectInput.setValidationRules(validationRules);
        binding.deadInput.setValidationRules(validationRules);

        populate(lastSaved);
    }

    void populate(ArrayList<SeedlingProduction> seedlingProduction){
        if (seedlingProduction != null){
            int size = seedlingProduction.size();
            Log.e("size","size is "+size);
            binding.nurseryInput.setInput(seedlingProduction.get(0).nursery_type);
            binding.countryInput.setInput(seedlingProduction.get(0).country);
            binding.countyInput.setInput(seedlingProduction.get(0).county);
            binding.locationInput.setInput(seedlingProduction.get(0).location);
            binding.subLocationInput.setInput(seedlingProduction.get(0).sub_location);
            binding.villageInput.setInput(seedlingProduction.get(0).village);
            binding.siteInput.setInput(seedlingProduction.get(0).site);
            binding.agentInput.setInput(seedlingProduction.get(0).nursery_agent);
            binding.seedsSownInput.setInput(seedlingProduction.get(0).seeds_sown);
            binding.seedsPrickedInput.setInput(seedlingProduction.get(0).seeds_pricked_out);
            binding.totalInput.setInput(seedlingProduction.get(0).total_seedlings);
            binding.readySeedsInput.setInput(seedlingProduction.get(0).distribution_seedlings);
            binding.rejectInput.setInput(seedlingProduction.get(0).reject_seedlings);
            binding.deadInput.setInput(seedlingProduction.get(0).dead_seedlings);
        }
    }

    boolean recordSaved = false;
    void showSaveDialog() {
        currentTime = formatter.format(Calendar.getInstance().getTime());
        if (validated() ) {
            sparta.realm.apps.farmercontractor.models.SeedlingProduction seedlingProduction1 = new sparta.realm.apps.farmercontractor.models.SeedlingProduction(
                     binding.nurseryInput.getInput()
                    , binding.countryInput.getInput()
                    , binding.countyInput.getInput()
                    , binding.locationInput.getInput()
                    , binding.subLocationInput.getInput()
                    , binding.villageInput.getInput()
                    , binding.siteInput.getInput()
                    , binding.agentInput.getInput()
                    , binding.seedsSownInput.getInput()
                    , binding.seedsPrickedInput.getInput()
                    , binding.totalInput.getInput()
                    , binding.readySeedsInput.getInput()
                    , binding.rejectInput.getInput()
                    , binding.deadInput.getInput()
                    , currentTime);
            View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_seedling_production, null);
            final AlertDialog ald = new AlertDialog.Builder(this)
                    .setView(aldv)
                    .show();
            ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button save = aldv.findViewById(R.id.save);
            save.setOnClickListener(view -> {

                if (recordSaved) {
//                    new T12Printer(SeedlingsProduction.this, printingObjects -> {
//                        PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
//                        CanvasPrint cp = new CanvasPrint();
//                        cp.init(PrinterType.TIII);
//                        cp.setUseSplit(true);
//                        cp.setTextAlignRight(false);
//                        cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(SeedlingsProduction.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
//                        FontProperty fp = new FontProperty();
//                        fp.setFont(true, false, true, false, 27, null);
//                        cp.setFontProperty(fp);
//                        cp.drawText(100, "Seedling Production");
//                        cp.drawText("");
//                        fp.setFont(true, false, false, false, 25, null);
//                        cp.setFontProperty(fp);
//                        cp.drawText("Transaction no: ");
//                        cp.drawText(seedlingProduction.transaction_no);
//                        fp.setFont(true, false, false, false, 25, null);
//                        cp.setFontProperty(fp);
//                        cp.setTextAlignRight(false);
//                        cp.drawText("");
//                        cp.drawText("Farmer info");
//                        fp.setFont(true, false, false, false, 20, null);
//                        cp.setFontProperty(fp);
//                        cp.drawText("");
//                        cp.drawText("Name: " + member.full_name);
//                        cp.drawText("ID number: " + member.nat_id);
//                        cp.drawText("Phone number : " + member.phone_no);
//
//                        cp.drawText("");
//                        fp.setFont(true, false, false, false, 25, null);
//                        cp.setFontProperty(fp);
//                        cp.setTextAlignRight(false);
//                        cp.drawText("Land info");
//                        fp.setFont(true, false, false, false, 20, null);
//                        cp.setFontProperty(fp);
//                        cp.setTextAlignRight(false);
//                        cp.drawText("");
//                        cp.drawText("Land size:" + addendum.land_size);
//                        cp.drawText("Country:" + Realm.databaseManager.loadObject(Country.class, new Query().setTableFilters("sid='" + addendum.country + "'")).name);
//                        cp.drawText("County:" + Realm.databaseManager.loadObject(County.class, new Query().setTableFilters("sid='" + addendum.county + "'")).name);
//                        cp.drawText("Location:" + Realm.databaseManager.loadObject(Location.class, new Query().setTableFilters("sid='" + addendum.location + "'")).name);
//                        cp.drawText("Sub-Location:" + Realm.databaseManager.loadObject(SubLocation.class, new Query().setTableFilters("sid='" + addendum.sub_location + "'")).name);
//                        cp.drawText("Village:" + Realm.databaseManager.loadObject(Village.class, new Query().setTableFilters("sid='" + addendum.village + "'")).name);
//                        cp.drawText("Site:" + Realm.databaseManager.loadObject(Site.class, new Query().setTableFilters("sid='" + addendum.site + "'")).name);
//                        cp.drawText("Seedling type:" + Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + addendum.seedling_type + "'")).name);
//                        cp.drawText("Total seedlings:" + addendum.total_seedlings);
//                        cp.drawText("Donated seedlings:" + addendum.donated_seedlings);
//                        cp.drawText("Years of maturity:" + addendum.years_of_maturity);
//                        cp.drawText("DBH at maturity:" + addendum.dbh_at_maturity);
//                        cp.drawText("");
//
//                        fp.setFont(false, false, false, false, 18, null);
//                        cp.setFontProperty(fp);
//                        cp.setTextAlignRight(false);
//                        cp.drawText("Report generated by: " + Globals.user().username);
//                        cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
//                        cp.drawText("");
//                        mPrinter.printImage(cp.getCanvasImage());
//                        Barcode bc = new Barcode(PDF417, 5, 300, 50, addendum.transaction_no);
//                        mPrinter.printBarCode(bc);
//                        mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
//                        mPrinter.closeConnection();
//                    }).print();

                } else {
                    //local db
                    recordSaved = Realm.databaseManager.insertObject(seedlingProduction1);
                    Log.e("records","seedling production records saved"+recordSaved);
                    if (recordSaved) {
                        //server
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(seedlingProduction1).get(0));
                        save.setText("Print");

                    }
                }

            });
//            ((TextView) aldv.findViewById(R.id.sub_title)).setText(member.full_name);
            ((TextView) aldv.findViewById(R.id.seedContent)).setText("List of production entries:" + "\n\n" +
                    "Nursery Type: " + Realm.databaseManager.loadObject(NurseryTypes.class, new Query().setTableFilters("sid='" + seedlingProduction1.nursery_type + "'")).name + "\n" +
                    "Country: " + Realm.databaseManager.loadObject(Country.class, new Query().setTableFilters("sid='" + seedlingProduction1.country + "'")).name + "\n" +
                    "County: " + Realm.databaseManager.loadObject(County.class, new Query().setTableFilters("sid='" + seedlingProduction1.county + "'")).name + "\n" +
                    "Ward: " + Realm.databaseManager.loadObject(Location.class, new Query().setTableFilters("sid='" + seedlingProduction1.location + "'")).name + "\n" +
                    "Sub-Location: " + Realm.databaseManager.loadObject(SubLocation.class, new Query().setTableFilters("sid='" + seedlingProduction1.sub_location + "'")).name + "\n" +
                    "Village: " + Realm.databaseManager.loadObject(Village.class, new Query().setTableFilters("sid='" + seedlingProduction1.village + "'")).name + "\n" +
                    "Site: " + Realm.databaseManager.loadObject(Site.class, new Query().setTableFilters("sid='" + seedlingProduction1.site + "'")).name + "\n" +
                    "Agroforest agent: " + Realm.databaseManager.loadObject(AgroForestAgent.class, new Query().setTableFilters("sid='" + seedlingProduction1.nursery_agent + "'")).name + "\n" +
                    "Seeds sown: "+seedlingProduction1.seeds_sown + "\n"+
                    "Pricked out seedlings: " + seedlingProduction1.seeds_pricked_out + "\n" +
                    "Total seedlings: " + seedlingProduction1.total_seedlings + "\n" +
                    "Seeds ready for distribution: " + seedlingProduction1.distribution_seedlings + "\n" +
                    "Reject seedlings: " + seedlingProduction1.reject_seedlings + "\n" +
                    "Dead seedlings: " + seedlingProduction1.dead_seedlings );

            aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
                ald.dismiss();
                if (recordSaved) {

//                    Intent intent = new Intent(SeedlingsProduction.this, MainActivity.class);
//                    startActivity(intent);
                    finish();
                }

            });

        }else{

            Toast.makeText(SeedlingsProduction.this, "Values Are Not Validated", Toast.LENGTH_LONG).show();

        }

    }

    boolean validated() {
        return
                binding.nurseryInput.isInputValid()
                & binding.countryInput.isInputValid()
                & binding.countyInput.isInputValid()
                & binding.locationInput.isInputValid()
                & binding.subLocationInput.isInputValid()
                & binding.villageInput.isInputValid()
                & binding.siteInput.isInputValid()
                & binding.agentInput.isInputValid()
                & binding.seedsSownInput.isInputValid()
                & binding.seedsPrickedInput.isInputValid()
                & binding.totalInput.isInputValid()
                & binding.readySeedsInput.isInputValid()
                & binding.rejectInput.isInputValid()
                & binding.deadInput.isInputValid();
    }
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.exit:
//                onBackPressed();
//                break;
//            case R.id.config:
//                startActivity(new Intent(this, Configuration.class));
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//
//    }
}