package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "addendum")

@SyncDescription( service_name = "Addendum",chunk_size = 10000, download_link = "/FarmersContract/Farmers/GetFarmerSubcontractDetails", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result",service_type = SyncDescription.service_type.Download)
@SyncDescription(service_name = "Addendum", upload_link = "/FarmersContract/Farmers/AddFarmerSubcontract",service_type = SyncDescription.service_type.Upload)

public class Addendum extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "contractno")
    public String contract_no;

    @DynamicProperty(json_key = "country_id")
    public String country;

    @DynamicProperty(json_key = "county_id")
    public String county;

    @DynamicProperty(json_key = "location_id")
    public String location;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location;

    @DynamicProperty(json_key = "village_id")
    public String village;

    @DynamicProperty(json_key = "site_id")
    public String site;

    @DynamicProperty(json_key = "land_size")
    public String land_size;

    @DynamicProperty(json_key = "total_seedlings")
    public String total_seedlings;

    @DynamicProperty(json_key = "donated_seedlings")
    public String donated_seedlings;

    @DynamicProperty(json_key = "Item_id")
    public String seedling_type;

    @DynamicProperty(json_key = "years_of_maturity")
    public String years_of_maturity;

    @DynamicProperty(json_key = "years_of_maturity")
    public String dbh_at_maturity;

    @DynamicProperty(json_key = "apk_version_creating")
    public String apk_version_creating;

    @DynamicProperty(json_key = "lat_coordinate")
    public String latitude;
    @DynamicProperty(json_key = "accuracy")
    public String accuracy;

    @DynamicProperty(json_key = "longi_coordinate")
    public String longitude;


    public Addendum() {

    }
    public Addendum(String latitude, String longitude, String accuracy){
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
    }

    public Addendum(String member_id, String contract_no, String country, String county, String location, String sub_location, String village, String site, String land_size, String total_seedlings, String donated_seedlings, String seedling_type, String years_of_maturity, String dbh_at_maturity, String latitude, String longitude, String accuracy) {

        this.member_id = member_id;
        this.contract_no = contract_no;
        this.country = country;
        this.county = county;
        this.location = location;
        this.sub_location = sub_location;
        this.village = village;
        this.site = site;
        this.land_size = land_size;
        this.total_seedlings = total_seedlings;
        this.donated_seedlings = donated_seedlings;
        this.seedling_type = seedling_type;
        this.years_of_maturity = years_of_maturity;
        this.dbh_at_maturity = dbh_at_maturity;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;

    }

}
