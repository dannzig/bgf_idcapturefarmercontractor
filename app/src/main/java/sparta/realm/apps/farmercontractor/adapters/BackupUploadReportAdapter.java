package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.realm.annotations.sync_status;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.BackupManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.BackupUploadEntry;
import sparta.realm.apps.farmercontractor.models.BackupUploadEntry;
import sparta.realm.utils.Conversions;


public class BackupUploadReportAdapter extends RecyclerView.Adapter<BackupUploadReportAdapter.view> {

    public ArrayList<BackupUploadEntry> items;
    Context cntxt;


    public BackupUploadReportAdapter(ArrayList<BackupUploadEntry> items) {
        this.items = items;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_backup_upload_entry, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface onItemClickListener {

        void onItemClick(BackupUploadEntry mem);
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        CircleImageView circleImageView;


        int position;


        view(View itemView) {
            super(itemView);

            circleImageView = itemView.findViewById(R.id.icon);
            name = itemView.findViewById(R.id.name);
                    }

        public void populate(int position) {
            this.position = position;
            BackupUploadEntry backupUploadEntry = items.get(position);
            name.setText(BackupManager.BackupType.values()[Integer.parseInt(backupUploadEntry.backup_type)].name());
            circleImageView.setImageDrawable(getDrawable(BackupManager.BackupType.values()[Integer.parseInt(backupUploadEntry.backup_type)]));
            if(backupUploadEntry.upload_status.equals(sync_status.pending.ordinal()+""))
            {
                circleImageView.setColorFilter(Color.RED);
            }



        }
        Drawable getDrawable(BackupManager.BackupType backupType){
            switch (backupType){
                case Mail:
                    return cntxt.getDrawable(R.drawable.ic_email);
                case SFTP:
                    return cntxt.getDrawable(R.drawable.ic_sftp);
            }
            return null;
        }

        @Override
        public void onClick(View view) {

        }
    }
}
