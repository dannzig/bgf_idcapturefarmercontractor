package sparta.realm.apps.farmercontractor.adapters;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.lzyzsd.circleprogress.ArcProgress;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.activities.SuitabilityAssessment;
import sparta.realm.apps.farmercontractor.models.SurveyQuestion;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.spartamodels.percent_calculation;


public class SurveyQuestionAdapter extends RecyclerView.Adapter<SurveyQuestionAdapter.view> {

    Context cntxt;
    public ArrayList<SurveyQuestion> items;

    public interface onItemClickListener {
        void onItemClick(SurveyQuestion mem);
    }

    public SurveyQuestionAdapter(ArrayList<SurveyQuestion> items) {
        this.items = items;

    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_survey_question, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView index, question, award_info, max_award;
        RecyclerView choice_list;
        EditText award;


        int position;


        view(View itemView) {
            super(itemView);

            index = itemView.findViewById(R.id.index);
            question = itemView.findViewById(R.id.question);
            award_info = itemView.findViewById(R.id.award_info);
            max_award = itemView.findViewById(R.id.max_award);

            choice_list = itemView.findViewById(R.id.choice_list);
            award = itemView.findViewById(R.id.award_edt);
            choice_list.setLayoutManager(new LinearLayoutManager(cntxt));

            award.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    items.get(position).result.marks = award.getText().toString();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }

        public void populate(int position) {
            this.position = position;
            SurveyQuestion surveyQuestion = items.get(position);

            index.setText((position + 1) + "");
            question.setText(surveyQuestion.question);
            award.setText(surveyQuestion.result.marks);

            award_info.setText(generateAwardInfo(surveyQuestion.surveyQuestionChoices));
            max_award.setText("/" + surveyQuestion.max_marks + " marks");
            if (surveyQuestion.surveyQuestionChoices.isEmpty()){
                max_award.setVisibility(View.GONE);

                if (surveyQuestion.question.contains("Number ") || surveyQuestion.question.contains("Total ") || surveyQuestion.question.contains("Dead") || surveyQuestion.question.contains("Standing")){
                    award.setHint("Enter Number");
                }
                if (surveyQuestion.question.contains("Name of")){
                    award.setHint("Enter Name");
                    award.setInputType(InputType.TYPE_CLASS_TEXT);
                }
                if (surveyQuestion.question.contains("Date ")){
                    award.setHint("Double Click,Pick Date");
                    award.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDatePicker();
                        }
                    });
                }
            }
            if(!surveyQuestion.surveyQuestionChoices.isEmpty()){
                award.setHint("Enter marks awarded");
            }
            choice_list.setAdapter(new SurveyQuestionChoiceAdapter(surveyQuestion.result.choice, surveyQuestion.surveyQuestionChoices, new SurveyQuestionChoiceAdapter.OnChoiceListener() {
                @Override
                public void onChoiceSelected(SurveyQuestionChoice surveyQuestionChoice) {
                    surveyQuestion.result.choice = surveyQuestionChoice.sid;
                    surveyQuestion.result.surveyQuestionChoice = surveyQuestionChoice;
                }
            }));

            if (surveyQuestion.uiError != null) {
                award.setError(surveyQuestion.uiError);
            }

        }
        private void showDatePicker() {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(cntxt,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            // Update the text of the EditText with the selected date
                            String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
                            award.setText(selectedDate);
                        }
                    }, year, month, day);

            datePickerDialog.show();
        }

        String generateAwardInfo(ArrayList<SurveyQuestionChoice> surveyQuestionChoices) {
            StringBuilder award_info_builder = new StringBuilder("Marks Rating: ");
            for (SurveyQuestionChoice surveyQuestionChoice : surveyQuestionChoices) {
                award_info_builder.append(surveyQuestionChoice.choice + "(" + surveyQuestionChoice.marks_from + "-" + surveyQuestionChoice.marks_to + ") ");

            }


            return award_info_builder.toString();

        }

        @Override
        public void onClick(View view) {

        }
    }
}
