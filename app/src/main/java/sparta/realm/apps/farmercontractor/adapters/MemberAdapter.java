package sparta.realm.apps.farmercontractor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;

import sparta.realm.Services.DatabaseManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberFingerprint;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.MemberImage;
import sparta.realm.apps.farmercontractor.utils.FastScrolRecyclerview.FastScrollRecyclerViewInterface;
import sparta.realm.spartautils.svars;


public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.view> implements FastScrollRecyclerViewInterface {

    Context cntxt;
    public ArrayList<Member> Members;
    onItemClickListener listener;
    HashMap<String, Integer> mapIndex;
    ArrayList<MemberGroup> groupMembers = new ArrayList<>();;

    @Override
    public HashMap<String, Integer> getMapIndex() {
        return mapIndex;
    }

    public interface onItemClickListener {

        void onItemClick(Member mem, View view);
    }


    public MemberAdapter(ArrayList<Member> Members, onItemClickListener listener) {
        this.Members = Members;
        this.mapIndex = calculateIndexesForName(Members);
        this.listener = listener;


    }

    private HashMap<String, Integer> calculateIndexesForName(ArrayList<Member> items) {
        HashMap<String, Integer> mapIndex = new LinkedHashMap<>();
        int its = items.size();
        for (int i = 0; i < its; i++) {
            try {
                String name = items.get(i).full_name.trim();
                String index = name.substring(0, 1);
                index = index.toUpperCase();

                if (!mapIndex.containsKey(index)) {
                    mapIndex.put(index, i);
                }
            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {

            }
        }
        return mapIndex;
    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_member, parent, false);

        return new view(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {


        //MemberGroup groupObj = DatabaseManager.database.execSQL("select member_id from group_participant where " + (obj.transaction_no == null || obj.transaction_no.length() < 1 ? " group_id='" + obj.sid + "')" : " group_transaction_no='" + obj.transaction_no + "')"));
        //MemberGroup groupObject = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("group_transaction_no='" + obj.transaction_no + "'", "member_transaction_no IS NOT NULL", "image_index='" + MemberImage.MemberImageType.ProfilePhoto.ordinal() + "'"));
        //MemberGroup memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(new String[]{"country='" + obj.country + "'","sid in(select member_id from group_participant where " + (obj.transaction_no == null || obj.transaction_no.length() < 1 ? " group_id='" + obj.sid + "')" : " group_transaction_no='" + obj.transaction_no + "')")}));
        //GroupParticipant groupParticipant  = Realm.databaseManager.loadObject(GroupParticipant.class, new Query().setTableFilters(new String[]{"member_id='" + (obj.transaction_no == null || obj.transaction_no.length() < 1 ? " group_id='" + obj.sid + "')" : " group_transaction_no='" + obj.transaction_no + "')") }));
        //MemberGroup memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(new String[]{"country_id='" + obj.country + "'","sid in(select member_id from group_participant where " + (obj.transaction_no == null || obj.transaction_no.length() < 1 ? " group_id='" + obj.sid + "')" : " group_transaction_no='" + obj.transaction_no + "')")}));


        Member obj = Members.get(position);
        //MemberGroup memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(new String[]{"country_id='" + obj.country + "'","member_id='" + obj.sid + "'" }));


        if (Globals.registeringMemberGroup != null){
            GroupParticipant groupParticipant  = Realm.databaseManager.loadObject(GroupParticipant.class, new Query().setTableFilters(new String[]{ "member_id='" + obj.sid + "'","group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "' and is_active = 'true'" }));

            if (groupParticipant != null ){
                Log.e("MemberAdapter", "onBindViewHolder: Group Rep: " + groupParticipant.group_rep.toString());
                if (groupParticipant.group_rep.equalsIgnoreCase("1")){
                    //change background colour of item
                    Log.e("MemberAdapter", "onBindViewHolder: Group Rep: HERE HERE");
                    //holder.memberItem.setBackgroundColor(Color.green(250));
                    //holder.memberItem.setBackgroundColor(R.color.red);
                    holder.name.setText(obj.full_name +" (ChairPerson)");
                    holder.name.setTextColor(R.color.blue);
                    Globals.groupRepChair = true;
                    Globals.hasGroupRep = true;
                }else if(groupParticipant.group_rep.equalsIgnoreCase("2")){
                    //holder.memberItem.setBackgroundColor(R.color.green);
                    holder.name.setText(obj.full_name +" (Treasurer)");
                    holder.name.setTextColor(R.color.blue);
                    Globals.groupRepTreas = true;
                    Globals.hasGroupRep = true;
                }
                else if(groupParticipant.group_rep.equalsIgnoreCase("3")){
                    //holder.memberItem.setBackgroundColor(R.color.blue);
                    holder.name.setText(obj.full_name +" (Secretary)");
                    holder.name.setTextColor(R.color.blue);
                    Globals.groupRepSec = true;
                    Globals.hasGroupRep = true;
                }
            }
        }

        Log.e("MemberAdapter", "onBindViewHolder: Name Clicked : " + obj.full_name +" Sid: " +obj.sid);
        //String [] stockArr = Globals.selectedMembersTraining.toArray(new String[0]);
        //Log.e("MemberAdapter", "onBindViewHolder:Trained List : " + Arrays.deepToString(Globals.selectedMembersTraining.toArray(new Member[0])));

        //Group Training Module
        if (Globals.selectedMembersTraining != null){
            if (Globals.selectedMembersTraining.contains(obj)){
                holder.present_mark.setVisibility(View.VISIBLE);
            }else {
                holder.present_mark.setVisibility(View.GONE);
            }
        }

        //Distribution Module
        if (Globals.selectedMembersIssued != null){
            if (Globals.selectedMembersIssued.contains(obj)){
                holder.issued_mark.setVisibility(View.VISIBLE);
            }else {
                holder.issued_mark.setVisibility(View.GONE);
            }
        }

//        MemberImage mi = Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_transaction_no='" + obj.transaction_no + "'","member_transaction_no IS NOT NULL","image_index='"+MemberImage.MemberImageType.ProfilePhoto.ordinal()+"'"));
        obj.profile_photo = obj.profile_photo == null ? Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_transaction_no='" + obj.transaction_no + "'", "member_transaction_no IS NOT NULL", "image_index='" + MemberImage.MemberImageType.ProfilePhoto.ordinal() + "'")) : obj.profile_photo;

        obj.memberFingerprint = obj.memberFingerprint == null ? Realm.databaseManager.loadObject(MemberFingerprint.class, new Query().setTableFilters("member_transaction_no='" + obj.transaction_no + "'")) : obj.memberFingerprint;
        if (obj.memberFingerprint != null && obj.memberFingerprint.image != null) {
            holder.fp_icon.setVisibility(View.VISIBLE);
        } else {
            holder.fp_icon.setVisibility(View.GONE);
        }

        holder.icon.setImageURI(null);
        if (obj.profile_photo != null && obj.profile_photo.image != null) {
            holder.icon.setImageURI(null);
            holder.icon.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(Realm.context).file_path_employee_data) + obj.profile_photo.image));
        }
//        holder.name.setText(obj.full_name +" "+obj.other_names);

        //For normal members without group reprentative roles
        if (holder.name.getText().toString().equalsIgnoreCase("User X")){
            holder.name.setText(obj.full_name);
        }
        holder.member_no.setText("Nat ID: " + obj.nat_id);
        holder.member_id = obj.sid;
        holder.itemView.setOnClickListener(view -> listener.onItemClick(obj, holder.itemView));




    }


    @Override
    public int getItemCount() {
        return Members.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public String member_id;
        public TextView name, member_no, present_mark, issued_mark;
        public CircleImageView icon, fp_icon;
        public androidx.constraintlayout.widget.ConstraintLayout memberItem;


        view(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            member_no = itemView.findViewById(R.id.info1);
            icon = itemView.findViewById(R.id.icon);
            fp_icon = itemView.findViewById(R.id.fp_icon);
            memberItem = itemView.findViewById(R.id.memberItem);
            present_mark = itemView.findViewById(R.id.present_mark);
            issued_mark = itemView.findViewById(R.id.issued_mark);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
