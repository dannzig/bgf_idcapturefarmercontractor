package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


import sparta.realm.apps.farmercontractor.R;
import sparta.realm.spartautils.app_control.models.module;


public class AppModulesAdapter extends RecyclerView.Adapter<AppModulesAdapter.view>  implements Filterable {

    Context cntxt;
    public ArrayList<module> items;
    ArrayList<module> originalList=new ArrayList<>();
    onItemClickListener listener;

    public interface onItemClickListener {

        void onItemClick(module mem, View view);
    }


    public AppModulesAdapter(ArrayList<module> items, onItemClickListener listener) {
        this.cntxt = cntxt;
        this.items = items;
        this.listener = listener;
        originalList= new ArrayList<>(items);


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_module, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        module obj = items.get(position);
        holder.moduleName.setText(obj.name);

        holder.icon.setImageDrawable(obj.icon);


        holder.icon.setColorFilter(Color.GRAY);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(obj, holder.itemView);

            }
        });


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView moduleName;
        public String sid;
        public ImageView icon;


        view(View itemView) {
            super(itemView);

            moduleName = itemView.findViewById(R.id.name);
            icon = itemView.findViewById(R.id.icon);


        }

        @Override
        public void onClick(View view) {

        }
    }


    DataFilter dataFilter;

    @Override
    public Filter getFilter() {
        if (dataFilter == null) {
            dataFilter = new DataFilter();
        }
        return dataFilter;
    }

    public class DataFilter extends Filter {


        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            FilterResults results = new FilterResults();

            ArrayList<module> filtered_list =charSequence==null?new ArrayList<>(originalList):new ArrayList<>();
            if(charSequence!=null){

                int tot=originalList.size();
                for (int i = 0; i < tot; i++) {
                    module selectionData = originalList.get(i);
                    if ((selectionData.name!=null&&selectionData.name.toLowerCase().contains(charSequence.toString().toLowerCase()) )|| (selectionData.code!=null&&selectionData.code.toLowerCase().contains(charSequence.toString().toLowerCase()))) {

                        filtered_list.add(selectionData);

                    }
                }
            }

            results.count = filtered_list.size();
            results.values = filtered_list;


            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            items.clear();
            if(filterResults.values!=null){
                items.addAll((ArrayList) filterResults.values);

            }
            notifyDataSetChanged();
        }
    }
}
