package sparta.realm.apps.farmercontractor.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.StaticDataLoader;
import sparta.realm.apps.farmercontractor.activities.registration.Page1;
import sparta.realm.apps.farmercontractor.adapters.MemberAdapter;
import sparta.realm.apps.farmercontractor.adapters.SelectedSearchFilterAdapter;
import sparta.realm.apps.farmercontractor.adapters.SearchListFilterAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityMemberRecordsBinding;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberSearchFilter;
import sparta.realm.apps.farmercontractor.models.SearchFilterItem;
import sparta.realm.apps.farmercontractor.models.SearchFilterUiItem;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.FastScrolRecyclerview.FastScrollRecyclerViewItemDecoration;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.apps.farmercontractor.utils.Pager;
import sparta.realm.apps.farmercontractor.utils.SnapHelper;
import sparta.realm.spartautils.svars;

public class MemberRecords extends AppCompatActivity {
    ActivityMemberRecordsBinding binding;
    ArrayList<Member> members;
    Pager<Member> pager;
    boolean search;
    String targetActivity;
    String[] defaultTableFilters = new String[0];
    Activity context;
    String module;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMemberRecordsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        context = this;
        search = getIntent().getBooleanExtra("search", false);
        targetActivity = getIntent().getStringExtra("targetActivity");
        defaultTableFilters = getIntent().getStringArrayExtra("defaultTableFilters");
        tableFilters = getIntent().getStringArrayExtra("defaultTableFilters");
        module = getIntent().getStringExtra("moduleName");

        initUI();

    }

    String[] tableFilters;

    void setTableFiler(String[] table_filter) {
        if (defaultTableFilters == null || defaultTableFilters.length < 1) {
            tableFilters = table_filter;
            if (pager != null) pager.setTableFilters(tableFilters);
        } else {
            tableFilters = DatabaseManager.concatenate(table_filter, defaultTableFilters);
            if (pager != null) pager.setTableFilters(tableFilters);
        }

    }

    LinearLayoutManager linearLayoutManager;
    MemberSearchFilter memberSearchFilter = new MemberSearchFilter();

    void initUI() {
        binding.include.back.setOnClickListener(v -> onBackPressed());
//        members = Realm.databaseManager.loadObjectArray(Member.class, new Query().setOrderFilters(true, "full_name").setLimit(1));
        members = new ArrayList<>();

        binding.memberList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        binding.memberList.setLayoutManager(linearLayoutManager);
        SnapHelper startSnapHelper = new SnapHelper();
        startSnapHelper.attachToRecyclerView(binding.memberList);
        setupFilter();
        pager = new Pager(Member.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                members = (ArrayList<Member>) data;
                binding.noRecordsLay.setVisibility((members.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.memberList.setAdapter(new MemberAdapter(members, new MemberAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(Member mem, View view) {
                        if (!search) {
//                            Toast.makeText(context, "The update feature is currently being worked on", Toast.LENGTH_LONG).show();
                            if (module.equalsIgnoreCase("Seedling Planting")){
                                //Intent intent = new Intent(GroupRecords.this, GroupTrainings.class);
                                Intent intent = new Intent(MemberRecords.this, GroupPlannedDistribution.class);
                                intent.putExtra("transaction_no", mem.transaction_no);
                                intent.putExtra("sid", mem.sid);
                                intent.putExtra("reg_mode", "2");

                                startActivity(intent);
                            }
                            Intent intent = new Intent(MemberRecords.this, Page1.class);
                            intent.putExtra("reg_mode", "2");
                            intent.putExtra("sid", mem.sid);
                            intent.putExtra("transaction_no", mem.transaction_no);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        } else {
                            if (targetActivity == null) {
                                Intent data = new Intent();
                                data.putExtra("sid", mem.sid);
                                data.putExtra("transaction_no", mem.transaction_no);

                                setResult(RESULT_OK, data);
                            } else {
//    finish();
                                Intent intent = null;
                                try {
                                    intent = new Intent(MemberRecords.this, Class.forName(targetActivity));
                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                                intent.putExtra("sid", mem.sid);
                                intent.putExtra("transaction_no", mem.transaction_no);
                                Pair<View, String> p0_ = Pair.create(view.findViewById(R.id.icon), "icon");//including this with circle imageview has library errors on back presses
                                Pair<View, String> p0 = Pair.create(view.findViewById(R.id.info1), "info");//including this with circle imageview has library errors on back presses
                                Pair<View, String> p1 = Pair.create(view.findViewById(R.id.name), "name");

                                ActivityOptionsCompat options = ActivityOptionsCompat.
                                        makeSceneTransitionAnimation(context, p1, p0);
                                startActivity(intent, options.toBundle());
                            }


                        }
                        finish();
                    }
                }));
                binding.memberList.setupThings();
                FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(context);
                binding.memberList.addItemDecoration(decoration);
                binding.memberList.setItemAnimator(new DefaultItemAnimator());
                binding.memberList.invalidate();
            }
        }, binding.include.searchField, binding.include.prev, binding.include.next, binding.include.positionIndicator, binding.include.progress, null, tableFilters, null, "full_name");

        if (search) {
            binding.newMember.setVisibility(View.GONE);
        }

        binding.newMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberRecords.this, Page1.class);
                intent.putExtra("reg_mode", "1");
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });


    }


    void setupFilter() {
        binding.include.filterIcon.setOnClickListener(view -> binding.include.filterList.setVisibility(binding.include.filterList.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE));
        ArrayList<SearchFilterItem> searchFilterItems = StaticDataLoader.sampleSearchFilterItems();
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.setItemPrefetchEnabled(true);
        binding.include.selectedFilterList.setLayoutManager(flowLayoutManager);
        binding.include.selectedFilterList.setAdapter(new SelectedSearchFilterAdapter(new ArrayList<>(), (mem, view) -> {
            mem.active = false;
            svars.setWorkingObject(mem, mem.sid);
            if (binding.include.filterList.getVisibility() == View.VISIBLE) {
                binding.include.filterList.getAdapter().notifyDataSetChanged();
            } else {
                setTableFiler(((SearchListFilterAdapter) binding.include.filterList.getAdapter()).generateTableFilter());
//                pager.setTableFilters(((SearchListFilterAdapter) binding.include.filterList.getAdapter()).generateTableFilter());
            }
            int pos = ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.indexOf(mem);
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.remove(mem);
            binding.include.selectedFilterList.getAdapter().notifyItemRemoved(pos);
        }));
        binding.include.filterList.setLayoutManager(new LinearLayoutManager(this));
        binding.include.filterList.setAdapter(new SearchListFilterAdapter(searchFilterItems, (mem, selectedItems, tableFilter) -> {
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.clear();
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.addAll(selectedItems);
            binding.include.selectedFilterList.getAdapter().notifyDataSetChanged();
//            pager.setTableFilters(tableFilter);
            setTableFiler(tableFilter);
            binding.include.selectedFilterList.invalidate();
        }));
    }


    void setupFilter__() {//idea 2
        binding.include.filterIcon.setOnClickListener(view -> binding.include.filterList.setVisibility(binding.include.filterList.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE));


        memberSearchFilter = Globals.memberSearchFilter() == null ? new MemberSearchFilter() : Globals.memberSearchFilter();
        ArrayList<SearchFilterUiItem> searchFilterUiItems = new ArrayList<>();
        SearchFilterItem countrySearchFilterItem = new SearchFilterItem("Country", "Country", "country", Country.class.getName());


        SearchFilterUiItem countrySearchFilter = new SearchFilterUiItem(countrySearchFilterItem, binding.include.filterLayout.countryCheck, binding.include.filterLayout.countryInput);
        searchFilterUiItems.add(countrySearchFilter);


        for (SearchFilterUiItem searchFilterUiItem : searchFilterUiItems) {
            searchFilterUiItem.checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                searchFilterUiItem.searchFilterItem.active = b;
                memberSearchFilter.country.active = b;
                Globals.setMemberSearchFilter(memberSearchFilter);
                ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.clear();
                ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.addAll(memberSearchFilter.activeMemberSearchFilterItems());
                binding.include.selectedFilterList.getAdapter().notifyDataSetChanged();
            });
        }

    }


    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            memberSearchFilter.country.value = input;
            binding.include.filterLayout.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.include.filterLayout.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);

            binding.include.filterLayout.countyInput.setInput(memberSearchFilter.county.value);
            binding.include.filterLayout.siteInput.setInput(memberSearchFilter.site.value);
            Globals.setMemberSearchFilter(memberSearchFilter);
        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            memberSearchFilter.county.value = input;
            binding.include.filterLayout.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
            binding.include.filterLayout.locationInput.setInput(memberSearchFilter.location.value);
            Globals.setMemberSearchFilter(memberSearchFilter);

        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            memberSearchFilter.location.value = input;
            binding.include.filterLayout.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
            binding.include.filterLayout.subLocationInput.setInput(memberSearchFilter.sub_location.value);
            Globals.setMemberSearchFilter(memberSearchFilter);

        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            memberSearchFilter.sub_location.value = input;
            Globals.setMemberSearchFilter(memberSearchFilter);

            binding.include.filterLayout.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
            binding.include.filterLayout.villageInput.setInput(memberSearchFilter.village.value);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            memberSearchFilter.site.value = input;
            Globals.setMemberSearchFilter(memberSearchFilter);
        }
    };
    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            memberSearchFilter.village.value = input;
            Globals.setMemberSearchFilter(memberSearchFilter);
        }
    };

    void setupFilter_() {
        binding.include.filterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.include.filterLayout.getRoot().setVisibility(binding.include.filterLayout.getRoot().getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        memberSearchFilter = Globals.memberSearchFilter() == null ? new MemberSearchFilter() : Globals.memberSearchFilter();
        binding.include.filterLayout.countryInput.setDataset(Country.class.getName(), countryInputListener);
        binding.include.filterLayout.countryInput.setInput(memberSearchFilter.country.value);

        binding.include.filterLayout.countryCheck.setChecked(memberSearchFilter.country.active);
        binding.include.filterLayout.countyCheck.setChecked(memberSearchFilter.county.active);
        binding.include.filterLayout.locationCheck.setChecked(memberSearchFilter.location.active);
        binding.include.filterLayout.subLocationCheck.setChecked(memberSearchFilter.sub_location.active);
        binding.include.filterLayout.villageCheck.setChecked(memberSearchFilter.village.active);
        binding.include.filterLayout.siteCheck.setChecked(memberSearchFilter.site.active);

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        binding.include.selectedFilterList.setLayoutManager(flowLayoutManager);
        binding.include.selectedFilterList.setAdapter(new SelectedSearchFilterAdapter(memberSearchFilter.activeMemberSearchFilterItems(), new SelectedSearchFilterAdapter.onItemClickListener() {
            @Override
            public void onItemClick(SearchFilterItem mem, View view) {
                if (memberSearchFilter.country.equals(mem)) {
                    memberSearchFilter.country.active = false;
                    binding.include.filterLayout.countryCheck.setChecked(false);

                } else if (memberSearchFilter.county.equals(mem)) {
                    memberSearchFilter.county.active = false;
                    binding.include.filterLayout.countyCheck.setChecked(false);

                } else if (memberSearchFilter.location.equals(mem)) {
                    memberSearchFilter.location.active = false;
                    binding.include.filterLayout.locationCheck.setChecked(false);

                } else if (memberSearchFilter.sub_location.equals(mem)) {
                    memberSearchFilter.sub_location.active = false;
                    binding.include.filterLayout.subLocationCheck.setChecked(false);

                } else if (memberSearchFilter.village.equals(mem)) {
                    memberSearchFilter.village.active = false;
                    binding.include.filterLayout.villageCheck.setChecked(false);

                } else if (memberSearchFilter.site.equals(mem)) {
                    memberSearchFilter.site.active = false;
                    binding.include.filterLayout.siteCheck.setChecked(false);

                }
                Globals.setMemberSearchFilter(memberSearchFilter);
                int pos = ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.indexOf(mem);
                ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.remove(mem);
                binding.include.selectedFilterList.getAdapter().notifyItemRemoved(pos);
            }
        }));


        binding.include.filterLayout.countryCheck.setOnCheckedChangeListener((compoundButton, b) -> {
            memberSearchFilter.country.active = b;
            Globals.setMemberSearchFilter(memberSearchFilter);
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.clear();
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.addAll(memberSearchFilter.activeMemberSearchFilterItems());
            binding.include.selectedFilterList.getAdapter().notifyDataSetChanged();
        });

        binding.include.filterLayout.countyCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                memberSearchFilter.county.active = b;
                Globals.setMemberSearchFilter(memberSearchFilter);
            }
        });

        binding.include.filterLayout.locationCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                memberSearchFilter.location.active = b;
                Globals.setMemberSearchFilter(memberSearchFilter);
            }
        });

        binding.include.filterLayout.subLocationCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                memberSearchFilter.sub_location.active = b;
                Globals.setMemberSearchFilter(memberSearchFilter);
            }
        });

        binding.include.filterLayout.villageCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                memberSearchFilter.village.active = b;
                Globals.setMemberSearchFilter(memberSearchFilter);
            }
        });
        binding.include.filterLayout.siteCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                memberSearchFilter.site.active = b;
                Globals.setMemberSearchFilter(memberSearchFilter);
            }
        });


    }

    ArrayList<SearchFilterItem> memberSearchFilterItems() {
        ArrayList<SearchFilterItem> searchFilterItems = new ArrayList<>();
        if (Globals.memberSearchFilter().country.active) {
            searchFilterItems.add(Globals.memberSearchFilter().country);
        }

        return searchFilterItems;
    }

}