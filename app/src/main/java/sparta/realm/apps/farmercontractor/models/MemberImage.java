package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "member_image_table")

@SyncDescription(service_id = "1", chunk_size = 1, service_name = "Member image", upload_link = "/FarmersContract/Farmers/FarmerImages", service_type = SyncDescription.service_type.Upload, storage_mode_check = true)
//@SyncDescription(service_name = "Member image download", download_link = "/MobiServices/SaveData/GetEmployeeImages", service_type = SyncDescription.service_type.Download, chunk_size = 1, download_array_position = "JO:result;JO:result", storage_mode_check = true)
public class MemberImage extends RealmModel implements Serializable {


    @DynamicProperty(json_key = "imageType")
    public String image_index;

    @DynamicProperty(json_key = "member_transaction_no")
    public String member_transaction_no;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "ImageName", storage_mode = DynamicProperty.storage_mode.FilePath)
    //@DynamicProperty(json_key = "ImageName" )
    public String image;


    public enum MemberImageType {
        None,
        FrontID,
        BackID,
        ProfilePhoto,
        ChiefSignature

    }


    public MemberImage(String member_transaction_no, String image, String image_index, String member_id) {
        this.member_transaction_no = member_transaction_no;
        this.image = image;
        this.image_index = image_index;
        this.member_id = member_id;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }
    public MemberImage(String member_transaction_no, String image, String image_index) {
        this.member_transaction_no = member_transaction_no;
        this.image = image;
        this.image_index = image_index;
        this.member_id = member_id;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public MemberImage() {


    }

}
