package sparta.realm.apps.farmercontractor.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import sparta.realm.Activities.SpartaAppCompactActivity;
import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.StaticDataLoader;
import sparta.realm.apps.farmercontractor.adapters.FormAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityRegistrationFormPlayerBinding;
import sparta.realm.apps.farmercontractor.models.system.Form;
import sparta.realm.apps.farmercontractor.models.system.InputField;
import sparta.realm.apps.farmercontractor.models.system.InputGroup;


public class RegistrationFormPlayer extends SpartaAppCompactActivity {

    Form form;
    InputGroup currentPage;
    boolean lastPage = false;
    boolean firstPage = false;
    ActivityRegistrationFormPlayerBinding binding;
    int operationMode = 0;
    int operationModeStatic = 0;
    int operationModeDb = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegistrationFormPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (operationMode == operationModeDb) {
            setupPages();
        } else {
            setupStatic();

        }
        initUI();
    }

    void initUI() {
        setSupportActionBar(binding.include.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.formControl.btnPrev.setOnClickListener(view -> {
            binding.formControl.btnPrev.setFocusable(true);
            binding.formControl.btnPrev.setFocusableInTouchMode(true);
            binding.formControl.btnPrev.requestFocus();
            if (firstPage) {
                showQuitDialog();
            } else {
                previousPage();

            }
        });

        binding.formControl.btnNext.setOnClickListener(view -> {
            binding.formControl.btnNext.requestFocus();
            if (lastPage) {
                // showQuitDialog();
            } else {
                nextPage();

            }
        });

        if (operationMode == operationModeDb) {
            binding.pageProgress.setStepsNumber(8);
            binding.pageProgress.go(3, true);
            binding.pageProgress.done(false);
            binding.pageProgress.setVisibility(View.VISIBLE);

        } else {
            binding.pageProgress.setStepsNumber(form.inputGroups.size());
            binding.pageProgress.go(form.inputGroups.indexOf(currentPage), true);
            binding.pageProgress.done(form.inputGroups.indexOf(currentPage) == form.inputGroups.size() - 1);
            binding.title.setText("Page " + (form.inputGroups.indexOf(currentPage) + 1) + " of " + form.inputGroups.size() + "  " + currentPage.title);


        }


    }

    void proceed() {

    }

    boolean currentFormValid() {
        boolean valid=true;
        for(InputField inputField:currentPage.inputFields)
        {
            if(!inputField.isInputValid()){
                valid= false;
            }
        }

        return valid;

    }

    void setupStatic() {
        String pageId = getIntent().getStringExtra("pageId");
        form = StaticDataLoader.sampleform();
        if (pageId != null) {
            for (InputGroup inputGroup : form.inputGroups) {
                if (inputGroup.sid.equalsIgnoreCase(pageId)) {
                    currentPage = inputGroup;
                    break;
                }
            }

        } else {
            currentPage = form.inputGroups.get(0);
        }

        lastPage = form.inputGroups.indexOf(currentPage) == form.inputGroups.size() - 1;
        firstPage = form.inputGroups.indexOf(currentPage) == 0;
        binding.form.setLayoutManager(new LinearLayoutManager(this));
        binding.form.setAdapter(new FormAdapter(currentPage));


    }


    void setupPages() {
        form = Realm.databaseManager.loadObject(Form.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("formId") + "'"));
        if (form != null) {
            currentPage = Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("form='" + form.sid + "'", "sid='" + getIntent().getStringExtra("pageId") + "'"));
            //if pageid not specified then its a new registration
            currentPage = currentPage != null ? currentPage : Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("form='" + form.sid + "'").setOffset(0).setLimit(1).setOrderFilters(true, "order_index"));

        }

        if (currentPage == null) {
            //   finish();
            return;
        }
//        currentPage = Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("order_index>'" + getIntent().getStringExtra("order_index") + "'").setOffset(0).setLimit(1).setOrderFilters(true, "order_index"));
        lastPage = null == Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("form='" + form.sid + "'", "order_index>'" + currentPage.order_index + "'"));
        firstPage = null == Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("form='" + form.sid + "'", "order_index<'" + currentPage.order_index + "'"));


    }

    void nextPage() {
        if (lastPage) {


        } else {
            InputGroup nextPage = operationMode == operationModeStatic ? form.inputGroups.get(form.inputGroups.indexOf(currentPage) + 1) : Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("form='" + form.sid + "'", "order_index>'" + currentPage.order_index + "'").setOffset(0).setLimit(1).setOrderFilters(true, "order_index"));

            Intent intent = new Intent(act, RegistrationFormPlayer.class);
            intent.putExtra("formId", form.sid);
            intent.putExtra("order_index", nextPage.order_index);
            intent.putExtra("pageId", nextPage.sid);
            start_activity(intent);
            finish();

        }


    }

    void previousPage() {
        if (firstPage) {


        } else {
            InputGroup prevPage = operationMode == operationModeStatic ? form.inputGroups.get(form.inputGroups.indexOf(currentPage) - 1) : Realm.databaseManager.loadObject(InputGroup.class, new Query().setTableFilters("form='" + form.sid + "'", "order_index<'" + currentPage.order_index + "'").setOffset(0).setLimit(1).setOrderFilters(true, "order_index"));

            Intent intent = new Intent(act, RegistrationFormPlayer.class);
            intent.putExtra("formId", form.sid);
            intent.putExtra("order_index", prevPage.order_index);
            intent.putExtra("pageId", prevPage.sid);
            start_activity(intent);
            finish();

        }

    }

    void showQuitDialog() {

        View aldv = LayoutInflater.from(act).inflate(R.layout.dialog_exit_enrollement_warning, null);
        final AlertDialog ald = new AlertDialog.Builder(act)
                .setView(aldv)
                .show();
        aldv.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ald.dismiss();

//                svars.set_working_employee(act,null);
//                svars.working_employee=null;
//                Intent reg_intent=new Intent(act, MainMenu.class);
//                reg_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK|FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(reg_intent);

                finish();
            }
        });

        aldv.findViewById(R.id.dismiss).setOnClickListener(view -> ald.dismiss());
    }

    void showClearAll() {
        View aldv = LayoutInflater.from(act).inflate(R.layout.dialog_confirm_clear_all, null);
        final AlertDialog ald = new AlertDialog.Builder(act)
                .setView(aldv)
                .show();
        aldv.findViewById(R.id.clear_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ald.dismiss();


            }
        });

        aldv.findViewById(R.id.dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ald.dismiss();
            }
        });


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.registration_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(act, "Clicked Menu", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.clear_all_menu:
                Toast.makeText(act, "Clicked clear all", Toast.LENGTH_LONG).show();
                break;
            case R.id.exit_registration:
                Toast.makeText(act, "Clicked Exit enrolment", Toast.LENGTH_LONG).show();
                showQuitDialog();
                break;
        }
        return super.onOptionsItemSelected(item);

    }


}