package sparta.realm.apps.farmercontractor.activities;

import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.navigation.NavigationBarView;
import com.realm.annotations.sync_service_description;
import com.realm.annotations.sync_status;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.Date;

import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;
import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.BackupManager;
import sparta.realm.apps.farmercontractor.BuildConfig;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.MyApplication;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.StaticDataLoader;
import sparta.realm.apps.farmercontractor.SyncOverride;
import sparta.realm.apps.farmercontractor.adapters.AppMenuModulesAdapter;
import sparta.realm.apps.farmercontractor.adapters.AppModulesAdapter;
import sparta.realm.apps.farmercontractor.adapters.BackupReportAdapter;
import sparta.realm.apps.farmercontractor.adapters.BackupTypeConfigAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityMainBinding;
import sparta.realm.apps.farmercontractor.models.BackupEntry;
import sparta.realm.apps.farmercontractor.models.BackupUploadEntry;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.LandLocation;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.SuitabilityAssessmentSurveyEntry;
import sparta.realm.apps.farmercontractor.models.Survey;
import sparta.realm.apps.farmercontractor.models.SurveyQuestion;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.apps.farmercontractor.models.User;
import sparta.realm.apps.farmercontractor.models.UserSite;
import sparta.realm.apps.farmercontractor.models.system.AppVersion;
import sparta.realm.apps.farmercontractor.services.AppUpdate;
import sparta.realm.spartautils.app_control.models.module;
import sparta.realm.spartautils.bluetooth.bt_device_connector;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.Conversions;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    ArrayList<module> sideMenuModules = new ArrayList<>();
    ArrayList<module> mainMenuModules = new ArrayList<>();
    Context act;
    BackupManager backup;
    String logTag = "MainActivity";
    AppUpdate appUpdateService;
    boolean mBound = false;
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            AppUpdate.AppUpdateBinder binder = (AppUpdate.AppUpdateBinder) service;
            appUpdateService = binder.getService();
            appUpdateService.setStatusChangeListener(new AppUpdate.StatusChangeListener() {
                @Override
                public void onStatusChanged(AppUpdate.Status status) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.contentMain.aboutApp.progress.setVisibility(View.GONE);
                            binding.contentMain.aboutApp.cancel.setVisibility(View.GONE);
                            binding.contentMain.aboutApp.progressLabel.setVisibility(View.GONE);

                            switch (status) {
                                case CheckingForUpdates:
                                    binding.contentMain.aboutApp.action.setText("Checking for updates");
                                    binding.contentMain.aboutApp.action.setOnClickListener(null);
                                    break;
                                case Idle:
                                case ReadyToCheckForUpdates: {
                                    AppVersion latest = Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false));
                                    if (latest == null) {
                                        binding.contentMain.aboutApp.title.setText("Never checked for updates");
                                        binding.contentMain.aboutApp.featuresTitle.setText("Never checked for updates");
                                        binding.contentMain.aboutApp.featuresInfo.setText("");

                                    } else {
                                        binding.contentMain.aboutApp.title.setText("Latest as of " + Conversions.sdfUserDisplayDate.format(new Date(Globals.updateCheckTime())));
                                        binding.contentMain.aboutApp.featuresTitle.setText("Current features and updates");
                                        binding.contentMain.aboutApp.featuresInfo.setText(latest.release_notes);

                                    }
//                                    binding.contentMain.aboutApp.title.setText("Latest as of " + Conversions.sdfUserDisplayDate.format(new Date(Globals.updateCheckTime())));
                                    binding.contentMain.aboutApp.action.setText("Check for updates");
                                    binding.contentMain.aboutApp.action.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            appUpdateService.checkForUpdates();
                                        }
                                    });
                                }

                                break;
                                case Downloading:

                                    binding.contentMain.aboutApp.action.setText("Downloading update");
                                    binding.contentMain.aboutApp.progress.setIndeterminate(true);
                                    binding.contentMain.aboutApp.progress.setVisibility(View.VISIBLE);
                                    binding.contentMain.aboutApp.cancel.setVisibility(View.VISIBLE);
                                    binding.contentMain.aboutApp.progressLabel.setVisibility(View.VISIBLE);
                                    binding.contentMain.aboutApp.action.setOnClickListener(null);
                                    break;
                                case ReadyToDownload: {
                                    AppVersion latest = Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false));

                                    binding.contentMain.aboutApp.title.setText("New version available V " + latest.version_name);
                                    binding.contentMain.aboutApp.featuresTitle.setText("New features and updates");
                                    binding.contentMain.aboutApp.featuresInfo.setText(latest.release_notes);
                                    binding.contentMain.aboutApp.action.setText("Download update");
                                    binding.contentMain.aboutApp.action.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            appUpdateService.downloadUpdates(Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false)));
                                        }
                                    });
                                }
                                break;
                                case ReadyToInstall: {
                                    AppVersion latest = Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false));

                                    binding.contentMain.aboutApp.title.setText("New version available V " + latest.version_name);
                                    binding.contentMain.aboutApp.featuresTitle.setText("New features and updates");
                                    binding.contentMain.aboutApp.featuresInfo.setText(latest.release_notes);
                                    binding.contentMain.aboutApp.action.setText("Install update");
                                    binding.contentMain.aboutApp.action.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Toast.makeText(act, "Installing...", Toast.LENGTH_SHORT).show();
                                            Log.e("MainActivity", "App Query: " + Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false)));
                                            appUpdateService.installApp(MainActivity.this, Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false)));
                                        }
                                    });
                                }

                                break;
                                case onDownloadFailed:
                                    binding.contentMain.aboutApp.action.setText("Download failed.Re-download ");
                                    binding.contentMain.aboutApp.action.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            appUpdateService.downloadUpdates(Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false)));
                                        }
                                    });

                                    break;
                                case OnTheLatestVersion:
                                    AppVersion latest = Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false));
                                    if (latest == null) {
                                        binding.contentMain.aboutApp.title.setText("Never checked for updates");
                                        binding.contentMain.aboutApp.featuresTitle.setText("Never checked for updates");
                                        binding.contentMain.aboutApp.featuresInfo.setText("");

                                    } else {
                                        binding.contentMain.aboutApp.title.setText("Latest as of " + Conversions.sdfUserDisplayDate.format(new Date(Globals.updateCheckTime())));
                                        binding.contentMain.aboutApp.featuresTitle.setText("Current features and updates");
                                        binding.contentMain.aboutApp.featuresInfo.setText(latest.release_notes);

                                    }
                                    binding.contentMain.aboutApp.action.setText("Check for updates");
                                    binding.contentMain.aboutApp.action.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            appUpdateService.checkForUpdates();
                                        }
                                    });
                                    break;
                            }
                        }
                    });

                }

                @Override
                public void onDownloadProgress(int percentage) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (percentage > 1) {
                                binding.contentMain.aboutApp.progress.setIndeterminate(false);
                                binding.contentMain.aboutApp.progress.setProgress(percentage);
                                binding.contentMain.aboutApp.progressLabel.setText(percentage + "% downloaded ...");
                            } else {
                                binding.contentMain.aboutApp.progress.setIndeterminate(true);
                                binding.contentMain.aboutApp.progressLabel.setText("Downloading ...");
                            }
                        }
                    });
                }
            });
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        act = this;
        initUi();
        backup = new BackupManager(this, binding.contentMain.getRoot());



        Intent intent = new Intent(this, AppUpdate.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
        binding.contentMain.aboutApp.versionInfo.setText(
                "Current version code: " + BuildConfig.VERSION_CODE + "" +
                        "\nCurrent version name: " + BuildConfig.VERSION_NAME + "" +
                        "\nCurrent Realm: 1.0.187");


    }

    void initUi() {
        setSupportActionBar(binding.contentMain.include.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        binding.contentMain.include.back.setVisibility(View.GONE);
//
//        binding.contentMain.include.back.setOnClickListener(v -> {
//            if (binding.drawerLayout.isDrawerOpen()) binding.drawerLayout.closeDrawer();
//            else binding.drawerLayout.openDrawer();
//        });

        DuoDrawerToggle toggle = new DuoDrawerToggle(this, binding.drawerLayout, binding.contentMain.include.toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        binding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        sideMenuModules = StaticDataLoader.sideMenuModules();
        mainMenuModules = StaticDataLoader.mainMenuModules();

//        binding.mainNavigationMenu.siteName.setText(svars.site_name(act));
//        binding.mainNavigationMenu.userName.setText(Globals.user().username);

        //   userMember=Realm.databaseManager.loadObject(Member.class,new Query().setTableFilters("system_user_id='"+svars.user_id(act)+"'"));
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
//        binding.contentMain.menuList.setLayoutManager(flowLayoutManager);
        binding.contentMain.menuList.setLayoutManager(new GridLayoutManager(this, 2));
        binding.contentMain.menuList.setAdapter(new AppMenuModulesAdapter(mainMenuModules, (mem, view) -> {
            try {

                Log.e("MainActivity", "initUi: MEM CODE " + mem.code );
                Log.e("MainActivity", "initUi: Members MEM CODE " + MemberSummary.class.getName() );
                Log.e("MainActivity", "initUi: Groups MEM CODE " + GroupRecords.class.getName() );

                if (mem.code.equals(GroupRecords.class.getName())) {

                    Intent intent = new Intent(act, GroupRecords.class);
                    intent.putExtra("search", true);
                    intent.putExtra("targetActivity", mem.code);
                    intent.putExtra("defaultTableFilters", new String[0]);
                    intent.putExtra("moduleName", mem.name);


                    startActivity(intent);

                } else if (mem.code.equals(MemberSummary.class.getName())) {

                    Intent intent = new Intent(act, MemberRecords.class);
                    intent.putExtra("search", true);
                    intent.putExtra("targetActivity", mem.code);
                    intent.putExtra("defaultTableFilters", new String[0]);

                    startActivity(intent);

                } else if (mem.code.equals(Addendum.class.getName())) {

                    Intent intent = new Intent(act, MemberRecords.class);
                    intent.putExtra("search", true);
                    intent.putExtra("targetActivity", mem.code);
                    intent.putExtra("defaultTableFilters", new String[]{"sid in (select member_id from farmer_contract where is_contract_active == 'true')"});

                    startActivity(intent);

                } else {
                    //startActivity(new Intent(act, Class.forName(mem.code)));
                    Intent intent = new Intent(act, Class.forName(mem.code));
                    intent.putExtra("moduleName", mem.name);
                    startActivity(intent);
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            binding.drawerLayout.closeDrawer();
        }));

        binding.mainNavigationMenu.menuList.setLayoutManager(new LinearLayoutManager(act));
        binding.mainNavigationMenu.menuList.setAdapter(new AppModulesAdapter(sideMenuModules, (mem, view) -> {
            try {
                startActivity(new Intent(act, Class.forName(mem.code)));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                binding.drawerLayout.closeDrawer();
                return;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            Log.e("MainActivity", "InitUi: Current Mem Code: " + mem.code);

            if (mem.code.equalsIgnoreCase("1")) {
                DatabaseManager.database.execSQL("Delete from member_info_table");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new Member()).get(0));


            } else if (mem.code.equalsIgnoreCase("2")) {
                DatabaseManager.database.execSQL("Delete from member_image_table");
//                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new MemberImage()).get(0));

            } else if (mem.code.equalsIgnoreCase("3")) {
                DatabaseManager.database.execSQL("Delete from member_fingerprint");
//                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new MemberFingerprint()).get(0));

            } else if (mem.code.equalsIgnoreCase("4")) {
                DatabaseManager.database.execSQL("Delete from survey");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new Survey()).get(0));

            } else if (mem.code.equalsIgnoreCase("5")) {
                DatabaseManager.database.execSQL("Delete from survey_question");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new SurveyQuestion()).get(0));

            } else if (mem.code.equalsIgnoreCase("6")) {
                DatabaseManager.database.execSQL("Delete from survey_question_choice");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new SurveyQuestionChoice()).get(0));

            } else if (mem.code.equalsIgnoreCase("7")) {
                DatabaseManager.database.execSQL("Delete from suitability_assessment_survey_entry");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new SuitabilityAssessmentSurveyEntry()).get(1));

            } else if (mem.code.equalsIgnoreCase("8")) {
                DatabaseManager.database.execSQL("Delete from survey_question_entry");

            } else if (mem.code.equalsIgnoreCase("9")) {
                DatabaseManager.database.execSQL("Delete from app_versions_table");

            } else if (mem.code.equalsIgnoreCase("10")) {
                DatabaseManager.database.execSQL("Delete from member_group");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new MemberGroup()).get(0));

            } else if (mem.code.equalsIgnoreCase("11")) {
                DatabaseManager.database.execSQL("Delete from group_participant");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new GroupParticipant()).get(1));

            } else if (mem.code.equalsIgnoreCase("12")) {
                DatabaseManager.database.execSQL("Delete from land_location");
//                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new LandLocation()).get(1));

            } else if (mem.code.equalsIgnoreCase("13")) {
                DatabaseManager.database.execSQL("Delete from addendum");
                MyApplication.syncOverride.sm.download_(Realm.realm.getSyncDescription(new Addendum()).get(0));

            } else if (mem.code.equalsIgnoreCase("a")) {
                binding.drawerLayout.closeDrawer();
//                start_activity(new Intent(act, SignIn.class));

                finish();
            } else if (mem.code.equalsIgnoreCase("b")) {
                onBackPressed();
                onBackPressed();
                onBackPressed();

            } else if (mem.code.equalsIgnoreCase("c")) {
                startActivity(new Intent(this, Configuration.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            } else if (mem.code.equalsIgnoreCase("fpc")) {
                new bt_device_connector(this, bt_device_connector.bt_device_type.fp_device).show(new bt_device_connector.device_selection_handler() {

                    @Override
                    public void on_device_paired_and_selected(BluetoothDevice device) {

                    }

                    @Override
                    public void on_device_slected(BluetoothDevice device) {

                    }

                    @Override
                    public void on_device_paired(BluetoothDevice device) {

                    }
                });


            } else if (mem.code.equalsIgnoreCase("pc")) {
                new bt_device_connector(act, bt_device_connector.bt_device_type.printer).show(new bt_device_connector.device_selection_handler() {


                    @Override
                    public void on_device_paired_and_selected(BluetoothDevice device) {

                    }

                    @Override
                    public void on_device_slected(BluetoothDevice device) {

                    }

                    @Override
                    public void on_device_paired(BluetoothDevice device) {

                    }
                });

            } else if (mem.code.equalsIgnoreCase("sync")) {

                //Resync missing ids
                int updatedRecs = Globals.updateSyncStatus("member_image_table");
                Log.e("MemberRecords", "onCreate: " + updatedRecs);
                //Update images sid from temp
                int imageUpdatedRecords = Globals.isSidPresentUpdate2("member_image_table");
                Log.e("MainActivity", "UpdatedImageRecords: " + imageUpdatedRecords);
                //Update fingerprint with member_id
                int fpUpdatedRecords = Globals.isSidPresentUpdate2("member_fingerprint");
                Log.e("MainActivity", "UpdatedFpRecords: " + fpUpdatedRecords);
                //Update suitability member_id from temp
                int suitabilityUpdatedRecords = Globals.isSidPresentUpdate("suitability_assessment_survey_entry");
                Log.e("MainActivity", "UpdatedSuitabilityRecords: " + suitabilityUpdatedRecords);
                //Update location member_id from temp
                int locationUpdatedRecords = Globals.isSidPresentUpdate("land_location");
                Log.e("MainActivity", "UpdatedLandMappingRecords: " + locationUpdatedRecords);

                MyApplication.syncOverride.sync();
                return;
            }
            binding.drawerLayout.closeDrawer();

        }));
        User user = Globals.user();
        user.user_sites = Realm.databaseManager.loadObjectArray(UserSite.class, new Query().setTableFilters("user='" + user.sid + "'"));
        if (user != null) {
            binding.mainNavigationMenu.userName.setText("Logged In User: " + user.username);
            //binding.mainNavigationMenu.siteName.setText("Logged In User Site: " + svars.site_name(Realm.context));
            try {
                //binding.mainNavigationMenu.siteName.setText(user.user_sites.get(0).name);
            } catch (Exception ex) {
            }
        }
        binding.mainNavigationMenu.todaysPrints.setText("Today's activities:" + todayActivity());
        binding.mainNavigationMenu.totalPrints.setText("Total activity:" + totalActivity());
        binding.contentMain.bottomNav.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.info_b_nav) {
                    binding.contentMain.aboutApp.getRoot().setVisibility(View.VISIBLE);
//                    binding.contentMain.aboutApp.title.setText("Latest as of "+Conversions.sdfUserDisplayDate.format(new Date(Globals.updateCheckTime())));
                    if (appUpdateService != null) {
                        appUpdateService.checkStatus();
                    }
                    binding.contentMain.backupReport.getRoot().setVisibility(View.GONE);
                    binding.contentMain.menuList.setVisibility(View.GONE);
                } else if (id == R.id.action_item2) {
                    binding.contentMain.menuList.setVisibility(View.VISIBLE);
                    binding.contentMain.aboutApp.getRoot().setVisibility(View.GONE);
                    binding.contentMain.backupReport.getRoot().setVisibility(View.GONE);
                } else if (id == R.id.action_item3) {
                    binding.contentMain.backupReport.getRoot().setVisibility(View.VISIBLE);
                    binding.contentMain.aboutApp.getRoot().setVisibility(View.GONE);
                    binding.contentMain.menuList.setVisibility(View.GONE);
//                    backup.triger_backup();
                }
                return false;
            }
        });
        setupSync();
//        binding.contentMain.getRoot().setBackground(getDrawable(R.drawable.main_background));//refused to work changing background
        binding.contentMain.aboutApp.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appUpdateService != null) {
                    appUpdateService.checkForUpdates();

                }
            }
        });
        binding.mainNavigationMenu.searchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ((AppModulesAdapter) binding.mainNavigationMenu.menuList.getAdapter()).getFilter().filter(binding.mainNavigationMenu.searchbar.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.contentMain.backupReport.cancel.setVisibility(View.GONE);
        binding.contentMain.backupReport.progress.setVisibility(View.GONE);
        binding.contentMain.backupReport.backupProgressInfo.setVisibility(View.GONE);
        binding.contentMain.backupReport.backup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.contentMain.backupReport.cancel.setVisibility(View.VISIBLE);
                binding.contentMain.backupReport.progress.setVisibility(View.VISIBLE);
                binding.contentMain.backupReport.backupProgressInfo.setVisibility(View.VISIBLE);
                new Thread(() -> BackupManager.backupAppData(MainActivity.this, new BackupManager.BackupListener() {
                    @Override
                    public void onBackupArchiveCreated(BackupEntry backupEntry) {
                        Realm.databaseManager.insertObject(backupEntry);
                    }

                    @Override
                    public void onStatusChanged(String status) {
                        Log.e(logTag, "Backup status: " + status);
                        runOnUiThread(() -> binding.contentMain.backupReport.backupProgressInfo.setText(status));
                    }

                    @Override
                    public void onBackupComplete() {
                        runOnUiThread(() -> loadBackupInfo());
                        new Handler(Looper.getMainLooper()).postDelayed(() -> {
                            binding.contentMain.backupReport.cancel.setVisibility(View.GONE);
                            binding.contentMain.backupReport.progress.setVisibility(View.GONE);
                            binding.contentMain.backupReport.backupProgressInfo.setVisibility(View.GONE);
                        }, 2000);
                    }

                    @Override
                    public void onBackupBegun(BackupManager.BackupType backupType, BackupEntry backupEntry) {
                        Realm.databaseManager.insertObject(new BackupUploadEntry(backupEntry.transaction_no, backupType.ordinal() + "", "" + sync_status.pending.ordinal()));
                        runOnUiThread(() -> loadBackupInfo());
                    }

                    @Override
                    public void onBackupComplete(BackupManager.BackupType backupType, BackupEntry backupEntry) {
                        DatabaseManager.database.execSQL("update backup_upload_entry set sync_status ='" + sync_status.pending.ordinal() + "' where backup_transaction_no='" + backupEntry.transaction_no + "' and backup_type='" + backupType.ordinal() + "'");
                        DatabaseManager.database.execSQL("update backup_upload_entry set upload_status ='" + sync_status.syned.ordinal() + "' where backup_transaction_no='" + backupEntry.transaction_no + "' and backup_type='" + backupType.ordinal() + "'");
                        runOnUiThread(() -> loadBackupInfo());
                    }

                    @Override
                    public void onBackupFailed(BackupManager.BackupType backupType, BackupEntry backupEntry) {
                        Log.e(logTag, "Backup error: " + backupType.name());
                        runOnUiThread(() -> binding.contentMain.backupReport.backupProgressInfo.setText("Backup for " + backupType.name() + " failed"));

                    }
                })).start();
            }
        });
        binding.contentMain.backupReport.backupTypeConfigList.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<BackupManager.BackupType> backupTypes = new ArrayList<>();
        backupTypes.add(BackupManager.BackupType.Mail);
        backupTypes.add(BackupManager.BackupType.SFTP);
        binding.contentMain.backupReport.backupTypeConfigList.setAdapter(new BackupTypeConfigAdapter(backupTypes));
        binding.contentMain.backupReport.backupRecordList.setLayoutManager(new LinearLayoutManager(this));
        binding.contentMain.backupReport.backupRecordList.setAdapter(new BackupReportAdapter(backups));
        loadBackupInfo();
    }

    BackupEntry latestBackup;
    ArrayList<BackupEntry> backups = new ArrayList<>();

    void loadBackupInfo() {
        latestBackup = Realm.databaseManager.loadObject(BackupEntry.class, new Query().addOrderFilters("reg_time", false));
        if (latestBackup == null) {
            binding.contentMain.backupReport.lastBackupInfo.setText("Never backed up");
        } else {
            binding.contentMain.backupReport.lastBackupInfo.setText("Last backup time: " + Conversions.getUserDisplayTimeFromDBTime(latestBackup.reg_time) + "\n" + "Size: " + BackupManager.getUserDisplayBytes(latestBackup.file_size));
        }
        backups.clear();
        backups.addAll(Realm.databaseManager.loadObjectArray(BackupEntry.class, new Query().addOrderFilters("reg_time", false)));
        binding.contentMain.backupReport.backupRecordList.getAdapter().notifyDataSetChanged();
        binding.contentMain.backupReport.noRecordsLay.setVisibility((backups.size()) > 0 ? View.GONE : View.VISIBLE);

    }

    void setupSync() {
        MyApplication.syncOverride = MyApplication.syncOverride != null ? MyApplication.syncOverride : new SyncOverride();

        MyApplication.syncOverride.setupsyncstatusreport(new SynchronizationManager.SynchronizationStatusHandler() {
            @Override
            public void on_status_changed(String status) {

                runOnUiThread(() -> binding.mainNavigationMenu.syncStatusTxt.setText(status));


            }

            @Override
            public void on_info_updated(String status) {
                runOnUiThread(() -> binding.mainNavigationMenu.syncStatusTxt.setText(status));

            }

            @Override
            public void on_main_percentage_changed(int progress) {
                Log.e("SSD :", "on_main_percentage_changed :" + "Floating    " + progress);
                runOnUiThread(() -> binding.mainNavigationMenu.arcProgress.setProgress(progress));
            }

            @Override
            public void on_secondary_progress_changed(int progress) {
                Log.e("SSD :", "on_secondary_progress_changed :" + "Min    " + progress);


            }

            @Override
            public void onSynchronizationCompleted() {
                Log.e("Main SSD :", "onSynchronizationCompleted Generally");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                                    String time_now = svars.gett_time();
                        String time_now = svars.gett_time();

                        svars.set_sync_time(act, time_now);
                        Log.e("Main SSD :", "SYnc _time " + time_now + "   " + svars.sync_time(act));
                        binding.mainNavigationMenu.arcProgress.setProgress(100);
                        binding.mainNavigationMenu.syncStatusTxt.setText("Sync complete\n" + svars.sync_time(act));
                        try {
                            binding.mainNavigationMenu.syncStatusTxt.setText("Sync complete \n" + (Conversions.sdfUserDisplayTime.format(Conversions.sdf_user_friendly_time.parse(svars.sync_time(act))).startsWith(Conversions.getUserDisplayTime().split(" ")[0]) ? "Today at " + svars.sync_time(act).split(" ")[1] : Conversions.sdfUserDisplayTime.format(Conversions.sdf_user_friendly_time.parse(svars.sync_time(act)))));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (appUpdateService != null) {
                            appUpdateService.checkForUpdates();

                        }
                    }
                });


            }

            @Override
            public void on_api_error(sync_service_description ssd, String error) {
                Log.e("Main SSD :", "Sync error :" + ssd.object_package + "\nError: " + error);

            }

            @Override
            public void onSynchronizationCompleted(sync_service_description ssd) {
                Log.e("Main SSD :", "onSynchronizationCompleted on :" + ssd.object_package);

            }
        });
        try {
            binding.mainNavigationMenu.syncStatusTxt.setText("Last synced \n" + (Conversions.sdfUserDisplayTime.format(Conversions.sdf_user_friendly_time.parse(svars.sync_time(act))).startsWith(Conversions.getUserDisplayTime().split(" ")[0]) ? "Today at " + svars.sync_time(act).split(" ")[1] : Conversions.sdfUserDisplayTime.format(Conversions.sdf_user_friendly_time.parse(svars.sync_time(act)))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainnav, menu);
        return super.onCreateOptionsMenu(menu);
    }

    int totalActivity() {
        Query query = new Query().setTableFilters("user_id='" + svars.user_id(Realm.context) + "'");
        return Realm.databaseManager.getRecordCount(Member.class, query) +
                Realm.databaseManager.getRecordCount(MemberGroup.class, query) +
                Realm.databaseManager.getRecordCount(SuitabilityAssessmentSurveyEntry.class, query) +
                Realm.databaseManager.getRecordCount(LandLocation.class, query) +
                Realm.databaseManager.getRecordCount(sparta.realm.apps.farmercontractor.models.Addendum.class, query);
    }

    int todayActivity() {
        Query query = new Query().setTableFilters("user_id='" + svars.user_id(Realm.context) + "'", "date(reg_time)=date('now')");
        return Realm.databaseManager.getRecordCount(Member.class, query) +
                Realm.databaseManager.getRecordCount(MemberGroup.class, query) +
                Realm.databaseManager.getRecordCount(SuitabilityAssessmentSurveyEntry.class, query) +
                Realm.databaseManager.getRecordCount(LandLocation.class, query) +
                Realm.databaseManager.getRecordCount(sparta.realm.apps.farmercontractor.models.Addendum.class, query);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(act, "Clicked Menu", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.clear_all_menu:
                Toast.makeText(act, "Clicked clear all", Toast.LENGTH_LONG).show();
                break;
            case R.id.exit_registration:
                Toast.makeText(act, "Clicked Exit enrolment", Toast.LENGTH_LONG).show();

                break;
        }
        return super.onOptionsItemSelected(item);

    }
}