package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.ReportEntry;
import sparta.realm.spartamodels.percent_calculation;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.Conversions;


public class ActivityReportAdapter extends RecyclerView.Adapter<ActivityReportAdapter.view> {

    public ArrayList<ReportEntry> items;
    Context cntxt;


    public ActivityReportAdapter(ArrayList<ReportEntry> items) {
        this.items = items;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_report_entry, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface onItemClickListener {

        void onItemClick(ReportEntry mem);
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, description, time;
        CircleImageView circleImageView;


        int position;


        view(View itemView) {
            super(itemView);

            circleImageView = itemView.findViewById(R.id.icon);
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.info1);
            time = itemView.findViewById(R.id.time);
        }

        public void populate(int position) {
            this.position = position;
            ReportEntry ssr = items.get(position);
            circleImageView.setVisibility(Globals.user().sid.equalsIgnoreCase(ssr.user_id)?View.VISIBLE:View.GONE);
            name.setText(ssr.description);
            description.setText(ssr.name);
            time.setText(Conversions.getUserDisplayDateFromDBTime(ssr.reg_time));


        }

        @Override
        public void onClick(View view) {

        }
    }
}
