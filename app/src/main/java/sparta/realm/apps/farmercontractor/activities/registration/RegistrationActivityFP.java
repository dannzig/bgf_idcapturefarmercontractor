package sparta.realm.apps.farmercontractor.activities.registration;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import sparta.realm.Activities.SpartaAppCompactFingerPrintActivity;
import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.Member;

public class RegistrationActivityFP extends SpartaAppCompactFingerPrintActivity {


    public void setupToolbar(Toolbar toolbar)
    {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    String checkTransactionNumber() {
        String transaction_number = Globals.getTransactionNo();

        while (Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("transaction_no='" + transaction_number + "'")) != null) {
            transaction_number = Globals.getTransactionNo();
        }
        return transaction_number;

    }

    void showQuitDialog() {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_exit_enrollement_warning, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        aldv.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ald.dismiss();

                finish();
            }
        });

        aldv.findViewById(R.id.dismiss).setOnClickListener(view -> ald.dismiss());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.registration_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "Clicked Menu", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.clear_all_menu:
                Toast.makeText(this, "Clicked clear all", Toast.LENGTH_LONG).show();
                break;
            case R.id.exit_registration:
                Toast.makeText(this, "Clicked Exit enrolment", Toast.LENGTH_LONG).show();
                showQuitDialog();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
