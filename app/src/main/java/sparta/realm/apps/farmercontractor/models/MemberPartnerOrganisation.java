package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "partner_organisation")
//@SyncDescription(service_id = "1", service_name = "Partner Organisation", download_link = "/Configurations/PartnerOrg/RebindGrid", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class MemberPartnerOrganisation extends RealmModel implements Serializable {


    @DynamicProperty(json_key = "member_transaction_no")
    public String member_transaction_no;

     @DynamicProperty(json_key = "partner_organisation")
    public String partner_organisation;

    public MemberPartnerOrganisation(String member_transaction_no,String partner_organisation) {
        this.member_transaction_no = member_transaction_no;
        this.partner_organisation = partner_organisation;


        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this. user_id= Globals.user().sid;
    }


    public MemberPartnerOrganisation() {


    }

}
