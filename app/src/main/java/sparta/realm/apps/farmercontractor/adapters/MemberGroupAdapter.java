package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.GroupStages;
import sparta.realm.apps.farmercontractor.models.MemberGroup;


public class MemberGroupAdapter extends RecyclerView.Adapter<MemberGroupAdapter.view> {

    Context cntxt;
    public ArrayList<MemberGroup> items;
    onItemClickListener listener;
    GroupStages groupStage;

    public interface onItemClickListener {

        void onItemClick(MemberGroup mem, View view);
    }


    public MemberGroupAdapter(ArrayList<MemberGroup> items, onItemClickListener listener) {

        this.items = items;
        this.listener = listener;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_member_group, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name_title,name, description_title,description, member_count, on_groups, on_groups_formation, groupStageTxt, newly_formed, organized_group, ready_for_certification, issued_with_certificate, group_stage_title, training_title;
        public sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner aa_agent_spn,group_stage, country,county_input,location_input,sub_location_input,village_input,site_input;
        public Button activate_btn;

        int position;


        view(View itemView) {
            super(itemView);

            name_title = itemView.findViewById(R.id.name_title);
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.info1);
            description_title = itemView.findViewById(R.id.info1_title);
            member_count = itemView.findViewById(R.id.member_count);


            aa_agent_spn = itemView.findViewById(R.id.aa);
            on_groups = itemView.findViewById(R.id.groupsTrained);
            on_groups_formation = itemView.findViewById(R.id.groupsTrainedFormation);
            //group_stage = itemView.findViewById(R.id.groupStage);
            country = itemView.findViewById(R.id.country);
            county_input = itemView.findViewById(R.id.county_input);
            location_input = itemView.findViewById(R.id.location_input);
            sub_location_input = itemView.findViewById(R.id.sub_location_input);
            village_input = itemView.findViewById(R.id.village_input);
            site_input = itemView.findViewById(R.id.site_input);

            activate_btn = itemView.findViewById(R.id.activate_btn);
            newly_formed = itemView.findViewById(R.id.newly_formed);
            organized_group = itemView.findViewById(R.id.organized_groups);
            ready_for_certification = itemView.findViewById(R.id.ready_for_Certification);
            issued_with_certificate = itemView.findViewById(R.id.with_Certification);
            group_stage_title = itemView.findViewById(R.id.group_stage_title);
            training_title = itemView.findViewById(R.id.training_title);


            itemView.setClickable(true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(position), view);
                }
            });
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(position), view);
                }
            });
            description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(position), view);
                }
            });
        }

        void populate(int position) {

            this.position = position;
            MemberGroup obj = items.get(position);
            //groupStage = Realm.databaseManager.loadObject(GroupStages.class, new Query().setTableFilters(obj.group_stages_id == null || obj.group_stages_id.length() < 1 ? "sid='1'" : "sid='" + obj.group_stages_id + "'"));

            name.setText(obj.name);

            description.setText(obj.description);
            description.setVisibility(obj.description == null ? View.GONE : obj.description.length() < 1 ? View.GONE : View.VISIBLE);



            aa_agent_spn.setVisibility(View.GONE);
            on_groups.setVisibility(View.GONE);
            on_groups_formation.setVisibility(View.GONE);
            //group_stage.setVisibility(View.GONE);
            country.setVisibility(View.GONE);
            county_input.setVisibility(View.GONE);
            location_input.setVisibility(View.GONE);
            sub_location_input.setVisibility(View.GONE);
            village_input.setVisibility(View.GONE);
            site_input.setVisibility(View.GONE);
            activate_btn.setVisibility(View.GONE);
            newly_formed.setVisibility(View.GONE);
            organized_group.setVisibility(View.GONE);
            ready_for_certification.setVisibility(View.GONE);
            issued_with_certificate.setVisibility(View.GONE);
            group_stage_title.setVisibility(View.GONE);
            training_title.setVisibility(View.GONE);

            name.setFocusable(false);
            description.setFocusable(false);
//            name.setBackground(null);
//            description.setBackground(null);
            //groupStageTxt.setText(groupStage == null ? "Specify Group Stage" : groupStage.name);
            member_count.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, obj.transaction_no == null || obj.transaction_no.length() < 1 ? "group_id='" + obj.sid + "'" : "is_active = 'true' AND "+"group_transaction_no='" + obj.transaction_no + "' OR group_id='" + obj.sid + "'") + " members");

//SELECT * FROM member_info_table WHERE sid in(select member_id from group_participant where  sid='54') AND  UPPER(full_name) LIKE '%%' ORDER BY reg_time DESC  LIMIT 1000
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(items.get(position), view);

        }
    }
}
