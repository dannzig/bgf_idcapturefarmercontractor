package sparta.realm.apps.farmercontractor.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.realm.annotations.sync_status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.AuthenticationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.databinding.ActivitySignInBinding;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.User;
import sparta.realm.apps.farmercontractor.models.UserSite;
import sparta.realm.spartautils.svars;

public class SignIn extends AppCompatActivity {

    ActivitySignInBinding binding;
    private final ArrayList<String> mStrings = new ArrayList<>();
    public String logTag="SignIn";
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivitySignInBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        context=this;
        initUi();
    }

    void initUi() {

        binding.include.back.setVisibility(View.GONE);
        binding.include.optionsMenu.setVisibility(View.GONE);

        if(Globals.operationMode()== Globals.OperationMode.Dev) {
            binding.include.subTitle.setText("Farmer Contractor  [  Dev mode  ]");
        }
        if(Globals.operationMode()== Globals.OperationMode.Demo) {
            binding.include.subTitle.setText("Farmer Contractor   [  DEMO  ]");
        }
        if(Globals.operationMode()== Globals.OperationMode.Live) {
            binding.include.subTitle.setText("Farmer Contractor   [  LIVE  ]");
        }

        if(Globals.user()!=null){
            binding.usernameEdt.setText(Globals.user().username);
            binding.paswordEdt.setText(Globals.user().password);
       }

        binding.usernameEdt.requestFocus();
        binding.paswordEdt.requestFocus();

        binding.paswordEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == 0) {

                    signIn(true);
                    return false;
                }
                return true;
            }
        });

        binding.signin.setOnClickListener(v -> {
            signIn(true);

        });
        binding.signUp.setOnClickListener(v -> {
//            startActivity(new Intent(act, SignUp.class));
            // finish();
        });



    }

    void signIn(boolean checkOnline) {

        User user = Realm.databaseManager.loadObject(User.class, new Query().setTableFilters("username='" + binding.usernameEdt.getText().toString() + "'", "password='" + binding.paswordEdt.getText().toString() + "'"));
        if (user != null) {
            Globals.setUser(user);
            SharedPreferences.Editor saver = this.getSharedPreferences(svars.sharedprefsname, this.MODE_PRIVATE).edit();
            saver.putString("username", user.username);
            saver.putString("pass", user.password);
            saver.commit();
            startActivity(new Intent(SignIn.this, MainActivity.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();


        } else if(checkOnline) {
            View snackbar_view = LayoutInflater.from(context).inflate(R.layout.dialog_verification_confirmation, null);
            TextView sub_title = snackbar_view.findViewById(R.id.sub_title);
            TextView info = snackbar_view.findViewById(R.id.content);
            sub_title.setText("Sign in Successful !");//:working_verification_type== DataMatcher.verification_type.clock_out?"Clocked out successfully":"Verified successfully");
            info.setText("Your Credentials Have Been verified ...");
            Toast t = new Toast(context);
            t.setView(snackbar_view);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.setDuration(Toast.LENGTH_LONG);
            t.show();
            onlineAuthenticate(binding.usernameEdt.getText().toString(), binding.paswordEdt.getText().toString());

        }else{
            View snackbar_view = LayoutInflater.from(context).inflate(R.layout.dialog_verification_confirmation, null);
            TextView sub_title = snackbar_view.findViewById(R.id.sub_title);
            TextView info = snackbar_view.findViewById(R.id.content);
            sub_title.setText("Sign in failed !");
            info.setText("Your Credentials couldn't be verified");
            Toast t = new Toast(context);
            t.setView(snackbar_view);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.setDuration(Toast.LENGTH_LONG);
            t.show();

        }

    }

    AuthenticationManager authenticationManager;

    void onlineAuthenticate(String username, String password) {
        authenticationManager = authenticationManager == null ? new AuthenticationManager() : authenticationManager;
        authenticationManager.authenticate(new AuthenticationManager.AuthenticationCallback() {
            @Override
            public void onApiConnectionFailed() {

            }
            public void OnAuthenticatedSuccessfully(String token, JSONObject response) {
                Log.e(logTag, "Auth ok:" + response.toString());
                JSONObject user_JO = null;
                try {
                    user_JO = response.getJSONObject("result").getJSONObject("result");
                    User user = new User();
                    user.username = username;
                    user.password = password;
                    user.sid = user_JO.getString("user_id");
                    user.sync_status = "" + sync_status.syned.ordinal();
                    JSONArray jsonArray =response.getJSONObject("result").getJSONObject("result").getJSONArray("user_branches");

                    Realm.databaseManager.database.execSQL("DELETE FROM user_table WHERE sid='"+user.sid+"'");
//                    Realm.databaseManager.database.execSQL("DELETE FROM user_table");
                    Realm.databaseManager.database.execSQL("DELETE FROM user_site WHERE user='"+user.sid+"'");
                    Realm.databaseManager.insertObject(user);
                    SharedPreferences.Editor saver = getSharedPreferences(svars.sharedprefsname, MODE_PRIVATE).edit();
                    saver.putString("username", username);
                    saver.putString("pass", password);
                    saver.commit();

                    int len=jsonArray.length();
                    for (int i=0;i<len;i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        UserSite site=new UserSite();
                        site.user=user.sid;
                        site.sid=jsonObject.getString("id");
                        site.name=jsonObject.getString("branch_name");
                        user.user_sites.add(site);
                        Realm.databaseManager.insertObject(site);
                    }


                    svars.set_user_id(context,user.sid);
                    svars.set_site_id(context, user_JO.getJSONArray("user_branches").getJSONObject(0).getString("id"));
                    svars.set_site_name(context, user_JO.getJSONArray("user_branches").getJSONObject(0).getString("branch_name"));

//                    MyApplication.syncOverride.sync();
                    signIn(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void OnAuthenticationFailed() {
                Log.e(logTag, "Auth failed!");
                signIn(false);
            }
        }, null, username, password);
    }
}