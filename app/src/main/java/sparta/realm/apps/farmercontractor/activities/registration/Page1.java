package sparta.realm.apps.farmercontractor.activities.registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.realm.annotations.RealmModel;
import com.realm.annotations.sync_status;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.BuildConfig;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.databinding.ActivityPage1Binding;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.SeedlingType;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.models.system.ValidationRules;
import sparta.realm.apps.farmercontractor.utils.FormTools.FormEdittext;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;

public class Page1 extends RegistrationActivity {

    ActivityPage1Binding binding;
    String enrollment_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPage1Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Globals.reg_mode = getIntent().getStringExtra("reg_mode");
        Globals.registeringMember = new Member(Globals.reg_mode);
        Globals.registeringMember.apk_version_creating = BuildConfig.VERSION_NAME;
        String member_transaction_no = getIntent().getStringExtra("transaction_no");
        Log.e("Page1", "onCreate: Trans No: " + member_transaction_no);


        if (Globals.reg_mode.equalsIgnoreCase("2"))//Editing
        {

            String member_id = getIntent().getStringExtra("sid");
            Log.e("Page1", "OnCreate: Member_id:  " + member_id);

            if (member_id == "0"){
                Toast.makeText(this, "Duplicate Record Click Other Record", Toast.LENGTH_SHORT).show();
                return;
            }
            if (member_id == null || member_id.isEmpty()) {
                if (member_transaction_no == null || member_transaction_no.isEmpty()) {

                    Toast.makeText(this, "Member Transaction Number is Null", Toast.LENGTH_SHORT).show();
                    finish();
                } else {

                    Globals.registeringMember = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("transaction_no='" + member_transaction_no + "'"));
                    Globals.registeringMember.transaction_no = member_transaction_no;

                }

            } else {
                Globals.registeringMember = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + member_id + "'"));
                Globals.registeringMember.transaction_no = member_transaction_no;

            }
            if (Globals.registeringMember == null) {
                finish();
                return;

            }
            Globals.setRegisteringMember(Globals.registeringMember);

        } else {

            Globals.registeringMember.sid = null;
        }


        try {
            Globals.registeringMember();
        } catch (Exception ex) {
            Globals.setRegisteringMember(null);
        }


        if (Globals.registeringMember() == null) {
            Globals.registeringMember.transaction_no = checkTransactionNumber();
            Globals.setRegisteringMember(Globals.registeringMember);
        } else {
            Globals.registeringMember = Globals.registeringMember();
            if (Globals.reg_mode.equalsIgnoreCase("1")) {

                Globals.registeringMember.transaction_no = checkTransactionNumber();
            }
        }


        Log.e("Page1", "Current Sid: " + Globals.registeringMember.sid);

        Globals.registeringMember.enrollment_type = Globals.reg_mode;
        Globals.registeringMember.sync_status = "" + sync_status.pending.ordinal();
        Globals.registeringMember.reg_start_time = System.currentTimeMillis() + "";


        initUi();
    }

    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);
            binding.seedlingTypeInput.setDataset(Realm.databaseManager.loadObjectArray(SeedlingType.class, new Query().setColumns("_id", "sid", "item_name as name").setTableFilters("country='" + input + "'")), seedlingTypeInputListener);

            Log.e("Page1", "Registered Member, County" + Globals.registeringMember().county);
            Log.e("Page1", "Registered Member, Ward" + Globals.registeringMember().location);
            Log.e("Page1", "Registered Member, Sub-Location" + Globals.registeringMember().sub_location);
            Log.e("Page1", "Registered Member, Village" + Globals.registeringMember().village);
            Log.e("Page1", "Registered Member, Site" + Globals.registeringMember().site);
            Log.e("Page1", "Registered Member, Seedling Type" + Globals.registeringMember().seedling_type);

            binding.countyInput.setInput(Globals.registeringMember().county);
            binding.siteInput.setInput(Globals.registeringMember().site);
            binding.seedlingTypeInput.setInput(Globals.registeringMember().seedling_type);

            //Changing Administrative titles for each Country
            if (input.equals("110")) {
                binding.countyInput.setTitle("County");
                binding.locationInput.setTitle("Ward");
                binding.subLocationInput.setTitle("Sub-Location");
            }
            if (input.equals("222")) {
                binding.countyInput.setTitle("District");
                binding.locationInput.setTitle("Sub-County");
                binding.subLocationInput.setTitle("Parish");
            }
        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
            binding.locationInput.setInput(Globals.registeringMember().location);

        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
            binding.subLocationInput.setInput(Globals.registeringMember().sub_location);

        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
            binding.villageInput.setInput(Globals.registeringMember().village);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("county='" + input + "'")), siteInputListener);
//            binding.siteInput.setInput(Globals.registeringMember().site);
        }
    };

    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

        }
    };
    SearchSpinner.InputListener seedlingTypeInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            SeedlingType seedlingType = Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + input + "'"));
            if (seedlingType != null) {
                binding.yearsOfMaturityInput.setInput(seedlingType.year_of_maturity);
                binding.dbhAtMaturityInput.setInput(seedlingType.dbh);
            }
        }
    };
    FormEdittext.InputListener totalSeedlings = new FormEdittext.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            //FormEdittext.InputListener.super.onInputAvailable(valid, input);

            if (binding.totalSeedlingsInput.getInput().length() > 0) {
                int number_trees = Integer.parseInt(binding.totalSeedlingsInput.getInput());
                binding.donatedSeedlingsInput.setInput("" + number_trees / 2);
            }

        }
    };
    //Making sure no new farmer is saved with same ID
    FormEdittext.InputListener national_id = new FormEdittext.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            Member member;
            member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("nat_id='" + binding.farmerIdNoInput.getInput() + "'"));
            if (Globals.reg_mode.equalsIgnoreCase("1")) {
                if (member != null) {
                    binding.farmerIdNoInput.setInput(null);
                    Toast.makeText(Page1.this, "ID Number Already Exists", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    void initUi() {
        setupToolbar(binding.include.toolbar);
        binding.pageProgress.setStepsNumber(3);
        binding.pageProgress.go(0, true);

        binding.formControl.btnPrev.setOnClickListener(view -> {
            binding.formControl.btnPrev.setFocusable(true);
            binding.formControl.btnPrev.setFocusableInTouchMode(true);
            binding.formControl.btnPrev.requestFocus();
            showQuitDialog();

        });

        binding.countryInput.setDataset(Country.class.getName(), countryInputListener);
        binding.totalSeedlingsInput.setInputListener(totalSeedlings);
//        binding.seedlingTypeInput.setDataset(SeedlingType.class.getName(), seedlingTypeInputListener);
        binding.formControl.btnNext.setOnClickListener(view -> prooceed());
        populate(Globals.registeringMember());
        //binding.farmerLandSizeInput.setInputListener(national_id);
        ValidationRules validationRules = new ValidationRules();
        validationRules.mandatory = ValidationRules.MandatoryStatus.Mandatory.ordinal() + "";
        validationRules.text_input_type = ValidationRules.TextInputType.numeric.ordinal() + "";
        binding.farmerLandSizeInput.setValidationRules(validationRules);
        binding.totalSeedlingsInput.setValidationRules(validationRules);
        binding.donatedSeedlingsInput.setValidationRules(validationRules);
        binding.dbhAtMaturityInput.setValidationRules(validationRules);
        binding.yearsOfMaturityInput.setValidationRules(validationRules);
        binding.farmerPhoneNoInput.setValidationRules(validationRules);

    }

    void populate(Member member) {
        enrollment_type = member.enrollment_type;
        binding.countryInput.setInput(member.country);
        binding.farmerNameInput.setInput(member.full_name);
        binding.farmerIdNoInput.setInput(member.nat_id);
        binding.farmerPhoneNoInput.setInput(member.phone_no);
        binding.farmerLandSizeInput.setInput(member.land_size);
        binding.totalSeedlingsInput.setInput(member.total_seedlings);
        binding.donatedSeedlingsInput.setInput(member.donated_seedlings);

        if (Globals.reg_mode.equalsIgnoreCase("1")) {
            binding.farmerNameInput.setInput(null);
            binding.farmerIdNoInput.setInput(null);
            binding.farmerPhoneNoInput.setInput(null);
            binding.farmerLandSizeInput.setInput(null);
            binding.totalSeedlingsInput.setInput(null);
            binding.donatedSeedlingsInput.setInput(null);
        }
        if (Globals.reg_mode.equalsIgnoreCase("1")) {
            binding.title.setText("Farmer Pre-Registration");
        } else {
            if (enrollment_type.equalsIgnoreCase("1")) {
                binding.title.setText("Update Existing Farmer(Mobile Farmer)");
            } else {
                binding.title.setText("Update Existing Farmer(Backend Farmer)");
            }
        }

    }

    void saveTempRegistration() {
        Globals.registeringMember.country = binding.countryInput.getInput();
        Globals.registeringMember.county = binding.countyInput.getInput();
        Globals.registeringMember.location = binding.locationInput.getInput();
        Globals.registeringMember.sub_location = binding.subLocationInput.getInput();
        Globals.registeringMember.village = binding.villageInput.getInput();
        Globals.registeringMember.site = binding.siteInput.getInput();
        Globals.registeringMember.full_name = binding.farmerNameInput.getInput();
        Globals.registeringMember.nat_id = binding.farmerIdNoInput.getInput();
        Globals.registeringMember.phone_no = binding.farmerPhoneNoInput.getInput();
        Globals.registeringMember.land_size = binding.farmerLandSizeInput.getInput();
        Globals.registeringMember.total_seedlings = binding.totalSeedlingsInput.getInput();
        Globals.registeringMember.donated_seedlings = binding.donatedSeedlingsInput.getInput();
        Globals.registeringMember.seedling_type = binding.seedlingTypeInput.getInput();
        Globals.registeringMember.years_of_maturity = binding.yearsOfMaturityInput.getInput();
        Globals.registeringMember.dbh_at_maturity = binding.dbhAtMaturityInput.getInput();
        if (Globals.reg_mode.equalsIgnoreCase("1")) {
            Globals.registeringMember.sid = "temp" + Globals.registeringMember.nat_id;
        }
        Globals.setRegisteringMember(Globals.registeringMember);

    }

    void prooceed() {
        saveTempRegistration();

        if (!binding.farmerIdNoInput.validInput("nat_id", binding.farmerIdNoInput.getInput())) {
            Toast.makeText(Page1.this, "ID Number Already Exists", Toast.LENGTH_SHORT).show();
            binding.farmerIdNoInput.setInput(null);
        }
        if (!binding.farmerPhoneNoInput.validInput("phone_no", binding.farmerPhoneNoInput.getInput())) {
            Toast.makeText(Page1.this, "Phone Number Already Exists", Toast.LENGTH_SHORT).show();
            binding.farmerPhoneNoInput.setInput(null);
        }
        if (validated()) {
            Intent intent = new Intent(this, Page2.class);
            intent.putExtra("reg_mode", Globals.reg_mode);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    boolean validated() {
        return binding.countryInput.isInputValid()
                & binding.countyInput.isInputValid()
                & binding.countyInput.isInputValid()
                & binding.locationInput.isInputValid()
                & binding.subLocationInput.isInputValid()
                & binding.villageInput.isInputValid()
                & binding.siteInput.isInputValid()
                & binding.farmerNameInput.isInputValid()
                & binding.farmerIdNoInput.isInputValid()
                & binding.farmerPhoneNoInput.isInputValid()
                & binding.farmerLandSizeInput.isInputValid()
                & binding.totalSeedlingsInput.isInputValid()
                & binding.donatedSeedlingsInput.isInputValid()
                & binding.yearsOfMaturityInput.isInputValid()
                & binding.dbhAtMaturityInput.isInputValid();
    }


    @Override
    public void onBackPressed() {
        showQuitDialog();
//        super.onBackPressed();
    }

    void showQuitDialog() {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_exit_enrollment_confirmation, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        aldv.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ald.dismiss();
                finish();
            }
        });

        aldv.findViewById(R.id.dismiss).setOnClickListener(view -> ald.dismiss());
    }
}