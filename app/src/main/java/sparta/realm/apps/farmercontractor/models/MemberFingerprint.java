package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.Conversions;


@DynamicClass(table_name = "member_fingerprint")
//@SyncDescription(service_name = "Member fingerprint", upload_link = "/FarmersContract/Farmers/AddFingerPrint",service_type = SyncDescription.service_type.Upload,storage_mode_check = true)
@SyncDescription(service_name = "Member fingerprint", upload_link = "/FarmersContract/Farmers/AddMemberFingerPrint",service_type = SyncDescription.service_type.Upload,storage_mode_check = true)
public class MemberFingerprint extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "fingerprint_index")
    public String fingerprint_index;

    @DynamicProperty(json_key = "member_transaction_no")
    public String member_transaction_no;

    @DynamicProperty(json_key = "image", storage_mode = DynamicProperty.storage_mode.FilePath)
    public String image;

    @DynamicProperty(json_key = "template")
    public String template;

    @DynamicProperty(json_key = "template_format")
    public String template_format;

    @DynamicProperty(json_key = "msid")
    public String msid = "";

    @DynamicProperty(json_key = "member_id")
    public String member_id;


    public MemberFingerprint(String member_transaction_no, String image, String fingerprint_index, String member_id) {
        this.member_transaction_no = member_transaction_no;
        this.image = image;
        this.fingerprint_index = fingerprint_index;
        this.member_id = member_id;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public MemberFingerprint(String member_transaction_no, String image, String fingerprint_index) {
        this.member_transaction_no = member_transaction_no;
        this.image = image;
        this.fingerprint_index = fingerprint_index;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public MemberFingerprint() {

    }

}
