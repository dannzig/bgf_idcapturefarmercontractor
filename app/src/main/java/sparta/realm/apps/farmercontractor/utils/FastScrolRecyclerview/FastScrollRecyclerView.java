package sparta.realm.apps.farmercontractor.utils.FastScrolRecyclerview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class FastScrollRecyclerView extends RecyclerView {
    private Context ctx;

    private boolean setupThings = false;
    public static int indWidth = 25;
    public static int indHeight= 18;
    public float scaledWidth;
    public float scaledHeight;
    public String[] sections;
    public float sx;
    public float sy;
    public String section;
    public boolean showLetter = false;
    private Handler listHandler;

    public FastScrollRecyclerView(Context context) {
        super(context);
        ctx = context;
    }

    public FastScrollRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
    }

    public FastScrollRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ctx = context;
    }

    @Override
    public void onDraw(Canvas c) {
        if (!setupThings) {
            setupThings();
        }
        super.onDraw(c);
    }

    public void setupThings() {
        //create az text data
        Set<String> sectionSet;
        try{
            sectionSet = ((FastScrollRecyclerViewInterface)getAdapter()).getMapIndex().keySet();
        }catch (Exception ex){return;}
        ArrayList<String> listSection = new ArrayList<>(sectionSet);
        Collections.sort(listSection);
        sections = new String[listSection.size()];
        int i=0;
        for(String s:listSection) {
            sections[i++] = s;
        }

        scaledWidth = indWidth * ctx.getResources().getDisplayMetrics().density;
        scaledHeight= indHeight* ctx.getResources().getDisplayMetrics().density;
        sx = this.getWidth() - this.getPaddingRight() - (float)(1.2*scaledWidth);
        sy = (float)((this.getHeight() - (scaledHeight * sections.length) )/2.0);
        setupThings = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
try {
    switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN: {
            if (x < sx - scaledWidth || y < sy || y > (sy + scaledHeight * sections.length))
                return super.onTouchEvent(event);
            else {
                // We touched the index bar
                float yy = y - this.getPaddingTop() - getPaddingBottom() - sy;
                int currentPosition = (int) Math.floor(yy / scaledHeight);
                if (currentPosition < 0) currentPosition = 0;
                if (currentPosition >= sections.length) currentPosition = sections.length - 1;
                section = sections[currentPosition];
                showLetter = true;
                int positionInData = 0;
                if (((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().containsKey(section.toUpperCase()))
                    positionInData = ((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().get(section.toUpperCase());
//                    this.scrollToPosition(positionInData);


                int offset = getCenterOffset(1, 0);
                ((LinearLayoutManager) getLayoutManager()).scrollToPositionWithOffset(positionInData, offset);

                FastScrollRecyclerView.this.invalidate();
            }
            break;
        }
        case MotionEvent.ACTION_MOVE: {

            if (!showLetter && (x < sx - scaledWidth || y < sy || y > (sy + scaledHeight * sections.length)))
                return super.onTouchEvent(event);
            else {
                float yy = y - sy;
                int currentPosition = (int) Math.floor(yy / scaledHeight);
                if (currentPosition < 0) currentPosition = 0;
                if (currentPosition >= sections.length) currentPosition = sections.length - 1;
                section = sections[currentPosition];
                showLetter = true;
                int positionInData = 0;
                if (((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().containsKey(section.toUpperCase()))
                    positionInData = ((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().get(section.toUpperCase());
//                    this.scrollToPosition(positionInData);
                int offset = getCenterOffset(1, 0);
                ((LinearLayoutManager) getLayoutManager()).scrollToPositionWithOffset(positionInData, offset);
                FastScrollRecyclerView.this.invalidate();

            }
            break;

        }
        case MotionEvent.ACTION_UP: {
            listHandler = new ListHandler();
            listHandler.sendEmptyMessageDelayed(0, 100);
            if (x < sx - scaledWidth || y < sy || y > (sy + scaledHeight * sections.length))
                return super.onTouchEvent(event);
            else
                return true;
        }
    }
}catch (Exception ex){}

        return true;
    }

    private int mFallbackCenterOffset;
    private int getCenterOffset(int orientation, int childPosition) {
        View child = getChildAt(childPosition);
        if (child == null) {
            return mFallbackCenterOffset;
        }

        final Rect r = new Rect();
        if (getGlobalVisibleRect(r)) {
            if (orientation == OrientationHelper.HORIZONTAL) {
                mFallbackCenterOffset = r.width() / 2 - child.getWidth() / 2;
            } else {
                mFallbackCenterOffset = r.height() / 2 - child.getHeight() / 2;
            }
        } else {
            if (orientation == OrientationHelper.HORIZONTAL) {
                mFallbackCenterOffset = getWidth() / 2 - child.getWidth() / 2;
            } else {
                mFallbackCenterOffset = getHeight() / 2 - child.getHeight() / 2;
            }
        }

        return mFallbackCenterOffset;
    }


    private static class CenterSmoothScroller extends LinearSmoothScroller {

        CenterSmoothScroller(Context context) {
            super(context);
        }

        @Override
        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
            return (boxStart + (boxEnd - boxStart) / 2) - (viewStart + (viewEnd - viewStart) / 2);
        }
    }
    private class ListHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            showLetter = false;
            FastScrollRecyclerView.this.invalidate();
        }


    }
}