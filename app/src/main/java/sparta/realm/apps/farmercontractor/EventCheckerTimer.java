package sparta.realm.apps.farmercontractor;

import android.app.ActivityManager;
import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;


public class EventCheckerTimer {
    Context context;

    public EventCheckerTimer(Context context) {
        this.context = context;
    }

    void initChecks() {
        int seconds_interval = 10;
        Timer service_timer = new Timer();
        service_timer.schedule(new TimerTask() {
            @Override
            public void run() {
//                Boolean service_running = isServiceRunning(context, HTTPCameraServer.class);
//
//                Log.e("Checking service", "" + service_running);
//                if (!service_running) {
//                    Intent serviceIntent = new Intent(context, HTTPCameraServer.class);
//                    context.startService(serviceIntent);
//                    context.bindService(serviceIntent, new ServiceConnection() {
//                        @Override
//                        public void onServiceConnected(ComponentName name, IBinder service) {
//                            //retrieve an instance of the service here from the IBinder returned
//                            //from the onBind method to communicate with
//                        }
//
//                        @Override
//                        public void onServiceDisconnected(ComponentName name) {
//                        }
//                    }, Context.BIND_AUTO_CREATE);
//                }

            }
        }, 1000, 1000 * seconds_interval);

    }

    public static boolean isServiceRunning(Context act, Class<?> serviceClass) {
//        Class<?> serviceClass=App_updates.class;

        ActivityManager manager;
        manager = (ActivityManager) act.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
