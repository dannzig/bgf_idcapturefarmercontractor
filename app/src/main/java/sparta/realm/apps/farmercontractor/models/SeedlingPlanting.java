package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "Seedling_planting")

@SyncDescription( service_name = "Seedling Planting",chunk_size = 10000, download_link = "/FarmersContract/Group/SeedlingPlantList", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result",service_type = SyncDescription.service_type.Download)
@SyncDescription(service_name = "Seedling Planting", upload_link = "/FarmersContract/Group/AddSeedlingPlanting",service_type = SyncDescription.service_type.Upload)

public class SeedlingPlanting extends RealmModel implements Serializable {

//    {
//        "group_id":"1",
//            "member_id":"2",
//            "transaction_no":"001",
//            "country_id":"1",
//            "county_id":"1",
//            "location_id":"1",
//            "sub_location_id":"1",
//            "village_id":"1",
//            "site_id":"1",
//            "seedling_received":"200",
//            "seeling_planted":"200",
//            "date_of_planting":"2024-02-25",
//            "user_id":"1",
//            "date":"2024-02-25",
//            "agroforest_agent_id":"2"
//    }

    @DynamicProperty(json_key = "group_id")
    public String group_id;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "country_id")
    public String country;

    @DynamicProperty(json_key = "county_id")
    public String county;

    @DynamicProperty(json_key = "location_id")
    public String location;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location;

    @DynamicProperty(json_key = "village_id")
    public String village;

    @DynamicProperty(json_key = "site_id")
    public String site;

    @DynamicProperty(json_key = "date_of_planting")
    public String date_of_planting;

    @DynamicProperty(json_key = "seedling_received")
    public String seedling_received;

    @DynamicProperty(json_key = "seeling_planted")
    public String seeling_planted;

    @DynamicProperty(json_key = "agroforest_agent_id")
    public String agroforest_agent_id;


    public SeedlingPlanting() {

    }

    public SeedlingPlanting(String group_id, String member_id, String country, String county, String location, String sub_location, String village, String site, String seedling_received, String seeling_planted, String agroforest_agent_id, String date_of_planting) {
        this.group_id = group_id;
        this.member_id = member_id;
        this.country = country;
        this.county = county;
        this.location = location;
        this.sub_location = sub_location;
        this.village = village;
        this.site = site;
        this.seedling_received = seedling_received;
        this.seeling_planted = seeling_planted;
        this.agroforest_agent_id = agroforest_agent_id;
        this.date_of_planting = date_of_planting;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }
}
