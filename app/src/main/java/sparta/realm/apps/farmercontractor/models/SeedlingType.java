package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "seedling_type")
@SyncDescription(service_id = "1",chunk_size = 10000, service_name = "Seedling type", download_link = "/Configurations/Item/RebindGrid", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class SeedlingType extends SelectionData implements Serializable {


    @DynamicProperty(json_key = "item_name")
    public String item_name;

    @DynamicProperty(json_key = "year_of_maturity")
    public String year_of_maturity;


  @DynamicProperty(json_key = "dbh")
    public String dbh;


  @DynamicProperty(json_key = "country_id")
    public String country;








    public SeedlingType() {


    }

}
