package sparta.realm.apps.farmercontractor.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.realm.annotations.sync_status;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.Calendar;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.adapters.MemberAdapter;
import sparta.realm.apps.farmercontractor.adapters.SelectedMemberAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupCreationBinding;
import sparta.realm.apps.farmercontractor.models.AgroForestAgent;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.FastScrolRecyclerview.FastScrollRecyclerViewItemDecoration;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.apps.farmercontractor.utils.Pager;
import sparta.realm.apps.farmercontractor.utils.SnapHelper;
import sparta.realm.apps.farmercontractor.utils.printing.Printer;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartautils.s_bitmap_handler;
import sparta.realm.utils.Conversions;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

public class GroupManagement extends AppCompatActivity {

    ActivityGroupCreationBinding binding;

    ArrayList<Member> members;
    ArrayList<Member> selectedMembers;
    ArrayList<Member> allSelectedMembers;
    Pager<Member> pager;
    Pager<Member> selectedPager;
    MemberGroup memberGroup;
    Boolean creationMode = true;
    String trainedOnGroups = "no";
    String trainedOnGroupFormation = "no";
    Boolean newly_formed = false;
    Boolean organized_group = false;
    Boolean ready_for_certification = false;
    Boolean with_certification = false;
    Boolean groupHasRep = false;
    String countryInput;
    String groupStatus;
    GroupParticipant roleGroupParticipant;
    private long mLastClickTime;
    private int mSecretNumber = 0;
    private static final long CLICK_INTERVAL = 600;
    private int clickNum = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGroupCreationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Globals.reg_mode = getIntent().getStringExtra("reg_mode");
        Globals.registeringMemberGroup = new MemberGroup(Globals.reg_mode);
        String member_group_transaction_no = getIntent().getStringExtra("transaction_no");

        Log.e("GroupManagement", "onCreate: Newly Formed" + newly_formed);

        memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(getIntent().getStringExtra("transaction_no") == null || getIntent().getStringExtra("transaction_no").length() < 1 ? "sid='" + getIntent().getStringExtra("sid") + "'" : "transaction_no='" + getIntent().getStringExtra("transaction_no") + "'"));
//        memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
        creationMode = memberGroup == null;

        if (Globals.reg_mode.equalsIgnoreCase("2"))//Editing
        {

            String group_id = getIntent().getStringExtra("sid");
            Log.e("Page1", "OnCreate: Member_id:  " + group_id);

            if (group_id == "0") {
                Toast.makeText(this, "Duplicate Record Click Other Record", Toast.LENGTH_SHORT).show();
                return;
            }
            if (group_id == null || group_id.isEmpty()) {
                if (member_group_transaction_no == null || member_group_transaction_no.isEmpty()) {

                    Toast.makeText(this, "Member Transaction Number is Null", Toast.LENGTH_SHORT).show();
                    finish();
                } else {

                    Globals.registeringMemberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("transaction_no='" + member_group_transaction_no + "'"));
                    Globals.registeringMemberGroup.transaction_no = member_group_transaction_no;

                }

            } else {
                Globals.registeringMemberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid='" + group_id + "'"));
                Globals.registeringMemberGroup.transaction_no = member_group_transaction_no;

            }
            if (Globals.registeringMemberGroup == null) {
                finish();
                return;

            }
            Globals.setRegisteringMemberGroup(Globals.registeringMemberGroup);

            Globals.registeringMemberGroup.sid = group_id;
        } else {

            Globals.registeringMemberGroup.sid = null;
        }

        if (Globals.registeringMemberGroup() == null) {
            Globals.registeringMemberGroup.transaction_no = checkTransactionNumber();
            Globals.setRegisteringMemberGroup(Globals.registeringMemberGroup);
        } else {
            Globals.registeringMemberGroup = Globals.registeringMemberGroup();
            if (Globals.reg_mode.equalsIgnoreCase("1")) {

                Globals.registeringMemberGroup.transaction_no = checkTransactionNumber();
            }
        }

//        if (creationMode){
//            memberGroup.transaction_no =
//        }

        Globals.registeringMemberGroup.enrollment_type = Globals.reg_mode;
        Globals.registeringMemberGroup.sync_status = "" + sync_status.pending.ordinal();
        Globals.registeringMemberGroup.reg_start_time = System.currentTimeMillis() + "";


        initUi();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("GroupRecords", "OnResume: HERE HERE");
        //initUi();
    }

    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

            countryInput = input;
            binding.include2.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.include2.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);
            binding.include2.aa.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query().setTableFilters("country='" + input + "'")), agroforestAgentInputListener);

            //binding.include2.aa.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query()), agroforestAgentInputListener);
            //Log.e("Page1", "Registered Member, County" + Globals.registeringMember().county);
            //Log.e("Page1", "Registered Member, Ward" + Globals.registeringMember().location);
            //Log.e("Page1", "Registered Member, Sub-Location" + Globals.registeringMember().sub_location);
            //Log.e("Page1", "Registered Member, Village" + Globals.registeringMember().village);
            //Log.e("Page1", "Registered Member, Site" + Globals.registeringMember().site);
            //Log.e("Page1", "Registered Member, Seedling Type" + Globals.registeringMember().seedling_type);

            binding.include2.countyInput.setInput(Globals.registeringMemberGroup().county);
            binding.include2.siteInput.setInput(Globals.registeringMemberGroup().site);

            //Changing Administrative titles for each Country
            if (input.equals("110")) {
                binding.include2.countyInput.setTitle("County");
                binding.include2.locationInput.setTitle("Ward");
                binding.include2.subLocationInput.setTitle("Sub-Location");
            }
            if (input.equals("222")) {
                binding.include2.countyInput.setTitle("District");
                binding.include2.locationInput.setTitle("Sub-County");
                binding.include2.subLocationInput.setTitle("Parish");
            }

            selectedPager.setTableFilters(new String[]{"country='" + input + "'", "sid in(select member_id from group_participant where " + (Globals.registeringMemberGroup.transaction_no == null || Globals.registeringMemberGroup.transaction_no.length() < 1 ? " group_id='" + Globals.registeringMemberGroup.sid + "')" : " group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "' and is_active = 'true') ")});
            //pager.setTableFilters(new String[]{"country='" + input + "'","sid not in(select member_id from group_participant where " + (Globals.registeringMemberGroup.transaction_no == null || Globals.registeringMemberGroup.transaction_no.length() < 1 ? " group_id='" + Globals.registeringMemberGroup.sid + "')" : " group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "')")});

            pager.setTableFilters(new String[]{"country='" + input + "'", "sid not in(SELECT DISTINCT member_id FROM group_participant where is_active = 'true')"});

        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
            binding.include2.locationInput.setInput(Globals.registeringMemberGroup().location);

        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
            binding.include2.subLocationInput.setInput(Globals.registeringMemberGroup().sub_location);

        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
            binding.include2.villageInput.setInput(Globals.registeringMemberGroup().village);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("county='" + input + "'")), siteInputListener);
//            binding.siteInput.setInput(Globals.registeringMemberGroup().site);
        }
    };

    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

        }
    };
    //    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
//        @Override
//        public void onInputAvailable(boolean valid, String input) {
//
//
//        }
//    };
    SearchSpinner.InputListener agroforestAgentInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };
    SearchSpinner.InputListener groupStageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };

    void initUi() {
        setupToolbar(binding.include.toolbar);
        members = new ArrayList<>();

        binding.memberList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.memberList.setLayoutManager(linearLayoutManager);
        SnapHelper startSnapHelper = new SnapHelper();
        startSnapHelper.attachToRecyclerView(binding.memberList);

        binding.selectedMemberList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        binding.selectedMemberList.setLayoutManager(linearLayoutManager2);
        SnapHelper startSnapHelper2 = new SnapHelper();
        startSnapHelper2.attachToRecyclerView(binding.selectedMemberList);

//        if (Globals.reg_mode.equalsIgnoreCase("1")) {
//            binding.selectedGroupMembersInfo.setVisibility(View.GONE);
//            binding.SelectedGroupMembersTitle.setVisibility(View.GONE);
//            binding.selectedMemberList.setVisibility(View.GONE);
//        }

        binding.include2.country.setDataset(Realm.databaseManager.loadObjectArray(Country.class, new Query()), countryInputListener);


        //binding.include2.groupStage.setDataset(Realm.databaseManager.loadObjectArray(GroupStages.class, new Query()), groupStageInputListener);


        selectedPager = new Pager(Member.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                members = (ArrayList<Member>) data;
                binding.noRecordsLay.setVisibility((members.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.selectedMemberList.setAdapter(new MemberAdapter(members, new MemberAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(Member mem, View view) {

                        showDeactivateDialog(mem);

//                        long curTime = System.currentTimeMillis();
//                        long durTime = curTime - mLastClickTime;
//                        mLastClickTime = curTime;
//
//                        Log.e("GroupManagement", "OnItemClick: HERE HERE 1, dur: " + durTime);
//
//                        if (durTime < CLICK_INTERVAL) {
//                            ++mSecretNumber;
//                            Log.e("GroupManagement", "OnItemClick: HERE HERE 2, secNum: " + mSecretNumber);
//
//                            if (mSecretNumber == 1) {
//                                showDeactivateDialog(mem);
//                            }else{
//                                mSecretNumber = 0;
//                            }
//                        } else {
//
//                            if (Globals.groupRepChair) {
//                                changeRoleDialog(mem);
//                            } else if (Globals.groupRepTreas) {
//                                changeRoleDialog(mem);
//                            } else if (Globals.groupRepSec) {
//                                changeRoleDialog(mem);
//                            } else {
//                                selectRoleDialog(mem);
//                            }
//                        }
//                        if (!creationMode) {
//
//                            return;
//                        }
//                        if (groupHasRep){
//                            selectRoleDialog(mem);
//                        }

                    }
                }));
                binding.selectedMemberList.setupThings();
                FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(GroupManagement.this);
                binding.selectedMemberList.addItemDecoration(decoration);
                binding.selectedMemberList.setItemAnimator(new DefaultItemAnimator());
                binding.selectedMemberList.invalidate();
            }
        }, binding.searchText, binding.prev, binding.next, binding.positionIndicator, binding.loadingBar, null, new String[0], null, "full_name");

        pager = new Pager(Member.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                members = (ArrayList<Member>) data;
                binding.noRecordsLay.setVisibility((members.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.memberList.setAdapter(new MemberAdapter(members, new MemberAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(Member mem, View view) {
//                        if (!creationMode) {
//
//                            return;
//                        }


                        //select country first for filtering
                        if (countryInput == null) {
                            Toast.makeText(GroupManagement.this, "SELECT COUNTRY FIRST", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //if (!groupHasRep && !Globals.hasGroupRep) {
                        if (!Globals.groupRepChair || !Globals.groupRepTreas || !Globals.groupRepSec) {
                            selectRoleDialog(mem);
                        } else {
                            if (!allSelectedMembers.contains(mem)) {
                                selectedMembers.add(0, mem);
                                allSelectedMembers.add(mem);
                                binding.selectionList.getAdapter().notifyDataSetChanged();
                                binding.include2.memberCount.setText(selectedMembers.size() + " members");

                            }
//                            if (!selectedMembers.contains(mem)) {
//                                selectedMembers.add(0, mem);
//                                binding.selectionList.getAdapter().notifyDataSetChanged();
//                                binding.include2.memberCount.setText(selectedMembers.size() + " members");
//
//                            }
                        }

                    }
                }));
                binding.memberList.setupThings();
                FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(GroupManagement.this);
                binding.memberList.addItemDecoration(decoration);
                binding.memberList.setItemAnimator(new DefaultItemAnimator());
                binding.memberList.invalidate();
            }
        }, binding.searchText, binding.prev, binding.next, binding.positionIndicator, binding.loadingBar, null, new String[0], null, "full_name");

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);

        binding.selectionList.setLayoutManager(flowLayoutManager);
        selectedMembers = new ArrayList<>();
        allSelectedMembers = new ArrayList<>();
        binding.selectionList.setAdapter(new SelectedMemberAdapter(selectedMembers,
                (mem, view) -> {

                    if (mem.group_rep.equalsIgnoreCase("1")) {
                        Globals.groupRepChair = false;
                    }
                    if (mem.group_rep.equalsIgnoreCase("2")) {
                        Globals.groupRepTreas = false;
                    }
                    if (mem.group_rep.equalsIgnoreCase("3")) {
                        Globals.groupRepSec = false;
                    }
                    selectedMembers.remove(mem);
                    allSelectedMembers.remove(mem);
                    binding.selectionList.getAdapter().notifyDataSetChanged();
                    binding.include2.memberCount.setText(selectedMembers.size() + " members");


                }));
        binding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (recordSaved) {
                    print();
                } else {
                    showSaveDialog();
                }

//                if (creationMode) {
//                } else {
//                    print();
//                }
            }
        });
        binding.include2.groupsTrained.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    trainedOnGroups = "yes";
                    Log.e("GroupManagement", "onCheckedChanged: Checked1" + trainedOnGroups);
                }
            }
        });
        binding.include2.groupsTrainedFormation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    trainedOnGroupFormation = "yes";
                    Log.e("GroupManagement", "onCheckedChanged: Checked2" + trainedOnGroupFormation);
                }
            }
        });

        binding.include2.newlyFormed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    newly_formed = true;
                }
            }
        });
        binding.include2.organizedGroups.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    organized_group = true;
                }
            }
        });
        binding.include2.readyForCertification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ready_for_certification = true;
                }
            }
        });
        binding.include2.withCertification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    with_certification = true;

                }
            }
        });

        binding.include2.activateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeStatusDialog();
            }
        });

        if (!creationMode) {
            //binding.create.setText("Print");
            binding.create.setText("SAVE DETAILS");

            //pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where group_id='"+memberGroup.sid+"')"});
            //pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where group_id='" + memberGroup.sid + "')"});
            //selectedPager.setTableFilters(new String[]{"sid in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "')")});
            //pager.setTableFilters(new String[]{"sid not in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "')")});
            //binding.searchText.setVisibility(View.GONE);
            //binding.prev.setVisibility(View.GONE);
            //binding.next.setVisibility(View.GONE);
            //binding.positionIndicator.setVisibility(View.GONE);
            //binding.groupInfoInfo.setText("Below is the information about this group\n");
            //binding.groupMembersInfo.setText("Below are the list of members in this group\n");
//            binding.include2.name.setBackground(null);
//            binding.include2.info1.setBackground(null);
            binding.include2.memberCount.setBackground(null);

            //binding.include2.cr.setBackground(null);
            //binding.include2.aa.setBackground(null);
            //binding.include2.country.setBackground(null);
            //binding.include2.groupsTrained.setBackground(null);
            //binding.include2.groupsTrainedFormation.setBackground(null);
            //binding.include2.groupStage.setBackground(null);
            //binding.include2.name.setFocusable(false);
            //binding.include2.info1.setFocusable(false);
            //binding.include2.cr.setFocusable(false);
            //binding.include2.country.setEnabled(false);
            //binding.include2.country.setClickable(false);
            //binding.include2.groupsTrained.setEnabled(false);
            //binding.include2.groupsTrainedFormation.setEnabled(false);

            binding.include2.name.setText(memberGroup.name);
            binding.include2.info1.setText(memberGroup.description);


            if (memberGroup.group_training.equalsIgnoreCase("yes")) {
                binding.include2.groupsTrained.setChecked(true);
            }
            if (memberGroup.triaining_group_formation.equalsIgnoreCase("yes")) {
                binding.include2.groupsTrainedFormation.setChecked(true);
            }

            if (memberGroup.newly_formed != null) {
                if (memberGroup.newly_formed.equalsIgnoreCase("true")) {
                    binding.include2.newlyFormed.setChecked(true);
                }
            }
            if (memberGroup.organized_group != null) {
                if (memberGroup.organized_group.equalsIgnoreCase("true")) {
                    binding.include2.organizedGroups.setChecked(true);
                }
            }
            if (memberGroup.ready_for_certification != null) {
                if (memberGroup.ready_for_certification.equalsIgnoreCase("true")) {
                    binding.include2.readyForCertification.setChecked(true);
                }
            }
            if (memberGroup.with_certification != null) {
                if (memberGroup.with_certification.equalsIgnoreCase("true")) {
                    binding.include2.withCertification.setChecked(true);
                }
            }

            if (memberGroup.data_status != null) {
                if (memberGroup.data_status.equalsIgnoreCase("true")) {
                    binding.include2.activateBtn.setText("Deactivate Group");
                }
            }


            //binding.include2.groupStage.setInput(memberGroup.group_stages_id);
            binding.include2.country.setInput(memberGroup.country_id);
            binding.include2.countyInput.setInput(memberGroup.county);
            binding.include2.locationInput.setInput(memberGroup.locationId);
            binding.include2.subLocationInput.setInput(memberGroup.sub_location);
            binding.include2.villageInput.setInput(memberGroup.village);
            binding.include2.siteInput.setInput(memberGroup.site);
            binding.include2.aa.setInput(memberGroup.agroforest_agent_id);

//            binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, new Query().setTableFilters("group_transaction_no='" + memberGroup.transaction_no + "'"))+" members");
            binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? "group_id='" + memberGroup.sid + "'" :  "is_active = 'true' AND "+"group_transaction_no='" + memberGroup.transaction_no + "' OR group_id='" + memberGroup.sid + "'" ) + " members");
            //binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, new Query().setTableFilters(new String[]{"sid in(select member_id from group_participant where " + (Globals.registeringMemberGroup.transaction_no == null || Globals.registeringMemberGroup.transaction_no.length() < 1 ? " group_id='" + Globals.registeringMemberGroup.sid + "')" : " group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "' and is_active = 'true') ")})) + " members");

//    binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class,new Query().setTableFilters("group_id='"+memberGroup.sid+"'")));
        } else {
            binding.selectedGroupMembersInfo.setVisibility(View.GONE);
            binding.SelectedGroupMembersTitle.setVisibility(View.GONE);
            binding.selectedMemberList.setVisibility(View.GONE);
            binding.include2.memberCount.setText("0 members");

        }
    }

    boolean recordSaved = false;

    String checkTransactionNumber() {
        String transaction_number = Globals.getTransactionNo();

        while (Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("transaction_no='" + transaction_number + "'")) != null) {
            transaction_number = Globals.getTransactionNo();
        }
        return transaction_number;

    }

    void selectRoleDialog(Member mem) {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_member_group_role, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView title = aldv.findViewById(R.id.sub_title);
        title.setText("Select Group Role");
        Button groupChair = aldv.findViewById(R.id.rep_chairman);
        Button groupTreasurer = aldv.findViewById(R.id.rep_treasurer);
        Button groupSecretary = aldv.findViewById(R.id.rep_secretary);
        Button groupMember = aldv.findViewById(R.id.member);
        Button revokeRole = aldv.findViewById(R.id.rep_revoke_role);

        revokeRole.setVisibility(View.GONE);
        if (Globals.groupRepChair) {
            groupChair.setVisibility(View.GONE);
        }
        if (Globals.groupRepTreas) {
            groupTreasurer.setVisibility(View.GONE);
        }
        if (Globals.groupRepSec) {
            groupSecretary.setVisibility(View.GONE);
        }
        //Log.e("GroupManagement", "selectRoleDialog: Selected Array:  " + Arrays.selectedMembers Arrays.asList(selectedMembers).toArray());// Arrays.toString(selectedMembers.toArray()));
        groupChair.setOnClickListener(view -> {

            if (!allSelectedMembers.contains(mem)) {
                Member assignedMember = new Member(mem.sid, "1", mem.full_name);
                selectedMembers.add(0, assignedMember);
                allSelectedMembers.add(mem);
                binding.selectionList.getAdapter().notifyDataSetChanged();
                binding.include2.memberCount.setText(selectedMembers.size() + " members");
                groupHasRep = true;
                Globals.groupRepChair = true;

            }

            ald.dismiss();
        });

        groupTreasurer.setOnClickListener(View -> {
            if (!allSelectedMembers.contains(mem)) {
                Member assignedMember = new Member(mem.sid, "2", mem.full_name);
                selectedMembers.add(0, assignedMember);
                allSelectedMembers.add(mem);
                binding.selectionList.getAdapter().notifyDataSetChanged();
                binding.include2.memberCount.setText(selectedMembers.size() + " members");
                groupHasRep = true;
                Globals.groupRepTreas = true;

            }

            ald.dismiss();
        });
        groupSecretary.setOnClickListener(View -> {
            if (!allSelectedMembers.contains(mem)) {
                Member assignedMember = new Member(mem.sid, "3", mem.full_name);
                selectedMembers.add(0, assignedMember);
                allSelectedMembers.add(mem);
                binding.selectionList.getAdapter().notifyDataSetChanged();
                binding.include2.memberCount.setText(selectedMembers.size() + " members");
                groupHasRep = true;
                Globals.groupRepSec = true;

            }

            ald.dismiss();

        });
        groupMember.setOnClickListener(view -> {

            if (!allSelectedMembers.contains(mem)) {
                selectedMembers.add(0, mem);
                allSelectedMembers.add(mem);
                binding.selectionList.getAdapter().notifyDataSetChanged();
                binding.include2.memberCount.setText(selectedMembers.size() + " members");

            }

            ald.dismiss();
        });


    }

    void changeRoleDialog(Member mem) {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_member_group_role, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView title = aldv.findViewById(R.id.sub_title);
        title.setText("Change Group Role");

        Button groupChair = aldv.findViewById(R.id.rep_chairman);
        Button groupTreasurer = aldv.findViewById(R.id.rep_treasurer);
        Button groupSecretary = aldv.findViewById(R.id.rep_secretary);
        Button groupMember = aldv.findViewById(R.id.member);
        Button revokeRole = aldv.findViewById(R.id.rep_revoke_role);

        groupMember.setVisibility(View.GONE);


        if (Globals.groupRepChair) {
            groupChair.setVisibility(View.GONE);
        }
        if (Globals.groupRepTreas) {
            groupTreasurer.setVisibility(View.GONE);
        }
        if (Globals.groupRepSec) {
            groupSecretary.setVisibility(View.GONE);
        }

        if (Globals.registeringMemberGroup != null) {
            roleGroupParticipant = Realm.databaseManager.loadObject(GroupParticipant.class, new Query().setTableFilters(new String[]{"member_id='" + mem.sid + "'", "group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "'"}));
        }
        if (roleGroupParticipant == null) return;
        Log.e("GroupManagement", "changeRoleDialog: GroupRole: " + roleGroupParticipant.group_rep);
        if (!roleGroupParticipant.group_rep.equalsIgnoreCase("1") &&
                !roleGroupParticipant.group_rep.equalsIgnoreCase("2") &&
                !roleGroupParticipant.group_rep.equalsIgnoreCase("3")) {

            revokeRole.setVisibility(View.GONE);
            Toast.makeText(this, "Member Has no role yet", Toast.LENGTH_SHORT).show();

        }
        if (Globals.groupRepChair && Globals.groupRepTreas && Globals.groupRepSec) {
            title.setText("No unassigned role to assign");
        }
        revokeRole.setOnClickListener(view -> {

            if (roleGroupParticipant != null) {
                Log.e("MemberAdapter", "onBindViewHolder: Group Rep: " + roleGroupParticipant.group_rep.toString());

                if (roleGroupParticipant.group_rep.equalsIgnoreCase("1")) {
                    // Edit role number.
                    Member assignedMember = new Member(mem.sid, "0", mem.full_name);
                    GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,"true");
                    DatabaseManager.database.execSQL("update group_participant set group_rep ='0', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
                    SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

                    //Realm.databaseManager.insertObject(group_participant);
                    // Update table GroupParticipants.
                    //DatabaseManager.database.execSQL("update group_participant set group_rep ='" + group_participant.group_rep + "' where transaction_no='" + group_participant.transaction_no + "'");

                    Globals.groupRepChair = false;
                    Globals.hasGroupRep = false;
                } else if (roleGroupParticipant.group_rep.equalsIgnoreCase("2")) {
                    // Edit role number.
                    Member assignedMember = new Member(mem.sid, "0", mem.full_name);

                    GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,"true");
                    DatabaseManager.database.execSQL("update group_participant set group_rep ='0', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
                    SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

                    //Realm.databaseManager.insertObject(group_participant);
                    // Update table GroupParticipants.
                    //DatabaseManager.database.execSQL("update group_participant set group_rep ='" + group_participant.group_rep + "' where transaction_no='" + group_participant.transaction_no + "'");

                    Globals.groupRepTreas = false;
                    Globals.hasGroupRep = false;
                } else if (roleGroupParticipant.group_rep.equalsIgnoreCase("3")) {
                    //holder.memberItem.setBackgroundColor(R.color.blue);
                    // Edit role number.
                    Member assignedMember = new Member(mem.sid, "0", mem.full_name);

                    GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,"true");
                    DatabaseManager.database.execSQL("update group_participant set group_rep ='0', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
                    SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

                    //Realm.databaseManager.insertObject(group_participant);
                    // Update table GroupParticipants.
                    //DatabaseManager.database.execSQL("update group_participant set group_rep ='" + group_participant.group_rep + "' where transaction_no='" + group_participant.transaction_no + "'");

                    Globals.groupRepSec = false;
                    Globals.hasGroupRep = false;
                }
            }

            ald.dismiss();
            finish();
            Intent intent = new Intent(GroupManagement.this, GroupRecords.class);
            startActivity(intent);
        });
        groupChair.setOnClickListener(view -> {

            Member assignedMember = new Member(mem.sid, "1", mem.full_name);
            GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,"true");
            DatabaseManager.database.execSQL("update group_participant set group_rep ='1', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
            SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

            //Realm.databaseManager.insertObject(group_participant);
            // Update table GroupParticipants.
            //DatabaseManager.database.execSQL("update group_participant set group_rep ='" + group_participant.group_rep + "' where transaction_no='" + group_participant.transaction_no + "'");

            Globals.groupRepChair = true;
            Globals.hasGroupRep = true;

            ald.dismiss();
            finish();
            Intent intent = new Intent(GroupManagement.this, GroupRecords.class);
            startActivity(intent);
        });

        groupTreasurer.setOnClickListener(View -> {
            Member assignedMember = new Member(mem.sid, "2", mem.full_name);
            GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,"true");
            DatabaseManager.database.execSQL("update group_participant set group_rep ='2', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
            SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

            //Realm.databaseManager.insertObject(group_participant);
            //Update table GroupParticipants.
            //DatabaseManager.database.execSQL("update group_participant set group_rep ='" + group_participant.group_rep + "' where transaction_no='" + group_participant.transaction_no + "'");

            Globals.groupRepTreas = true;
            Globals.hasGroupRep = true;

            ald.dismiss();
            finish();
            Intent intent = new Intent(GroupManagement.this, GroupRecords.class);
            startActivity(intent);

        });
        groupSecretary.setOnClickListener(View -> {
            Member assignedMember = new Member(mem.sid, "3", mem.full_name);
            GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,"true");
            DatabaseManager.database.execSQL("update group_participant set group_rep ='3', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
            SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

            //Realm.databaseManager.insertObject(group_participant);
            // Update table GroupParticipants.
            //DatabaseManager.database.execSQL("update group_participant set group_rep ='" + group_participant.group_rep + "' where transaction_no='" + group_participant.transaction_no + "'");

            Globals.groupRepSec = true;
            Globals.hasGroupRep = true;

            ald.dismiss();
            finish();
            Intent intent = new Intent(GroupManagement.this, GroupRecords.class);
            startActivity(intent);
        });
        //Log.e("GroupManagement", "selectRoleDialog: Selected Array:  " + Arrays.selectedMembers Arrays.asList(selectedMembers).toArray());// Arrays.toString(selectedMembers.toArray()));

    }

    void showDeactivateDialog(Member mem) {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_group_modules, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView title = aldv.findViewById(R.id.sub_title);
        Button groupDetails = aldv.findViewById(R.id.group_details);
        Button groupReport = aldv.findViewById(R.id.group_comp_report);
        title.setText("Deactivate Participant || Assign Role");
        groupDetails.setText("Deactivate");
        groupReport.setText("Roles");

        if (Globals.registeringMemberGroup != null) {
            roleGroupParticipant = Realm.databaseManager.loadObject(GroupParticipant.class, new Query().setTableFilters(new String[]{"member_id='" + mem.sid + "'", "group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "'"}));
        }


        groupDetails.setOnClickListener(view -> {
            confirmDeactivation(mem);

            ald.dismiss();
        });


        groupReport.setOnClickListener(view -> {

            if (Globals.groupRepChair) {
                changeRoleDialog(mem);
            } else if (Globals.groupRepTreas) {
                changeRoleDialog(mem);
            } else if (Globals.groupRepSec) {
                changeRoleDialog(mem);
            } else {
                selectRoleDialog(mem);
            }

            ald.dismiss();
        });


    }

    void confirmDeactivation(Member mem) {
        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_group_modules, null);
        final AlertDialog ald = new AlertDialog.Builder(this).setView(aldv).show();

        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView title = aldv.findViewById(R.id.sub_title);
        Button groupDetails = aldv.findViewById(R.id.group_details);
        Button groupReport = aldv.findViewById(R.id.group_comp_report);

        if (Globals.registeringMemberGroup != null) {
            roleGroupParticipant = Realm.databaseManager.loadObject(GroupParticipant.class, new Query().setTableFilters(new String[]{"member_id='" + mem.sid + "'", "group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "'"}));
        }
        title.setText("Deactivate this  Group member.");
        groupDetails.setText("OK");
        groupReport.setText("Cancel");

        groupDetails.setOnClickListener(view -> {
            //Deactivate the member.
            Member assignedMember = new Member(mem.sid, roleGroupParticipant.group_rep, mem.full_name);
            GroupParticipant group_participant = new GroupParticipant(memberGroup.transaction_no, assignedMember.sid, assignedMember.group_rep == null || assignedMember.group_rep == "" ? "0" : assignedMember.group_rep, roleGroupParticipant.transaction_no,roleGroupParticipant.sid, "false");

            Log.e("GroupManagement", "confirmDeactivation: DETAILS" + group_participant.group_rep +" || "+ group_participant.sid );
            DatabaseManager.database.execSQL("update group_participant set is_active ='false', sync_status = '0' where transaction_no='" + group_participant.transaction_no + "'");
            SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));

            //Realm.databaseManager.insertObject(group_participant);
            //Update table GroupParticipants.
            //This is not updating the status.
            //DatabaseManager.database.execSQL("update group_participant set data_status ='false' where transaction_no='" + group_participant.transaction_no + "'");

            Globals.groupRepChair = true;
            Globals.hasGroupRep = true;

            ald.dismiss();
            finish();
            Intent intent = new Intent(GroupManagement.this, GroupRecords.class);
            startActivity(intent);
        });

        groupReport.setOnClickListener(view -> {
            ald.dismiss();
        });


    }

    void showSaveDialog() {

        if (validated()) {

//            memberGroup = new MemberGroup(binding.include2.name.getText().toString(), binding.include2.info1.getText().toString(),"Not set",
//                    "",trainedOnGroups,
//                    trainedOnGroupFormation, binding.include2.cr.getText().toString(),
//                    binding.include2.aa.getInput(), binding.include2.groupStage.getInput(), binding.include2.country.getInput());

//            Globals.registeringMemberGroup.description =binding.include2.info1.getText().toString();
//            Globals.registeringMemberGroup.name = binding.include2.name.getText().toString();
//            Globals.registeringMemberGroup.group_training = trainedOnGroups;
//            Globals.registeringMemberGroup.triaining_group_formation = trainedOnGroupFormation;
//            Globals.registeringMemberGroup.CR = binding.include2.cr.getText().toString();
//            Globals.registeringMemberGroup.agroforest_agent_id = binding.include2.aa.getInput();
//            Globals.registeringMemberGroup.group_stages_id = binding.include2.groupStage.getInput();
//            Globals.registeringMemberGroup.country_id = binding.include2.country.getInput();
//            Globals.registeringMemberGroup.county = binding.include2.countyInput.getInput();
//            Globals.registeringMemberGroup.locationId = binding.include2.locationInput.getInput();
//            Globals.registeringMemberGroup.sub_location = binding.include2.subLocationInput.getInput();
//            Globals.registeringMemberGroup.village = binding.include2.villageInput.getInput();
//            Globals.registeringMemberGroup.site = binding.include2.siteInput.getInput();

            //                DatabaseManager.database.execSQL("UPDATE member_group SET transaction_no = '"+Globals.registeringMemberGroup.transaction_no+"', description = '"+binding.include2.info1.getText().toString()+"'," +
//                        "name='"+binding.include2.name.getText().toString()+"', group_training = '"+trainedOnGroups+"',triaining_group_formation='"+trainedOnGroupFormation+"'," +
//                        "CR='"+binding.include2.cr.getText().toString()+"',agroforest_agent_id='"+binding.include2.aa.getInput()+"',group_stages_id='"+binding.include2.groupStage.getInput()+"',enrollment_type = '"+Globals.registeringMemberGroup.enrollment_type+"'," +
//                        "reg_start_time='"+Globals.registeringMemberGroup.reg_start_time+"',country_id='"+binding.include2.country.getInput()+"',county='"+binding.include2.countyInput.getInput()+"'," +
//                        "locationId ='"+binding.include2.locationInput.getInput()+"' ,sub_location ='"+binding.include2.subLocationInput.getInput()+"' ,village='"+binding.include2.villageInput.getInput()+"' ,site='"+binding.include2.siteInput.getInput()+"' WHERE transaction_no='" + Globals.registeringMemberGroup.transaction_no + "'");


            memberGroup = new MemberGroup(Globals.registeringMemberGroup.transaction_no, binding.include2.info1.getText().toString(), binding.include2.name.getText().toString(), "Not set",
                    trainedOnGroups, trainedOnGroupFormation, "",
                    binding.include2.aa.getInput(), "", Globals.registeringMemberGroup.enrollment_type,
                    Globals.registeringMemberGroup.reg_start_time, binding.include2.country.getInput(), binding.include2.countyInput.getInput(),
                    binding.include2.locationInput.getInput(), binding.include2.subLocationInput.getInput(), binding.include2.villageInput.getInput(),
                    binding.include2.siteInput.getInput(), newly_formed.toString(), organized_group.toString(), ready_for_certification.toString(), with_certification.toString(), groupStatus);


            Log.e("GroupManagement", "ShowSaveDialog: Transaction No:  " + memberGroup.transaction_no);
            Log.e("GroupManagement", "ShowSaveDialog: Group Sid:  " + memberGroup.sid);

            View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_group_confirmation, null);
            final AlertDialog ald = new AlertDialog.Builder(this)
                    .setView(aldv)
                    .show();
            ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button save = aldv.findViewById(R.id.save);
            save.setOnClickListener(view -> {
                if (recordSaved) {
                    print();

                } else {
                    if (Realm.databaseManager.insertObject(memberGroup)) {
                        for (Member member : selectedMembers) {
                            GroupParticipant groupParticipant = new GroupParticipant(memberGroup.transaction_no, member.sid, member.group_rep == null || member.group_rep == "" ? "0" : member.group_rep,"true");
                            Realm.databaseManager.insertObject(groupParticipant);

                        }
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(memberGroup).get(1));
                        recordSaved = true;
                        save.setText("Print");
                    }

                }

            });
            ((TextView) aldv.findViewById(R.id.sub_title)).setText(memberGroup.name);
            ((TextView) aldv.findViewById(R.id.description)).setText(memberGroup.description + "\nBelow is the list of the group members");
            RecyclerView memberlist = aldv.findViewById(R.id.member_list);
            memberlist.setLayoutManager(new LinearLayoutManager(this));
            memberlist.setAdapter(new MemberAdapter(selectedMembers, (mem, view) -> {

            }));
            aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
                if (recordSaved) {
                    ald.dismiss();
                    finish();
                } else {
                    ald.dismiss();
                }
            });
        }

    }

    void showChangeStatusDialog() {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_group_confirmation, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button save = aldv.findViewById(R.id.save);
        save.setText("Change");
        save.setOnClickListener(view -> {

            if (binding.include2.activateBtn.getText().toString().equalsIgnoreCase("Activate Group")) {
                groupStatus = "true";
                binding.include2.activateBtn.setText("Deactivate Group");
            } else {
                //groupStatus = "false";
                groupStatus = "not_true";
                binding.include2.activateBtn.setText("Activate Group");

            }
            ald.dismiss();

        });
        if (memberGroup == null) {
            ((TextView) aldv.findViewById(R.id.sub_title)).setText("New Group");
        } else {
            ((TextView) aldv.findViewById(R.id.sub_title)).setText(memberGroup.name);
        }
        ((TextView) aldv.findViewById(R.id.description)).setText("Are you sure you want to Change the group status ? ");
        RecyclerView memberlist = aldv.findViewById(R.id.member_list);
        memberlist.setVisibility(View.GONE);


        aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {

            ald.dismiss();
        });


    }

    void print() {
        new T12Printer(GroupManagement.this, new Printer.PrintingInterface() {
            @Override
            public void onReadyToPrint(Object... printingObjects) {
                PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                CanvasPrint cp = new CanvasPrint();
                cp.init(PrinterType.TIII);
                cp.setUseSplit(true);
                cp.setTextAlignRight(false);
                cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(GroupManagement.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                FontProperty fp = new FontProperty();
                fp.setFont(true, false, true, false, 27, null);
                cp.setFontProperty(fp);
                cp.drawText(100, creationMode ? "Group creation report" : "Group report");
                cp.drawText("");
                fp.setFont(true, false, false, false, 25, null);
                cp.setFontProperty(fp);
                cp.drawText("Transaction no: ");
                cp.drawText(memberGroup.transaction_no);
                ;
                cp.drawText("");
                cp.drawText("Group info");
                fp.setFont(true, false, false, false, 20, null);
                cp.setFontProperty(fp);
                cp.drawText("");
                cp.drawText("Group name: " + memberGroup.name);
                cp.drawText("Group description: " + memberGroup.description);
                cp.drawText("Members: " + (creationMode ? selectedMembers.size() : members.size()));
                cp.drawText("");
                fp.setFont(true, false, false, false, 25, null);
                cp.setFontProperty(fp);
                cp.setTextAlignRight(false);
                cp.drawText("Group members");
                cp.drawText("");
                mPrinter.printImage(cp.getCanvasImage());
                boolean empty = true;
                for (Member member : creationMode ? selectedMembers : members) {
                    empty = false;
                    cp = new CanvasPrint();
                    cp.init(PrinterType.TIII);
                    fp.setFont(true, false, false, false, 20, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText(member.full_name);
                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(true);
                    cp.drawText("National ID number: " + member.nat_id);
                    cp.drawLine(0, cp.getCurrentPointY(), 450, cp.getCurrentPointY());
                    mPrinter.printImage(cp.getCanvasImage());
                }
                cp = new CanvasPrint();
                cp.init(PrinterType.TIII);
                if (empty) {
                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText(130, "**No records**");

                }

                fp.setFont(false, false, false, false, 18, null);
                cp.setFontProperty(fp);
                cp.setTextAlignRight(false);
                cp.drawText("");
                cp.drawText("Report generated by: " + Globals.user().username);
                cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                cp.drawText("");
                mPrinter.printImage(cp.getCanvasImage());
                Barcode bc = new Barcode(PDF417, 5, 300, 50, memberGroup.transaction_no);
                mPrinter.printBarCode(bc);
                mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                mPrinter.closeConnection();

            }

        }).print();
    }

    boolean validated() {
//        if (selectedMembers.size() < 1) {
//            return false;
//        }
        if (binding.include2.name.getText().length() < 1) {
            binding.include2.name.setError("Invalid input");
            return false;
        }
        if (binding.include2.info1.getText().length() < 1) {
            binding.include2.info1.setError("Invalid input");
            return false;
        }
        return binding.include2.country.isInputValid()
                & binding.include2.countyInput.isInputValid()
                & binding.include2.locationInput.isInputValid()
                & binding.include2.subLocationInput.isInputValid()
                & binding.include2.villageInput.isInputValid()
                & binding.include2.siteInput.isInputValid()
                & binding.include2.aa.isInputValid();
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.non_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                Globals.hasGroupRep = false;
                Globals.groupRepChair = false;
                Globals.groupRepTreas = false;
                Globals.groupRepSec = false;
                onBackPressed();

                break;
            case R.id.config:
                startActivity(new Intent(this, Configuration.class));

                break;
        }
        return super.onOptionsItemSelected(item);

    }
}