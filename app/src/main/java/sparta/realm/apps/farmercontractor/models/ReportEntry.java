package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "report_entry")//9091325 -green belt hardware till no
public class ReportEntry extends RealmModel implements Serializable {


    @DynamicProperty(json_key = "description")
    public String description;

 @DynamicProperty(json_key = "date")
    public String date;

    @DynamicProperty(json_key = "name")
    public String name;




    public ReportEntry() {


    }
    public ReportEntry(String member, String survey, String total_score) {



        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id= Globals.user().sid;
    }

}
