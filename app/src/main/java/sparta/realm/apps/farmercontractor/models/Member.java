package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "member_info_table")
@SyncDescription(service_name = "Member", upload_link = "/FarmersContract/Farmers/FarmersRegistration", service_type = SyncDescription.service_type.Upload)
@SyncDescription(service_name = "Member", chunk_size = 10000, download_link = "/FarmersContract/Farmers/GetRegistedFarmers", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class Member extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "full_name")
    public String full_name;

    @DynamicProperty(json_key = "phone_no")
    public String phone_no;

    @DynamicProperty(json_key = "nat_id")
    public String nat_id;

    @DynamicProperty(json_key = "country_id")
    public String country;

    @DynamicProperty(json_key = "county_id")
    public String county;

    @DynamicProperty(json_key = "location_id")
    public String location;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location;

    @DynamicProperty(json_key = "village_id")
    public String village;

    @DynamicProperty(json_key = "site_id")
    public String site;

    @DynamicProperty(json_key = "land_size")
    public String land_size;

    @DynamicProperty(json_key = "total_seedlings")
    public String total_seedlings;

    @DynamicProperty(json_key = "donated_seedlings")
    public String donated_seedlings;

    @DynamicProperty(json_key = "item_id")
    public String seedling_type;

    @DynamicProperty(json_key = "member_type_id", column_default_value = "1")
    public String member_type_id = "1";

    @DynamicProperty(json_key = "years_of_maturity")
    public String years_of_maturity;

    @DynamicProperty(json_key = "dbh")
    public String dbh_at_maturity;

    @DynamicProperty(json_key = "agro_forestry_agent_id")
    public String agro_forest_agent;

    @DynamicProperty(json_key = "group_rep_name")
    public String group_rep_name;

    // @DynamicProperty(json_key = "community_rep_natid")
    @DynamicProperty(json_key = "group_rep_nat_id")
    public String group_rep_nat_id;

    @DynamicProperty(json_key = "chief_name")
    public String reg_end_time;

    @DynamicProperty(json_key = "enrollment_type")
    public String enrollment_type;

    @DynamicProperty(json_key = "apk_version_creating")
    public String apk_version_creating;

    @DynamicProperty(json_key = "reg_start_time")
    public String reg_start_time;

    @DynamicProperty(json_key = "chief_name")
    public String chief_name;

    @DynamicProperty(json_key = "meeting_name")
    public String meeting_name;

    @DynamicProperty(json_key = "group_rep")
    public String group_rep;

    //column transaction_no
    //@DynamicProperty(json_key = "transaction_no")
    //public String member_transaction_no;

    public ArrayList<PartnerOrganisation> partnerOrganisations;

    public MemberImage profile_photo = null;
    public MemberImage id_photo_front = null;
    public MemberImage id_photo_back = null;
    public MemberImage chiefSignature = null;
    //    public  MemberImage memberFingerprint = null;
    public MemberFingerprint memberFingerprint = null;

    public Member(String memberSid, String groupRep, String full_name) {
        this.sid = memberSid;
        this.group_rep = groupRep;
        this.full_name = full_name;
    }

    public Member() {
    }

    public Member(String enrollment_type) {
        this.enrollment_type = enrollment_type;
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;

    }

}
