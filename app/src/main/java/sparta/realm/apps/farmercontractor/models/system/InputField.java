package sparta.realm.apps.farmercontractor.models.system;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;

import java.io.Serializable;

@DynamicClass(table_name = "input_field")
public class InputField extends InputGroup implements Serializable {





    @DynamicProperty(json_key = "dataset")
    public String dataset;

    @DynamicProperty(json_key = "placeholder")
    public String placeholder;

 @DynamicProperty(json_key = "input_type")
    public String input_type;


//@DynamicProperty(json_key = "input_type")
    public String input;




    @DynamicProperty(json_key = "order_index")
    public String order_index;

//    @DynamicProperty(json_key = "validationRules"/*,parent_column="",child_column=""*/)
    public ValidationRules validationRules=new ValidationRules();

    public InputField() {



    }
    public boolean inputValid=false;
    public boolean isInputValid()
    {
        return true;
    }

    public InputField(String sid, String title, String placeholder,String dataset, String input_type, String parent, String order_index, ValidationRules validationRules) {
        super(sid,title,null,parent,order_index);
        this.title = title;
        this.placeholder = placeholder;
        this.dataset = dataset;
        this.input_type = input_type;
        this.parent = parent;
        this.order_index = order_index;
        this.validationRules = validationRules;
        this.sid = sid;

    }



    public enum InputType{
        None,
        Text,
        Number,
        Selection,
        MultiSelection,
        RadioSelection,
        Date,
        Time,
        DateTime,
        BarcodeScanText,
        Image,
        Signature

    }
}
