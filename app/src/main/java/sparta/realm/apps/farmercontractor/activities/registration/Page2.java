package sparta.realm.apps.farmercontractor.activities.registration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.databinding.ActivityPage2Binding;
import sparta.realm.apps.farmercontractor.models.AgroForestAgent;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;

public class Page2 extends RegistrationActivity {

    ActivityPage2Binding binding;
    String reg_mode;
    static String datez;
    static Village village;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPage2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        reg_mode = getIntent().getStringExtra("reg_mode");
        datez = (DateFormat.format("yyyy-MM-dd", new java.util.Date()).toString());
        village = Realm.databaseManager.loadObject(Village.class, new Query().setTableFilters("sid='" + Globals.registeringMember().village + "'"));

        initUi();

    }
    SearchSpinner.InputListener agroforestAgentInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };

    void initUi()
    {
        setupToolbar(binding.include.toolbar);
        binding.pageProgress.setStepsNumber(3);
        binding.pageProgress.go(1, true);
        binding.title.setText("Referee info");

        binding.formControl.btnPrev.setOnClickListener(view -> {
            binding.formControl.btnPrev.setFocusable(true);
            binding.formControl.btnPrev.setFocusableInTouchMode(true);
            binding.formControl.btnPrev.requestFocus();
           onBackPressed();

        });

        binding.formControl.btnNext.setOnClickListener(view -> proceed());

        String country = Globals.registeringMember().country;
        Log.e("Page2", "Selected Country: " + country);
        binding.agroforestAgentInput.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class,new Query().setTableFilters("country='" + country + "'")), agroforestAgentInputListener);
//        binding.agroforestAgentInput.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query()), agroforestAgentInputListener);
        populate(Globals.registeringMember());

        if (reg_mode.equalsIgnoreCase("2")){
            binding.chiefNameInput.setVisibility(View.GONE);
            binding.meetingNameInput.setVisibility(View.GONE);

        }
    }
    void populate(Member member) {
        binding.agroforestAgentInput.setInput(member.agro_forest_agent);
        binding.groupRepNameInput.setInput(member.group_rep_name);
        binding.groupRepIdnoInput.setInput(member.group_rep_nat_id);
        binding.chiefNameInput.setInput(member.chief_name);
        binding.meetingNameInput.setInput( village.name + "_" + datez + "_");

    }
    void proceed() {
        saveTempRegistration();
        if(validated()||true)
        {
            Intent intent = new Intent(this, Page3.class);
            intent.putExtra("reg_mode", reg_mode);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
    boolean validated() {
        return binding.agroforestAgentInput.isInputValid()
                &binding.groupRepNameInput.isInputValid()
                & binding.groupRepIdnoInput.isInputValid();

    }
    void saveTempRegistration() {
        Globals.registeringMember.agro_forest_agent = binding.agroforestAgentInput.getInput();
        Globals.registeringMember.group_rep_name = binding.groupRepNameInput.getInput();
        Globals.registeringMember.group_rep_nat_id = binding.groupRepIdnoInput.getInput();
        Globals.registeringMember.chief_name = binding.chiefNameInput.getInput();
        Globals.registeringMember.meeting_name = binding.meetingNameInput.getInput();
        Globals.setRegisteringMember(Globals.registeringMember);
    }
    }