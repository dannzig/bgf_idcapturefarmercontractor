package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "member_group")
//@SyncDescription(service_id = "1", service_name = "Member group", download_link = "/FarmersContract/Group/ActiveGroups", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class AppActivity extends RealmModel implements Serializable {



    @DynamicProperty(json_key = "lat")
    public String lat;

    @DynamicProperty(json_key = "lon")
    public String lon;

  @DynamicProperty(json_key = "activity_type")
    public String activity_type;

    public enum ActivityType{
        None,
        RegisteredFarmer,
        Updatedfarmer,
        CreatedGroup

    }


    public AppActivity(ActivityType activityType) {
        this.activity_type = activityType.ordinal()+"";
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this. user_id= Globals.user().sid;

    }
   public AppActivity() {



    }

}
