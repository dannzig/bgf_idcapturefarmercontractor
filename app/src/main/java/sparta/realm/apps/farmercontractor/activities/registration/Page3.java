package sparta.realm.apps.farmercontractor.activities.registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.ImageViewCompat;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.activities.MainActivity;
import sparta.realm.apps.farmercontractor.databinding.ActivityPage3Binding;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberFingerprint;
import sparta.realm.apps.farmercontractor.models.MemberImage;
import sparta.realm.apps.farmercontractor.models.SeedlingType;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.Signature;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartautils.biometrics.fp.BTV2;

import sparta.realm.spartautils.biometrics.fp.FP08_UAREU;
import sparta.realm.spartautils.biometrics.fp.FingerprintManger;
import sparta.realm.spartautils.biometrics.fp.T801;
import sparta.realm.spartautils.s_bitmap_handler;

import sparta.realm.spartautils.svars;
import sparta.realm.utils.Conversions;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

public class Page3 extends RegistrationActivityFP {

    ActivityPage3Binding binding;
    ArrayList<ImageToCapture> image_inputs = new ArrayList<>();

    boolean search;
    Activity context;
    Boolean recordSaved = false;
    Signature sig;
    String reg_mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPage3Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        reg_mode = getIntent().getStringExtra("reg_mode");

        context = this;

        initUi();
        startFPModule(getFingerprintManger());
        fingerprintManger.captureImage = true;
        fingerprintManger.captureTemplate = false;
        fingerprintManger.captureWsq = false;
    }

    FingerprintManger getFingerprintManger() {
        Log.e(logTag, "Device model:" + Build.MODEL);
        ArrayList<String> uareu_devices = new ArrayList<>();
        ArrayList<String> t801_devices = new ArrayList<>();
        uareu_devices.add("SF807N");
        uareu_devices.add("F807");
        uareu_devices.add("FP-08");
        uareu_devices.add("SF-08");
        uareu_devices.add("SF-807");
        uareu_devices.add("S807");
        uareu_devices.add("ax6737_65_n");
        uareu_devices.add("SF-807N");
        t801_devices.add("SEEA900");
        if (uareu_devices.contains(Build.MODEL)) return new FP08_UAREU(this);
        if (t801_devices.contains(Build.MODEL)) return new T801(this);

        return new BTV2(this);
    }

    void initUi() {

        setupToolbar(binding.include.toolbar);
        binding.pageProgress.setStepsNumber(3);
        binding.pageProgress.go(2, true);
        binding.title.setText("Document capture");

        binding.formControl.btnNext.setText("Save and finish");
        binding.formControl.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (reg_mode.equalsIgnoreCase("1")) {
                    Log.e("Page3", "Members ID Signature: " + Globals.registeringMember.sid);
                    Globals.registeringMember.chiefSignature = new MemberImage(Globals.registeringMember.transaction_no, sig.getImage(), MemberImage.MemberImageType.ChiefSignature.ordinal() + "", Globals.registeringMember.sid);
                }

                Globals.setRegisteringMember(Globals.registeringMember);
                Log.e("Page3", "Is It Validated: " + validated());
                if (validated()) {
                    showSaveDialog();
                } else {
                    Toast.makeText(Page3.this, "All image fields are mandatory", Toast.LENGTH_LONG);
                }
            }
        });
        binding.formControl.btnPrev.setOnClickListener(view -> {
            binding.formControl.btnPrev.setFocusable(true);
            binding.formControl.btnPrev.setFocusableInTouchMode(true);
            binding.formControl.btnPrev.requestFocus();
            onBackPressed();

        });
        image_inputs.add(new ImageToCapture("profile_photo", binding.profilePhoto.frontPhoto.image, binding.profilePhoto.frontPhoto.deleteIcon, MemberImage.MemberImageType.ProfilePhoto));
        image_inputs.add(new ImageToCapture("id_photo_front", binding.idPhoto.frontPhoto.image, binding.idPhoto.frontPhoto.deleteIcon, MemberImage.MemberImageType.FrontID));
        image_inputs.add(new ImageToCapture("id_photo_back", binding.idPhoto.backPhoto.image, binding.idPhoto.backPhoto.deleteIcon, MemberImage.MemberImageType.BackID));
        int cnt_t = 0;
        for (ImageToCapture itc : image_inputs) {
            int finalCnt_t = cnt_t;
            itc.iv.setOnClickListener(v -> {

                //svars.set_photo_camera_type(Realm.context, image_inputs.indexOf(itc) + 1, 5);
                svars.set_photo_camera_type(Realm.context, image_inputs.indexOf(itc) + 1, 4);
                take_photo(image_inputs.indexOf(itc) + 1, "");
                //itc.iv.setImageURI(null);
                //itc.iv.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + Globals.registeringMember.profile_photo.image));

            });
            itc.delete.setOnClickListener(v -> removeMemberPhoto(itc));
            cnt_t++;
        }

        binding.profilePhoto.backPhoto.getRoot().setVisibility(View.GONE);
        binding.profilePhoto.title.setText("Profile photo");
        binding.profilePhoto.frontPhoto.title.setText("Passport photo");
        binding.idPhoto.backPhoto.title.setText("Back");
        binding.fingerprintCapture.frontPhoto.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//        take_photo(1,"1");
                fingerprintManger.capture();
            }
        });
        populate(Globals.registeringMember());
        sig = new Signature(act, null, binding.signatureCapture.signature.sig);
        sig.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                sig.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        binding.signatureCapture.signature.lockIcon.setOnClickListener(view -> {
            sig.locked = !sig.locked;
            binding.signatureCapture.signature.lockIcon.setImageDrawable(sig.locked ? getDrawable(R.drawable.ic_open_lock) : getDrawable(R.drawable.ic_lock));
        });
        binding.signatureCapture.signature.deleteIcon.setOnClickListener(view -> sig.clear());

        if (reg_mode.equalsIgnoreCase("2")) {
            binding.signatureCapture.layout.setVisibility(View.GONE);
        }
    }

    //Showing images of previous farmer registered
    void populate(Member member) {
        for (ImageToCapture imageToCapture : image_inputs) {
            String image = null;
            try {
                Field ff = member.getClass().getField(imageToCapture.data_field);
                ff.setAccessible(true);
                try {
//                    MemberImage memberImage = (MemberImage) ff.get(Globals.registeringMember);
//                    Log.e("TABLE CLASS ", "TABLE :" + memberImage);
                    image = ((MemberImage) ff.get(Globals.registeringMember)).image;
                } catch (NullPointerException | ClassCastException | IllegalAccessException ex) {

                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            try {

                imageToCapture.iv.setImageURI(null);
                //imageToCapture.iv.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + image));
                imageToCapture.iv.setColorFilter(null);
                imageToCapture.iv.getDrawable().setTintList(null);
                ImageViewCompat.setImageTintList(imageToCapture.iv, null);
            } catch (Exception ex) {

            }
        }
        MemberFingerprint memberFingerprint = Globals.registeringMember.memberFingerprint;
        try {

            binding.fingerprintCapture.frontPhoto.image.setImageURI(null);
            //binding.fingerprintCapture.frontPhoto.image.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + memberFingerprint.image));
            binding.fingerprintCapture.frontPhoto.image.setColorFilter(null);
            binding.fingerprintCapture.frontPhoto.image.getDrawable().setTintList(null);
            ImageViewCompat.setImageTintList(binding.fingerprintCapture.frontPhoto.image, null);
        } catch (Exception ex) {

        }
    }

    boolean validated() {
        //Validation new farmer
        if (reg_mode.equalsIgnoreCase("1")) {
            return Globals.registeringMember.profile_photo != null && Globals.registeringMember.profile_photo.image != null &&
                    Globals.registeringMember.id_photo_front != null && Globals.registeringMember.id_photo_front.image != null &&
                    Globals.registeringMember.id_photo_back != null && Globals.registeringMember.id_photo_back.image != null &&
                    Globals.registeringMember.memberFingerprint != null && Globals.registeringMember.memberFingerprint.image != null &&
                    Globals.registeringMember.chiefSignature != null && Globals.registeringMember.chiefSignature.image != null;

        } else {
            //No validation for updating farmers
            return true;
        }

    }

    boolean saveAll() {

        binding.pageProgress.done(true);
        Globals.registeringMember.user_id = Globals.user().sid;
        DatabaseManager.database.beginTransaction();

        if (Globals.registeringMember.enrollment_type.equals("2")) {
//            if (Globals.registeringMember.sid == null || Globals.registeringMember.sid.length() < 1) {
//                DatabaseManager.database.endTransaction();
//                Toast.makeText(context, "Member has not Synced to Backend Yet,Sid null", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//            if (Globals.registeringMember.sid.contains("temp")) {
//                DatabaseManager.database.endTransaction();
//                Toast.makeText(context, "Member has not Synced to Backend Yet,Sid temp", Toast.LENGTH_SHORT).show();
//                return false;
//            }
            DatabaseManager.database.execSQL("UPDATE suitability_assessment_survey_entry SET is_suitability_active = 'false' WHERE member='" + Globals.registeringMember.sid + "'");
            DatabaseManager.database.execSQL("DELETE FROM member_info_table WHERE sid='" + Globals.registeringMember.sid + "'");
            //DatabaseManager.database.execSQL("UPDATE land_location SET is_location_active = 'false' WHERE member='" + Globals.registeringMember.sid + "'");
        }
        try {
            Globals.registeringMember.profile_photo.member_transaction_no = Globals.registeringMember.transaction_no;
            Globals.registeringMember.id_photo_front.member_transaction_no = Globals.registeringMember.transaction_no;
            Globals.registeringMember.id_photo_back.member_transaction_no = Globals.registeringMember.transaction_no;
            Globals.registeringMember.memberFingerprint.member_transaction_no = Globals.registeringMember.transaction_no;
            Globals.registeringMember.chiefSignature.member_transaction_no = Globals.registeringMember.transaction_no;

        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        if (reg_mode.equalsIgnoreCase("1")) {
            //New farmer

            if (Realm.databaseManager.insertObject(Globals.registeringMember.profile_photo)
                    && Realm.databaseManager.insertObject(Globals.registeringMember.id_photo_front)
                    && Realm.databaseManager.insertObject(Globals.registeringMember.id_photo_back)
                    && Realm.databaseManager.insertObject(Globals.registeringMember.memberFingerprint)
                    && Realm.databaseManager.insertObject(Globals.registeringMember.chiefSignature)
                    && Realm.databaseManager.insertObject(Globals.registeringMember)

            ) {

                DatabaseManager.database.setTransactionSuccessful();
                DatabaseManager.database.endTransaction();
                SynchronizationManager.upload_(Realm.realm.getSyncDescription(Globals.registeringMember).get(0));
                SynchronizationManager.upload_(Realm.realm.getSyncDescription(Globals.registeringMember.profile_photo).get(0));
                SynchronizationManager.upload_(Realm.realm.getSyncDescription(Globals.registeringMember.memberFingerprint).get(0));
                recordSaved = true;

                return true;

            } else {
                View snackbar_view = LayoutInflater.from(context).inflate(R.layout.dialog_verification_confirmation, null);
                TextView sub_title = snackbar_view.findViewById(R.id.sub_title);
                TextView info = snackbar_view.findViewById(R.id.content);
                sub_title.setText("Saving details failed !");
                info.setText("Some error occurred while saving the record ");
                Toast t = new Toast(context);
                t.setView(snackbar_view);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.setDuration(Toast.LENGTH_LONG);
                t.show();
//              DatabaseManager.database.endTransaction();
                DatabaseManager.database.endTransaction();
                return false;

            }
        } else {

            if (Realm.databaseManager.insertObject(Globals.registeringMember)) {

                if (Globals.registeringMember.memberFingerprint != null) {
                    Realm.databaseManager.insertObject(Globals.registeringMember.memberFingerprint);
                }
                if (Globals.registeringMember.id_photo_back != null) {
                    Realm.databaseManager.insertObject(Globals.registeringMember.id_photo_back);
                }
                if (Globals.registeringMember.id_photo_front != null) {
                    Realm.databaseManager.insertObject(Globals.registeringMember.id_photo_front);
                }
                if (Globals.registeringMember.profile_photo != null) {
                    Realm.databaseManager.insertObject(Globals.registeringMember.profile_photo);
                }

                DatabaseManager.database.setTransactionSuccessful();
                DatabaseManager.database.endTransaction();

                SynchronizationManager.upload_(Realm.realm.getSyncDescription(Globals.registeringMember).get(0));
                if (Globals.registeringMember.profile_photo != null) {
                    SynchronizationManager.upload_(Realm.realm.getSyncDescription(Globals.registeringMember.profile_photo).get(0));

                }
                if (Globals.registeringMember.memberFingerprint != null) {
                    SynchronizationManager.upload_(Realm.realm.getSyncDescription(Globals.registeringMember.memberFingerprint).get(0));

                }
                recordSaved = true;
                return true;

            } else {
                View snackbar_view = LayoutInflater.from(context).inflate(R.layout.dialog_verification_confirmation, null);
                TextView sub_title = snackbar_view.findViewById(R.id.sub_title);
                TextView info = snackbar_view.findViewById(R.id.content);
                sub_title.setText("Saving details failed !");
                info.setText("Some error occurred while saving the record ");
                Toast t = new Toast(context);
                t.setView(snackbar_view);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.setDuration(Toast.LENGTH_LONG);
                t.show();
                //DatabaseManager.database.endTransaction();
                DatabaseManager.database.endTransaction();
                return false;

            }
        }
    }

    private boolean validInput(String column, String inputField){
        Member member;
        try {
            member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters(" "+ column +" = '" + inputField + "'"));
            if (Globals.reg_mode.equalsIgnoreCase("1")){
                if (member != null){

                    return false;
                }
            }
            return true;
        }catch (Exception ex){
            Log.e("FormEdittext", "InvalidInput: ");

        }
        return false;
    }
    void exitRegistration() {
        Globals.registeringMember = null;
        Intent reg_intent = new Intent(act, MainActivity.class);
        reg_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
        start_activity(reg_intent);
    }

    void showSaveDialog() {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_enrollment_confirmation, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button save = aldv.findViewById(R.id.save);
        save.setOnClickListener(view -> {
            if (recordSaved) {
                new T12Printer(Page3.this, printingObjects -> {
                    PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                    CanvasPrint cp = new CanvasPrint();
                    cp.init(PrinterType.TIII);
                    cp.setUseSplit(true);
                    cp.setTextAlignRight(false);
                    cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(Page3.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                    FontProperty fp = new FontProperty();
                    fp.setFont(true, false, true, false, 27, null);
                    cp.setFontProperty(fp);
                    cp.drawText(100, Globals.registeringMember.enrollment_type.equals("1") ? "Member registration" : "Member update");
                    cp.drawText("");
                    fp.setFont(true, false, false, false, 25, null);
                    cp.setFontProperty(fp);
                    cp.drawText("Transaction no: ");
                    cp.drawText(Globals.registeringMember.transaction_no);
                    fp.setFont(true, false, false, false, 25, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText("");
                    cp.drawText("Farmer info");
                    fp.setFont(true, false, false, false, 20, null);
                    cp.setFontProperty(fp);
                    cp.drawText("");
                    cp.drawText("Name: " + Globals.registeringMember.full_name);
                    cp.drawText("ID number: " + Globals.registeringMember.nat_id);
                    cp.drawText("Phone number : " + Globals.registeringMember.phone_no);

                    cp.drawText("");
                    fp.setFont(true, false, false, false, 25, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText("Land info");
                    fp.setFont(true, false, false, false, 20, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText("");
                    cp.drawText("Land size:" + Globals.registeringMember.land_size);
                    cp.drawText("Country:" + Realm.databaseManager.loadObject(Country.class, new Query().setTableFilters("sid='" + Globals.registeringMember.country + "'")).name);
                    cp.drawText("County:" + Realm.databaseManager.loadObject(County.class, new Query().setTableFilters("sid='" + Globals.registeringMember.county + "'")).name);
                    cp.drawText("Location:" + Realm.databaseManager.loadObject(Location.class, new Query().setTableFilters("sid='" + Globals.registeringMember.location + "'")).name);
                    cp.drawText("Sub-Location:" + Realm.databaseManager.loadObject(SubLocation.class, new Query().setTableFilters("sid='" + Globals.registeringMember.sub_location + "'")).name);
                    cp.drawText("Village:" + Realm.databaseManager.loadObject(Village.class, new Query().setTableFilters("sid='" + Globals.registeringMember.village + "'")).name);
                    cp.drawText("Site:" + Realm.databaseManager.loadObject(Site.class, new Query().setTableFilters("sid='" + Globals.registeringMember.site + "'")).name);
                    cp.drawText("Seedling type:" + Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + Globals.registeringMember.seedling_type + "'")).name);
                    cp.drawText("Total seedlings:" + Globals.registeringMember.total_seedlings);
                    cp.drawText("Donated seedlings:" + Globals.registeringMember.donated_seedlings);
                    cp.drawText("");

                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText("Report generated by: " + Globals.user().username);
                    cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                    cp.drawText("");
                    mPrinter.printImage(cp.getCanvasImage());
                    Barcode bc = new Barcode(PDF417, 5, 300, 50, Globals.registeringMember.transaction_no);
                    mPrinter.printBarCode(bc);
                    mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                    new Handler().postDelayed(() -> mPrinter.closeConnection(), 2000);
                }).print();

            } else {
                recordSaved = saveAll();
                Log.e("Page3", "Is It recordSaved: " + recordSaved);
                if (recordSaved) {
                    save.setText("Print");
                }
            }

        });
        ((TextView) aldv.findViewById(R.id.sub_title)).setText(Globals.registeringMember.full_name);

        ((TextView) aldv.findViewById(R.id.content)).setText((Globals.registeringMember.enrollment_type.equals("1") ? "Confirm registration details of this farmer" : "Confirm detail update of this farmer") + "\n\n"
                + "\n\nNational ID number:" + Globals.registeringMember.nat_id
                + "\nCountry:" + Realm.databaseManager.loadObject(Country.class, new Query().setTableFilters("sid='" + Globals.registeringMember.country + "'")).name
                + "\nSite:" + Realm.databaseManager.loadObject(Site.class, new Query().setTableFilters("sid='" + Globals.registeringMember.site + "'")).name
                + "\nDonated " + Globals.registeringMember.donated_seedlings + " " + Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + Globals.registeringMember.seedling_type + "'")).item_name);

        try {
            ImageView iv = aldv.findViewById(R.id.icon);
            iv.setImageURI(null);
            iv.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + Globals.registeringMember.profile_photo.image));

        } catch (Exception ex) {

        }
        aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
            if (recordSaved) {
                exitRegistration();
            } else {
                ald.dismiss();

            }
        });
    }


    void removeMemberPhoto(ImageToCapture imageToCapture) {
        imageToCapture.iv.setImageURI(null);
        imageToCapture.iv.setImageBitmap(null);
        try {
            Field ff = Globals.registeringMember.getClass().getField(imageToCapture.data_field);
            ff.setAccessible(true);
            try {
                MemberImage memberImage = (MemberImage) ff.get(Globals.registeringMember);
                Log.e("TABLE CLASS ", "TABLE :" + memberImage);
                ff.set(Globals.registeringMember, null);
            } catch (ClassCastException | IllegalAccessException ex) {

            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void on_result_image_obtained(Bitmap capt_result_img) {
        super.on_result_image_obtained(capt_result_img);
//        save_app_image(capt_result_img);
        File file = new File(svars.current_app_config(this).file_path_employee_data, save_app_image(capt_result_img));
        if (!file.exists() || file.length() < 100) {
//            removeMemberPhoto(itc);
            return;
        }
//        MemberImage memberFingerprint=new MemberImage(Globals.registeringMember.transaction_no,file.getName(),MemberImage.MemberImageType.Fingerprint.ordinal()+"");
        MemberFingerprint memberFingerprint = new MemberFingerprint(Globals.registeringMember.transaction_no, file.getName(), "1", Globals.registeringMember.sid);

        Globals.registeringMember.memberFingerprint = memberFingerprint;
        try {
            binding.fingerprintCapture.frontPhoto.image.setImageURI(null);
            binding.fingerprintCapture.frontPhoto.image.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + memberFingerprint.image));
            binding.fingerprintCapture.frontPhoto.image.setColorFilter(null);
            binding.fingerprintCapture.frontPhoto.image.getDrawable().setTintList(null);
            ImageViewCompat.setImageTintList(binding.fingerprintCapture.frontPhoto.image, null);

        } catch (Exception ex) {

        }
    }

//    public static String save_app_image(Bitmap fpb)
//    {
//        String img_name="RE_DAT"+ System.currentTimeMillis()+"_"+Globals.registeringMember.nat_id+"_IMG.JPG";
//
//        File file = new File(svars.current_app_config(Realm.context).file_path_employee_data);
//        if (!file.exists()) {
//            Log.e(logTag,"Creating data dir: "+ (file.mkdirs()?"Successfully created":"Failed to create !"));
//        }
//        file = new File(svars.current_app_config(Realm.context).file_path_employee_data, img_name);
//
//        try (OutputStream fOutputStream = new FileOutputStream(file)){
//
//
//            fpb.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
//
//            fOutputStream.flush();
////            fOutputStream.close();
//
//            //  MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//
//            return "--------------";
//        } catch (IOException e) {
//            e.printStackTrace();
//
//            return "--------------";
//        }
//        return img_name;
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ImageToCapture itc = image_inputs.get(requestCode - 1);
        try {

            File file = new File(svars.current_app_config(this).file_path_employee_data, data.getStringExtra("ImageUrl"));
            if (!file.exists() || file.length() < 500) {
                removeMemberPhoto(itc);
                return;
            }
            Log.e("Page3", "Members ID All Photos: " + Globals.registeringMember.sid);
            MemberImage memberPhoto = new MemberImage(Globals.registeringMember.transaction_no, data.getStringExtra("ImageUrl"), itc.imageType.ordinal() + "", Globals.registeringMember.sid);

//            Globals.registeringMember.profile_photo = memberPhoto;
            try {
                Field ff = Globals.registeringMember.getClass().getField(itc.data_field);
                ff.setAccessible(true);
                try {
//                    MemberImage memberImage = (MemberImage) ff.get(Globals.registeringMember);
//                    Log.e("TABLE CLASS ", "TABLE :" + memberImage);
                    ff.set(Globals.registeringMember, memberPhoto);
                } catch (ClassCastException | IllegalAccessException ex) {

                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            try {

                itc.iv.setImageURI(null);
                itc.iv.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + memberPhoto.image));
                itc.iv.setColorFilter(null);
                itc.iv.getDrawable().setTintList(null);
                ImageViewCompat.setImageTintList(itc.iv, null);
            } catch (Exception ex) {

            }

        } catch (Exception ex) {

        }
    }
}