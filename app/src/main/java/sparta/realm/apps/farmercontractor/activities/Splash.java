package sparta.realm.apps.farmercontractor.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.realm.annotations.sync_service_description;

import java.util.ArrayList;
import java.util.List;

import sparta.realm.Activities.SpartaAppCompactActivity;
import sparta.realm.Realm;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.MyApplication;
import sparta.realm.apps.farmercontractor.SyncOverride;
import sparta.realm.apps.farmercontractor.databinding.ActivitySplashBinding;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.PermissionManagement;

public class Splash extends SpartaAppCompactActivity {

    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initUi();
        setupSync();
    }

    void initUi() {
        binding.include.back.setVisibility(View.GONE);
        binding.include.optionsMenu.setVisibility(View.GONE);
        if (Globals.operationMode() == Globals.OperationMode.Dev) {
            binding.include.subTitle.setText("Farmer contractor (Dev mode)");
        }
    }

    long lastpermcheck = 0;
    long maxperiod = 2000;

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            AlarmManager alarmManager = (AlarmManager) ContextCompat.getSystemService(this, AlarmManager.class);
            if (!alarmManager.canScheduleExactAlarms()) {
                Intent intent = new Intent(Settings.ACTION_REQUEST_SCHEDULE_EXACT_ALARM, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
//              5993429
            }
        }
        Log.e("Splash", "Version: " + Build.VERSION.SDK_INT);
        Log.e("Splash", "Version_Code: " + Build.VERSION_CODES.R);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivity(intent);
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(act)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + act.getPackageName()));
            startActivity(intent);
            return;
        }
        if (System.currentTimeMillis() - lastpermcheck > maxperiod) {
            lastpermcheck = System.currentTimeMillis();

            Log.e("Splash", "All Permissions: " + checkAndRequestAllPermissions(this));


            if (checkAndRequestAllPermissions(this)) {
                new Handler().postDelayed(() -> {
                    startActivity(new Intent(this, SignIn.class));
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                }, 3000);

            }
        }

    }

    void setupSync() {
        MyApplication.syncOverride = new SyncOverride();

        MyApplication.syncOverride.setupsyncstatusreport(new SynchronizationManager.SynchronizationStatusHandler() {
            @Override
            public void on_status_changed(String status) {


            }

            @Override
            public void on_info_updated(String status) {

            }

            @Override
            public void on_main_percentage_changed(int progress) {
                Log.e("SSD :", "on_main_percentage_changed :" + "Floating    " + progress);

            }

            @Override
            public void on_secondary_progress_changed(int progress) {
                Log.e("SSD :", "on_secondary_progress_changed :" + "Min    " + progress);


            }

            @Override
            public void onSynchronizationCompleted() {
                Log.e("Main SSD :", "onSynchronizationCompleted Generally");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                                    String time_now = svars.gett_time();
                        String time_now = svars.gett_time();

                        svars.set_sync_time(act, time_now);
                        Log.e("Main SSD :", "SYnc _time " + time_now + "   " + svars.sync_time(act));


                    }
                });


            }

            @Override
            public void onSynchronizationCompleted(sync_service_description ssd) {
                Log.e("Main SSD :", "onSynchronizationCompleted on :" + ssd.object_package);

            }
        });
    }

    public static boolean checkAndRequestAllPermissions(Activity context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);

            if (info.requestedPermissions != null) {
                return checkAndRequestPermissions(context, info.requestedPermissions);
            }
        } catch (Exception var2) {
            var2.printStackTrace();
        }

        return false;
    }

    public static boolean checkAndRequestPermissions(Activity context, String[] permissions) {
        boolean ok = true;
        List<String> listPermissionsNeeded = new ArrayList();
        String[] var5 = permissions;
        int var6 = permissions.length;

        for (int var7 = 0; var7 < var6; ++var7) {
            String p = var5[var7];
            int result = ContextCompat.checkSelfPermission(context, p);
            if (result != 0) {
                listPermissionsNeeded.add(p);
            }
        }
        Log.e("Splash", "List All Permissions 1: " + listPermissionsNeeded + "Size :" + listPermissionsNeeded.size());


        if (listPermissionsNeeded.contains("android.permission.MANAGE_EXTERNAL_STORAGE")) {
            if (Build.VERSION.SDK_INT >= 30) {
                if (Environment.isExternalStorageManager()) {
                    listPermissionsNeeded.remove("android.permission.MANAGE_EXTERNAL_STORAGE");
                } else {
                    Intent intent = new Intent("android.settings.MANAGE_APP_ALL_FILES_ACCESS_PERMISSION");
                    Uri uri = Uri.fromParts("package", context.getPackageName(), (String) null);
                    intent.setData(uri);
                    context.startActivity(intent);
                }
            } else {
                listPermissionsNeeded.remove("android.permission.MANAGE_EXTERNAL_STORAGE");
            }
        }
//        if (listPermissionsNeeded.contains("android.permission.NEARBY_WIFI_DEVICES")){
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
//                listPermissionsNeeded.remove("android.permission.NEARBY_WIFI_DEVICES");
//            }
//        }

        if (listPermissionsNeeded.contains(Manifest.permission.SYSTEM_ALERT_WINDOW)) {
            if (!Settings.canDrawOverlays(context)) {
                openOverlaySettings(context);
                ok = false;
            } else {
                listPermissionsNeeded.remove(Manifest.permission.SYSTEM_ALERT_WINDOW);
            }
        }
        if (listPermissionsNeeded.contains(Manifest.permission.PACKAGE_USAGE_STATS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (hasPermission(Manifest.permission.PACKAGE_USAGE_STATS)) {
                    listPermissionsNeeded.remove(Manifest.permission.PACKAGE_USAGE_STATS);

                } else {
                    context.startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                }
            } else {
                listPermissionsNeeded.remove(Manifest.permission.PACKAGE_USAGE_STATS);

            }
        }
        if (listPermissionsNeeded.contains(Manifest.permission.BLUETOOTH_CONNECT)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            } else {
                listPermissionsNeeded.remove(Manifest.permission.BLUETOOTH_CONNECT);
            }
        }
        if (listPermissionsNeeded.contains(Manifest.permission.SCHEDULE_EXACT_ALARM)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            } else {
                listPermissionsNeeded.remove(Manifest.permission.SCHEDULE_EXACT_ALARM);
            }
        }
        if (listPermissionsNeeded.contains(Manifest.permission.BLUETOOTH_SCAN)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            } else {
                listPermissionsNeeded.remove(Manifest.permission.BLUETOOTH_SCAN);
            }
        }
        //For android 13 devices.
        if (listPermissionsNeeded.contains(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {

            } else {
                listPermissionsNeeded.remove(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
        if (listPermissionsNeeded.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {

            } else {
                listPermissionsNeeded.remove(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        if (listPermissionsNeeded.contains(Manifest.permission.BLUETOOTH_PRIVILEGED)) {
            listPermissionsNeeded.remove(Manifest.permission.BLUETOOTH_PRIVILEGED);
        }
        if (listPermissionsNeeded.contains(Manifest.permission.READ_PRECISE_PHONE_STATE)) {
            listPermissionsNeeded.remove(Manifest.permission.READ_PRECISE_PHONE_STATE);
        }

        if (listPermissionsNeeded.contains("android.permission.READ_PRIVILEGED_PHONE_STATE")) {
            listPermissionsNeeded.remove("android.permission.READ_PRIVILEGED_PHONE_STATE");
        }

        if (listPermissionsNeeded.contains("android.permission.REQUEST_INSTALL_PACKAGES")) {
            listPermissionsNeeded.remove("android.permission.REQUEST_INSTALL_PACKAGES");
        }

        if (listPermissionsNeeded.contains("com.mediatek.permission.CTA_ENABLE_BT")) {
            listPermissionsNeeded.remove("com.mediatek.permission.CTA_ENABLE_BT");
        }

        Log.e("Splash", "List All Permissions this: " + listPermissionsNeeded + "Size :" + listPermissionsNeeded.size());

        if (!listPermissionsNeeded.isEmpty() && !(listPermissionsNeeded.size() == 1 & ((String) listPermissionsNeeded.get(0)).equalsIgnoreCase("android.permission.ACCESS_FINE_LOCATION"))) {
            ActivityCompat.requestPermissions(context, (String[]) listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 10);
            ok = false;
        }

        if (ContextCompat.checkSelfPermission(context, "android.permission.SYSTEM_ALERT_WINDOW") != 0) {
        }

        return ok;
    }


    @RequiresApi(api = Build.VERSION_CODES.R)
    public static boolean hasPermission(String specialPermission) {
        Context context = Realm.context;
        ApplicationInfo info = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            info = packageManager.getApplicationInfo(context.getPackageName(), 0);
//            Log.d("TAG", "onCreate: "+hasPermission(this,info));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        boolean granted = false;
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        String op = appOps.permissionToOp(specialPermission);
        if (op == null) return true;
        int mode = appOps.unsafeCheckOpNoThrow(op, info.uid, info.packageName);
        if (mode == AppOpsManager.MODE_DEFAULT)
            granted = (context.checkCallingOrSelfPermission(Manifest.permission.MANAGE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        else
            granted = (mode == AppOpsManager.MODE_ALLOWED);
        return granted;
    }

    @TargetApi(23)
    public static void openOverlaySettings(Activity activity) {
        Intent intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse("package:" + activity.getPackageName()));

        try {
            activity.startActivityForResult(intent, 6);
        } catch (ActivityNotFoundException var3) {
            Log.e("Drawers permission :", var3.getMessage());
        }

    }
}