package sparta.realm.apps.farmercontractor.models;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.SyncDescription;
import java.io.Serializable;
import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;

@DynamicClass(table_name = "nursery_types")
@SyncDescription(service_id = "1", service_name = "Nursery Types", download_link = "/FarmersContract/Group/TypeofNurseriesListMobile", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class NurseryTypes extends SelectionData implements Serializable {

    public NurseryTypes() {
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }
}
