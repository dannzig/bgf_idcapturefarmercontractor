package sparta.realm.apps.farmercontractor;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import net.sqlcipher.Cursor;

import java.util.ArrayList;
import java.util.Calendar;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.MemberSearchFilter;
import sparta.realm.apps.farmercontractor.models.User;
import sparta.realm.apps.farmercontractor.models.system.EmailConfiguration;
import sparta.realm.spartautils.svars;


public class Globals {


    public static String user_request_url = svars.current_app_config(Realm.context).APP_MAINLINK + "/MobiServices/GeneralData/GetAllUsers/";//https://ciw.cs4africa.com/cmu/MobiServices/GeneralData/GetAllUsers
    public static String faceDatabase = "test04.dat";
    public static Member registeringMember;
    public static MemberGroup registeringMemberGroup;
    public static String reg_mode;
    public static boolean hasGroupRep = false;
    public static Boolean groupRepChair = false;
    public static Boolean groupRepTreas = false;
    public static Boolean groupRepSec = false;
    public static ArrayList<Member> selectedMembersTraining;
    public static ArrayList<Member> selectedMembersIssued;

    //0716306038
    public static String getTransactionNo() {
        return svars.getTransactionNo().replace(":", "_");

    }

    public static String sync_time(Context act) {
        SharedPreferences prefs = act.getSharedPreferences(svars.sharedprefsname, act.MODE_PRIVATE);
        return prefs.getString("sync_time_var", null);

    }

    public static void setPrintingTemplate(int printingTemplate) {
        Context act = Realm.context;

        SharedPreferences.Editor saver = act.getSharedPreferences(svars.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putInt("printingTemplate", printingTemplate);
        saver.commit();

    }

    public static int printingTemplate() {
        Context act = Realm.context;
        SharedPreferences prefs = act.getSharedPreferences(svars.sharedprefsname, act.MODE_PRIVATE);
        return prefs.getInt("printingTemplate", 1);


    }

    public enum OperationMode {
        NONE,
        NormalUser,
        Dev,
        Demo,
        Live

    }

    public static void setOperationMode(OperationMode operationMode) {
        Context act = Realm.context;

        SharedPreferences.Editor saver = act.getSharedPreferences(svars.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putInt("AppOperationMode", operationMode.ordinal());
        saver.commit();

    }

    public static OperationMode operationMode() {
        Context act = Realm.context;
        SharedPreferences prefs = act.getSharedPreferences(svars.sharedprefsname, act.MODE_PRIVATE);
        return OperationMode.values()[prefs.getInt("AppOperationMode", 1)];


    }

    public static void set_sync_time(Context act, String sync_time_var) {

        SharedPreferences.Editor saver = act.getSharedPreferences(svars.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putString("sync_time_var", sync_time_var);
        saver.commit();

    }

    public static String backupEmail() {
        SharedPreferences prefs = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE);
        return prefs.getString("backupEmail", null);

    }

    public static void setBackupEmail(String backupEmail) {

        SharedPreferences.Editor saver = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE).edit();

        saver.putString("backupEmail", backupEmail);
        saver.commit();

    }

    public static boolean isBackupTypeActive(BackupManager.BackupType backupType) {
        SharedPreferences prefs = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE);
        return prefs.getBoolean("isBackupTypeActive_" + backupType.name(), true);

    }

    public static void setIsBackupTypeActive(BackupManager.BackupType backupType, boolean active) {

        SharedPreferences.Editor saver = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE).edit();

        saver.putBoolean("isBackupTypeActive_" + backupType.name(), active);
        saver.commit();

    }

    public static MemberSearchFilter memberSearchFilter() {
        return svars.workingObject(MemberSearchFilter.class, "0");
    }

    public static void setMemberSearchFilter(MemberSearchFilter memberSearchFilter) {
        svars.setWorkingObject(memberSearchFilter, "0");
    }

    public static User user() {
        return svars.workingObject(User.class, "0");
    }

    public static void setUser(User user) {
        svars.setWorkingObject(user, "0");
    }

    public static Member registeringMember() {
        return svars.workingObject(Member.class, "0");
    }
    public static MemberGroup registeringMemberGroup() {
        return svars.workingObject(MemberGroup.class, "0");
    }

    public static void setRegisteringMember(Member member) {
        svars.setWorkingObject(member, "0");
    }

    public static MemberGroup getRegisteringMemberGroup() {
        return registeringMemberGroup;
    }

    public static void setRegisteringMemberGroup(MemberGroup registeringMemberGroup) {
        svars.setWorkingObject(registeringMemberGroup, "0");
    }

    public static void setUpdateCheckTime(long update_check_time) {

        SharedPreferences.Editor saver = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE).edit();
        saver.putLong("updateCheckTime", update_check_time);
        saver.commit();
    }

    public static long updateCheckTime() {
        SharedPreferences prefs = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE);
        return prefs.getLong("updateCheckTime", 0);

    }

// public static Date updateCheckTime() {
//        SharedPreferences prefs = Realm.context.getSharedPreferences(svars.sharedprefsname, Realm.context.MODE_PRIVATE);
//        return new Date(prefs.getLong("updateCheckTime", 0));
//
//    }
//    public static Device myDevice() {
//        return working_object(Realm.context, Device.class, "0");
//    }
//
//    public static void setMyDevice(Device mem) {
//        set_working_object(Realm.context, mem, "0");
//    }

    public static void setBackupEmailConfiguration(EmailConfiguration config) {
        svars.setWorkingObject(config, "backup_email");
    }

    public static EmailConfiguration backupEmailConfiguration() {
        return svars.workingObject(EmailConfiguration.class, "backup_email");
    }

    public static int isSidPresentUpdate(String table) {
        int update = 0;
        Cursor c = null;
        c = DatabaseManager.database.rawQuery("SELECT member FROM " + table + " WHERE member like 'TEMP%'", null);
        if (c.moveToFirst()) {
            do {
                String temp_member_no_id, member_id;

                temp_member_no_id = c.getString(0);

                member_id = temp_member_no_id.substring(4);
                Log.e("Globals", "MemberNo: " + member_id);
                Member foundMember = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("nat_id='" + member_id + "'", "sync_status='" + 1 + "'"));
                //Log.e("Globals", "FoundMember: " + foundMember.full_name);
                if (foundMember != null) {
                    if (foundMember.sid != null && !foundMember.sid.isEmpty()) {
                        //DatabaseManager.database.execSQL("update suitability_assessment_survey_entry set member ='" + foundMember.sid + "' where phone_no='" + phoneNo + "'");
                        DatabaseManager.database.execSQL("update '" + table + "' set member ='" + foundMember.sid + "' where member='" + temp_member_no_id + "'");

                        update++;
                    }
                }
            } while (c.moveToNext());
        }
        c.close();

        return update;
    }

    public static int isSidPresentUpdate2(String table) {
        int update = 0;
        Cursor c = null;
        c = DatabaseManager.database.rawQuery("SELECT member_id FROM " + table + " WHERE member_id like 'TEMP%'", null);
        if (c.moveToFirst()) {
            do {
                String temp_member_no_id, member_id;

                temp_member_no_id = c.getString(0);

                member_id = temp_member_no_id.substring(4);
                Log.e("Globals", "MemberNo: " + member_id);
                Member foundMember = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("nat_id='" + member_id + "'", "sync_status='" + 1 + "'"));
                //Log.e("Globals", "FoundMember: " + foundMember.full_name);
                if (foundMember != null) {
                    if (foundMember.sid != null && !foundMember.sid.isEmpty()) {
                        //DatabaseManager.database.execSQL("update suitability_assessment_survey_entry set member ='" + foundMember.sid + "' where phone_no='" + phoneNo + "'");
                        DatabaseManager.database.execSQL("update '" + table + "' set member_id ='" + foundMember.sid + "' where member_id='" + temp_member_no_id + "'");

                        update++;
                    }
                }
            } while (c.moveToNext());
        }
        c.close();

        return update;
    }

    public static int updateDuplicateImagesRecord(String transaction_no, String table) {
        int update = 0;
        Cursor c = null;
        c = DatabaseManager.database.rawQuery("SELECT * FROM " + table + " WHERE member_transaction_no = '" + transaction_no + "'", null);
        if (c.moveToFirst()) {
            do {

                DatabaseManager.database.execSQL("update'" + table + "' set sync_status = '1'");
                update++;

            } while (c.moveToNext());
        }
        c.close();

        return update;
    }

    public static int updateImageTransaction(String table) {
        int update = 0;
        Cursor c = null;
        c = DatabaseManager.database.rawQuery("SELECT member_id FROM " + table + " WHERE member_id like 'TEMP%'", null);
        if (c.moveToFirst()) {
            do {
                String temp_member_no_id, member_id;

                temp_member_no_id = c.getString(0);

                member_id = temp_member_no_id.substring(4);
                Log.e("Globals", "MemberNo: " + member_id);
                Member foundMember = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("nat_id='" + member_id + "'", "sync_status='" + 1 + "'"));
                //Log.e("Globals", "FoundMember: " + foundMember.full_name);
                if (foundMember != null) {
                    if (foundMember.sid != null && !foundMember.sid.isEmpty()) {
                        //DatabaseManager.database.execSQL("update suitability_assessment_survey_entry set member ='" + foundMember.sid + "' where phone_no='" + phoneNo + "'");
                        DatabaseManager.database.execSQL("update '" + table + "' set member_id ='" + foundMember.sid + "' where member_id='" + temp_member_no_id + "'");

                        update++;
                    }
                }
            } while (c.moveToNext());
        }
        c.close();

        return update;
    }

    public static int updateSyncStatus(String table) {
        int update = 0;
        Cursor c = null;
        //c = DatabaseManager.database.rawQuery("SELECT member FROM " + table + " WHERE member like 'TEMP%'", null);
        //c = DatabaseManager.database.rawQuery("SELECT * FROM " + table + " WHERE sync_status ='1' and reg_time < '2023-08-23' and data_status is NULL", null);
        c = DatabaseManager.database.rawQuery("SELECT * FROM " + table + " WHERE sync_status ='1' and reg_time >= '2023-10-30' and reg_time < '2023-10-31' and data_status is NULL", null);

        if (c.moveToFirst()) {
            do {

                DatabaseManager.database.execSQL("update '" + table + "' set sync_status ='0' where reg_time >= '2023-10-30' and reg_time < '2023-10-31'");
                DatabaseManager.database.execSQL("update '" + table + "' set data_status ='re_synched' where reg_time >= '2023-10-30' and reg_time < '2023-10-31'");

                update++;
            } while (c.moveToNext());
        }
        c.close();

        return update;
    }

    public static void populate_date(final Context context, final EditText edt, final Calendar cb, final boolean include_time, long min, long max) {
        Calendar cc = Calendar.getInstance();
        int mYear = cc.get(Calendar.YEAR);
        int mMonth = cc.get(Calendar.MONTH);
        int mDay = cc.get(Calendar.DAY_OF_MONTH);


        try {


            mYear = cb.get(Calendar.YEAR);
            mMonth = cb.get(Calendar.MONTH);
            mDay = cb.get(Calendar.DAY_OF_MONTH);

        } catch (Exception ex) {
        }


        DatePickerDialog dpd = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {


                    edt.setText((dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "-" + ((monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : (monthOfYear + 1)) + "-" + year);


                    try {
                        cb.setTime(svars.sdf_user_friendly_date.parse(edt.getText().toString()));
                    } catch (Exception ex) {
                    }


                    if (include_time) {


                        new TimePickerDialog(context, (view1, hourOfDay, minute) -> {

                            String dom = "" + hourOfDay;
                            String moy = "" + minute;
                            char[] domch = dom.toCharArray();
                            char[] moych = moy.toCharArray();

                            if (domch.length < 2) {
                                dom = "0" + dom;
                            }
                            if (moych.length < 2) {
                                moy = "0" + moy;
                            }
                            edt.setText(edt.getText().toString() + " " + dom + ":" + moy + ":00");
                            try {
                                cb.setTime(svars.sdf_user_friendly_time.parse(edt.getText().toString()));
                            } catch (Exception ex) {
                            }


                        }, cb.get(Calendar.HOUR_OF_DAY), cb.get(Calendar.MINUTE), false).show();
                    }

                }, mYear, mMonth, mDay);
        dpd.show();
        Calendar calendar_min = Calendar.getInstance();
        calendar_min.set(Calendar.YEAR, calendar_min.get(Calendar.YEAR));

        if (min > 0) {
            dpd.getDatePicker().setMinDate(min);
        }
        if (max > 0) {
            dpd.getDatePicker().setMaxDate(max);
        }

        //  dpd.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
    }

    public static void populate_date(EditText edt, Calendar cb) {
        Calendar cc = Calendar.getInstance();
        int mYear = cc.get(Calendar.YEAR);
        int mMonth = cc.get(Calendar.MONTH);
        int mDay = cc.get(Calendar.DAY_OF_MONTH);


        try {


            mYear = cb.get(Calendar.YEAR);
            mMonth = cb.get(Calendar.MONTH);
            mDay = cb.get(Calendar.DAY_OF_MONTH);

        } catch (Exception ex) {
        }


        DatePickerDialog dpd = new DatePickerDialog(edt.getContext(),
                new DatePickerDialog.OnDateSetListener() {


                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        edt.setText((dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + " - " + ((monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : (monthOfYear + 1)) + " - " + year);


                        try {
                            cb.setTime(svars.sdf_user_friendly_date.parse(edt.getText().toString()));
                        } catch (Exception ex) {
                        }

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
        Calendar calendar_min = Calendar.getInstance();
        calendar_min.set(Calendar.YEAR, calendar_min.get(Calendar.YEAR));


        dpd.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());

        //  dpd.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
    }

    public static String confirmation_email(String code, String name) {

        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title></title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
                "    <style type=\"text/css\">\n" +
                "        @media screen {\n" +
                "            @font-face {\n" +
                "                font-family: 'Lato';\n" +
                "                font-style: normal;\n" +
                "                font-weight: 400;\n" +
                "                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');\n" +
                "            }\n" +
                "\n" +
                "            @font-face {\n" +
                "                font-family: 'Lato';\n" +
                "                font-style: normal;\n" +
                "                font-weight: 700;\n" +
                "                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');\n" +
                "            }\n" +
                "\n" +
                "            @font-face {\n" +
                "                font-family: 'Lato';\n" +
                "                font-style: italic;\n" +
                "                font-weight: 400;\n" +
                "                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');\n" +
                "            }\n" +
                "\n" +
                "            @font-face {\n" +
                "                font-family: 'Lato';\n" +
                "                font-style: italic;\n" +
                "                font-weight: 700;\n" +
                "                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        /* CLIENT-SPECIFIC STYLES */\n" +
                "        body,\n" +
                "        table,\n" +
                "        td,\n" +
                "        a {\n" +
                "            -webkit-text-size-adjust: 100%;\n" +
                "            -ms-text-size-adjust: 100%;\n" +
                "        }\n" +
                "\n" +
                "        table,\n" +
                "        td {\n" +
                "            mso-table-lspace: 0pt;\n" +
                "            mso-table-rspace: 0pt;\n" +
                "        }\n" +
                "\n" +
                "        img {\n" +
                "            -ms-interpolation-mode: bicubic;\n" +
                "        }\n" +
                "\n" +
                "        /* RESET STYLES */\n" +
                "        img {\n" +
                "            border: 0;\n" +
                "            height: auto;\n" +
                "            line-height: 100%;\n" +
                "            outline: none;\n" +
                "            text-decoration: none;\n" +
                "        }\n" +
                "\n" +
                "        table {\n" +
                "            border-collapse: collapse !important;\n" +
                "        }\n" +
                "\n" +
                "        body {\n" +
                "            height: 100% !important;\n" +
                "            margin: 0 !important;\n" +
                "            padding: 0 !important;\n" +
                "            width: 100% !important;\n" +
                "        }\n" +
                "\n" +
                "        /* iOS BLUE LINKS */\n" +
                "        a[x-apple-data-detectors] {\n" +
                "            color: inherit !important;\n" +
                "            text-decoration: none !important;\n" +
                "            font-size: inherit !important;\n" +
                "            font-family: inherit !important;\n" +
                "            font-weight: inherit !important;\n" +
                "            line-height: inherit !important;\n" +
                "        }\n" +
                "\n" +
                "        /* MOBILE STYLES */\n" +
                "        @media screen and (max-width:600px) {\n" +
                "            h1 {\n" +
                "                font-size: 32px !important;\n" +
                "                line-height: 32px !important;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        /* ANDROID CENTER FIX */\n" +
                "        div[style*=\"margin: 16px 0;\"] {\n" +
                "            margin: 0 !important;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body style=\"background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;\">\n" +
                "    <!-- HIDDEN PREHEADER TEXT -->\n" +
                "    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\"> We're thrilled to have you here! Get ready to dive into your new account. </div>\n" +
                "    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
                "        <!-- LOGO -->\n" +
                "        <tr>\n" +
                "            <td bgcolor=\"#000000\" align=\"center\">\n" +
                "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                    <tr>\n" +
                "                        <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 40px 10px;\"> </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td bgcolor=\"#000000\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n" +
                "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                    <tr>\n" +
                "                        <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;\">\n" +
                "                            <h1 style=\"font-size: 48px; font-weight: 400; margin: 2;\">Welcome!</h1> <img src=\" https://img.icons8.com/clouds/100/000000/handshake.png\" width=\"125\" height=\"120\" style=\"display: block; border: 0px;\" />\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n" +
                "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                    <tr>\n" +
                "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n" +
                "                            <p style=\"margin: 0;\">Hi " + name + ", we're excited to have you get started. First, you need to confirm your account. Just enter the code below on the app.</p>\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                    <tr>\n" +
                "                        <td bgcolor=\"#ffffff\" align=\"left\">\n" +
                "                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                <tr>\n" +
                "                                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 30px 60px 30px;\">\n" +
                "                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"center\" style=\"border-radius: 3px;\" bgcolor=\"#696969\"><a  style=\"font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #000000; display: inline-block;\">" + code + "</a></td>\n" +
                "                                            </tr>\n" +
                "                                        </table>\n" +
                "                                    </td>\n" +
                "                                </tr>\n" +
                "                            </table>\n" +
                "                        </td>\n" +
                "                    </tr> <!-- COPY -->\n" +
                "                    \n" +
                "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n" +
                "                            <p style=\"margin: 0;\">If you have any questions, just reply to this email—we're always happy to help out.</p>\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                    <tr>\n" +
                "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n" +
                "                            <p style=\"margin: 0;\">Cheers,<br>Capture solutions Team</p>\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 10px 0px 10px;\">\n" +
                "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                    <tr>\n" +
                "                        <td bgcolor=\"#696969\" align=\"center\" style=\"padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n" +
                "                            <h2 style=\"font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;\">Need more help?</h2>\n" +
                "                            <p style=\"margin: 0;\"><a href=\"#\" target=\"_blank\" style=\"color: #ffffff;\">We&rsquo;re here to help you out</a></p>\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n" +
                "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
                "                   \n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</body>\n" +
                "\n" +
                "</html>";
    }
}
