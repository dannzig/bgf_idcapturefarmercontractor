package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.GroupStages;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.NurseryTypes;
import sparta.realm.apps.farmercontractor.models.SeedlingDistributionPlan;
import sparta.realm.apps.farmercontractor.models.TrainingGroup;
import sparta.realm.apps.farmercontractor.models.TrainingTypes;


public class DistributionGroupAdapter extends RecyclerView.Adapter<DistributionGroupAdapter.view> {

    Context cntxt;
    public ArrayList<SeedlingDistributionPlan> items;
    onItemClickListener listener;
    GroupStages groupStage;

    public interface onItemClickListener {

        void onItemClick(SeedlingDistributionPlan mem, View view);
    }


    public DistributionGroupAdapter(ArrayList<SeedlingDistributionPlan> items, onItemClickListener listener) {

        this.items = items;
        this.listener = listener;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_distribution_group, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name_title,name, description_title,description, member_count, on_groups, on_groups_formation, groupStageTxt, newly_formed, organized_group, ready_for_certification, issued_with_certificate, group_stage_title, training_title;
        public sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner aa_agent_spn,group_stage, country,county_input,location_input,sub_location_input,village_input,site_input;
        public Button activate_btn;
        public TextView group_name, remarks;

        int position;


        view(View itemView) {
            super(itemView);

            name_title = itemView.findViewById(R.id.name_title);
            group_name = itemView.findViewById(R.id.group_name);
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.info1);
            description_title = itemView.findViewById(R.id.info1_title);
            member_count = itemView.findViewById(R.id.member_count);


            aa_agent_spn = itemView.findViewById(R.id.aa);

            //group_stage = itemView.findViewById(R.id.groupStage);
            country = itemView.findViewById(R.id.country);
            county_input = itemView.findViewById(R.id.county_input);
            location_input = itemView.findViewById(R.id.location_input);
            sub_location_input = itemView.findViewById(R.id.sub_location_input);
            village_input = itemView.findViewById(R.id.village_input);
            site_input = itemView.findViewById(R.id.site_input);

            group_stage_title = itemView.findViewById(R.id.group_stage_title);
            training_title = itemView.findViewById(R.id.training_title);
            remarks = itemView.findViewById(R.id.remarks);


            itemView.setClickable(true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(position), view);
                }
            });

            group_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(position), view);
                }
            });
            description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(position), view);
                }
            });
        }

        void populate(int position) {

            this.position = position;
            SeedlingDistributionPlan obj = items.get(position);

            NurseryTypes nurseryTypes = Realm.databaseManager.loadObject(NurseryTypes.class, new Query().setTableFilters("sid= '" + obj.nursery_type_id + "'"));

            MemberGroup memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid= '" + obj.group_id + "'"));


            //TrainingTypes trainingTypes = Realm.databaseManager.loadObject(TrainingTypes.class, new Query().setTableFilters("sid= '" + obj.training_type_id + "'"));

            //groupStage = Realm.databaseManager.loadObject(GroupStages.class, new Query().setTableFilters(obj.group_stages_id == null || obj.group_stages_id.length() < 1 ? "sid='1'" : "sid='" + obj.group_stages_id + "'"));
            name.setText(obj.name);
            group_name.setText(memberGroup.name);
            description.setText(nurseryTypes.name);

            //description.setVisibility(obj.date == null ? View.GONE : obj.date.length() < 1 ? View.GONE : View.VISIBLE);

            aa_agent_spn.setVisibility(View.GONE);
            //group_stage.setVisibility(View.GONE);
            country.setVisibility(View.GONE);
            county_input.setVisibility(View.GONE);
            location_input.setVisibility(View.GONE);
            sub_location_input.setVisibility(View.GONE);
            village_input.setVisibility(View.GONE);
            site_input.setVisibility(View.GONE);
            remarks.setVisibility(View.GONE);


            group_name.setFocusable(false);
            name.setFocusable(false);
            description.setFocusable(false);
//            name.setBackground(null);
//            description.setBackground(null);
            //groupStageTxt.setText(groupStage == null ? "Specify Group Stage" : groupStage.name);
            member_count.setText(obj.distribution_date.split("T")[0]);
            //member_count.setText(obj.distribution_date);
            //member_count.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, obj.transaction_no == null || obj.transaction_no.length() < 1 ? "group_id='" + obj.sid + "'" : "is_active = 'true' AND "+"group_transaction_no='" + obj.transaction_no + "' OR group_id='" + obj.sid + "'") + " members");

//SELECT * FROM member_info_table WHERE sid in(select member_id from group_participant where  sid='54') AND  UPPER(full_name) LIKE '%%' ORDER BY reg_time DESC  LIMIT 1000
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(items.get(position), view);

        }
    }
}
