package sparta.realm.apps.farmercontractor.utils;

import android.database.Cursor;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;

import static sparta.realm.Realm.realm;

public class Pager<RM> {
    public interface PagerCallback {
        default <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {

        }
    }

    Class<RM> realm_model;
    int pageSize;
    PagerCallback pagerCallback;
    String[] searchFields;
    ArrayList<RM> allRecords = new ArrayList<>();
    int pagerEventId;

    public Pager(Class<RM> realm_model, int pagerEventId, int pageSize, PagerCallback pagerCallback, String... searchFields) {
        this.realm_model = realm_model;
        this.pagerEventId = pagerEventId;
        this.pageSize = pageSize;
        this.pagerCallback = pagerCallback;
        this.searchFields = searchFields;

    }

    View prev, next;
    EditText search_text;
    TextView position_indicator;
    ProgressBar loading_bar;

    //    public Pager(Class<RM> realm_model, int pagerEventId, int pageSize, PagerCallback pagerCallback, EditText searchTextInput, View prev, View next, TextView positionIndicator, ProgressBar loadingBar, String... searchFields) {
//    public Pager(Class<RM> realm_model, int pagerEventId, int pageSize, PagerCallback pagerCallback, EditText searchTextInput, View prev, View next, TextView positionIndicator, ProgressBar loadingBar, String customQuery, String[] tableFilters, String... searchFields) {
    public Pager(Class<RM> realm_model, int pagerEventId, int pageSize, PagerCallback pagerCallback, EditText searchTextInput, View prev, View next, TextView positionIndicator, ProgressBar loadingBar, String customQuery, String[] tableFilters, String[] columns, String... searchFields) {
        this.realm_model = realm_model;
        this.pagerEventId = pagerEventId;
        this.pageSize = pageSize;
        this.pagerCallback = pagerCallback;
        this.prev = prev;
        this.next = next;
        this.search_text = searchTextInput;
        this.position_indicator = positionIndicator;
        this.loading_bar = loadingBar;
        this.customQuery = customQuery;
        this.tableFilters = tableFilters;
        this.columns = columns;
        this.searchFields = searchFields;
        initUi();

    }

    String searchTerm = "";

    public void search(String searchTerm) {
        this.searchTerm = searchTerm;
        reset_list();
    }

    public void next() {
        offset = ((pageSize + offset) < total) ? offset + pageSize : offset;
        reset_list();
    }

    public void previous() {
        offset = (offset != 0) ? offset - pageSize : offset;
        reset_list();
    }


    void initUi() {


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous();
            }
        });
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                offset = 0;
                pageSize = 100;
                search(search_text.getText().toString());

            }
        });
        reset_list();

    }


    public static int search_counter = 0;

    Thread search_thread;
    int offset = 0, total = 0;

    String[] tableFilters = new String[0];

    public void setTableFilters(String... tableFilters) {
        this.tableFilters = tableFilters == null ? new String[0] : tableFilters;
        reset_list();
    }

    String customQuery;

    public void setCustomQuery(String customQuery) {
        this.customQuery = customQuery;
        reset_list();
    }

    String[] columns;

    public void setColumns(String[] columns) {
        this.columns = columns;
        reset_list();
    }

    void reset_list() {
        String search_tearm = searchTerm;
        search_tearm = search_tearm == null ? "" : search_tearm.toUpperCase();
        loading_bar.setVisibility(View.VISIBLE);
        search_counter++;
        DatabaseManager.pagerEventMap.put(pagerEventId, search_counter);
        String finalSearch_tearm = search_tearm;
        search_thread = new Thread(() -> {

            int int_counter = search_counter;
            String column = "sid";
            String inputField = "0";
            allRecords = Realm.databaseManager.loadObjectArray(realm_model, pagerEventId, search_counter, new Query()
                    .setColumns(columns)
                    .setCustomQuery(customQuery)
                    .setTableFilters(DatabaseManager.concatenate(tableFilters, new String[]{conccat_sql_or_like_filters(searchFields, finalSearch_tearm)}))
                    .setOffset(offset)
                    .setLimit(pageSize)
                    .addOrderFilters("reg_time", false));
                    //.setTableFilters(" "+ column +" != '" + inputField + "'"));

            if (search_counter == int_counter && allRecords != null) {
//                total = Realm.databaseManager.getRecordCount(realm_model, new Query()
//                        .setTableFilters(DatabaseManager.concatenate(tableFilters,new String[]{ conccat_sql_or_like_filters(searchFields, finalSearch_tearm)})));
                total = Realm.databaseManager.getRecordCount(realm_model, new Query()
                        .setColumns(columns)
                        .setCustomQuery(customQuery)
                        .setTableFilters(DatabaseManager.concatenate(tableFilters, new String[]{conccat_sql_or_like_filters(searchFields, finalSearch_tearm)})));


                loading_bar.post(() -> {

                    try {
                        pagerCallback.onDataRefreshed(allRecords, offset, offset + allRecords.size(), total);
                        position_indicator.setText(offset + " - " + (offset + allRecords.size()) + " of " + total);
                    } catch (Exception ex) {

                    }

                    loading_bar.setVisibility(View.GONE);

                });
                Runtime.getRuntime().gc();

            }

        });

        search_thread.start();
    }


    public String conccat_sql_or_filters(String[] str_to_join, String searchTerm) {
        String result = "";
        for (int i = 0; i < str_to_join.length; i++) {
            result = result + (i == 0 ? "WHERE " : " OR ") + str_to_join[i] + "='" + searchTerm + "'";
        }
        return result;

    }

    public String conccat_sql_or_like_filters(String[] str_to_join, String searchTerm) {
        String result = "";
        for (int i = 0; i < str_to_join.length; i++) {
            result = result + (i == 0 ? " UPPER(" : " OR UPPER(") + str_to_join[i] + ") LIKE '%" + searchTerm + "%'";
        }
        return result;

    }


    public <RM> ArrayList<RM> loadObjectArray(Class<RM> realm_model, int pagerEventId, int searchIndex, Query query) {
        return loadObjectArray(realm_model, pagerEventId, searchIndex, null, query.columns, query.tableFilters, query.order_filters, query.order_asc, query.limit, query.offset);

    }

    public static HashMap<Integer, Integer> pagerEventMap = new HashMap<>();

    public <RM> ArrayList<RM> loadObjectArray(Class<RM> realm_model, int pagerEventId, int searchIndex, String customQuery, String[] columns, String[] table_filters, String[] order_filters, boolean order_asc, int limit, int offset) {
        ArrayList<RM> objs = new ArrayList<RM>();
        String table_name = realm.getPackageTable(realm_model.getName());
//        String qry = "SELECT " + (columns == null ? "*" : Realm.databaseManager.concatString(",", columns)) + " FROM " + table_name + (table_filters == null ? "" : " " + Realm.databaseManager.conccat_sql_filters(table_filters)) + (order_filters == null ? "" : " ORDER BY " + Realm.databaseManager.concatString(",", order_filters) + " " + (order_asc ? "ASC" : "DESC")) + (limit <= 0 ? "" : " LIMIT " + limit + (offset <= 0 ? "" : " OFFSET " + offset));
        String qry = "SELECT " + (columns == null ? "*" : Realm.databaseManager.concatString(",", columns)) + " FROM " + (customQuery == null ? table_name : "(" + customQuery + ")") + table_name + (table_filters == null ? "" : " " + Realm.databaseManager.conccat_sql_filters(table_filters)) + (order_filters == null ? "" : " ORDER BY " + Realm.databaseManager.concatString(",", order_filters) + " " + (order_asc ? "ASC" : "DESC")) + (limit <= 0 ? "" : " LIMIT " + limit + (offset <= 0 ? "" : " OFFSET " + offset));
        Cursor c = Realm.databaseManager.database.rawQuery(qry, null);


        if (c.moveToFirst()) {
            do {
                objs.add((RM) realm.getObjectFromCursor(c, realm_model.getName()));
            } while (c.moveToNext() && pagerEventMap.get(pagerEventId) == searchIndex);
        }
        c.close();


        return pagerEventMap.get(pagerEventId) == searchIndex ? objs : null;
//        return currentActiveIndex(pagerEventId)==searchIndex?objs:null;
    }

}
