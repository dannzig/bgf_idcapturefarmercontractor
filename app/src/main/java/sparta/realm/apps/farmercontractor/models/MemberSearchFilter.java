package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.RealmModel;

import java.io.Serializable;
import java.util.ArrayList;


@DynamicClass(table_name = "member_search_filter")
public class MemberSearchFilter extends RealmModel implements Serializable {


    //    @DynamicProperty(json_key = "Country")
    public SearchFilterItem country = new SearchFilterItem("Country","country",Country.class.getName());


    //   @DynamicProperty(json_key = "county")
    public SearchFilterItem county = new SearchFilterItem("County","county",County.class.getName());

    //  @DynamicProperty(json_key = "location")
    public SearchFilterItem location = new SearchFilterItem("Location","location",Location.class.getName());

    //  @DynamicProperty(json_key = "sub_location")
    public SearchFilterItem sub_location = new SearchFilterItem("Sub loc","sub_location",SubLocation.class.getName());

    // @DynamicProperty(json_key = "village")
    public SearchFilterItem village = new SearchFilterItem("Village","village",Village.class.getName());

    // @DynamicProperty(json_key = "site")
    public SearchFilterItem site = new SearchFilterItem("Site","site",Site.class.getName());


    public ArrayList<SearchFilterItem> activeMemberSearchFilterItems() {
        ArrayList<SearchFilterItem> searchFilterItems = new ArrayList<>();
        if (country.active) {
            searchFilterItems.add(country);
        }
        if (county.active) {
            searchFilterItems.add(county);
        }
        if (location.active) {
            searchFilterItems.add(location);
        }
        if (sub_location.active) {
            searchFilterItems.add(sub_location);
        }
        if (village.active) {
            searchFilterItems.add(village);
        }
        if (site.active) {
            searchFilterItems.add(site);
        }

        return searchFilterItems;
    }

    public String[] getTableFilters()
    {
        ArrayList<SearchFilterItem> searchFilterItems =activeMemberSearchFilterItems();
        int tot_size= searchFilterItems.size();
        if(tot_size<1){
            return null;
        }
        String[] table_filters=new String[tot_size];
        for (int i=0;i<tot_size;i++){
            SearchFilterItem searchFilterItem = searchFilterItems.get(i);
            table_filters[i]= searchFilterItem.column+"='"+ searchFilterItem.value+"'";
        }

return table_filters;

    }

    public MemberSearchFilter() {


    }

}
