package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "location")
@SyncDescription(service_id = "1",chunk_size = 100000, service_name = "Location", download_link = "/Configurations/Location/RebindGrid", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class Location extends SelectionData implements Serializable {


    @DynamicProperty(json_key = "county_id")
    public String county;




    public Location() {
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }

}
