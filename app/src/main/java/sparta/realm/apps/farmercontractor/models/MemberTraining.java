package sparta.realm.apps.farmercontractor.models;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;

@DynamicClass(table_name = "member_training")
@SyncDescription(service_name = "Member Training", download_link = "/FarmersContract/Group/FarmersTrainingActivitesList", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
@SyncDescription(service_name = "Member Training", upload_link = "/FarmersContract/Group/AddFarmerTrainingActivies",service_type = SyncDescription.service_type.Upload)

public class MemberTraining  extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "group_id")
    public String group_id;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "country_id")
    public String country_id;

    @DynamicProperty(json_key = "county_id")
    public String county_id;

    @DynamicProperty(json_key = "location_id")
    public String location_id;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location_id;

    @DynamicProperty(json_key = "village_id")
    public String village_id;

    @DynamicProperty(json_key = "site_id")
    public String site_id;

    @DynamicProperty(json_key = "agroforest_agent_id")
    public String agroforest_agent_id;

    @DynamicProperty(json_key = "training_activity_id")
    public String training_activity_id;

    @DynamicProperty(json_key = "date")
    public String date;

    @DynamicProperty(json_key = "Remarks")
    public String Remarks;

    public MemberTraining(String group_id, String member_id, String country_id, String county_id, String location_id, String sub_location_id, String village_id, String site_id,String agroforest_agent_id, String training_activity_id, String remarks) {
        this.group_id = group_id;
        this.member_id = member_id;
        this.country_id = country_id;
        this.county_id = county_id;
        this.location_id = location_id;
        this.sub_location_id = sub_location_id;
        this.village_id = village_id;
        this.site_id = site_id;
        this.agroforest_agent_id = agroforest_agent_id;
        this.training_activity_id = training_activity_id;
        this.Remarks = remarks;

        //this.transaction_no = transaction_no;

        this.transaction_no = svars.getTransactionNo();


        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public MemberTraining() {
    }
}

