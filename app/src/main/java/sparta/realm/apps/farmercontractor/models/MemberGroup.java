package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "member_group")
@SyncDescription(service_name = "Member group", download_link = "/FarmersContract/Group/ActiveGroups", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
@SyncDescription(service_name = "Member group", upload_link = "/FarmersContract/Group/AddFarmGroup",service_type = SyncDescription.service_type.Upload)
public class MemberGroup extends RealmModel implements Serializable {


    @DynamicProperty(json_key = "group_description")
    public String description;

    @DynamicProperty(json_key = "group_name")
    public String name;

    @DynamicProperty(json_key = "group_representative")
    public String group_representative;

    @DynamicProperty(json_key = "location")
    public String location;

    @DynamicProperty(json_key = "group_training")
    public String group_training;

    @DynamicProperty(json_key = "triaining_group_formation")
    public String triaining_group_formation;

    @DynamicProperty(json_key = "newly_formed")
    public String newly_formed;

    @DynamicProperty(json_key = "organized_group")
    public String organized_group;

    @DynamicProperty(json_key = "ready_for_certification")
    public String ready_for_certification;

    @DynamicProperty(json_key = "with_certification")
    public String with_certification;

    @DynamicProperty(json_key = "cr")
    public String CR;

    @DynamicProperty(json_key = "agroforest_agent_id")
    public String agroforest_agent_id;

    @DynamicProperty(json_key = "group_stages_id")
    public String group_stages_id;

    @DynamicProperty(json_key = "enrollment_type")
    public String enrollment_type;

    @DynamicProperty(json_key = "reg_start_time")
    public String reg_start_time;

    @DynamicProperty(json_key = "country_id")
    public String country_id;

    @DynamicProperty(json_key = "county_id")
    public String county;

    @DynamicProperty(json_key = "location_id")
    public String locationId;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location;

    @DynamicProperty(json_key = "village_id")
    public String village;

    @DynamicProperty(json_key = "site_id")
    public String site;


    public MemberGroup(String transaction_no,String description, String name, String group_representative, String group_training, String triaining_group_formation, String CR, String agroforest_agent_id, String group_stages_id, String enrollment_type, String reg_start_time, String country_id, String county, String locationId, String sub_location, String village, String site, String newly_formed,String organized_group, String ready_for_certification, String with_certification, String is_active) {
        this.description = description;
        this.name = name;
        this.group_representative = group_representative;
        this.group_training = group_training;
        this.triaining_group_formation = triaining_group_formation;
        this.CR = CR;
        this.agroforest_agent_id = agroforest_agent_id;
        this.group_stages_id = group_stages_id;
        this.enrollment_type = enrollment_type;
        this.reg_start_time = reg_start_time;
        this.country_id = country_id;
        this.county = county;
        this.locationId = locationId;
        this.sub_location = sub_location;
        this.village = village;
        this.site = site;
        this.newly_formed = newly_formed;
        this.organized_group = organized_group;
        this.ready_for_certification = ready_for_certification;
        this.with_certification = with_certification;
        this.data_status = is_active;

        this.transaction_no = transaction_no;

        //this.transaction_no = svars.getTransactionNo();


        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public MemberGroup(String sid,String transaction_no,String description, String name, String group_representative, String group_training, String triaining_group_formation, String CR, String agroforest_agent_id, String group_stages_id, String enrollment_type, String reg_start_time, String country_id, String county, String locationId, String sub_location, String village, String site) {
        this.description = description;
        this.name = name;
        this.group_representative = group_representative;
        this.group_training = group_training;
        this.triaining_group_formation = triaining_group_formation;
        this.CR = CR;
        this.agroforest_agent_id = agroforest_agent_id;
        this.group_stages_id = group_stages_id;
        this.enrollment_type = enrollment_type;
        this.reg_start_time = reg_start_time;
        this.country_id = country_id;
        this.county = county;
        this.locationId = locationId;
        this.sub_location = sub_location;
        this.village = village;
        this.site = site;
        this.sid = sid;

        this.transaction_no = transaction_no;

        //this.transaction_no = svars.getTransactionNo();


        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public MemberGroup(String name, String description, String group_representative, String location, String group_training, String triaining_group_formation, String CR, String agroforest_agent_id, String group_stages_id, String country_id) {
        this.description = description;
        this.name = name;
        this.group_representative = group_representative;
        this.location = location;
        this.group_training = group_training;
        this.triaining_group_formation = triaining_group_formation;
        this.CR = CR;
        this.agroforest_agent_id = agroforest_agent_id;
        this.group_stages_id = group_stages_id;
        this.country_id = country_id;
        this.transaction_no = svars.getTransactionNo();


        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;


    }
    public MemberGroup(String name, String description) {
        this.description = description;
        this.name = name;
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;

    }
    public MemberGroup(String enrollment_type) {
        this.enrollment_type = enrollment_type;
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;

    }

    public MemberGroup() {


    }

}
