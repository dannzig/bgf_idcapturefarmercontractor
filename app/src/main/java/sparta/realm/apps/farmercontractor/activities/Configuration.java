package sparta.realm.apps.farmercontractor.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.databinding.ActivityConfigurationBinding;
import sparta.realm.apps.farmercontractor.databinding.ActivityMemberSummaryBinding;

public class Configuration extends AppCompatActivity {

    ActivityConfigurationBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityConfigurationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initUi();

    }



    void initUi()
    {
        setupToolbar(binding.include.toolbar);

    }


    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.registration_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "Clicked Menu", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.clear_all_menu:
                Toast.makeText(this, "Clicked clear all", Toast.LENGTH_LONG).show();
                break;
            case R.id.exit_registration:
                Toast.makeText(this, "Clicked Exit enrolment", Toast.LENGTH_LONG).show();
//                showQuitDialog();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}