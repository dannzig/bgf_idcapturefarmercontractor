package sparta.realm.apps.farmercontractor.activities;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.google.android.gms.maps.GoogleMap;

import java.util.Calendar;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.MyApplication;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.activities.registration.Page3;
import sparta.realm.apps.farmercontractor.adapters.MemberAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityAddendumBinding;
import sparta.realm.apps.farmercontractor.databinding.ActivitySyncReportBinding;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.FarmerContract;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.MemberImage;
import sparta.realm.apps.farmercontractor.models.SeedlingType;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Survey;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.models.system.ValidationRules;
import sparta.realm.apps.farmercontractor.utils.FormTools.FormEdittext;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartautils.Gpsprobe_r;
import sparta.realm.spartautils.s_bitmap_handler;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.Conversions;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

public class Addendum extends AppCompatActivity {

    Member member;
    String latitude = "0.0";
    String longitude = "0.0";
    String accuracy = "0.0";
    String member_sid;

    ActivityAddendumBinding binding;
    ActivityResultLauncher<Intent> activityLauncherForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + data.getStringExtra("sid") + "'", "sid IS NOT NULL"));
                        member.profile_photo = Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_id='" + member.sid + "'"));

                    } else {
                        finish();
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddendumBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        latitude = getIntent().getStringExtra("latitude");
        longitude = getIntent().getStringExtra("longitude");
        accuracy = getIntent().getStringExtra("accuracy");
        //member_sid = getIntent().getStringExtra("mem_sid");

        member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
        if (member == null && latitude == null) {

            Intent intent = new Intent(this, MemberRecords.class);
            intent.putExtra("search", true);
            intent.putExtra("defaultTableFilters", new String[]{"sid in (select member_id from farmer_contract where is_contract_active == 'true')"});

            activityLauncherForResult.launch(intent);
        }
        if (latitude == null) {
            Intent intent = new Intent(Addendum.this, AddendumLandMapping.class);
            intent.putExtra("member_fname", member.full_name + "");
            intent.putExtra("member_nat_id", member.nat_id + "");
            intent.putExtra("member_sid", member.sid + "");
            startActivity(intent);
        }else {

            member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("mem_sid") + "'"));
            initUi();
        }

        if (DatabaseManager.gps == null) {
            DatabaseManager.gps = new Gpsprobe_r(this);

        }
        if (!DatabaseManager.gps.canGetLocation()){
            showSettingsAlert();
        }

    }

    FarmerContract farmerContract;

    @Override
    protected void onResume() {
        super.onResume();
        if (member != null) {
            farmerContract = Realm.databaseManager.loadObject(FarmerContract.class, new Query().setTableFilters("member_id='" + member.sid + "'"));
            if (farmerContract == null) {
                finish();
                return;
            }
            initUi();
        }
    }

    //district ,subcounty,parish,village
    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);
            binding.seedlingTypeInput.setDataset(Realm.databaseManager.loadObjectArray(SeedlingType.class, new Query().setColumns("_id", "sid", "item_name as name").setTableFilters("country='" + input + "'")), seedlingTypeInputListener);
            binding.countyInput.setInput(member.county);
            binding.siteInput.setInput(member.site);
//            binding.seedlingTypeInput.setInput(member.seedling_type);
            binding.seedlingTypeInput.setInput(null);
            binding.locationInput.setInput(null);
            binding.subLocationInput.setInput(null);
            binding.villageInput.setInput(null);
            if (input.equals("110")) {
                binding.countyInput.setTitle("County");
                binding.locationInput.setTitle("Ward");
                binding.subLocationInput.setTitle("Sub-location");

            } else {

                binding.countyInput.setTitle("District");
                binding.locationInput.setTitle("Sub-county");
                binding.subLocationInput.setTitle("Parish");
            }
        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
            binding.locationInput.setInput(member.location);
            binding.subLocationInput.setInput(null);
            binding.villageInput.setInput(null);

        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
            binding.subLocationInput.setInput(member.sub_location);
            binding.villageInput.setInput(null);
        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
            binding.villageInput.setInput(member.village);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            binding.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), countyInputListener);
//            binding.villageInput.setInput(Globals.registeringMember().village);
        }
    };
    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

        }
    };
    SearchSpinner.InputListener seedlingTypeInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            SeedlingType seedlingType = Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + input + "'"));
            if (seedlingType != null) {
                binding.yearsOfMaturityInput.setInput(seedlingType.year_of_maturity);
                binding.dbhAtMaturityInput.setInput(seedlingType.dbh);
            }

        }
    };
    FormEdittext.InputListener totalSeedlings = new FormEdittext.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            //FormEdittext.InputListener.super.onInputAvailable(valid, input);

            if (binding.totalSeedlingsInput.getInput().length() > 0) {
                int number_trees = Integer.parseInt(binding.totalSeedlingsInput.getInput());
                binding.donatedSeedlingsInput.setInput("" + number_trees / 2);
            }
        }
    };

    void initUi() {
        setupToolbar(binding.include.toolbar);
        ValidationRules validationRules = new ValidationRules();
        validationRules.mandatory = ValidationRules.MandatoryStatus.Mandatory.ordinal() + "";
        validationRules.text_input_type = ValidationRules.TextInputType.numeric.ordinal() + "";
        binding.farmerLandSizeInput.setValidationRules(validationRules);
        binding.totalSeedlingsInput.setValidationRules(validationRules);
        binding.donatedSeedlingsInput.setValidationRules(validationRules);
        binding.dbhAtMaturityInput.setValidationRules(validationRules);
        binding.yearsOfMaturityInput.setValidationRules(validationRules);
        loadMemberInfo();
        binding.countryInput.setDataset(Country.class.getName(), countryInputListener);

        binding.totalSeedlingsInput.setInputListener(totalSeedlings);
        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSaveDialog();
            }
        });
        populate(member);
    }
    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Addendum.this);
        alertDialog.setTitle("GPS settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Enable GPS in settings menu");
        alertDialog.setCancelable(false);

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                Toast.makeText(Addendum.this, "ACCEPT APP TO ACCESS LOACTION TO CONTINUE", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                Addendum.this.startActivity(intent);

            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    void populate(Member member) {
        binding.countryInput.setInput(member.country);
        //binding.farmerLandSizeInput.setInput(member.land_size);
        //binding.totalSeedlingsInput.setInput(member.total_seedlings);
        //binding.donatedSeedlingsInput.setInput(member.donated_seedlings);
//        binding.yearsOfMaturityInput.setInput(member.years_of_maturity);
//        binding.dbhAtMaturityInput.setInput(member.dbh_at_maturity);

    }

    void loadMemberInfo() {
        binding.include2.info1.setText("Nat ID: " + member.nat_id);
        binding.include2.name.setText(member.full_name);
        member.profile_photo = Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_id='" + member.sid + "'"));
        if (member.profile_photo != null) {

            binding.include2.icon.setImageURI(null);
            binding.include2.icon.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + member.profile_photo.image));

        }
    }

    boolean recordSaved = false;
    boolean coordinatesPicked = false;

    void showSaveDialog() {
        Log.e("AddendumActivity", "Latitude: " + latitude);
        if (validated() ) {
            sparta.realm.apps.farmercontractor.models.Addendum addendum = new sparta.realm.apps.farmercontractor.models.Addendum(
                    member.sid
                    , farmerContract.contract_no
                    , binding.countryInput.getInput()
                    , binding.countyInput.getInput()
                    , binding.locationInput.getInput()
                    , binding.subLocationInput.getInput()
                    , binding.villageInput.getInput()
                    , binding.siteInput.getInput()
                    , binding.farmerLandSizeInput.getInput()
                    , binding.totalSeedlingsInput.getInput()
                    , binding.donatedSeedlingsInput.getInput()
                    , binding.seedlingTypeInput.getInput()
                    , binding.yearsOfMaturityInput.getInput()
                    , binding.dbhAtMaturityInput.getInput()
                    , latitude + ""
                    , longitude + ""
                    , accuracy + "");
            View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_addendum_confirmation, null);
            final AlertDialog ald = new AlertDialog.Builder(this)
                    .setView(aldv)
                    .show();
            ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button save = aldv.findViewById(R.id.save);
            save.setOnClickListener(view -> {

                if (recordSaved) {
                    new T12Printer(Addendum.this, printingObjects -> {
                        PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                        CanvasPrint cp = new CanvasPrint();
                        cp.init(PrinterType.TIII);
                        cp.setUseSplit(true);
                        cp.setTextAlignRight(false);
                        cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(Addendum.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                        FontProperty fp = new FontProperty();
                        fp.setFont(true, false, true, false, 27, null);
                        cp.setFontProperty(fp);
                        cp.drawText(100, "Contract addendum");
                        cp.drawText("");
                        fp.setFont(true, false, false, false, 25, null);
                        cp.setFontProperty(fp);
                        cp.drawText("Transaction no: ");
                        cp.drawText(addendum.transaction_no);
                        fp.setFont(true, false, false, false, 25, null);
                        cp.setFontProperty(fp);
                        cp.setTextAlignRight(false);
                        cp.drawText("");
                        cp.drawText("Farmer info");
                        fp.setFont(true, false, false, false, 20, null);
                        cp.setFontProperty(fp);
                        cp.drawText("");
                        cp.drawText("Name: " + member.full_name);
                        cp.drawText("ID number: " + member.nat_id);
                        cp.drawText("Phone number : " + member.phone_no);

                        cp.drawText("");
                        fp.setFont(true, false, false, false, 25, null);
                        cp.setFontProperty(fp);
                        cp.setTextAlignRight(false);
                        cp.drawText("Land info");
                        fp.setFont(true, false, false, false, 20, null);
                        cp.setFontProperty(fp);
                        cp.setTextAlignRight(false);
                        cp.drawText("");
                        cp.drawText("Land size:" + addendum.land_size);
                        cp.drawText("Country:" + Realm.databaseManager.loadObject(Country.class, new Query().setTableFilters("sid='" + addendum.country + "'")).name);
                        cp.drawText("County:" + Realm.databaseManager.loadObject(County.class, new Query().setTableFilters("sid='" + addendum.county + "'")).name);
                        cp.drawText("Location:" + Realm.databaseManager.loadObject(Location.class, new Query().setTableFilters("sid='" + addendum.location + "'")).name);
                        cp.drawText("Sub-Location:" + Realm.databaseManager.loadObject(SubLocation.class, new Query().setTableFilters("sid='" + addendum.sub_location + "'")).name);
                        cp.drawText("Village:" + Realm.databaseManager.loadObject(Village.class, new Query().setTableFilters("sid='" + addendum.village + "'")).name);
                        cp.drawText("Site:" + Realm.databaseManager.loadObject(Site.class, new Query().setTableFilters("sid='" + addendum.site + "'")).name);
                        cp.drawText("Seedling type:" + Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + addendum.seedling_type + "'")).name);
                        cp.drawText("Total seedlings:" + addendum.total_seedlings);
                        cp.drawText("Donated seedlings:" + addendum.donated_seedlings);
                        cp.drawText("Years of maturity:" + addendum.years_of_maturity);
                        cp.drawText("DBH at maturity:" + addendum.dbh_at_maturity);
                        cp.drawText("");

                        fp.setFont(false, false, false, false, 18, null);
                        cp.setFontProperty(fp);
                        cp.setTextAlignRight(false);
                        cp.drawText("Report generated by: " + Globals.user().username);
                        cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                        cp.drawText("");
                        mPrinter.printImage(cp.getCanvasImage());
                        Barcode bc = new Barcode(PDF417, 5, 300, 50, addendum.transaction_no);
                        mPrinter.printBarCode(bc);
                        mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                        mPrinter.closeConnection();
                    }).print();

                } else {
                    recordSaved = Realm.databaseManager.insertObject(addendum);
                    if (recordSaved) {
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(addendum).get(1));
                        save.setText("Print");

                    }
                }

            });
            ((TextView) aldv.findViewById(R.id.sub_title)).setText(member.full_name);
            ((TextView) aldv.findViewById(R.id.content)).setText("Confirm details of the addendum.Below is the list of entries" + "\n\n" +
                    "Land size: " + addendum.land_size + " acres\n" +
                    "Village: " + Realm.databaseManager.loadObject(Village.class, new Query().setTableFilters("sid='" + addendum.village + "'")).name + "\n" +
                    "Site: " + Realm.databaseManager.loadObject(Site.class, new Query().setTableFilters("sid='" + addendum.site + "'")).name + "\n" +
                    "Total seedlings: " + addendum.total_seedlings + "\n" +
                    "Donated seedlings: " + addendum.donated_seedlings + "\n" +
                    "Seedling type: " + Realm.databaseManager.loadObject(SeedlingType.class, new Query().setTableFilters("sid='" + addendum.seedling_type + "'")).name + "\n" +
                    "Years of maturity: " + addendum.years_of_maturity + "\n" +
                    "DBH at maturity: " + addendum.dbh_at_maturity + "\n" +
                    "Latitude: " + latitude + "\n" +
                    "Longitude: " + longitude + "\n" +
                    "Accuracy: " + accuracy );

            aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
                ald.dismiss();
                if (recordSaved) {

                    Intent intent = new Intent(Addendum.this, MainActivity.class);
                    startActivity(intent);
                    //finish();
                }

            });

        }else{

            Toast.makeText(Addendum.this, "Location Latitude= 0.0, Longitude = 0.0 OR Not Validated", Toast.LENGTH_LONG).show();

        }

    }

    boolean validated() {
        return binding.countyInput.isInputValid()
                & binding.locationInput.isInputValid()
                & binding.subLocationInput.isInputValid()
                & binding.villageInput.isInputValid()
                & binding.siteInput.isInputValid()
                & binding.farmerLandSizeInput.isInputValid()
                & binding.totalSeedlingsInput.isInputValid()
                & binding.donatedSeedlingsInput.isInputValid()
                & binding.yearsOfMaturityInput.isInputValid()
                & binding.dbhAtMaturityInput.isInputValid();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.non_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                onBackPressed();
                break;
            case R.id.config:
                startActivity(new Intent(this, Configuration.class));
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}