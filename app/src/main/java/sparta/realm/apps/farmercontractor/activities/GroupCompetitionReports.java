package sparta.realm.apps.farmercontractor.activities;

import static android.graphics.Color.rgb;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.realm.annotations.sync_status;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.adapters.CompetitionQuestionReportAdapter;
import sparta.realm.apps.farmercontractor.adapters.MemberAdapter;
import sparta.realm.apps.farmercontractor.adapters.SelectedMemberAdapter;
import sparta.realm.apps.farmercontractor.adapters.SurveyQuestionAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupCompetitionReportsBinding;
import sparta.realm.apps.farmercontractor.models.AgroForestAgent;
import sparta.realm.apps.farmercontractor.models.CompetitionAssessmentSurveyEntry;
import sparta.realm.apps.farmercontractor.models.CompetitionQuestionEntry;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Survey;
import sparta.realm.apps.farmercontractor.models.SurveyQuestion;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.FastScrolRecyclerview.FastScrollRecyclerViewItemDecoration;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.apps.farmercontractor.utils.Pager;
import sparta.realm.apps.farmercontractor.utils.SnapHelper;
import sparta.realm.apps.farmercontractor.utils.printing.Printer;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartamodels.percent_calculation;
import sparta.realm.spartautils.s_bitmap_handler;
import sparta.realm.utils.Conversions;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

public class GroupCompetitionReports extends AppCompatActivity {


    ActivityGroupCompetitionReportsBinding binding;

    ArrayList<Member> members;
    ArrayList<Member> selectedMembers;
    Pager<Member> pager;
    MemberGroup memberGroup;
    Boolean creationMode = true;
    String trainedOnGroups = "no";
    String trainedOnGroupFormation = "no";
    Survey survey;
    MemberGroup member_group;
    CompetitionAssessmentSurveyEntry competitionEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGroupCompetitionReportsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Globals.reg_mode = getIntent().getStringExtra("reg_mode");
        Globals.registeringMemberGroup = new MemberGroup(Globals.reg_mode);
        String member_group_transaction_no = getIntent().getStringExtra("transaction_no");

        memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(getIntent().getStringExtra("transaction_no") == null || getIntent().getStringExtra("transaction_no").length() < 1 ? "sid='" + getIntent().getStringExtra("sid") + "'" : "transaction_no='" + getIntent().getStringExtra("transaction_no") + "'"));
//        memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
        creationMode = memberGroup == null;

        if (Globals.reg_mode.equalsIgnoreCase("2"))//Editing
        {

            String group_id = getIntent().getStringExtra("sid");
            Log.e("Page1", "OnCreate: Member_id:  " + group_id);

            if (group_id == "0") {
                Toast.makeText(this, "Duplicate Record Click Other Record", Toast.LENGTH_SHORT).show();
                return;
            }
            if (group_id == null || group_id.isEmpty()) {
                if (member_group_transaction_no == null || member_group_transaction_no.isEmpty()) {

                    Toast.makeText(this, "Member Transaction Number is Null", Toast.LENGTH_SHORT).show();
                    finish();
                } else {

                    Globals.registeringMemberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("transaction_no='" + member_group_transaction_no + "'"));
                    Globals.registeringMemberGroup.transaction_no = member_group_transaction_no;

                }

            } else {
                Globals.registeringMemberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid='" + group_id + "'"));
                Globals.registeringMemberGroup.transaction_no = member_group_transaction_no;

            }
            if (Globals.registeringMemberGroup == null) {
                finish();
                return;

            }
            Globals.setRegisteringMemberGroup(Globals.registeringMemberGroup);

            Globals.registeringMemberGroup.sid = group_id;
        } else {

            Globals.registeringMemberGroup.sid = null;
        }

        if (Globals.registeringMemberGroup() == null) {
            Globals.registeringMemberGroup.transaction_no = checkTransactionNumber();
            Globals.setRegisteringMemberGroup(Globals.registeringMemberGroup);
        } else {
            Globals.registeringMemberGroup = Globals.registeringMemberGroup();
            if (Globals.reg_mode.equalsIgnoreCase("1")) {

                Globals.registeringMemberGroup.transaction_no = checkTransactionNumber();
            }
        }

//        if (creationMode){
//            memberGroup.transaction_no =
//        }

        Globals.registeringMemberGroup.enrollment_type = Globals.reg_mode;
        Globals.registeringMemberGroup.sync_status = "" + sync_status.pending.ordinal();
        Globals.registeringMemberGroup.reg_start_time = System.currentTimeMillis() + "";
//        if (creationMode){
//            memberGroup.transaction_no =
//        }
        if (Globals.registeringMemberGroup != null) {
            //member_group = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(new String[]{"sid='" + Globals.registeringMemberGroup.sid + "'"}));
            competitionEntry = Realm.databaseManager.loadObject(CompetitionAssessmentSurveyEntry.class, new Query().setTableFilters(new String[]{"group_id='" + Globals.registeringMemberGroup.sid + "'"}));
        }

        initUi();

    }

    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.include2.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);
            binding.include2.aa.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query().setTableFilters("country='" + input + "'")), agroforestAgentInputListener);

            //Log.e("Page1", "Registered Member, County" + Globals.registeringMember().county);
            //Log.e("Page1", "Registered Member, Ward" + Globals.registeringMember().location);
            //Log.e("Page1", "Registered Member, Sub-Location" + Globals.registeringMember().sub_location);
            //Log.e("Page1", "Registered Member, Village" + Globals.registeringMember().village);
            //Log.e("Page1", "Registered Member, Site" + Globals.registeringMember().site);
            //Log.e("Page1", "Registered Member, Seedling Type" + Globals.registeringMember().seedling_type);

            binding.include2.countyInput.setInput(Globals.registeringMemberGroup().county);
            binding.include2.siteInput.setInput(Globals.registeringMemberGroup().site);

            //Changing Administrative titles for each Country
            if (input.equals("110")) {
                binding.include2.countyInput.setTitle("County");
                binding.include2.locationInput.setTitle("Ward");
                binding.include2.subLocationInput.setTitle("Sub-Location");
            }
            if (input.equals("222")) {
                binding.include2.countyInput.setTitle("District");
                binding.include2.locationInput.setTitle("Sub-County");
                binding.include2.subLocationInput.setTitle("Parish");
            }

            //pager.setTableFilters(new String[]{"country='" + input + "'","sid in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "')")});
            pager.setTableFilters(new String[]{"country='" + input + "'", "sid in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "' and is_active = 'true') ")});


        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
            binding.include2.locationInput.setInput(Globals.registeringMemberGroup().location);

        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
            binding.include2.subLocationInput.setInput(Globals.registeringMemberGroup().sub_location);

        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
            binding.include2.villageInput.setInput(Globals.registeringMemberGroup().village);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("county='" + input + "'")), siteInputListener);
//            binding.siteInput.setInput(Globals.registeringMemberGroup().site);
        }
    };

    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

        }
    };

//    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
//        @Override
//        public void onInputAvailable(boolean valid, String input) {
//
//
//        }
//    };
    SearchSpinner.InputListener agroforestAgentInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };
    SearchSpinner.InputListener groupStageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };

    void initUi() {
        setupToolbar(binding.include.toolbar);
        members = new ArrayList<>();

        if (competitionEntry != null){
            Toast.makeText(this, "Group Has Competition already", Toast.LENGTH_SHORT).show();
            binding.create.setText("Update Group Results");
            binding.create.setBackground(getDrawable(R.drawable.button_negative));
            binding.create.setTextColor(rgb(0,0,0));

        }

        binding.memberList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.memberList.setLayoutManager(linearLayoutManager);
        SnapHelper startSnapHelper = new SnapHelper();
        startSnapHelper.attachToRecyclerView(binding.memberList);
        //String country = Globals.registeringMember().country;
        //Log.e("Page2", "Selected Country: " + country);
        binding.include2.country.setDataset(Realm.databaseManager.loadObjectArray(Country.class, new Query()), countryInputListener);

        //binding.include2.aa.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query()), agroforestAgentInputListener);

        //binding.include2.groupStage.setDataset(Realm.databaseManager.loadObjectArray(GroupStages.class, new Query()), groupStageInputListener);


        pager = new Pager(Member.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                members = (ArrayList<Member>) data;
                binding.noRecordsLay.setVisibility((members.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.memberList.setAdapter(new MemberAdapter(members, new MemberAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(Member mem, View view) {
                        if (!creationMode) {

                            return;
                        }
                        if (!selectedMembers.contains(mem)) {
                            selectedMembers.add(0, mem);
                            binding.selectionList.getAdapter().notifyDataSetChanged();
                            binding.include2.memberCount.setText(selectedMembers.size() + " members");

                        }

                    }
                }));
                binding.memberList.setupThings();
                FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(GroupCompetitionReports.this);
                binding.memberList.addItemDecoration(decoration);
                binding.memberList.setItemAnimator(new DefaultItemAnimator());
                binding.memberList.invalidate();
            }
        }, binding.searchText, binding.prev, binding.next, binding.positionIndicator, binding.loadingBar, null, new String[0], null, "full_name");

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);

        binding.selectionList.setLayoutManager(flowLayoutManager);
        selectedMembers = new ArrayList<>();
        binding.selectionList.setAdapter(new SelectedMemberAdapter(selectedMembers,
                (mem, view) -> {
                    selectedMembers.remove(mem);
                    binding.selectionList.getAdapter().notifyDataSetChanged();
                    binding.include2.memberCount.setText(selectedMembers.size() + " members");


                }));
        binding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (competitionEntry != null){
                    Toast.makeText(GroupCompetitionReports.this, "Group Competition already Done, Check Web App", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!creationMode) {
                    if (recordSaved) {
                        print();
                    } else {
                        showSaveDialog();
                    }

                }
//                else {
//                    print();
//                }
            }
        });
        binding.include2.groupsTrained.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    trainedOnGroups = "yes";
                    Log.e("GroupManagement", "onCheckedChanged: Checked1" + trainedOnGroups);
                }
            }
        });
        binding.include2.groupsTrainedFormation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    trainedOnGroupFormation = "yes";
                    Log.e("GroupManagement", "onCheckedChanged: Checked2" + trainedOnGroupFormation);
                }
            }
        });

        //Loading Group Competition Questions
        loadSurveyQuestions();

        if (!creationMode) {
            //binding.create.setText("Print");
//    pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where group_id='"+memberGroup.sid+"')"});
            //pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "')")});
//            pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where group_id='" + memberGroup.sid + "')"});
            binding.searchText.setVisibility(View.GONE);
            binding.prev.setVisibility(View.GONE);
            binding.next.setVisibility(View.GONE);
            binding.positionIndicator.setVisibility(View.GONE);
            binding.groupInfoInfo.setText("Below is the information about this group\n");
            binding.groupMembersInfo.setText("Below are the list of members in this group\n");
//            binding.include2.name.setBackground(null);
//            binding.include2.info1.setBackground(null);
//            binding.include2.memberCount.setBackground(null);
//            binding.include2.cr.setBackground(null);
//            binding.include2.aa.setBackground(null);
//            binding.include2.country.setBackground(null);
//            binding.include2.groupsTrained.setBackground(null);
//            binding.include2.groupsTrainedFormation.setBackground(null);
//            binding.include2.groupStage.setBackground(null);
            binding.include2.name.setFocusable(false);
            binding.include2.info1.setFocusable(false);
            binding.include2.country.setEnabled(false);
            binding.include2.country.setClickable(false);
            binding.include2.aa.setEnabled(false);
            binding.include2.aa.setClickable(false);
            binding.include2.groupsTrained.setEnabled(false);
            binding.include2.groupsTrainedFormation.setEnabled(false);
            //binding.include2.groupStage.setEnabled(false);
            //binding.include2.groupStage.setClickable(false);

            binding.include2.name.setText(memberGroup.name);
            binding.include2.info1.setText(memberGroup.description);

            Log.e("COMPETITION", "initUi: Name: " + memberGroup.name);
            Log.e("COMPETITION", "initUi: Country: " + memberGroup.country_id);

            if (memberGroup.group_training.equalsIgnoreCase("yes")) {
                binding.include2.groupsTrained.setChecked(true);
            }
            if (memberGroup.triaining_group_formation.equalsIgnoreCase("yes")) {
                binding.include2.groupsTrainedFormation.setChecked(true);
            }

            if (memberGroup.newly_formed != null) {
                if (memberGroup.newly_formed.equalsIgnoreCase("true")) {
                    binding.include2.newlyFormed.setChecked(true);
                }
            }
            if (memberGroup.organized_group != null) {
                if (memberGroup.organized_group.equalsIgnoreCase("true")) {
                    binding.include2.organizedGroups.setChecked(true);
                }
            }
            if (memberGroup.ready_for_certification != null) {
                if (memberGroup.ready_for_certification.equalsIgnoreCase("true")) {
                    binding.include2.readyForCertification.setChecked(true);
                }
            }
            if (memberGroup.with_certification != null) {
                if (memberGroup.with_certification.equalsIgnoreCase("true")) {
                    binding.include2.withCertification.setChecked(true);
                }
            }

            if (memberGroup.data_status != null) {
                if (memberGroup.data_status.equalsIgnoreCase("false")) {
                    binding.include2.activateBtn.setText("Deactivate Group");
                }
            }


            //binding.include2.groupStage.setInput(memberGroup.group_stages_id);
            binding.include2.country.setInput(memberGroup.country_id);
            binding.include2.countyInput.setInput(memberGroup.county);
            binding.include2.locationInput.setInput(memberGroup.locationId);
            binding.include2.subLocationInput.setInput(memberGroup.sub_location);
            binding.include2.villageInput.setInput(memberGroup.village);
            binding.include2.siteInput.setInput(memberGroup.site);
            binding.include2.aa.setInput(memberGroup.agroforest_agent_id);



//            if (memberGroup.group_training.equalsIgnoreCase("yes")) {
//                binding.include2.groupsTrained.setChecked(true);
//            }
//            if (memberGroup.triaining_group_formation.equalsIgnoreCase("yes")) {
//                binding.include2.groupsTrainedFormation.setChecked(true);
//            }
//
//            binding.include2.aa.setInput(memberGroup.agroforest_agent_id);
//            //binding.include2.groupStage.setInput(memberGroup.group_stages_id);
//            binding.include2.country.setInput(memberGroup.country);
//            binding.include2.countyInput.setInput(memberGroup.county);
//            binding.include2.locationInput.setInput(memberGroup.locationId);
//            binding.include2.subLocationInput.setInput(memberGroup.sub_location);
//            binding.include2.villageInput.setInput(memberGroup.village);
//            binding.include2.siteInput.setInput(memberGroup.site);

//            binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, new Query().setTableFilters("group_transaction_no='" + memberGroup.transaction_no + "'"))+" members");
            binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? "group_id='" + memberGroup.sid + "'" :  "is_active = 'true' AND "+"group_transaction_no='" + memberGroup.transaction_no + "' OR group_id='" + memberGroup.sid + "'" ) + " members");

            //binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? "group_id='" + memberGroup.sid + "'" : "group_transaction_no='" + memberGroup.transaction_no + "' OR group_id='" + memberGroup.sid + "'") + " members");
//    binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class,new Query().setTableFilters("group_id='"+memberGroup.sid+"'")));
        } else {
            binding.include2.memberCount.setText("0 members");

        }
    }

    boolean recordSaved = false;

    String checkTransactionNumber() {
        String transaction_number = Globals.getTransactionNo();

        while (Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("transaction_no='" + transaction_number + "'")) != null) {
            transaction_number = Globals.getTransactionNo();
        }
        return transaction_number;

    }
    void showSaveDialog() {

        if (validated()) {
            //

            CompetitionAssessmentSurveyEntry surveyEntry = new CompetitionAssessmentSurveyEntry(memberGroup.sid, memberGroup.transaction_no, survey.sid, "" + totalScore());

            ArrayList<CompetitionQuestionEntry> surveyQuestionEntries = new ArrayList<>();
            for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
                CompetitionQuestionEntry surveyQuestionEntry = new CompetitionQuestionEntry(surveyEntry.transaction_no, surveyQuestion.sid, surveyQuestion.result.choice, surveyQuestion.result.marks);
                surveyQuestionEntries.add(surveyQuestionEntry);
            }

//            memberGroup = new MemberGroup(binding.include2.name.getText().toString(), binding.include2.info1.getText().toString(), "Not set",
//                    binding.include2.location.getText().toString(), trainedOnGroups,
//                    trainedOnGroupFormation, binding.include2.cr.getText().toString(),
//                    binding.include2.aa.getInput(), binding.include2.groupStage.getInput(), binding.include2.country.getInput());
//
//            Log.e("GroupManagement", "ShowSaveDialog: Transaction No:  " + memberGroup.transaction_no);

            View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_survey_confirmation, null);
            final AlertDialog ald = new AlertDialog.Builder(this)
                    .setView(aldv)
                    .show();
            ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button save = aldv.findViewById(R.id.save);
            save.setOnClickListener(view -> {
                if (recordSaved) {
                    print();

                } else {
//                    if (Realm.databaseManager.insertObject(memberGroup)) {
//                        for (Member member : selectedMembers) {
//                            GroupParticipant groupParticipant = new GroupParticipant(memberGroup.transaction_no, member.sid);
//                            Realm.databaseManager.insertObject(groupParticipant);
//
//                        }
//                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(new GroupParticipant()).get(0));
//                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(memberGroup).get(1));
//                        recordSaved = true;
//                        save.setText("Print");
//                    }
                    if (Realm.databaseManager.insertObject(surveyEntry)) {
                        for (CompetitionQuestionEntry surveyQuestionEntry : surveyQuestionEntries) {// new SurveyQuestionEntry(surveyEntry.transaction_no, surveyQuestion.sid, surveyQuestion.result.choice, surveyQuestion.result.marks);
                            Realm.databaseManager.insertObject(surveyQuestionEntry);
                        }
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(surveyEntry).get(0));
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(new CompetitionQuestionEntry()).get(0));

                        recordSaved = true;
                        save.setText("Print");

                    }

                }

            });
            ((TextView) aldv.findViewById(R.id.sub_title)).setText(memberGroup.name);
            ((TextView) aldv.findViewById(R.id.description)).setText(memberGroup.description + "\nBelow is the list of the group members");
//            RecyclerView memberlist = aldv.findViewById(R.id.member_list);
//            memberlist.setLayoutManager(new LinearLayoutManager(this));
//            memberlist.setAdapter(new MemberAdapter(selectedMembers, (mem, view) -> {
//
//            }));
            aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
                if (recordSaved) {
                    ald.dismiss();
                    finish();
                } else {
                    ald.dismiss();
                }
            });

            DonutProgress donutProgress = aldv.findViewById(R.id.icon);
            TextView rating_note = aldv.findViewById(R.id.rating_note);
            TextView sub_title = aldv.findViewById(R.id.sub_title);
            TextView description = aldv.findViewById(R.id.description);
            RecyclerView recyclerView = aldv.findViewById(R.id.record_list);

            percent_calculation pc = new percent_calculation(maxScore() + "", surveyEntry.total_score);
            donutProgress.setProgress(Float.parseFloat(pc.per_balance));
//            rating_note.setText(surveyEntry.total_score+"/"+maxScore()+" "+(Float.parseFloat(pc.per_balance) > 80 ? "Good" : Float.parseFloat(pc.per_balance) > 60 ? "Average" : Float.parseFloat(pc.per_balance) > 40 ? "Poor" : "Unacceptable"));
            rating_note.setText(surveyEntry.total_score + "/" + maxScore() + " " + getScoreRating(Integer.parseInt(surveyEntry.total_score)));

            sub_title.setText(memberGroup.name);
            description.setText("Confirm your entries for this Competition below\n");

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new CompetitionQuestionReportAdapter(surveyQuestionEntries));
        }

    }

    void loadSurveyQuestions() {
        survey = Realm.databaseManager.loadObject(Survey.class, new Query().setTableFilters("sid=" + 3 + ""));
        survey.surveyQuestions = Realm.databaseManager.loadObjectArray(SurveyQuestion.class, new Query().setTableFilters("survey='" + survey.sid + "'").setLimit(50));

        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {

            surveyQuestion.surveyQuestionChoices = Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class, new Query().setTableFilters("question='" + surveyQuestion.sid + "'").addOrderFilters("choice_index", true));
        }
        binding.surveyList.setLayoutManager(new LinearLayoutManager(this));
        binding.surveyList.setAdapter(new SurveyQuestionAdapter(survey.surveyQuestions));
        binding.noRecordsLay2.setVisibility(View.GONE);
        //binding.submit.setOnClickListener(view -> saveSurvey());
    }

    String getScoreRating(int score) {
        int total = 0;
        HashMap<String, Integer> higest_marks_map = new HashMap<>();
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
            for (SurveyQuestionChoice surveyQuestionChoice : Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class, new Query().setTableFilters("question='" + surveyQuestion.sid + "'"))) {
                int marks_from = Integer.parseInt(surveyQuestionChoice.marks_from);
                int marks_from_hash = higest_marks_map.get(surveyQuestionChoice.choice) == null ? 0 : higest_marks_map.get(surveyQuestionChoice.choice);
                higest_marks_map.put(surveyQuestionChoice.choice, marks_from + marks_from_hash);

            }
            total += Integer.parseInt(surveyQuestion.result.marks);
        }

        int map_size = higest_marks_map.size();
        for (int i = 0; i < map_size; i++) {
            Map.Entry highest = null;
            for (Map.Entry choice_max_marks : higest_marks_map.entrySet()) {
                if (highest == null) {
                    highest = choice_max_marks;
                    continue;
                } else {
                    if ((int) choice_max_marks.getValue() > (int) highest.getValue()) {
                        highest = choice_max_marks;
                    }
                }
            }
            if (score >= (int) highest.getValue()) {
                return (String) highest.getKey();
            }
            higest_marks_map.remove(highest.getKey());
        }

        return null;
    }
    int totalScore() {
        int total = 0;
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
            if (!surveyQuestion.max_marks.equalsIgnoreCase("0")){
                total += Integer.parseInt(surveyQuestion.result.marks);
            }

        }

        return total;
    }

    int maxScore() {
        int total = 0;

        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
            if (!surveyQuestion.surveyQuestionChoices.isEmpty()){
                total += Integer.parseInt(getMaxScore(surveyQuestion));
            }
        }

        return total;
    }

    String getMaxScore(SurveyQuestion surveyQuestion) {
        ArrayList<SurveyQuestionChoice> surveyQuestionChoices = Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class, new Query().setTableFilters("question='" + surveyQuestion.sid + "'"));
        SurveyQuestionChoice highestSurveyQuestionChoice = null;
        for (SurveyQuestionChoice surveyQuestionChoice : surveyQuestionChoices) {
            if (highestSurveyQuestionChoice == null) {
                highestSurveyQuestionChoice = surveyQuestionChoice;
                continue;
            }
            if (Integer.parseInt(surveyQuestionChoice.marks_to) > Integer.parseInt(highestSurveyQuestionChoice.marks_to)) {
                highestSurveyQuestionChoice = surveyQuestionChoice;

            }
        }

        return highestSurveyQuestionChoice.marks_to;
    }

    void print() {
        new T12Printer(GroupCompetitionReports.this, new Printer.PrintingInterface() {
            @Override
            public void onReadyToPrint(Object... printingObjects) {
                PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                CanvasPrint cp = new CanvasPrint();
                cp.init(PrinterType.TIII);
                cp.setUseSplit(true);
                cp.setTextAlignRight(false);
                cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(GroupCompetitionReports.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                FontProperty fp = new FontProperty();
                fp.setFont(true, false, true, false, 27, null);
                cp.setFontProperty(fp);
                cp.drawText(100, creationMode ? "Group creation report" : "Group report");
                cp.drawText("");
                fp.setFont(true, false, false, false, 25, null);
                cp.setFontProperty(fp);
                cp.drawText("Transaction no: ");
                cp.drawText(memberGroup.transaction_no);
                ;
                cp.drawText("");
                cp.drawText("Group info");
                fp.setFont(true, false, false, false, 20, null);
                cp.setFontProperty(fp);
                cp.drawText("");
                cp.drawText("Group name: " + memberGroup.name);
                cp.drawText("Group description: " + memberGroup.description);
                cp.drawText("Members: " + (creationMode ? selectedMembers.size() : members.size()));
                cp.drawText("");
                fp.setFont(true, false, false, false, 25, null);
                cp.setFontProperty(fp);
                cp.setTextAlignRight(false);
                cp.drawText("Group members");
                cp.drawText("");
                mPrinter.printImage(cp.getCanvasImage());
                boolean empty = true;
                for (Member member : creationMode ? selectedMembers : members) {
                    empty = false;
                    cp = new CanvasPrint();
                    cp.init(PrinterType.TIII);
                    fp.setFont(true, false, false, false, 20, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText(member.full_name);
                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(true);
                    cp.drawText("National ID number: " + member.nat_id);
                    cp.drawLine(0, cp.getCurrentPointY(), 450, cp.getCurrentPointY());
                    mPrinter.printImage(cp.getCanvasImage());
                }
                cp = new CanvasPrint();
                cp.init(PrinterType.TIII);
                if (empty) {
                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText(130, "**No records**");

                }

                fp.setFont(false, false, false, false, 18, null);
                cp.setFontProperty(fp);
                cp.setTextAlignRight(false);
                cp.drawText("");
                cp.drawText("Report generated by: " + Globals.user().username);
                cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                cp.drawText("");
                mPrinter.printImage(cp.getCanvasImage());
                Barcode bc = new Barcode(PDF417, 5, 300, 50, memberGroup.transaction_no);
                mPrinter.printBarCode(bc);
                mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                mPrinter.closeConnection();

            }

        }).print();
    }

    boolean validated() {

        Boolean valid = true;
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {

            if (!surveyQuestion.max_marks.equalsIgnoreCase("0")){
                if (surveyQuestion.result.choice == null) {
                    surveyQuestion.uiError = "Choice not selected";
                    binding.surveyList.getAdapter().notifyDataSetChanged();
                    valid = false;
                    continue;
                }
            }

            if (surveyQuestion.result.marks == null) {
                surveyQuestion.uiError = "Marks not awarded";
                binding.surveyList.getAdapter().notifyDataSetChanged();
                valid = false;
                continue;
            }
            try {
                if (!surveyQuestion.max_marks.equalsIgnoreCase("0")){
                    if (Integer.parseInt(surveyQuestion.result.marks) < Integer.parseInt(surveyQuestion.result.surveyQuestionChoice.marks_from) || Integer.parseInt(surveyQuestion.result.marks) > Integer.parseInt(surveyQuestion.result.surveyQuestionChoice.marks_to)) {
                        surveyQuestion.uiError = "Marks not properly awarded";
                        binding.surveyList.getAdapter().notifyDataSetChanged();
                        valid = false;
                        continue;
                    }
                }else{
                    if (surveyQuestion.result.marks.equalsIgnoreCase("")){
                        surveyQuestion.uiError = "Marks not properly awarded";
                        binding.surveyList.getAdapter().notifyDataSetChanged();
                        valid = false;
                        continue;
                    }
                }

            } catch (Exception e) {
                surveyQuestion.uiError = "Marks not properly awarded";
                binding.surveyList.getAdapter().notifyDataSetChanged();
                valid = false;
                continue;
            }
            surveyQuestion.uiError = null;

        }
//        if (selectedMembers.size() < 1) {
//            return false;
//        }
//        if (binding.include2.name.getText().length() < 1) {
//            binding.include2.name.setError("Invalid input");
//            return false;
//        }
//        if (binding.include2.info1.getText().length() < 1) {
//            binding.include2.info1.setError("Invalid input");
//            return false;
//        }
        return valid;
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.non_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                onBackPressed();
                break;
            case R.id.config:
                startActivity(new Intent(this, Configuration.class));

                break;
        }
        return super.onOptionsItemSelected(item);

    }
}