package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "user_table")
//@SyncDescription(service_name = "Users",service_type = SyncDescription.service_type.Download,chunk_size = 100,storage_mode_check = true)
public class User extends SelectionData implements Serializable {

    

@DynamicProperty(json_key = "user_full_name")
 public String user_full_name;

@DynamicProperty(json_key = "username")
 public String username;


@DynamicProperty(json_key = "password")
 public String password;


public ArrayList<UserSite> user_sites=new ArrayList<>();



  public User(String sid, String user_full_name, String username, String password, String code)
    {
        this.sid=sid;
        this.code=code;
        this.user_full_name=user_full_name;
        this.username=username;
        this.name=username;
        this.password=password;
        
        this.transaction_no= svars.getTransactionNo();
        this.sync_status= com.realm.annotations.sync_status.syned.ordinal()+"";

    }
public User()
    {


    }

}
