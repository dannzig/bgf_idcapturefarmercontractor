package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "group_participant")
@SyncDescription(service_name = "Group member", upload_link = "/FarmersContract/Group/AddFarmToGroup", service_type = SyncDescription.service_type.Upload)
@SyncDescription(service_id = "1", chunk_size = 100000, service_name = "Group member", download_link = "/FarmersContract/Farmers/GetFarmerGroupList", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class GroupParticipant extends SelectionData implements Serializable {


    @DynamicProperty(json_key = "group_transaction_no")
    public String group_transaction_no;

    @DynamicProperty(json_key = "farmer_group_id")
    public String group_id;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "group_rep")
    public String group_rep;
    @DynamicProperty(json_key = "isactive")
    public String is_active;


    public GroupParticipant(String group_transaction_no, String member, String group_rep,String is_active) {
        this.group_transaction_no = group_transaction_no;
        this.member_id = member;
        this.group_rep = group_rep;


        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
        this.is_active = is_active;

    }
    public GroupParticipant(String group_transaction_no, String member, String group_rep, String transaction_no,String sid, String is_active) {
        this.group_transaction_no = group_transaction_no;
        this.member_id = member;
        this.group_rep = group_rep;


        this.transaction_no = transaction_no;
        this.sid = sid;
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
        this.is_active = is_active;

    }
    public GroupParticipant(String group_transaction_no, String member, String group_rep, String transaction_no, String is_active) {
        this.group_transaction_no = group_transaction_no;
        this.member_id = member;
        this.group_rep = group_rep;


        this.transaction_no = transaction_no;
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
        this.is_active = is_active;

    }


    public GroupParticipant() {


    }

}
