package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.BackupManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.BackupEntry;
import sparta.realm.apps.farmercontractor.models.BackupUploadEntry;
import sparta.realm.utils.Conversions;


public class BackupTypeConfigAdapter extends RecyclerView.Adapter<BackupTypeConfigAdapter.view> {

    public ArrayList<BackupManager.BackupType> items;
    Context cntxt;


    public BackupTypeConfigAdapter(ArrayList<BackupManager.BackupType> items) {
        this.items = items;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_backup_report, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface onItemClickListener {

        void onItemClick(BackupManager.BackupType mem);
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, info;
        Switch active;
        ImageView icon;


        int position;


        view(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.title);
            icon = itemView.findViewById(R.id.icon);
            info = itemView.findViewById(R.id.info);
            active = itemView.findViewById(R.id.active);
        }

        public void populate(int position) {
            this.position = position;
            BackupManager.BackupType backupType = items.get(position);
            name.setText(backupType.name()+" backup");
            active.setChecked(Globals.isBackupTypeActive(backupType));
            active.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Globals.setIsBackupTypeActive(backupType,b);
                }
            });
            icon.setImageDrawable(getDrawable(backupType));
            BackupUploadEntry backupUploadEntry=Realm.databaseManager.loadObject(BackupUploadEntry.class,new Query().setTableFilters("backup_type='"+backupType.ordinal()+"'").addOrderFilters("reg_time",false));
            if(backupUploadEntry==null){
                info.setText("Never backed up");

            }else {
                BackupEntry backupEntry=Realm.databaseManager.loadObject(BackupEntry.class,new Query().setTableFilters("transaction_no='"+backupUploadEntry.backup_transaction_no+"'"));

                info.setText("Last backup: "+Conversions.getUserDisplayTimeFromDBTime(backupUploadEntry.reg_time)+"\nSize: "+ BackupManager.getUserDisplayBytes(backupEntry.file_size));

            }



        }
        Drawable getDrawable(BackupManager.BackupType backupType){
            switch (backupType){
                case Mail:
                    return cntxt.getDrawable(R.drawable.ic_email);
                case SFTP:
                    return cntxt.getDrawable(R.drawable.ic_sftp);
            }
            return null;
        }
        @Override
        public void onClick(View view) {

        }
    }
}
