package sparta.realm.apps.farmercontractor.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.realm.annotations.sync_status;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.adapters.MemberAdapter;
import sparta.realm.apps.farmercontractor.adapters.SelectedMemberAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupCreationBinding;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupDistributionBinding;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupTrainingsBinding;
import sparta.realm.apps.farmercontractor.models.AgroForestAgent;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.FarmerSeedlingDistribution;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.MemberTraining;
import sparta.realm.apps.farmercontractor.models.NurseryTypes;
import sparta.realm.apps.farmercontractor.models.SeedlingDistributionPlan;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.TrainingGroup;
import sparta.realm.apps.farmercontractor.models.TrainingTypes;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.FastScrolRecyclerview.FastScrollRecyclerViewItemDecoration;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.apps.farmercontractor.utils.Pager;
import sparta.realm.apps.farmercontractor.utils.SnapHelper;
import sparta.realm.apps.farmercontractor.utils.printing.Printer;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartautils.s_bitmap_handler;
import sparta.realm.utils.Conversions;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

public class GroupDistribution extends AppCompatActivity {

    //ActivityGroupCreationBinding binding;
    //ActivityGroupTrainingsBinding binding;
    ActivityGroupDistributionBinding binding;

    ArrayList<Member> members;
    ArrayList<Member> selectedMembers;
    ArrayList<Member> allSelectedMembers;
    Pager<Member> pager;
    Pager<Member> selectedPager;
    MemberGroup memberGroup;
    Boolean creationMode = true;
    String trainedOnGroups = "no";
    String trainedOnGroupFormation = "no";
    Boolean newly_formed = false;
    Boolean organized_group = false;
    Boolean ready_for_certification = false;
    Boolean with_certification = false;
    Boolean groupHasRep = false;
    String countryInput;
    String groupStatus;
    GroupParticipant roleGroupParticipant;
    SeedlingDistributionPlan seedlingDistributionPlan;
    NurseryTypes nurseryTypes;
    ArrayList<FarmerSeedlingDistribution> farmerSeedlingDistributionList;
    FarmerSeedlingDistribution farmerSeedlingDistribution;

    private long mLastClickTime;
    private int mSecretNumber = 0;
    private static final long CLICK_INTERVAL = 600;
    private int clickNum = 0;


    //ArrayList<Member> selectedMembersTraining;
    ArrayList<MemberTraining> member_trainings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGroupDistributionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Globals.reg_mode = getIntent().getStringExtra("reg_mode");
        Globals.registeringMemberGroup = new MemberGroup(Globals.reg_mode);
        //String member_group_transaction_no = getIntent().getStringExtra("transaction_no");
        String member_group_transaction_no = getIntent().getStringExtra("group_transaction_no");
        seedlingDistributionPlan = new SeedlingDistributionPlan();
        member_trainings = new ArrayList<>();
        farmerSeedlingDistributionList = new ArrayList<>();

        Log.e("GroupManagement", "onCreate: Newly Formed" + newly_formed);

        memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters(getIntent().getStringExtra("group_transaction_no") == null || getIntent().getStringExtra("group_transaction_no").length() < 1 ? "sid='" + getIntent().getStringExtra("group_sid") + "'" : "transaction_no='" + getIntent().getStringExtra("group_transaction_no") + "'"));
        seedlingDistributionPlan = Realm.databaseManager.loadObject(SeedlingDistributionPlan.class, new Query().setTableFilters(getIntent().getStringExtra("transaction_no") == null || getIntent().getStringExtra("transaction_no").length() < 1 ? "sid='" + getIntent().getStringExtra("sid") + "'" : "transaction_no='" + getIntent().getStringExtra("transaction_no") + "'"));
        nurseryTypes = Realm.databaseManager.loadObject(NurseryTypes.class, new Query().setTableFilters("sid= '" + seedlingDistributionPlan.nursery_type_id + "'"));

//        memberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
        creationMode = memberGroup == null;

        if (Globals.reg_mode.equalsIgnoreCase("2"))//Editing
        {
            String group_id = getIntent().getStringExtra("group_sid");
            Log.e("Page1", "OnCreate: Member_id:  " + group_id);

            if (group_id == "0") {
                Toast.makeText(this, "Duplicate Record Click Other Record", Toast.LENGTH_SHORT).show();
                return;
            }
            if (group_id == null || group_id.isEmpty()) {
                if (member_group_transaction_no == null || member_group_transaction_no.isEmpty()) {

                    Toast.makeText(this, "Member Transaction Number is Null", Toast.LENGTH_SHORT).show();
                    finish();
                } else {

                    Globals.registeringMemberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("transaction_no='" + member_group_transaction_no + "'"));
                    Globals.registeringMemberGroup.transaction_no = member_group_transaction_no;

                }

            } else {
                Globals.registeringMemberGroup = Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("sid='" + group_id + "'"));
                Globals.registeringMemberGroup.transaction_no = member_group_transaction_no;

            }
            if (Globals.registeringMemberGroup == null) {
                finish();
                return;

            }
            Globals.setRegisteringMemberGroup(Globals.registeringMemberGroup);

            Globals.registeringMemberGroup.sid = group_id;
        } else {

            Globals.registeringMemberGroup.sid = null;
        }

        if (Globals.registeringMemberGroup() == null) {
            Globals.registeringMemberGroup.transaction_no = checkTransactionNumber();
            Globals.setRegisteringMemberGroup(Globals.registeringMemberGroup);
        } else {
            Globals.registeringMemberGroup = Globals.registeringMemberGroup();
            if (Globals.reg_mode.equalsIgnoreCase("1")) {

                Globals.registeringMemberGroup.transaction_no = checkTransactionNumber();
            }
        }

//        if (creationMode){
//            memberGroup.transaction_no =
//        }

        Globals.registeringMemberGroup.enrollment_type = Globals.reg_mode;
        Globals.registeringMemberGroup.sync_status = "" + sync_status.pending.ordinal();
        Globals.registeringMemberGroup.reg_start_time = System.currentTimeMillis() + "";


        initUi();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("GroupRecords", "OnResume: HERE HERE");
        //initUi();
    }

    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

            countryInput = input;
            binding.include2.countyInput.setDataset(Realm.databaseManager.loadObjectArray(County.class, new Query().setTableFilters("country='" + input + "'")), countyInputListener);
            binding.include2.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("country='" + input + "'")), siteInputListener);
            binding.include2.aa.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query().setTableFilters("country='" + input + "'")), agroforestAgentInputListener);

            //binding.include2.aa.setDataset(Realm.databaseManager.loadObjectArray(AgroForestAgent.class, new Query()), agroforestAgentInputListener);
            //Log.e("Page1", "Registered Member, County" + Globals.registeringMember().county);
            //Log.e("Page1", "Registered Member, Ward" + Globals.registeringMember().location);
            //Log.e("Page1", "Registered Member, Sub-Location" + Globals.registeringMember().sub_location);
            //Log.e("Page1", "Registered Member, Village" + Globals.registeringMember().village);
            //Log.e("Page1", "Registered Member, Site" + Globals.registeringMember().site);
            //Log.e("Page1", "Registered Member, Seedling Type" + Globals.registeringMember().seedling_type);

            //binding.include2.countyInput.setInput(Globals.registeringMemberGroup().county);
            //binding.include2.siteInput.setInput(Globals.registeringMemberGroup().site);

            //Changing Administrative titles for each Country
            if (input.equals("110")) {
                binding.include2.countyInput.setTitle("County");
                binding.include2.locationInput.setTitle("Ward");
                binding.include2.subLocationInput.setTitle("Sub-Location");
            }
            if (input.equals("222")) {
                binding.include2.countyInput.setTitle("District");
                binding.include2.locationInput.setTitle("Sub-County");
                binding.include2.subLocationInput.setTitle("Parish");
            }

            selectedPager.setTableFilters(new String[]{"country='" + input + "'", "sid in(select member_id from group_participant where " + (Globals.registeringMemberGroup.transaction_no == null || Globals.registeringMemberGroup.transaction_no.length() < 1 ? " group_id='" + Globals.registeringMemberGroup.sid + "')" : " group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "' and is_active = 'true') ")});
            //pager.setTableFilters(new String[]{"country='" + input + "'","sid not in(select member_id from group_participant where " + (Globals.registeringMemberGroup.transaction_no == null || Globals.registeringMemberGroup.transaction_no.length() < 1 ? " group_id='" + Globals.registeringMemberGroup.sid + "')" : " group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "')")});

            pager.setTableFilters(new String[]{"country='" + input + "'", "sid not in(SELECT DISTINCT member_id FROM group_participant where is_active = 'true')"});

        }
    };
    SearchSpinner.InputListener countyInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.locationInput.setDataset(Realm.databaseManager.loadObjectArray(Location.class, new Query().setTableFilters("county='" + input + "'")), locationInputListener);
            binding.include2.locationInput.setInput(Globals.registeringMemberGroup().location);

        }
    };
    SearchSpinner.InputListener locationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.subLocationInput.setDataset(Realm.databaseManager.loadObjectArray(SubLocation.class, new Query().setTableFilters("location='" + input + "'")), subLocationInputListener);
            binding.include2.subLocationInput.setInput(Globals.registeringMemberGroup().sub_location);

        }
    };

    SearchSpinner.InputListener subLocationInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
            binding.include2.villageInput.setDataset(Realm.databaseManager.loadObjectArray(Village.class, new Query().setTableFilters("sub_location='" + input + "'")), villageInputListener);
            binding.include2.villageInput.setInput(Globals.registeringMemberGroup().village);
        }
    };
    SearchSpinner.InputListener siteInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {
//            binding.siteInput.setDataset(Realm.databaseManager.loadObjectArray(Site.class, new Query().setTableFilters("county='" + input + "'")), siteInputListener);
//            binding.siteInput.setInput(Globals.registeringMemberGroup().site);
        }
    };

    SearchSpinner.InputListener villageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {

        }
    };
    //    SearchSpinner.InputListener countryInputListener = new SearchSpinner.InputListener() {
//        @Override
//        public void onInputAvailable(boolean valid, String input) {
//
//
//        }
//    };
    SearchSpinner.InputListener agroforestAgentInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };
    SearchSpinner.InputListener groupStageInputListener = new SearchSpinner.InputListener() {
        @Override
        public void onInputAvailable(boolean valid, String input) {


        }
    };

    void initUi() {
        setupToolbar(binding.include.toolbar);
        members = new ArrayList<>();

        binding.memberList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.memberList.setLayoutManager(linearLayoutManager);
        SnapHelper startSnapHelper = new SnapHelper();
        startSnapHelper.attachToRecyclerView(binding.memberList);

        binding.selectedMemberList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        binding.selectedMemberList.setLayoutManager(linearLayoutManager2);
        SnapHelper startSnapHelper2 = new SnapHelper();
        startSnapHelper2.attachToRecyclerView(binding.selectedMemberList);

//        if (Globals.reg_mode.equalsIgnoreCase("1")) {
//            binding.selectedGroupMembersInfo.setVisibility(View.GONE);
//            binding.SelectedGroupMembersTitle.setVisibility(View.GONE);
//            binding.selectedMemberList.setVisibility(View.GONE);
//        }

        binding.include2.country.setDataset(Realm.databaseManager.loadObjectArray(Country.class, new Query()), countryInputListener);


        //binding.include2.groupStage.setDataset(Realm.databaseManager.loadObjectArray(GroupStages.class, new Query()), groupStageInputListener);


        selectedPager = new Pager(Member.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                members = (ArrayList<Member>) data;
                binding.noRecordsLay.setVisibility((members.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.selectedMemberList.setAdapter(new MemberAdapter(members, new MemberAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(Member mem, View view) {

                        // Take up the training.
                        if (binding.include2.aa.isInputValid()) {
                            showDeactivateDialog(mem);
                        } else {
                            Toast.makeText(GroupDistribution.this, "Select aa agent", Toast.LENGTH_SHORT).show();
                        }


                    }
                }));
                binding.selectedMemberList.setupThings();
                FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(sparta.realm.apps.farmercontractor.activities.GroupDistribution.this);
                binding.selectedMemberList.addItemDecoration(decoration);
                binding.selectedMemberList.setItemAnimator(new DefaultItemAnimator());
                binding.selectedMemberList.invalidate();
            }
        }, binding.searchText, binding.prev, binding.next, binding.positionIndicator, binding.loadingBar, null, new String[0], null, "full_name");

        pager = new Pager(Member.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                members = (ArrayList<Member>) data;
                binding.noRecordsLay.setVisibility((members.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.memberList.setAdapter(new MemberAdapter(members, new MemberAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(Member mem, View view) {

                        //select country first for filtering
                        if (countryInput == null) {
                            Toast.makeText(sparta.realm.apps.farmercontractor.activities.GroupDistribution.this, "SELECT COUNTRY FIRST", Toast.LENGTH_SHORT).show();
                            return;
                        }


                    }
                }));
                binding.memberList.setupThings();
                FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(sparta.realm.apps.farmercontractor.activities.GroupDistribution.this);
                binding.memberList.addItemDecoration(decoration);
                binding.memberList.setItemAnimator(new DefaultItemAnimator());
                binding.memberList.invalidate();
            }
        }, binding.searchText, binding.prev, binding.next, binding.positionIndicator, binding.loadingBar, null, new String[0], null, "full_name");

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);

        binding.selectionList.setLayoutManager(flowLayoutManager);
        selectedMembers = new ArrayList<>();
        allSelectedMembers = new ArrayList<>();
        Globals.selectedMembersIssued = new ArrayList<>();
        binding.selectionList.setAdapter(new SelectedMemberAdapter(selectedMembers,
                (mem, view) -> {

                    if (mem.group_rep.equalsIgnoreCase("1")) {
                        Globals.groupRepChair = false;
                    }
                    if (mem.group_rep.equalsIgnoreCase("2")) {
                        Globals.groupRepTreas = false;
                    }
                    if (mem.group_rep.equalsIgnoreCase("3")) {
                        Globals.groupRepSec = false;
                    }
                    selectedMembers.remove(mem);
                    allSelectedMembers.remove(mem);
                    binding.selectionList.getAdapter().notifyDataSetChanged();
                    binding.include2.memberCount.setText(selectedMembers.size() + " members");


                }));
        binding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (recordSaved) {
                    print();
                } else {
                    showSaveDialog();
                }

//                if (creationMode) {
//                } else {
//                    print();
//                }
            }
        });


        Log.e("GroupTrainings", "initUi: Creation Mode: " + creationMode);
        if (!creationMode) {
            //binding.create.setText("Print");
            binding.create.setText("SAVE TRAINING DETAILS");

            //binding.include2.itemMemberGroup.setVisibility(View.GONE);

            //binding.include2.country.setVisibility(View.GONE);
            binding.include2.countyInput.setVisibility(View.GONE);
            binding.include2.locationInput.setVisibility(View.GONE);
            binding.include2.subLocationInput.setVisibility(View.GONE);
            binding.include2.villageInput.setVisibility(View.GONE);
            binding.include2.siteInput.setVisibility(View.GONE);
            //binding.include2.aa.setVisibility(View.GONE);


            binding.groupMembersTitle.setVisibility(View.GONE);
            binding.groupMembersInfo.setVisibility(View.GONE);
            binding.selectionList.setVisibility(View.GONE);
            binding.loadingBar.setVisibility(View.GONE);
            binding.memberList.setVisibility(View.GONE);
            binding.noRecordsLay.setVisibility(View.GONE);
            binding.searchText.setVisibility(View.GONE);
            binding.navigationLay.setVisibility(View.GONE);
            binding.include2.remarks.setVisibility(View.GONE);


            binding.selectedGroupMembersInfo.setText("Below is a list of Group Members, Click if In attendance");

            //pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where group_id='"+memberGroup.sid+"')"});
            //pager.setTableFilters(new String[]{"sid in(select member_id from group_participant where group_id='" + memberGroup.sid + "')"});
            //selectedPager.setTableFilters(new String[]{"sid in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "')")});
            //pager.setTableFilters(new String[]{"sid not in(select member_id from group_participant where " + (memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? " group_id='" + memberGroup.sid + "')" : " group_transaction_no='" + memberGroup.transaction_no + "')")});
            //binding.searchText.setVisibility(View.GONE);
            //binding.prev.setVisibility(View.GONE);
            //binding.next.setVisibility(View.GONE);
            //binding.positionIndicator.setVisibility(View.GONE);
            //binding.groupInfoInfo.setText("Below is the information about this group\n");
            //binding.groupMembersInfo.setText("Below are the list of members in this group\n");
//            binding.include2.name.setBackground(null);
//            binding.include2.info1.setBackground(null);
            binding.include2.memberCount.setBackground(null);

            //binding.include2.cr.setBackground(null);
            //binding.include2.aa.setBackground(null);
            //binding.include2.country.setBackground(null);
            //binding.include2.groupsTrained.setBackground(null);
            //binding.include2.groupsTrainedFormation.setBackground(null);
            //binding.include2.groupStage.setBackground(null);
            //binding.include2.name.setFocusable(false);
            //binding.include2.info1.setFocusable(false);
            //binding.include2.cr.setFocusable(false);
            //binding.include2.country.setEnabled(false);
            //binding.include2.country.setClickable(false);
            //binding.include2.groupsTrained.setEnabled(false);
            //binding.include2.groupsTrainedFormation.setEnabled(false);

            binding.include2.groupName.setText(memberGroup.name);
            binding.include2.name.setText(seedlingDistributionPlan.name);
            binding.include2.info1.setText(nurseryTypes.name);
            binding.include2.country.setInput(memberGroup.country_id);


            //binding.include2.groupStage.setInput(memberGroup.group_stages_id);

//                binding.include2.countyInput.setInput(memberGroup.county);
//                binding.include2.locationInput.setInput(memberGroup.locationId);
//                binding.include2.subLocationInput.setInput(memberGroup.sub_location);
//                binding.include2.villageInput.setInput(memberGroup.village);
//                binding.include2.siteInput.setInput(memberGroup.site);
//                binding.include2.aa.setInput(memberGroup.agroforest_agent_id);
//                binding.include2.remarks.setText();

//            binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, new Query().setTableFilters("group_transaction_no='" + memberGroup.transaction_no + "'"))+" members");
            binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, memberGroup.transaction_no == null || memberGroup.transaction_no.length() < 1 ? "group_id='" + memberGroup.sid + "'" : "is_active = 'true' AND " + "group_transaction_no='" + memberGroup.transaction_no + "' OR group_id='" + memberGroup.sid + "'") + " members");
            //binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class, new Query().setTableFilters(new String[]{"sid in(select member_id from group_participant where " + (Globals.registeringMemberGroup.transaction_no == null || Globals.registeringMemberGroup.transaction_no.length() < 1 ? " group_id='" + Globals.registeringMemberGroup.sid + "')" : " group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "' and is_active = 'true') ")})) + " members");

//    binding.include2.memberCount.setText(Realm.databaseManager.getRecordCount(GroupParticipant.class,new Query().setTableFilters("group_id='"+memberGroup.sid+"'")));
        } else {

            // Hide Everything I'm not using.
            binding.selectedGroupMembersInfo.setVisibility(View.GONE);
            binding.SelectedGroupMembersTitle.setVisibility(View.GONE);
            binding.selectedMemberList.setVisibility(View.GONE);
            binding.include2.memberCount.setText("0 members");

            //binding.include2.itemMemberGroup.setVisibility(View.GONE);
            //binding.groupMembersTitle.setVisibility(View.GONE);
            //binding.groupMembersInfo.setVisibility(View.GONE);
            //binding.searchText.setVisibility(View.GONE);
            //binding.navigationLay.setVisibility(View.GONE);
            //binding.create.setVisibility(View.GONE);


        }
    }

    boolean recordSaved = false;

    String checkTransactionNumber() {
        String transaction_number = Globals.getTransactionNo();

        while (Realm.databaseManager.loadObject(MemberGroup.class, new Query().setTableFilters("transaction_no='" + transaction_number + "'")) != null) {
            transaction_number = Globals.getTransactionNo();
        }
        return transaction_number;

    }


    EditText no_of_seedlings;
    EditText received_by;

    void showDeactivateDialog(Member mem) {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_seedling_distribution_module, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView title = aldv.findViewById(R.id.sub_title);
        Button groupDetails = aldv.findViewById(R.id.group_details);
        Button groupReport = aldv.findViewById(R.id.group_comp_report);
        TextView seedlingsTitle = aldv.findViewById(R.id.name_title);
        no_of_seedlings = aldv.findViewById(R.id.no_of_seedlings);
        received_by = aldv.findViewById(R.id.received_by);

        seedlingsTitle.setText("Seedlings Issued ( Expected: " + mem.total_seedlings + " )");
        title.setText("Mark Member As Issued");
        groupDetails.setText("Remove");
        groupReport.setText("Ok");


        if (Globals.registeringMemberGroup != null) {
            roleGroupParticipant = Realm.databaseManager.loadObject(GroupParticipant.class, new Query().setTableFilters(new String[]{"member_id='" + mem.sid + "'", "group_transaction_no='" + Globals.registeringMemberGroup.transaction_no + "'"}));
        }

        for (Iterator<FarmerSeedlingDistribution> iterator = farmerSeedlingDistributionList.iterator(); iterator.hasNext(); ) {
            FarmerSeedlingDistribution fsd = iterator.next();
            if (fsd.member_id.equalsIgnoreCase(mem.sid)) {
                Log.e("GroupDistribution", "showDeactivateDialog: Globals.selectedMembersIssued : HERE HERE REMOVE");

                no_of_seedlings.setText(fsd.seedling_issued);
                received_by.setText(fsd.Received_by);

            }
        }

        groupDetails.setOnClickListener(view -> {
            //confirmDeactivation(mem);

            if (Globals.selectedMembersIssued.contains(mem)) {
                Globals.selectedMembersIssued.remove(mem);

                //Modify a list while looping through it, the advanced for loop fails.
                for (Iterator<FarmerSeedlingDistribution> iterator = farmerSeedlingDistributionList.iterator(); iterator.hasNext(); ) {
                    FarmerSeedlingDistribution fsd = iterator.next();
                    if (fsd.member_id.equalsIgnoreCase(mem.sid)) {
                        Log.e("GroupDistribution", "showDeactivateDialog: Globals.selectedMembersIssued : HERE HERE REMOVE");

                        farmerSeedlingDistributionList.remove(fsd);

                    }
                }
//                for (FarmerSeedlingDistribution fsd : farmerSeedlingDistributionList) {
//                    //Print clicked farmerSeedlingDistributionList
//
//                    Log.e("GroupDistribution", "showDeactivateDialog: Globals.selectedMembersIssued : FarmerSeedlingDistribution" + fsd.toString());
//
//                    if (fsd.member_id.equalsIgnoreCase(mem.sid)) {
//                        Log.e("GroupDistribution", "showDeactivateDialog: Globals.selectedMembersIssued : HERE HERE REMOVE";
//
//                        farmerSeedlingDistributionList.remove(fsd);
//
//                    }
//
//                }
                binding.selectedMemberList.getAdapter().notifyDataSetChanged();
            } else {
                Toast.makeText(this, "Member Not Issued with Seedlings Yet", Toast.LENGTH_SHORT).show();
            }
            ald.dismiss();
        });


        groupReport.setOnClickListener(view -> {
            // Mark as Issued
            if (validatedDialogBox()) {
                if (!Globals.selectedMembersIssued.contains(mem)) {

                    FarmerSeedlingDistribution farmerDistribution = new FarmerSeedlingDistribution(seedlingDistributionPlan.sid, mem.sid,
                            no_of_seedlings.getText().toString(), binding.include2.aa.getInput()
                            , received_by.getText().toString());

                    farmerSeedlingDistributionList.add(farmerDistribution);
                    Globals.selectedMembersIssued.add(0, mem);
                    binding.selectedMemberList.getAdapter().notifyDataSetChanged();


                    Log.e("GroupDistribution", "showDeactivateDialog: Globals.selectedMembersIssued size: " + Globals.selectedMembersIssued.size() + "  farm_seedling_distribution_list Size: " + farmerSeedlingDistributionList.size());
                } else {
                    Toast.makeText(this, "Member Already Issued with Seedlings", Toast.LENGTH_SHORT).show();
                }

                ald.dismiss();
            }

            // reset pager
            //resetSelectedPager();


        });


    }

    boolean validatedDialogBox() {
        if (no_of_seedlings.getText().length() < 1) {
            no_of_seedlings.setError("Invalid input");
            return false;
        }
        if (received_by.getText().length() < 1) {
            received_by.setError("Invalid input");
            return false;
        }

        return true;
    }


    void showSaveDialog() {

        if (validated()) {
//            memberTraining.group_id = trainingGroup.group_id;
//
//            memberTraining.Remarks = binding.include2.remarks.getText().toString();
//            memberTraining.training_activity_id = trainingGroup.sid;
//            memberTraining.country_id = binding.include2.country.getInput();
//            memberTraining.county_id = binding.include2.countyInput.getInput();
//            memberTraining.location_id = binding.include2.locationInput.getInput();
//            memberTraining.sub_location_id = binding.include2.subLocationInput.getInput();
//            memberTraining.village_id = binding.include2.villageInput.getInput();
//            memberTraining.site_id = binding.include2.siteInput.getInput();
//            memberTraining.agroforest_agent_id = binding.include2.aa.getInput();


            Log.e("GroupManagement", "ShowSaveDialog: Transaction No:  " + memberGroup.transaction_no);
            Log.e("GroupManagement", "ShowSaveDialog: Group Sid:  " + memberGroup.sid);

            View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_group_confirmation, null);
            final AlertDialog ald = new AlertDialog.Builder(this)
                    .setView(aldv)
                    .show();
            ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button save = aldv.findViewById(R.id.save);
            save.setOnClickListener(view -> {
                if (recordSaved) {
                    print();

                } else {

                    for (FarmerSeedlingDistribution fsd : farmerSeedlingDistributionList) {
                        //If member is removed from list not to add to the farmers
                        Log.e("GroupTrainings", "showSaveDialog: Loop: Members: " + fsd.member_id);
                        Realm.databaseManager.insertObject(fsd);
                    }
                    SynchronizationManager.upload_(Realm.realm.getSyncDescription(new FarmerSeedlingDistribution()).get(1));
                    recordSaved = true;
                    save.setText("Print");
                    // Inserting individual farmers training.
//                    for (MemberTraining training : member_trainings) {
//                        Log.e("GroupTrainings", "showSaveDialog: Loop");
//                        Realm.databaseManager.insertObject(training);
//                        //index of upload service.
//                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(training).get(1));
//                        recordSaved = true;
//                        save.setText("Print");
//
//                    }


                }

            });
            ((TextView) aldv.findViewById(R.id.sub_title)).setText(memberGroup.name);
            ((TextView) aldv.findViewById(R.id.description)).setText(memberGroup.description + "\nBelow is the list of the group members who participated");
            RecyclerView memberlist = aldv.findViewById(R.id.member_list);
            memberlist.setLayoutManager(new LinearLayoutManager(this));
            memberlist.setAdapter(new MemberAdapter(Globals.selectedMembersIssued, (mem, view) -> {

            }));
            aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
                if (recordSaved) {
                    ald.dismiss();
                    finish();
                } else {
                    ald.dismiss();
                }
            });
        }

    }


    void print() {
        new T12Printer(sparta.realm.apps.farmercontractor.activities.GroupDistribution.this, new Printer.PrintingInterface() {
            @Override
            public void onReadyToPrint(Object... printingObjects) {
                PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                CanvasPrint cp = new CanvasPrint();
                cp.init(PrinterType.TIII);
                cp.setUseSplit(true);
                cp.setTextAlignRight(false);
                cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(sparta.realm.apps.farmercontractor.activities.GroupDistribution.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                FontProperty fp = new FontProperty();
                fp.setFont(true, false, true, false, 27, null);
                cp.setFontProperty(fp);
                cp.drawText(100, creationMode ? "Group creation report" : "Group report");
                cp.drawText("");
                fp.setFont(true, false, false, false, 25, null);
                cp.setFontProperty(fp);
                cp.drawText("Transaction no: ");
                cp.drawText(memberGroup.transaction_no);
                ;
                cp.drawText("");
                cp.drawText("Group info");
                fp.setFont(true, false, false, false, 20, null);
                cp.setFontProperty(fp);
                cp.drawText("");
                cp.drawText("Group name: " + memberGroup.name);
                cp.drawText("Group description: " + memberGroup.description);
                cp.drawText("Members: " + (creationMode ? selectedMembers.size() : members.size()));
                cp.drawText("");
                fp.setFont(true, false, false, false, 25, null);
                cp.setFontProperty(fp);
                cp.setTextAlignRight(false);
                cp.drawText("Group members");
                cp.drawText("");
                mPrinter.printImage(cp.getCanvasImage());
                boolean empty = true;
                for (Member member : creationMode ? selectedMembers : members) {
                    empty = false;
                    cp = new CanvasPrint();
                    cp.init(PrinterType.TIII);
                    fp.setFont(true, false, false, false, 20, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText(member.full_name);
                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(true);
                    cp.drawText("National ID number: " + member.nat_id);
                    cp.drawLine(0, cp.getCurrentPointY(), 450, cp.getCurrentPointY());
                    mPrinter.printImage(cp.getCanvasImage());
                }
                cp = new CanvasPrint();
                cp.init(PrinterType.TIII);
                if (empty) {
                    fp.setFont(false, false, false, false, 18, null);
                    cp.setFontProperty(fp);
                    cp.setTextAlignRight(false);
                    cp.drawText(130, "**No records**");

                }

                fp.setFont(false, false, false, false, 18, null);
                cp.setFontProperty(fp);
                cp.setTextAlignRight(false);
                cp.drawText("");
                cp.drawText("Report generated by: " + Globals.user().username);
                cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                cp.drawText("");
                mPrinter.printImage(cp.getCanvasImage());
                Barcode bc = new Barcode(PDF417, 5, 300, 50, memberGroup.transaction_no);
                mPrinter.printBarCode(bc);
                mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                mPrinter.closeConnection();

            }

        }).print();
    }

    boolean validated() {
        if (Globals.selectedMembersIssued.size() < 1) {
            Toast.makeText(this, "Select At least one Farmer to proceed", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.include2.name.getText().length() < 1) {
            binding.include2.name.setError("Invalid input");
            return false;
        }
        if (binding.include2.info1.getText().length() < 1) {
            binding.include2.info1.setError("Invalid input");
            return false;
        }

        return binding.include2.aa.isInputValid();
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.non_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                Globals.hasGroupRep = false;
                Globals.groupRepChair = false;
                Globals.groupRepTreas = false;
                Globals.groupRepSec = false;
                onBackPressed();

                break;
            case R.id.config:
                startActivity(new Intent(this, Configuration.class));

                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
