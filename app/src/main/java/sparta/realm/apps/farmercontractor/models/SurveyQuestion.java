package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "survey_question")
@SyncDescription(service_name = "Survey question", download_link = "/FarmersContract/Questionnaire/GetFarmersAssesmentQuestionsOnly", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class SurveyQuestion extends RealmModel implements Serializable {

    //    {
//    "$id":"6"
//    ,"id":1
//    ,"question":"Quality of Farm protection: Presence of a fence around the place to be planted. Etc. "
//    ,"max_marks":15.0
//    ,"datecomparer":1},
//    {"$id":"7"
//    ,"id":2
//    ,"question":"Afforestation efforts: Presence of planted trees in the farm e.g. fruit trees, ornamentals"
//    ,"max_marks":15.0
//    ,"datecomparer":1}
    @DynamicProperty(json_key = "survey_id")
    public String survey;

    @DynamicProperty(json_key = "question")
    public String question;

    @DynamicProperty(json_key = "max_marks")
    public String max_marks;

    @DynamicProperty(json_key = "question_index")
    public String question_index;

    @DynamicProperty(json_key = "index_display_mode")
    public String index_display_mode;

    public ArrayList<SurveyQuestionChoice> surveyQuestionChoices = new ArrayList<>();
    public SurveyQuestionEntry result = new SurveyQuestionEntry();

    public String uiError;

    public SurveyQuestion() {


    }

    public SurveyQuestion(String sid, String survey, String question, String max_marks, String question_index) {//Dummy insert

        this.survey = survey;
        this.sid = sid;
        this.question = question;
        this.max_marks = max_marks;
        this.question_index = question_index;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }

}
