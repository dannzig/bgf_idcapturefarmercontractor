package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.system.InputField;
import sparta.realm.apps.farmercontractor.models.system.InputGroup;
import sparta.realm.apps.farmercontractor.utils.FormTools.FormEdittext;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;


public class FormAdapter extends RecyclerView.Adapter<FormAdapter.view> {

    Context cntxt;
    public InputGroup page;

    public FormAdapter(InputGroup page) {
        this.page = page;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_form_input, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
holder.populate(position);

    }


    @Override
    public int getItemCount() {
        return page.inputFields.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public SearchSpinner selectionInput;
        public FormEdittext textInput;
        public int position;
        public ImageView icon;


        view(View itemView) {
            super(itemView);

            selectionInput = itemView.findViewById(R.id.selection_input);
            textInput = itemView.findViewById(R.id.text_input);

        }


        void populate(int position) {
            this.position = position;
            InputField inputField=page.inputFields.get(position);
            selectionInput.setVisibility(inputField.input_type.equalsIgnoreCase(InputField.InputType.Selection.ordinal() + "") ? View.VISIBLE : View.GONE);
            textInput.setVisibility(inputField.input_type.equalsIgnoreCase(InputField.InputType.Text.ordinal() + "") ? View.VISIBLE :
                    inputField.input_type.equalsIgnoreCase(InputField.InputType.Date.ordinal() + "") ? View.VISIBLE :
                            inputField.input_type.equalsIgnoreCase(InputField.InputType.DateTime.ordinal() + "") ? View.VISIBLE :
                                    inputField.input_type.equalsIgnoreCase(InputField.InputType.Number.ordinal() + "") ? View.VISIBLE :
                                            View.GONE);

            if(textInput.getVisibility()==View.VISIBLE)
            {
                textInput.setInputField(inputField, new FormEdittext.InputListener() {
                    @Override
                    public void onInputAvailable(boolean valid, String input) {
                        inputField.input=input;

                    }
                });


            }else if(selectionInput.getVisibility()==View.VISIBLE)
            {
                selectionInput.setInputField(inputField, new SearchSpinner.InputListener() {
                    @Override
                    public void onInputAvailable(boolean valid, String input) {
                        inputField.input=input;
                    }
                });

            }


        }

        @Override
        public void onClick(View view) {

        }
    }
}
