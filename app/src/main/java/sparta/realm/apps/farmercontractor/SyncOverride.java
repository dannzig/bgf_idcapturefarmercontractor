package sparta.realm.apps.farmercontractor;

import static java.sql.Types.NULL;

import android.content.ContentValues;
import android.media.MediaPlayer;
import android.util.Log;

import com.realm.annotations.sync_service_description;

import org.json.JSONException;
import org.json.JSONObject;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.SuitabilityAssessmentSurveyEntry;


public class SyncOverride implements SynchronizationManager.SynchronizationHandler {
    public SynchronizationManager sm;
    String logTag = "SyncOverride";

    public SyncOverride() {
        sm = new SynchronizationManager();
        sm.OverrideSynchronization(this);
        sm.InitialiseAutosync();

    }
    public boolean memberTransactionHasRecord(String column, String transactionNo){
        SuitabilityAssessmentSurveyEntry memberSuitability;
        memberSuitability = Realm.databaseManager.loadObject(SuitabilityAssessmentSurveyEntry.class, new Query().setTableFilters(" "+ column +" = '" + transactionNo + "'"));

        if (memberSuitability != null){
            Log.e("SyncOverride", "Member Sid: " + memberSuitability.member);
            if (memberSuitability.member.contains("temp")) {

                    return true;
            }
        }

        return false;
    }
    @Override
    public JSONObject OnUploadedObject(sync_service_description ssd, JSONObject object, JSONObject response) {

        if (ssd.object_package.equalsIgnoreCase(Member.class.getName())) {
            DatabaseManager.log_String("Upload: " + object.toString() + "Response: " + response.toString());
            try {
                Log.e("SyncOverride", "OnUploadedObject: HERE RESPONSE" + response.toString());
                if (response.getJSONObject("result").getString("id").equals("0")) {
                    DatabaseManager.log_String("Uploading returned id 0 ");
                    try {

                        MediaPlayer player = MediaPlayer.create(Realm.context, R.raw.stop);
                        player.start();
                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                        int MAX_VOLUME = 100;
                        int ACT_VOLUME = 100;
                        float volume = (float) (1 - (Math.log(MAX_VOLUME - ACT_VOLUME) / Math.log(MAX_VOLUME)));
                        player.setVolume(volume, volume);
                        player.setLooping(true);

                    } catch (Exception var4) {

                        Log.e(logTag, "MEDIA player Error =>" + var4.getMessage());
                        var4.printStackTrace();
                    }
                }
                if(memberTransactionHasRecord("member_transaction_no",response.getJSONObject("result").getString("transaction_no"))){

                    DatabaseManager.database.execSQL("update suitability_assessment_survey_entry set member ='" + response.getJSONObject("result").getString("id") + "' where member_transaction_no='" + response.getJSONObject("result").getString("transaction_no") + "'");
                    DatabaseManager.database.execSQL("update land_location set member ='" + response.getJSONObject("result").getString("id") + "' where member_transaction_no='" + response.getJSONObject("result").getString("transaction_no") + "'");
                }

//                if (!response.getBoolean("isOkay") && response.getString("message").contains("Exist")){
//
//                    Log.e("SyncOverride", "OnUploadedObject: IsOkay FALSE ");
//                    ContentValues cv = new ContentValues();
//                    cv.put("sid",NULL);
//                    cv.put("sync_status","1");
//                    DatabaseManager.database.update("member_info_table",cv,"transaction_no = ?",new String[]{"" + response.getJSONObject("result").getString("transaction_no")});
//                    Globals.updateDuplicateImagesRecord("" + response.getJSONObject("result").getString("transaction_no"),"member_info_table");
//                    Globals.updateDuplicateImagesRecord("" + response.getJSONObject("result").getString("transaction_no"),"member_fingerprint");
//
//                }
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            System.gc();
        }

        return response;
    }

    public void setupsyncstatusreport(SynchronizationManager.SynchronizationStatusHandler ssd) {
        sm.setSynchronizationStatusHandler(ssd);
    }


    @Override
    public JSONObject OnDownloadedObject(sync_service_description ssd, JSONObject object, JSONObject response) {
//        if(ssd.object_package.equalsIgnoreCase(MemberImage.class.getName())){
//
//
//            System.gc();
//        }

        return response;
    }

    public void sync() {
        sm.sync_now();
    }


}
