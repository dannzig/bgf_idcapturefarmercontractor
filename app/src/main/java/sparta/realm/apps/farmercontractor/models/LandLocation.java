package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;

@DynamicClass(table_name = "land_location")
@SyncDescription(service_name = "Land location", upload_link = "/FarmersContract/Farmers/AddFarmerCoordinate", service_type = SyncDescription.service_type.Upload)
@SyncDescription(chunk_size = 100000, service_name = "Land location", download_link = "/FarmersContract/Farmers/GetFarmersCoordinates", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class LandLocation extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "member_id")
    public String member;

    @DynamicProperty(json_key = "latitude")
    public String latitude;

    @DynamicProperty(json_key = "accuracy")
    public String accuracy;

    @DynamicProperty(json_key = "longitude")
    public String longitude;

    @DynamicProperty(json_key = "is_location_active")
    public String is_location_active;

    @DynamicProperty(json_key = "member_transaction_no")
    public String member_transaction_no;

    public LandLocation() {

    }

    public LandLocation(String member,String member_transaction_no, String latitude, String longitude, String accuracy) {
        this.member = member;
        this.member_transaction_no = member_transaction_no;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;


        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public LandLocation(String member, String latitude, String longitude, String accuracy) {
        this.member = member;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;


        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

}
