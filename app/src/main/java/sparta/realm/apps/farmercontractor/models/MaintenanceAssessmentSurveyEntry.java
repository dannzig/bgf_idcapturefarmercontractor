package sparta.realm.apps.farmercontractor.models;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "maintenance_assessment_survey_entry")

@SyncDescription(service_name = "Maintenance assessment survey entry", upload_link = "/FarmersContract/Group/AddmaintenanceResult", service_type = SyncDescription.service_type.Upload)
//@SyncDescription(chunk_size = 100000, service_name = "Suitability assessment survey entry", download_link = "/FarmersContract/Farmers/GetAssessmentResult", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class MaintenanceAssessmentSurveyEntry extends SurveyEntry implements Serializable {

    @DynamicProperty(json_key = "full_name")
    public String full_name;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "group_id")
    public String group_id;

    @DynamicProperty(json_key = "country_id")
    public String country_id;

    @DynamicProperty(json_key = "county_id")
    public String county;

    @DynamicProperty(json_key = "location_id")
    public String locationId;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location;

    @DynamicProperty(json_key = "village_id")
    public String village;

    @DynamicProperty(json_key = "site_id")
    public String site;

    @DynamicProperty(json_key = "agroforest_agent_id")
    public String agroforest_agent_id;

    @DynamicProperty(json_key = "is_maintenance_active")
    public String is_maintenance_active;

    @DynamicProperty(json_key = "group_name")
    public String group_name;

    @DynamicProperty(json_key = "pruning_done")
    public String pruning_done;

    @DynamicProperty(json_key = "crops_planted")
    public String crops_planted;

    @DynamicProperty(json_key = "iga_fsa")
    public String IGA_FSA;

    @DynamicProperty(json_key = "existing_indigenous_trees")
    public String existing_indigenous_trees;


    public SurveyQuestionChoice surveyQuestionChoice = new SurveyQuestionChoice();

    public MaintenanceAssessmentSurveyEntry() {

    }

    public MaintenanceAssessmentSurveyEntry(String member, String survey, String total_score) {
        this.member = member;
        this.survey = survey;
        this.total_score = total_score;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

}


