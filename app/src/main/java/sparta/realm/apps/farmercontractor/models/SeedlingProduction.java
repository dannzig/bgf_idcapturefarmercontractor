package sparta.realm.apps.farmercontractor.models;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.spartautils.svars;

@DynamicClass(table_name = "seedling_production")

@SyncDescription(service_name = "seedling production",chunk_size = 10000, download_link = "/Configurations/SeedlingNurseries/MobileTestGetSeedlingsGrid",is_ok_position = "JO:isOkay",download_array_position = "JO:result;JO:result",service_type = SyncDescription.service_type.Download)
@SyncDescription(service_name = "seedling Production",upload_link = "/Configurations/SeedlingNurseries/AddSeedlings",service_type = SyncDescription.service_type.Upload)
public class SeedlingProduction extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "id")
    public String id;

    @DynamicProperty(json_key = "nursery_type_id")
    public String nursery_type;

    @DynamicProperty(json_key = "country_id")
    public String country;

    @DynamicProperty(json_key = "county_id")
    public String county;

    @DynamicProperty(json_key = "location_id")
    public String location;

    @DynamicProperty(json_key = "sub_location_id")
    public String sub_location;

    @DynamicProperty(json_key = "village_id")
    public String village;

    @DynamicProperty(json_key = "site_id")
    public String site;

    @DynamicProperty(json_key = "agro_forest_agent_id")
    public String nursery_agent;

    @DynamicProperty(json_key = "seedlings_sown")
    public String seeds_sown;

    @DynamicProperty(json_key = "seedlings_pricked_out")
    public String seeds_pricked_out;

    @DynamicProperty(json_key = "total_no_seedlings")
    public String total_seedlings;

    @DynamicProperty(json_key = "seedlings_for_distribution")
    public String distribution_seedlings;

    @DynamicProperty(json_key = "reject_seedlings")
    public String reject_seedlings;

    @DynamicProperty(json_key = "dead_seedlings")
    public String dead_seedlings;

    @DynamicProperty(json_key = "local_insert_time")
    public String time_inserted;
    public SeedlingProduction(){}

    public SeedlingProduction(String nursery_type,String country, String county, String location, String sub_location, String village, String site, String nursery_agent, String seeds_sown, String seeds_pricked_out, String total_seedlings, String distribution_seedlings, String reject_seedlings, String dead_seedlings, String time_inserted){
        this.nursery_type = nursery_type;
        this.country = country;
        this.county = county;
        this.location = location;
        this.sub_location = sub_location;
        this.village = village;
        this.site = site;
        this.nursery_agent = nursery_agent;
        this.seeds_sown = seeds_sown;
        this.seeds_pricked_out = seeds_pricked_out;
        this.total_seedlings = total_seedlings;
        this.distribution_seedlings = distribution_seedlings;
        this.reject_seedlings = reject_seedlings;
        this.dead_seedlings = dead_seedlings;
        this.time_inserted = time_inserted;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
//        this.user_id = Globals.user().sid;

    }
}
