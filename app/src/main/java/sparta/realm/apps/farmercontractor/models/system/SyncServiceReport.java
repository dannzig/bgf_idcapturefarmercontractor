package sparta.realm.apps.farmercontractor.models.system;

public class SyncServiceReport {
    public String service_name="";
    public String total_records="";
    public String synchronized_records="";
    public String issues_records="";
}
