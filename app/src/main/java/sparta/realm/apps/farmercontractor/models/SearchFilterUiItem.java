package sparta.realm.apps.farmercontractor.models;


import android.widget.CheckBox;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.RealmModel;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;


public class SearchFilterUiItem extends RealmModel implements Serializable {


    public SearchFilterItem searchFilterItem;
    public CheckBox checkBox;
    public SearchSpinner searchSpinner;

    public SearchFilterUiItem(SearchFilterItem searchFilterItem, CheckBox checkBox, SearchSpinner searchSpinner) {
        this.searchFilterItem = searchFilterItem;
        this.checkBox = checkBox;
        this.searchSpinner = searchSpinner;
    }

}
