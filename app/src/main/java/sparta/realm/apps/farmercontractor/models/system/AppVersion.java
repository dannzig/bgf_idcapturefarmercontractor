package sparta.realm.apps.farmercontractor.models.system;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.db_class_;

import java.io.Serializable;


@DynamicClass(table_name = "app_versions_table")
public class AppVersion extends RealmModel implements Serializable {


    @DynamicProperty(json_key = "Version_name")
    public String version_name = "";

    @DynamicProperty(json_key = "Version_code")
    public String version_code = "";

    @DynamicProperty(json_key = "Release_name")
    public String release_name = "";

    @DynamicProperty(json_key = "release_time")
    public String release_time = "";

    @DynamicProperty(json_key = "Release_notes")
    public String release_notes = "";

    @DynamicProperty(json_key = "creation_time")
    public String creation_time = "";


    @DynamicProperty(json_key = "Landing_page_url")
    public String download_link = "";


    @DynamicProperty(json_key = "file")
    public String file = "";

    @DynamicProperty(json_key = "local_path")
    public String local_path = "";


    @DynamicProperty(json_key = "icon")
    public String icon = "";


    public AppVersion() {


    }


}
