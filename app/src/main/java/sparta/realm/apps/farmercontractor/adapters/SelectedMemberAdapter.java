package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberImage;
import sparta.realm.spartautils.svars;


public class SelectedMemberAdapter extends RecyclerView.Adapter<SelectedMemberAdapter.view> {

    Context cntxt;
    public ArrayList<Member> items;
    onItemClickListener listener;

    public interface onItemClickListener {

        void onItemClick(Member mem, View view);
    }


    public SelectedMemberAdapter(ArrayList<Member> items, onItemClickListener listener) {

        this.items = items;
        this.listener = listener;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_selected_member, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        Member obj = items.get(position);
        MemberImage mi = Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_transaction_no='" + obj.transaction_no + "'","member_transaction_no IS NOT NULL","image_index='"+MemberImage.MemberImageType.ProfilePhoto.ordinal()+"'"));

        obj.profile_photo = mi;
        holder.icon.setImageURI(null);
        if(mi!=null&&mi.image!=null) {
            holder.icon.setImageURI(null);
            holder.icon.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(Realm.context).file_path_employee_data) + obj.profile_photo.image));
        }

        holder.name.setText(obj.full_name);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(obj, holder.itemView);

            }
        });


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public String sid;
        public ImageView icon,close;


        view(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            icon = itemView.findViewById(R.id.icon);
//            icon = itemView.findViewById(R.id.icon);


        }

        @Override
        public void onClick(View view) {

        }
    }
}
