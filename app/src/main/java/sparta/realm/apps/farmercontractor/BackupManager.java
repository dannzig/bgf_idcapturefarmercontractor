package sparta.realm.apps.farmercontractor;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.os.EnvironmentCompat;

import com.github.lzyzsd.circleprogress.ArcProgress;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.realm.annotations.SyncDescription;
import com.realm.annotations.sync_service_description;
import com.realm.annotations.sync_status;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.EncryptionMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.CopyOption;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.models.BackupEntry;
import sparta.realm.spartaadapters.storage_item_adapter;
import sparta.realm.spartautils.StorageUtils;
import sparta.realm.spartautils.app_control.backup_;
import sparta.realm.spartautils.sparta_loc_util;
import sparta.realm.spartautils.sparta_mail_probe;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.Mail.MailActionCallback;
import sparta.realm.utils.Mail.MailData;
import sparta.realm.utils.Mail.OVHMailBuilder;

import static sparta.realm.Realm.realm;

public class BackupManager {
    static String logTag = "BackupManager";
    View backup_view;
    boolean backup_view_added = false;
    boolean backup_view_droped = false;
    backup_.backup_progress_listener backup_i;
    ConstraintLayout main;
    Activity act;
    ArrayList<View> launch_button;
    Button initialize_backup;
    TextView backup_view_title, backup_view_secondary_status, backup_view_secondary_percent_label, backup_view_last_backup_time, backup_log;
    ArcProgress backup_view_primary_backup_progress;
    ProgressBar backup_view_secondary_backup_progress;
    CheckBox backup_view_email_backup_check, backup_view_ftp_backup_check, backup_view_sd_backup_check, recrusive_backup_check;
    RelativeLayout backup_view_lay;
    Thread backup_thread;

    public BackupManager(Activity act, ConstraintLayout main) {
        this.act = act;
        this.main = main;

        setup_backup_view();


    }

    public static void backup_db(Activity act) {


        File s_file = new File(act.getExternalFilesDir(null).getAbsolutePath());
        s_file.mkdirs();
        File t_file = new File(act.getExternalFilesDir(null).getAbsolutePath() + "/backups");
        t_file.mkdirs();

        try {


            File sourceLocation = new File(s_file, svars.DB_NAME);
            File targetLocation = new File(t_file, System.currentTimeMillis() + ".spartadb");

            InputStream in = null;

            in = new FileInputStream(sourceLocation);

            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void backup_app_data_(Activity act, final backup_.backup_progress_listener listener) {

        String backup_transaction_code = "SPA_IDC" + System.currentTimeMillis() + "$" + svars.device_code(act).replace("|", "$") + "BU_RTA";
        listener.on_primary_status_changed(act.getString(R.string.verifying_files_to_backup));
        //   listener.on_secondary_status_changed("...");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  File fp_images_folder = new File(Environment.getExternalStorageDirectory().toString() + "/realm", "/.UNCOMPRESSED_FP_IMGS");
        File fp_images_folder = new File(svars.current_app_config(act).file_path_employee_data);
        fp_images_folder.mkdirs();
        //   listener.on_secondary_status_changed(act.getString(R.string.image_files_ok));
        listener.on_secondary_status_changed(act.getString(R.string.employee_data_files_ok));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        File db_folder = new File(act.getExternalFilesDir(null).getAbsolutePath());
        db_folder.mkdirs();
        listener.on_secondary_status_changed(act.getString(R.string.db_ok));
        listener.on_primary_progress_changed(10);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        File db_backup_folder = new File(svars.current_app_config(act).file_path_db_backup);
        db_backup_folder.mkdirs();
        listener.on_secondary_status_changed(act.getString(R.string.db_backup_ok));
        listener.on_primary_progress_changed(20);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        File log_folder = new File(act.getExternalFilesDir(null).getAbsolutePath() + "/logs");
        log_folder.mkdirs();
        listener.on_secondary_status_changed(act.getString(R.string.log_file_ok));
        listener.on_primary_progress_changed(30);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        File log_files_backup_folder = new File(svars.current_app_config(act).file_path_log_backup);
        log_files_backup_folder.mkdirs();
        listener.on_secondary_status_changed(act.getString(R.string.log_backup_ok));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        File global_backup_folder = new File(svars.current_app_config(act).file_path_general_backup);
        global_backup_folder.mkdirs();
        listener.on_secondary_status_changed(act.getString(R.string.backup_location_ok));
        listener.on_primary_progress_changed(40);

        listener.on_primary_status_changed(act.getString(R.string.staging_current_database));
        listener.on_secondary_status_changed(act.getString(R.string.staging_current_database));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            File sourceLocation = new File(db_folder, svars.DB_NAME);
            File targetLocation = new File(db_backup_folder, "SPA" + System.currentTimeMillis() + "DB_RTA");

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);


            byte[] buf = new byte[1024];
            int len;
            int per_counter = 0;

            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
                per_counter += len;
                // final double finalPercent = Math.round(((double) per_counter / (double) c.getContentLength()) * 100);
                final double finalPercent = sparta_loc_util.round(((double) per_counter / (double) sourceLocation.length()), 2) * 100;
                listener.on_secondary_progress_changed((int) finalPercent);
                // listener.on_secondary_progress_changed(Integer.parseInt((((""+finalPercent).split(".")[0])+((""+finalPercent).split(".")[1].toCharArray()[0])+((""+finalPercent).split(".")[1].toCharArray()[1]))));
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            listener.on_error_encountered(e.getMessage());
        }
        listener.on_primary_status_changed(act.getString(R.string.staging_log_files));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            File sourceLocation = new File(log_folder, svars.Log_file_name);
            File targetLocation = new File(log_files_backup_folder, "SPA" + System.currentTimeMillis() + "LG_RTA");

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);


            byte[] buf = new byte[1024];
            int len;
            int per_counter = 0;

            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
                per_counter += len;
                // final double finalPercent = Math.round(((double) per_counter / (double) c.getContentLength()) * 100);
                final double finalPercent = sparta_loc_util.round(((double) per_counter / (double) sourceLocation.length()), 2) * 100;


                listener.on_secondary_progress_changed((int) finalPercent);


                // listener.on_secondary_progress_changed(Integer.parseInt((((""+finalPercent).split(".")[0])+((""+finalPercent).split(".")[1].toCharArray()[0])+((""+finalPercent).split(".")[1].toCharArray()[1]))));
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            listener.on_error_encountered(e.getMessage());
        }
        listener.on_primary_progress_changed(50);

        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        listener.on_primary_status_changed(act.getString(R.string.prepairing_files_to_archive));

        ArrayList<String> files_tobackup = new ArrayList<>();
        File[] dump_files = fp_images_folder.listFiles();
        if (dump_files != null) {
            for (int i = 0; i < dump_files.length; i++) {
                files_tobackup.add(dump_files[i].getAbsolutePath());
            }
        }

        if (svars.recrusive_backup(act)) {

            dump_files = db_backup_folder.listFiles();
            if (dump_files != null) {
                for (int i = 0; i < dump_files.length; i++) {
                    files_tobackup.add(dump_files[i].getAbsolutePath());
                }
            }

        } else {
            files_tobackup.add(new File(db_folder, svars.DB_NAME).getAbsolutePath());

        }


        dump_files = log_files_backup_folder.listFiles();
        if (dump_files != null) {
            for (int i = 0; i < dump_files.length; i++) {
                files_tobackup.add(dump_files[i].getAbsolutePath());
            }

        }
        listener.on_primary_status_changed(act.getString(R.string.creating_archive));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        zip_array(act, files_tobackup, global_backup_folder.getAbsolutePath() + "/" + backup_transaction_code + ".zip", listener);
        listener.on_primary_status_changed(act.getString(R.string.archive_creation_complete));
        listener.on_primary_progress_changed(60);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        listener.on_primary_status_changed(act.getString(R.string.initializing_backup));

        if (svars.email_backup(act)) {
            listener.on_secondary_status_changed(act.getString(R.string.email_backup_begun));

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            send_backup_mail(global_backup_folder.getAbsolutePath() + "/" + backup_transaction_code + ".zip", act, listener);

        }
        if (svars.ftp_backup(act)) {
            listener.on_secondary_status_changed(act.getString(R.string.sftp_backup_begun));

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            send_backup_ftp(global_backup_folder.getAbsolutePath() + "/" + backup_transaction_code + ".zip", act, listener);
        }
        if (svars.sd_backup(act)) {
            listener.on_secondary_status_changed(act.getString(R.string.sd_card_backup_begun));

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
// String dstPath = Environment.getExternalStorageDirectory() + File.separator + ".realm_BACKUP" + File.separator;
// String dstPath = Environment.get()+ File.separator + ".realm_BACKUP" + File.separator;
//String dstPath =getSdCardPaths(act,false).get(0)+ File.separator + "realm_BACKUP";
            // String dstPath ="storage/emulated/1"+ File.separator + "realm_BACKUP" + File.separator;

            try {
                String dstPath = act.getExternalFilesDirs(null)[1] + File.separator + "realm_BACKUP";
                File sourceLocation = new File(global_backup_folder, backup_transaction_code + ".zip");
                File targetLocation = new File(dstPath);

                targetLocation.mkdirs();


                if (!targetLocation.exists()) {
                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    listener.on_error_encountered(act.getString(R.string.sd_card_not_available));
                    listener.on_secondary_status_changed(act.getString(R.string.sd_card_not_available));
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    listener.on_secondary_status_changed(act.getString(R.string.sd_card_backup_failed));
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    listener.on_secondary_status_changed(act.getString(R.string.sd_card_backup_terminated));
                    return;
                }
                targetLocation = new File(targetLocation, backup_transaction_code + ".zip");

                InputStream in = new FileInputStream(sourceLocation);
                OutputStream out = new FileOutputStream(targetLocation);


                byte[] buf = new byte[1024];
                int len;
                int per_counter = 0;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                    per_counter += len;
                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    // final double finalPercent = Math.round(((double) per_counter / (double) c.getContentLength()) * 100);
                    final double finalPercent = sparta_loc_util.round(((double) per_counter / (double) sourceLocation.length()), 2) * 100;


                    listener.on_secondary_progress_changed((int) finalPercent);


                    // listener.on_secondary_progress_changed(Integer.parseInt((((""+finalPercent).split(".")[0])+((""+finalPercent).split(".")[1].toCharArray()[0])+((""+finalPercent).split(".")[1].toCharArray()[1]))));
                }
                in.close();
                out.close();

                try {

                    File oldFolder = new File(dstPath, backup_transaction_code + ".zip");
                    File newFolder = new File(dstPath, Calendar.getInstance().getTime().toString().split("G")[0].trim().replace(" ", "_").replace(":", "-") + ".zip");
                    boolean success = oldFolder.renameTo(newFolder);
                    Log.e("Renaming backup =>", "" + success);

                } catch (Exception ex) {
                }

                listener.on_secondary_status_changed(act.getString(R.string.sd_card_backup_complete));
            } catch (Exception e) {
                e.printStackTrace();
                listener.on_error_encountered(e.getMessage());
                listener.on_secondary_status_changed(act.getString(R.string.sd_card_backup_failed));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ez) {
                    ez.printStackTrace();
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        listener.on_primary_progress_changed(100);
        listener.on_secondary_progress_changed(100);


        listener.on_primary_status_changed(act.getString(R.string.backup_complete));
        listener.on_secondary_status_changed(act.getString(R.string.backup_complete));
        svars.set_backup_time(act, Calendar.getInstance().getTime().toString().split("G")[0].trim());
        listener.on_backup_complete();
       /* File ff=new File(global_backup_folder.getAbsolutePath()+"/"+backup_transaction_code+".zip");
        ff.delete();*/
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    BackupListener backupListener=new BackupListener() {
        @Override
        public void onBackupArchiveCreated(BackupEntry backupEntry) {

        }

        @Override
        public void onStatusChanged(String status) {

        }

        @Override
        public void onBackupComplete() {

        }

        @Override
        public void onBackupBegun(BackupType backupType, BackupEntry backupEntry) {

        }

        @Override
        public void onBackupComplete(BackupType backupType, BackupEntry backupEntry) {

        }

        @Override
        public void onBackupFailed(BackupType backupType, BackupEntry backupEntry) {

        }
    };
    public static void backupAppData(Context act, BackupListener backupListener) {
        String backupFile = "RBV_2_" + svars.getTransactionNo().replace(":", "_") + ".zip";
        backupListener.onStatusChanged(act.getString(R.string.verifying_files_to_backup));
        //   listener.on_secondary_status_changed("...");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (validateFilepath(svars.current_app_config(act).file_path_employee_data))
            backupListener.onStatusChanged(act.getString(R.string.employee_data_files_ok));
        if (validateFilepath(svars.current_app_config(act).file_path_db_folder))
            backupListener.onStatusChanged(act.getString(R.string.db_ok));
        if (validateFilepath(svars.current_app_config(act).file_path_logs))
            backupListener.onStatusChanged(act.getString(R.string.log_files_ok));
        if (validateFilepath(svars.current_app_config(act).crashReportsFolder))
            backupListener.onStatusChanged(act.getString(R.string.trace_files_ok));

        String backup_folder = svars.current_app_config(act).file_path_general_backup + backupFile.replace(".zip", "") + "/";
        if (validateFilepath(backup_folder))
            backupListener.onStatusChanged(act.getString(R.string.backup_location_ok));
        ArrayList<String> backupList = new ArrayList<>();
        backupList.add(svars.current_app_config(act).file_path_db_folder + svars.current_app_config(act).DB_NAME);
        for (sync_service_description ssd : Realm.realm.getSyncDescription()) {
            if (ssd.servic_type == SyncDescription.service_type.Upload && ssd.storage_mode_check) {
//ArrayList<JSONObject> jsonObjects =Realm.databaseManager.load_dynamic_json_records_ann(ssd,new String[]{"sync_status='"+ sync_status.pending.ordinal()+"'"});
                ArrayList<JSONObject> jsonObjects = null;
                try {
                    jsonObjects = Realm.databaseManager.loadJSONArray(Class.forName(ssd.object_package), new Query().setTableFilters("sync_status='" + sync_status.pending.ordinal() + "'"));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                int total = jsonObjects.size();
                for (int i = 0; i < total; i++) {
//                    JSONObject jsonObject =Realm.databaseManager.loadJSONObject(Class.forName(ssd.object_package),new Query().setTableFilters("sync_status='"+ sync_status.pending.ordinal()+"'"));
                    JSONObject jsonObject = jsonObjects.get(i);
                    Iterator keys = jsonObject.keys();
                    List<String> key_list = new ArrayList<>();
                    while (keys.hasNext()) {
                        key_list.add((String) keys.next());
                    }

                    for (String k : realm.getFilePathFields(ssd.object_package, key_list)) {
                        try {
//                            backupList.add(svars.current_app_config(Realm.context).file_path_employee_data + jsonObject.getString(k));
                            File file = new File(svars.current_app_config(Realm.context).file_path_employee_data + jsonObject.getString(k));
                            copyFile(file.getAbsolutePath(), backup_folder + "ApplicationFiles/" + file.getName());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
        }


try{
    for (File file : new File(svars.current_app_config(act).file_path_logs).listFiles()) {
        if (file.isFile()) {
//                backupList.add(file.getAbsolutePath());
            copyFile(file.getAbsolutePath(), backup_folder + "Logs/" + file.getName());
        }
    }
}catch (Exception exception){

}
    try{    for (File file : new File(svars.current_app_config(act).crashReportsFolder).listFiles()) {
            if (file.isFile()) {
//                backupList.add(file.getAbsolutePath());
                copyFile(file.getAbsolutePath(), backup_folder + "CrashReports/" + file.getName());
            }
        }
    }catch (Exception exception){

    }
        backupListener.onStatusChanged("Staging files");
        for (String s : backupList) {

            copyFile(s, backup_folder + new File(s).getName());
        }

//zip folder

        backupListener.onStatusChanged(act.getString(R.string.creating_archive));
        try {
            ZipParameters zipParameters = new ZipParameters();
            zipParameters.setEncryptFiles(true);
            zipParameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);

            ZipFile zipFile = new ZipFile(svars.current_app_config(act).file_path_general_backup + backupFile, "pass".toCharArray());
            zipFile.addFolder(new File(backup_folder), zipParameters);
        } catch (ZipException e) {
            e.printStackTrace();
        }
        BackupEntry backupEntry = new BackupEntry(backupFile, new File(svars.current_app_config(act).file_path_general_backup + backupFile).length() + "", svars.current_app_config(act).file_path_general_backup + backupFile);

        backupListener.onBackupArchiveCreated(backupEntry);
        backupListener.onStatusChanged(act.getString(R.string.archive_creation_complete));

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        backupListener.onStatusChanged(act.getString(R.string.initializing_backup));

        if (Globals.isBackupTypeActive(BackupType.Mail)) {
            backupListener.onStatusChanged(act.getString(R.string.email_backup_begun));
            backupListener.onBackupBegun(BackupType.Mail,backupEntry);

            sendEmailBackup(backupEntry, backupListener);
        }

        if (Globals.isBackupTypeActive(BackupType.SFTP)) {
            backupListener.onBackupBegun(BackupType.SFTP,backupEntry);
            backupListener.onStatusChanged(act.getString(R.string.sftp_backup_begun));
            sendSFTPBackup(backupEntry, backupListener);
        }


        backupListener.onStatusChanged(act.getString(R.string.backup_complete));
//        svars.set_backup_time(act, Calendar.getInstance().getTime().toString().split("G")[0].trim());
        backupListener.onBackupComplete();


    }

    public static boolean validateFilepath(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        file.mkdirs();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return file.exists();
    }

    private static void copyFile(String inputPath, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(new File(outputPath).getParent());
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
//            new File(inputPath ).delete();


        } catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    static void send_backup_mail(final String file_path, final Activity act, backup_.backup_progress_listener listener) {

        if (Thread.currentThread().isInterrupted()) {
            return;
        }

        sparta_mail_probe smp = new sparta_mail_probe("IDCAPTURE Farmer contractor backup", "Device =" + svars.device_code(act) + "\nUsername =" + svars.user_name(act) + "\nUser id=" + svars.user_id(act) + "\nM link=" + svars.current_app_config(act).ACCOUNT + "\nApp version " + svars.current_version());

        try {
            File file = new File(file_path);
            long fileSizeInBytes = file.length();
            long fileSizeInKB = fileSizeInBytes / 1024;
            long fileSizeInMB = fileSizeInKB / 1024;

            smp.send_attachement(file_path);


        } catch (Exception ex) {


        }
    }

    static void sendEmailBackup(BackupEntry backupEntry, BackupListener backupListener) {

//        new Thread(() -> {
            new OVHMailBuilder().from(Globals.backupEmailConfiguration().username)
                    .setPassword(Globals.backupEmailConfiguration().password)
                    .to(Globals.backupEmail())
                    .subject("Application backup for " + Realm.context.getString(R.string.app_name))
                    .body(htmlBackupReport(backupEntry))
                    .addCCEmailAddress("idcapturefarmercontractor@capturesolutions.com")
                    .addAttachmentPath(new File(backupEntry.file_path),backupEntry.file_name)
                    .setBodyType(MailData.messageBodyType.HTML)
                    .setCallback(new MailActionCallback() {
                        @Override
                        public void onMailSent() {
                            backupListener.onBackupComplete(BackupType.Mail, backupEntry);

                        }

                        @Override
                        public void onActionLogged(String log) {
                            Log.e(logTag, log);
                            backupListener.onStatusChanged(log);
                        }

                        @Override
                        public void onMailSendingFailed(Exception ex) {
                            backupListener.onBackupFailed(BackupType.Mail, backupEntry);


                        }
                    })
                    .sendMail();
//        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    static String zipBackupReport(BackupEntry backupEntry) throws ZipException {
        StringBuilder reportBuilder = new StringBuilder();
        List<FileHeader> fileHeaders = new ZipFile(backupEntry.file_path, "pass".toCharArray()).getFileHeaders();
        fileHeaders.stream().forEach(fileHeader -> System.out.println(fileHeader.getFileName()));
        return null;
    }

    static String htmlBackupReport(BackupEntry backupEntry) {
        StringBuilder reportBuilder = new StringBuilder();
//        List<FileHeader> fileHeaders = new ZipFile(backupEntry.file_path, "pass".toCharArray()).getFileHeaders();
        reportBuilder.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n" +
                "<head>\n" +
                "    <meta charset=\"utf-8\"> <!-- utf-8 works for most cases -->\n" +
                "    <meta name=\"viewport\" content=\"width=device-width\"> <!-- Forcing initial-scale shouldn't be necessary -->\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> <!-- Use the latest (edge) version of IE rendering engine -->\n" +
                "    <meta name=\"x-apple-disable-message-reformatting\">  <!-- Disable auto-scale in iOS 10 Mail entirely -->\n" +
                "    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->\n" +
                "\n" +
                "    <link href=\"https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700\" rel=\"stylesheet\">\n" +
                "\n" +
                "    <!-- CSS Reset : BEGIN -->\n" +
                "    <style>\n" +
                "\n" +
                "        /* What it does: Remove spaces around the email design added by some email clients. */\n" +
                "        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */\n" +
                "        html,\n" +
                "body {\n" +
                "    margin: 0 auto !important;\n" +
                "    padding: 0 !important;\n" +
                "    height: 100% !important;\n" +
                "    width: 100% !important;\n" +
                "    background: #f1f1f1;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Stops email clients resizing small text. */\n" +
                "* {\n" +
                "    -ms-text-size-adjust: 100%;\n" +
                "    -webkit-text-size-adjust: 100%;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Centers email on Android 4.4 */\n" +
                "div[style*=\"margin: 16px 0\"] {\n" +
                "    margin: 0 !important;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Stops Outlook from adding extra spacing to tables. */\n" +
                "table,\n" +
                "td {\n" +
                "    mso-table-lspace: 0pt !important;\n" +
                "    mso-table-rspace: 0pt !important;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Fixes webkit padding issue. */\n" +
                "table {\n" +
                "    border-spacing: 0 !important;\n" +
                "    border-collapse: collapse !important;\n" +
                "    table-layout: fixed !important;\n" +
                "    margin: 0 auto !important;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Uses a better rendering method when resizing images in IE. */\n" +
                "img {\n" +
                "    -ms-interpolation-mode:bicubic;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */\n" +
                "a {\n" +
                "    text-decoration: none;\n" +
                "}\n" +
                "\n" +
                "/* What it does: A work-around for email clients meddling in triggered links. */\n" +
                "*[x-apple-data-detectors],  /* iOS */\n" +
                ".unstyle-auto-detected-links *,\n" +
                ".aBn {\n" +
                "    border-bottom: 0 !important;\n" +
                "    cursor: default !important;\n" +
                "    color: inherit !important;\n" +
                "    text-decoration: none !important;\n" +
                "    font-size: inherit !important;\n" +
                "    font-family: inherit !important;\n" +
                "    font-weight: inherit !important;\n" +
                "    line-height: inherit !important;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */\n" +
                ".a6S {\n" +
                "    display: none !important;\n" +
                "    opacity: 0.01 !important;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Prevents Gmail from changing the text color in conversation threads. */\n" +
                ".im {\n" +
                "    color: inherit !important;\n" +
                "}\n" +
                "\n" +
                "/* If the above doesn't work, add a .g-img class to any image in question. */\n" +
                "img.g-img + div {\n" +
                "    display: none !important;\n" +
                "}\n" +
                "\n" +
                "/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */\n" +
                "/* Create one of these media queries for each additional viewport size you'd like to fix */\n" +
                "\n" +
                "/* iPhone 4, 4S, 5, 5S, 5C, and 5SE */\n" +
                "@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {\n" +
                "    u ~ div .email-container {\n" +
                "        min-width: 320px !important;\n" +
                "    }\n" +
                "}\n" +
                "/* iPhone 6, 6S, 7, 8, and X */\n" +
                "@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {\n" +
                "    u ~ div .email-container {\n" +
                "        min-width: 375px !important;\n" +
                "    }\n" +
                "}\n" +
                "/* iPhone 6+, 7+, and 8+ */\n" +
                "@media only screen and (min-device-width: 414px) {\n" +
                "    u ~ div .email-container {\n" +
                "        min-width: 414px !important;\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "\n" +
                "    </style>\n" +
                "\n" +
                "    <!-- CSS Reset : END -->\n" +
                "\n" +
                "    <!-- Progressive Enhancements : BEGIN -->\n" +
                "    <style>\n" +
                "\n" +
                "\t    .primary{\n" +
                "\tbackground: #17bebb;\n" +
                "}\n" +
                ".bg_white{\n" +
                "\tbackground: #ffffff;\n" +
                "}\n" +
                ".bg_light{\n" +
                "\tbackground: #f7fafa;\n" +
                "}\n" +
                ".bg_black{\n" +
                "\tbackground: #000000;\n" +
                "}\n" +
                ".bg_dark{\n" +
                "\tbackground: rgba(0,0,0,.8);\n" +
                "}\n" +
                ".email-section{\n" +
                "\tpadding:2.5em;\n" +
                "}\n" +
                "\n" +
                "/*BUTTON*/\n" +
                ".btn{\n" +
                "\tpadding: 10px 15px;\n" +
                "\tdisplay: inline-block;\n" +
                "}\n" +
                ".btn.btn-primary{\n" +
                "\tborder-radius: 5px;\n" +
                "\tbackground: #17bebb;\n" +
                "\tcolor: #ffffff;\n" +
                "}\n" +
                ".btn.btn-white{\n" +
                "\tborder-radius: 5px;\n" +
                "\tbackground: #ffffff;\n" +
                "\tcolor: #000000;\n" +
                "}\n" +
                ".btn.btn-white-outline{\n" +
                "\tborder-radius: 5px;\n" +
                "\tbackground: transparent;\n" +
                "\tborder: 1px solid #fff;\n" +
                "\tcolor: #fff;\n" +
                "}\n" +
                ".btn.btn-black-outline{\n" +
                "\tborder-radius: 0px;\n" +
                "\tbackground: transparent;\n" +
                "\tborder: 2px solid #000;\n" +
                "\tcolor: #000;\n" +
                "\tfont-weight: 700;\n" +
                "}\n" +
                ".btn-custom{\n" +
                "\tcolor: rgba(0,0,0,.3);\n" +
                "\ttext-decoration: underline;\n" +
                "}\n" +
                "\n" +
                "h1,h2,h3,h4,h5,h6{\n" +
                "\tfont-family: 'Poppins', sans-serif;\n" +
                "\tcolor: #000000;\n" +
                "\tmargin-top: 0;\n" +
                "\tfont-weight: 400;\n" +
                "}\n" +
                "\n" +
                "body{\n" +
                "\tfont-family: 'Poppins', sans-serif;\n" +
                "\tfont-weight: 400;\n" +
                "\tfont-size: 15px;\n" +
                "\tline-height: 1.8;\n" +
                "\tcolor: rgba(0,0,0,.4);\n" +
                "}\n" +
                "\n" +
                "a{\n" +
                "\tcolor: #17bebb;\n" +
                "}\n" +
                "\n" +
                "table{\n" +
                "}\n" +
                "/*LOGO*/\n" +
                "\n" +
                ".logo h1{\n" +
                "\tmargin: 0;\n" +
                "}\n" +
                ".logo h1 a{\n" +
                "\tcolor: #17bebb;\n" +
                "\tfont-size: 24px;\n" +
                "\tfont-weight: 700;\n" +
                "\tfont-family: 'Poppins', sans-serif;\n" +
                "}\n" +
                "\n" +
                "/*HERO*/\n" +
                ".hero{\n" +
                "\tposition: relative;\n" +
                "\tz-index: 0;\n" +
                "}\n" +
                "\n" +
                ".hero .text{\n" +
                "\tcolor: rgba(0,0,0,.3);\n" +
                "}\n" +
                ".hero .text h2{\n" +
                "\tcolor: #000;\n" +
                "\tfont-size: 34px;\n" +
                "\tmargin-bottom: 0;\n" +
                "\tfont-weight: 200;\n" +
                "\tline-height: 1.4;\n" +
                "}\n" +
                ".hero .text h3{\n" +
                "\tfont-size: 24px;\n" +
                "\tfont-weight: 300;\n" +
                "}\n" +
                ".hero .text h2 span{\n" +
                "\tfont-weight: 600;\n" +
                "\tcolor: #000;\n" +
                "}\n" +
                "\n" +
                ".text-author{\n" +
                "\tbordeR: 1px solid rgba(0,0,0,.05);\n" +
                "\tmax-width: 50%;\n" +
                "\tmargin: 0 auto;\n" +
                "\tpadding: 2em;\n" +
                "}\n" +
                ".text-author img{\n" +
                "\tborder-radius: 50%;\n" +
                "\tpadding-bottom: 20px;\n" +
                "}\n" +
                ".text-author h3{\n" +
                "\tmargin-bottom: 0;\n" +
                "}\n" +
                "ul.social{\n" +
                "\tpadding: 0;\n" +
                "}\n" +
                "ul.social li{\n" +
                "\tdisplay: inline-block;\n" +
                "\tmargin-right: 10px;\n" +
                "}\n" +
                "\n" +
                "/*FOOTER*/\n" +
                "\n" +
                ".footer{\n" +
                "\tborder-top: 1px solid rgba(0,0,0,.05);\n" +
                "\tcolor: rgba(0,0,0,.5);\n" +
                "}\n" +
                ".footer .heading{\n" +
                "\tcolor: #000;\n" +
                "\tfont-size: 20px;\n" +
                "}\n" +
                ".footer ul{\n" +
                "\tmargin: 0;\n" +
                "\tpadding: 0;\n" +
                "}\n" +
                ".footer ul li{\n" +
                "\tlist-style: none;\n" +
                "\tmargin-bottom: 10px;\n" +
                "}\n" +
                ".footer ul li a{\n" +
                "\tcolor: rgba(0,0,0,1);\n" +
                "}\n" +
                "\n" +
                "\n" +
                "@media screen and (max-width: 500px) {\n" +
                "\n" +
                "\n" +
                "}\n" +
                "\n" +
                "\n" +
                "    </style>\n" +
                "\n" +
                "\n" +
                "</head>\n" +
                "\n" +
                "<body width=\"100%\" style=\"margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;\">\n" +
                "\t<center style=\"width: 100%; background-color: #f1f1f1;\">\n" +
                "    <div style=\"display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;\">\n" +
                "      &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;\n" +
                "    </div>\n" +
                "    <div style=\"max-width: 600px; margin: 0 auto;\" class=\"email-container\">\n" +
                "    \t<!-- BEGIN BODY -->\n" +
                "      <table align=\"center\" role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"margin: auto;\">\n" +
                "      \t<tr>\n" +
                "          <td valign=\"top\" class=\"bg_white\" style=\"padding: 1em 2.5em 0 2.5em;\">\n" +
                "          \t<table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
                "          \t\t<tr>\n" +
                "          \t\t\t<td class=\"logo\" style=\"text-align: center;\">\n" +
                "\t\t\t            <h1><a href=\"#\">Capture Solutions</a></h1>\n" +
                "\t\t\t          </td>\n" +
                "          \t\t</tr>\n" +
                "          \t</table>\n" +
                "          </td>\n" +
                "\t      </tr><!-- end tr -->\n" +
                "\t\t\t\t<tr>\n" +
                "          <td valign=\"middle\" class=\"hero bg_white\" style=\"padding: 2em 0 4em 0;\">\n" +
                "            <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
                "            \t<tr>\n" +
                "            \t\t<td style=\"padding: 0 2.5em; text-align: center; padding-bottom: 3em;\">\n" +
                "            \t\t\t<div class=\"text\">\n" +
                "            \t\t\t\t<h2>Application backup for Idcapture farmer contractor</h2>\n" +
                "            \t\t\t</div>\n" +
                "            \t\t</td>\n" +
                "            \t</tr>\n" +
                "            \t<tr>\n" +
                "\t\t\t          <td style=\"text-align: center;\">\n" +
                "\t\t\t          \t<div class=\"text-author\">\n" +
                "\t\t\t\t          \t<img src=\"images/person_2.jpg\" alt=\"\" style=\"width: 100px; max-width: 600px; height: auto; margin: auto; display: block;\">\n" +
                "\t\t\t\t          \t<h3 class=\"name\">" + splitNL(backupEntry.file_name, 20) + "</h3>\n" +
                "\t\t\t\t          \t<span class=\"position\">Backup size:" + getUserDisplayBytes(backupEntry.file_size) + "</span>\n" +
                "\t\t\t\t           \t<p><a href=\"#\" class=\"btn btn-primary\">Download backup</a></p>\n" +
                "\t\t\t\t           \t<p><a href=\"#\" class=\"btn-custom\">Ignore Request</a></p>\n" +
                "\t\t\t           \t</div>\n" +
                "\t\t\t          </td>\n" +
                "\t\t\t        </tr>\n" +
                "            </table>\n" +
                "          </td>\n" +
                "\t      </tr><!-- end tr -->\n" +
                "      <!-- 1 Column Text + Button : END -->\n" +
                "      </table>\n" +
                "      <table align=\"center\" role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"margin: auto;\">\n" +
                "      \t<tr>\n" +
                "          <td valign=\"middle\" class=\"bg_light footer email-section\">\n" +
                "            <table>\n" +
                "            \t<tr>\n" +
                "                <td valign=\"top\" width=\"33.333%\" style=\"padding-top: 20px;\">\n" +
                "                  <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" +
                "                    <tr>\n" +
                "                      <td style=\"text-align: left; padding-right: 10px;\">\n" +
                "                      \t<h3 class=\"heading\">About</h3>\n" +
                "                      \t<p>Capture solutions application backup.Realm v 1.0.190</p>\n" +
                "                      </td>\n" +
                "                    </tr>\n" +
                "                  </table>\n" +
                "                </td>\n" +
                "                <td valign=\"top\" width=\"33.333%\" style=\"padding-top: 20px;\">\n" +
                "                  <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" +
                "                    <tr>\n" +
                "                      <td style=\"text-align: left; padding-left: 5px; padding-right: 5px;\">\n" +
                "                      \t<h3 class=\"heading\">Contact Info</h3>\n" +
                "                      \t<ul>\n" +
                "\t\t\t\t\t                <li><span class=\"text\">Kenya-Nairobi- Parklands,6th avenue, valley view offic park,</span></li>\n" +
                "\t\t\t\t\t                <li><span class=\"text\">+254 715 300 161</span></a></li>\n" +
                "\t\t\t\t\t              </ul>\n" +
                "                      </td>\n" +
                "                    </tr>\n" +
                "                  </table>\n" +
                "                </td>\n" +
                "                <td valign=\"top\" width=\"33.333%\" style=\"padding-top: 20px;\">\n" +
                "                  <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" +
                "                    <tr>\n" +
                "                      <td style=\"text-align: left; padding-left: 10px;\">\n" +
                "                      \t<h3 class=\"heading\">Useful Links</h3>\n" +
                "                      \t<ul>\n" +
                "\t\t\t\t\t                <li><a href=\"https://www.cs4africa.com/\">Home</a></li>\n" +
                "                                    <li><a href=\"https://www.capturesolutions.com/id-capture\">IDCAPTURE</a></li>\n" +
                "                                    <li><a href=\"https://www.capturesolutions.com/timecapture\">Timecapture</a></li>\n" +
                "\t\t\t\t\t                <li><a href=\"https://www.cs4africa.com/agricapture/\">Agricapture</a></li>\n" +
                "                                    </ul>\n" +
                "                      </td>\n" +
                "                    </tr>\n" +
                "                  </table>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "            </table>\n" +
                "          </td>\n" +
                "        </tr><!-- end: tr -->\n" +
                "        <tr>\n" +
                "         \n" +
                "         \n" +
                "        </tr>\n" +
                "      </table>\n" +
                "\n" +
                "    </div>\n" +
                "  </center>\n" +
                "</body>\n" +
                "</html>");
        return reportBuilder.toString();
    }

    static String splitNL(String string, int len) {
        if (string.length() < len) return string;
        String out = "";
        for (int i = 0; i < string.length(); i++) {
            out += string.charAt(i) + ((i % len == 0) ? "\n" : "");
        }
        return out.trim();
    }

    public static String getUserDisplayBytes(String bytes) {

        long fileSizeInBytes = Long.parseLong(bytes);
        if (fileSizeInBytes > 1024) {
            long fileSizeInKB = fileSizeInBytes / 1024;
            if (fileSizeInKB > 1024) {
                long fileSizeInMB = fileSizeInKB / 1024;
                if (fileSizeInMB > 1024) {
                    double fileSizeInGB = (double) fileSizeInMB / (double) 1024;

                    return Math.round(fileSizeInGB) + " gb";


                } else {
                    return fileSizeInMB + " mb";

                }

            } else {
                return fileSizeInKB + " kb";
            }


        } else {
            return fileSizeInBytes + " bytes";
        }


    }

    static void sendSFTPBackup(BackupEntry backupEntry, BackupListener backupListener) {
        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        Log.e(logTag, "Uploading SFTP : " + backupEntry.file_path);

        File uploadFilePath;
        Session session;
        Channel channel = null;
        ChannelSftp sftp = null;
        uploadFilePath = new File(backupEntry.file_path);


        JSch ssh = new JSch();
        int reconnection_count = 0;
        int max_reconnection_count = 10;
        while (true) {

            try {


                session = ssh.getSession("FarmerContractorBackup", "ta.cs4africa.com", 5032);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("@FarmerContractorBackup123");
                session.connect();
                backupListener.onStatusChanged(Realm.context.getString(R.string.connected_to_sftp_server));


                channel = session.openChannel("sftp");
                channel.connect();
                sftp = (ChannelSftp) channel;

                try{
                    sftp.mkdir("User_" + svars.user_id(Realm.context));
                }catch (Exception ex){}
                sftp.cd("User_" + svars.user_id(Realm.context));


                break;
            } catch (Exception e) {
                Log.e("Upload error =>", "" + e.getMessage());
                backupListener.onBackupFailed(BackupType.SFTP, backupEntry);
//                listener.on_error_encountered(act.getString(R.string.connection_failed));
                if (reconnection_count < max_reconnection_count) {
                    reconnection_count++;
                    backupListener.onStatusChanged(Realm.context.getString(R.string.attempting_sftp_reconnection));

                } else {
//                    session.disconnect();
                    return;
                }

            }
        }
        backupListener.onStatusChanged(Realm.context.getString(R.string.uploading_backup));
        try {
            Log.e(logTag,"Upload file check:Exists:"+uploadFilePath.exists());
            FileInputStream fileInputStream = new FileInputStream(uploadFilePath);
//            ByteArrayInputStream fileInputStream2 =new ByteArrayInputStream( new FileInputStream(uploadFilePath));
            sftp.put(fileInputStream, uploadFilePath.getName(), null);
            fileInputStream.close();
        } catch (SftpException e) {
            Log.e(logTag,"Upload error:SftpException:"+e);
            backupListener.onBackupFailed(BackupType.SFTP, backupEntry);
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            Log.e(logTag,"Upload error:FileNotFoundException:"+e);
            backupListener.onBackupFailed(BackupType.SFTP, backupEntry);
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(logTag,"Upload error:IOException:"+e);
            backupListener.onBackupFailed(BackupType.SFTP, backupEntry);
            e.printStackTrace();
        }


        if (sftp.getExitStatus() == -1) {
            backupListener.onBackupComplete(BackupType.SFTP, backupEntry);
        } else {
            backupListener.onBackupFailed(BackupType.SFTP, backupEntry);


        }
    }

    static void send_backup_ftp(String path, Activity act, backup_.backup_progress_listener listener) {
        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        Log.e(logTag, "UPLOADING TO SFTP : " + path);

        File uploadFilePath;
        Session session;
        Channel channel = null;
        ChannelSftp sftp = null;
        uploadFilePath = new File(path);
        //uploadFilePath=new File(Environment.getExternalStorageDirectory()+"/Android/data/sparta.farmercontractor/files/Output/"+filename);
        byte[] bufr = new byte[(int) uploadFilePath.length()];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(uploadFilePath);
        } catch (FileNotFoundException e1) {
            Log.e(logTag, "File not found error encountered " + uploadFilePath);
            listener.on_error_encountered(act.getString(R.string.missing_file) + uploadFilePath.getName());
            e1.printStackTrace();

        }
        try {
            fis.read(bufr);
        } catch (Exception e1) {
            Log.e(logTag, "File not found error encountered " + uploadFilePath);
            e1.printStackTrace();
            listener.on_error_encountered(act.getString(R.string.missing_file) + uploadFilePath.getName());
        }


        JSch ssh = new JSch();
        int reconnection_count = 0;
        int max_reconnection_count = 10;
        while (true) {

            try {

                session = ssh.getSession("Togo", "www.cs4africa.com", 1989);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("TOGO");


                session.connect();
                listener.on_secondary_status_changed(act.getString(R.string.connected_to_sftp_server));


                channel = session.openChannel("sftp");
                channel.connect();
                sftp = (ChannelSftp) channel;

                try {
                    sftp.mkdir(svars.ftp_folder(act));
                } catch (Exception ex) {
                }
                try {
                    sftp.cd(svars.ftp_folder(act));
                } catch (Exception ee) {
                }

                break;
            } catch (Exception e) {
                Log.e("Upload error =>", "" + e.getMessage());
                listener.on_error_encountered(act.getString(R.string.connection_failed));
                if (reconnection_count < max_reconnection_count) {
                    reconnection_count++;
                    listener.on_secondary_status_changed(act.getString(R.string.attempting_sftp_reconnection));

                } else {
                    return;
                }

            }
        }
        listener.on_secondary_status_changed(act.getString(R.string.uploading_backup));

        ByteArrayInputStream in = new ByteArrayInputStream(bufr);
        try {
            sftp.put(in, uploadFilePath.getName(), null);
            // listener.on_secondary_status_changed("Uploading backup");
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            listener.on_error_encountered(act.getString(R.string.sftp_upload_failed) + uploadFilePath.getName());
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e) {
            listener.on_error_encountered("SFTP server disconnection failed :" + uploadFilePath.getName());

            e.printStackTrace();
        }

        if (sftp.getExitStatus() == -1) {
            System.out.println("file uploaded");
            Log.e("upload result", "succeeded");
            listener.on_secondary_status_changed(act.getString(R.string.sftp_upload_successfull));

            //toast = 1;

            //toastHandlefinish.sendEmptyMessage(0);

        } else {
            Log.e("upload failed ", "failed");
            listener.on_error_encountered(act.getString(R.string.sftp_upload_failed));

        }
    }

    public static List<String> getSdCardPaths(final Context context, final boolean includePrimaryExternalStorage) {
        final File[] externalCacheDirs = ContextCompat.getExternalCacheDirs(context);
        if (externalCacheDirs == null || externalCacheDirs.length == 0)
            return null;
        if (externalCacheDirs.length == 1) {
            if (externalCacheDirs[0] == null)
                return null;
            final String storageState = EnvironmentCompat.getStorageState(externalCacheDirs[0]);
            if (!Environment.MEDIA_MOUNTED.equals(storageState))
                return null;
            if (!includePrimaryExternalStorage && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && Environment.isExternalStorageEmulated())
                return null;
        }
        final List<String> result = new ArrayList<>();
        if (includePrimaryExternalStorage || externalCacheDirs.length == 1)
            result.add(getRootOfInnerSdCardFolder(externalCacheDirs[0]));
        for (int i = 1; i < externalCacheDirs.length; ++i) {
            final File file = externalCacheDirs[i];
            if (file == null)
                continue;
            final String storageState = EnvironmentCompat.getStorageState(file);
            if (Environment.MEDIA_MOUNTED.equals(storageState))
                result.add(getRootOfInnerSdCardFolder(externalCacheDirs[i]));
        }
        if (result.isEmpty())
            return null;
        return result;
    }

    private static String getRootOfInnerSdCardFolder(File file) {
        if (file == null)
            return null;
        final long totalSpace = file.getTotalSpace();
        while (true) {
            final File parentFile = file.getParentFile();
            if (parentFile == null || parentFile.getTotalSpace() != totalSpace)
                return file.getAbsolutePath();
            file = parentFile;
        }
    }

    public static void zip_array(Activity act, ArrayList<String> _files, String zipFileName, backup_.backup_progress_listener listener) {
        try {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            int BUFFER = 1024;
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.size(); i++) {
                if (Thread.currentThread().isInterrupted()) {
                    return;
                }
                Log.e("Compression :", "Adding: " + _files.get(i));
                FileInputStream fi = new FileInputStream(_files.get(i));
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files.get(i).substring(_files.get(i).lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
                listener.on_secondary_status_changed(act.getString(R.string.archiving) + _files.get(i).substring(_files.get(i).lastIndexOf("/") + 1));
                listener.on_secondary_progress_changed((int) ((((double) i) / ((double) _files.size())) * 100));
                // listener.on_secondary_status_changed("Archiving :"+_files.get(i).substring(_files.get(i).lastIndexOf("/") + 1));
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            listener.on_error_encountered(e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void zipFolderNio(Path source) throws IOException {

        // get current working directory
        String currentPath = System.getProperty("user.dir") + File.separator;

        // get folder name as zip file name
        // can be other extension, .foo .bar .whatever
        String zipFileName = source.getFileName().toString() + ".zip";
        URI uri = URI.create("jar:file:" + currentPath + zipFileName);

        Files.walkFileTree(source, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {

                // Copying of symbolic links not supported
                if (attributes.isSymbolicLink()) {
                    return FileVisitResult.CONTINUE;
                }

                Map<String, String> env = new HashMap<>();
                env.put("create", "true");

                try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {

                    Path targetFile = file.relativize(file);
                    Path pathInZipfile = zipfs.getPath(targetFile.toString());

                    // NoSuchFileException, need create parent directories in zip path
                    if (pathInZipfile.getParent() != null) {
                        Files.createDirectories(pathInZipfile.getParent());
                    }

                    // copy file attributes
                    CopyOption[] options = {
                            StandardCopyOption.REPLACE_EXISTING,
                            StandardCopyOption.COPY_ATTRIBUTES,
                            LinkOption.NOFOLLOW_LINKS
                    };
                    // Copy a file into the zip file path
                    Files.copy(file, pathInZipfile, options);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                System.err.printf("Unable to zip : %s%n%s%n", file, exc);
                return FileVisitResult.CONTINUE;
            }

        });

    }

    void animate_backup_view() {

        if (!backup_view_added) {
            main.addView(backup_view);
            backup_view_added = true;
            ObjectAnimator obj = ObjectAnimator.ofFloat(backup_view, "translationX", 100, 0f);
            obj.setDuration(1000);
            obj.start();
        }
        backup_view.bringToFront();
        if (!backup_view_droped) {
            ObjectAnimator obj = ObjectAnimator.ofFloat(backup_view, "translationX", 180, 0f);
            obj.setDuration(1000);
            obj.start();
            ObjectAnimator obj2 = ObjectAnimator.ofFloat(backup_view, "alpha", 0, 1f);
            obj2.setDuration(1500);
            obj2.start();


            backup_view_droped = true;


        } else {
            ObjectAnimator obj = ObjectAnimator.ofFloat(backup_view, "translationX", 0f, 180f);
            obj.setDuration(1000);
            obj.start();
            ObjectAnimator obj2 = ObjectAnimator.ofFloat(backup_view, "alpha", 1, 0f);
            obj2.setDuration(950);
            obj2.start();


            backup_view_droped = false;


        }
    }

    void setup_backup_view() {

        backup_view = LayoutInflater.from(act).inflate(R.layout.backup_view, null, false);
        backup_view_lay = (RelativeLayout) backup_view.findViewById(R.id.backup_lay);

        initialize_backup = (Button) backup_view.findViewById(R.id.init_backup);

        backup_view_primary_backup_progress = (ArcProgress) backup_view.findViewById(R.id.arc_primary_progress);
        backup_view_secondary_backup_progress = (ProgressBar) backup_view.findViewById(R.id.secondary_backup_progress);

        backup_view_secondary_percent_label = (TextView) backup_view.findViewById(R.id.secondary_percent_val);
        backup_view_title = (TextView) backup_view.findViewById(R.id.primary_status_val);
        backup_view_secondary_status = (TextView) backup_view.findViewById(R.id.secondary_status_val);
        backup_view_last_backup_time = (TextView) backup_view.findViewById(R.id.last_backup_time_val);
        backup_log = (TextView) backup_view.findViewById(R.id.backup_log);
        backup_log.setMovementMethod(new ScrollingMovementMethod());

        backup_view_email_backup_check = (CheckBox) backup_view.findViewById(R.id.email_backup_check);
        backup_view_ftp_backup_check = (CheckBox) backup_view.findViewById(R.id.ftp_backup_check);
        backup_view_sd_backup_check = (CheckBox) backup_view.findViewById(R.id.sd_backup_check);
        recrusive_backup_check = (CheckBox) backup_view.findViewById(R.id.recrusive_backup);

        backup_view_email_backup_check.setChecked(svars.email_backup(act));
        backup_view_ftp_backup_check.setChecked(svars.ftp_backup(act));
        backup_view_sd_backup_check.setChecked(svars.sd_backup(act));
        recrusive_backup_check.setChecked(svars.recrusive_backup(act));

        backup_view_sd_backup_check.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                display_storage_diagnostics();
                return false;
            }
        });
        recrusive_backup_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                svars.set_recrusive_backupp(act, b);
            }
        });

        backup_view_email_backup_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                svars.set_email_backup(act, b);
            }
        });


        backup_view_ftp_backup_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                svars.set_ftp_backup(act, b);
            }
        });


        backup_view_sd_backup_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                svars.set_sd_backup(act, b);
            }
        });


        backup_i = new backup_.backup_progress_listener() {
            @Override
            public void on_primary_status_changed(final String status) {
                Log.e("Primary status =>", "" + status);
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        backup_view_title.setText(status);
                        backup_log.setText(status + "\n" + backup_log.getText().toString());
                        backup_log.invalidate();
                        backup_view_last_backup_time.setText(svars.backup_time(act));
                        backup_view_last_backup_time.invalidate();
                    }
                });
            }

            @Override
            public void on_secondary_status_changed(final String status) {
                Log.e("Secondary status =>", "" + status);
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        backup_view_secondary_status.setText(status);
                        backup_log.setText(status + "\n" + backup_log.getText().toString());

                        backup_view_secondary_status.invalidate();
                        backup_log.invalidate();
                    }
                });
            }

            @Override
            public void on_primary_progress_changed(final int progress) {
                Log.e("Primary progress =>", "" + progress);
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        backup_view_primary_backup_progress.setProgress(progress);
                    }
                });
            }

            @Override
            public void on_secondary_progress_changed(final int progress) {
                Log.e("Secondary progress =>", "" + progress);
                //   backup_view_secondary_backup_progress.invalidate();
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        backup_view_secondary_backup_progress.setProgress(progress);
                        backup_view_secondary_percent_label.setText(progress + "%");
                        //   Log.e("Secondary progress M =>",""+progress);
                    }
                });
            }

            @Override
            public void on_error_encountered(final String status) {
                Log.e("Error =>", "" + status);
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String next = "<font color='#EE0000'>" + "<br/>" + backup_log.getText().toString() + "</font>";
                        // backup_log.setText(Html.fromHtml(status + next));
                        backup_log.setText("Error : " + status + "\n" + backup_log.getText().toString());
                        backup_log.invalidate();
                    }
                });


            }

            @Override
            public void on_backup_complete() {
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initialize_backup.setEnabled(true);
                        backup_view_last_backup_time.setText(svars.backup_time(act));
                    }
                });
            }
        };
        initialize_backup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    backup_thread.interrupt();
                } catch (Exception ex) {
                }
                backup_thread = null;
                backup_view_title.setText(act.getString(R.string.backup_title));
                backup_view_secondary_percent_label.setText("0%");
                backup_view_secondary_status.setText("");
                backup_view_secondary_backup_progress.setProgress(0);
                backup_view_primary_backup_progress.setProgress(0);
                backup_log.setText("");
                initialize_backup.setEnabled(false);
                backup_thread = new Thread(new Runnable() {
                    @Override
                    public void run() {


                        backupAppData(act, backupListener);
                    }
                });

                backup_thread.start();

            }
        });
    }

    void display_storage_diagnostics() {
        final View aldv = LayoutInflater.from(act).inflate(R.layout.dialog_storage_report, null);
        final GridView storage_list = (GridView) aldv.findViewById(R.id.version_list);

        final TextView log_file_size_label = (TextView) aldv.findViewById(R.id.log_file_size_label);
        final TextView database_file_size_label = (TextView) aldv.findViewById(R.id.database_file_size_label);
        final TextView backup_folder_size_label = (TextView) aldv.findViewById(R.id.backup_label_size_label);

        log_file_size_label.setText(StorageUtils.getfile_size(new File(act.getExternalFilesDir(null).getAbsolutePath() + "/logs/" + svars.Log_file_name)));
        database_file_size_label.setText(StorageUtils.getfile_size(new File(act.getExternalFilesDir(null).getAbsolutePath() + "/" + svars.DB_NAME)));
        backup_folder_size_label.setText(StorageUtils.getfolder_size(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/TimeandAttendance/backups")));

        final Button dismiss = (Button) aldv.findViewById(R.id.update);


        storage_list.setAdapter(new storage_item_adapter(act, StorageUtils.getStorageList()));
        final AlertDialog ald = new AlertDialog.Builder(act)
                .setView(aldv)
                .show();
    }

    public void triger_backup() {
        animate_backup_view();
    }

    public void zip(String[] _files, String zipFileName) {
        try {

            int BUFFER = 1250 * 1250;
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.length; i++) {
                Log.e("Compression :", "Adding: " + _files[i]);
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum BackupType {
        Null,
        Mail,
        SFTP,
        GoogleDrive

    }

    public interface BackupListener {
        void onBackupArchiveCreated(BackupEntry backupEntry);
        void onStatusChanged(String status);

        void onBackupComplete();

        void onBackupBegun(BackupType backupType, BackupEntry backupEntry);
        void onBackupComplete(BackupType backupType, BackupEntry backupEntry);

        void onBackupFailed(BackupType backupType, BackupEntry backupEntry);


    }


    class BackupItem {


    }


}
