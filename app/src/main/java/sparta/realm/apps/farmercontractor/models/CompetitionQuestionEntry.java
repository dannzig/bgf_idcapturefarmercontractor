package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "competition_question_entry")
@SyncDescription(service_name = "Competition question entry", upload_link = "/FarmersContract/Group/AddGroupCompetitionProgresdetails", service_type = SyncDescription.service_type.Upload)

public class CompetitionQuestionEntry extends RealmModel implements Serializable {
//{"$id":"6","id":1,"member_id":4120,"farmer_assessment_questionnaire_id":10,"marks":4,"local_insert_time":"0001-01-01T00:00:00","rating":10,"survey_entry_transaction_no":"973bb825-53b7-11ed-960e-fa163ec2df5f","transaction_no":"81744d25-53b9-11ed-960e-fa163ec2df5f","full_name":"ACEN ROSE MARY","nat_id":"20953015","question_name":"Farm yields: quality of land, crop yields (e.g. green grams, sorghum)

//    {
//        "farmer_assessment_questionnaire_id" :3,
//            "transaction_no" : "0101",
//            "total_marks" :10,
//            "actual_score" : 2,
//            "date_time" : "2023-12-01",
//            "member_transansaction_no": "202764",
//            "user_id":1
//    }

    @DynamicProperty(json_key = "survey_entry_transaction_no")
    public String survey_entry_transaction_no;
    @DynamicProperty(json_key = "farmer_assessment_questionnaire_id")
    public String question;
    @DynamicProperty(json_key = "rating")
    public String choice;
    @DynamicProperty(json_key = "actual_score")
    public String marks;
    @DynamicProperty(json_key = "date_time")
    public String date_time;


    public SurveyQuestionChoice surveyQuestionChoice = new SurveyQuestionChoice();


    public CompetitionQuestionEntry() {


    }

    public CompetitionQuestionEntry(String survey_entry_transaction_no, String question, String choice, String marks) {
        this.survey_entry_transaction_no = survey_entry_transaction_no;
        this.question = question;
        this.choice = choice;
        this.marks = marks;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

}
