package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


public class SurveyEntry extends RealmModel implements Serializable {

//{"$id":"6","id":1,"member_id":4120,"farmer_assessment_questionnaire_id":10,"marks":4,"local_insert_time":"0001-01-01T00:00:00","rating":10,"survey_entry_transaction_no":"973bb825-53b7-11ed-960e-fa163ec2df5f","transaction_no":"81744d25-53b9-11ed-960e-fa163ec2df5f","full_name":"ACEN ROSE MARY","nat_id":"20953015","question_name":"Farm yields: quality of land, crop yields (e.g. green grams, sorghum)

    @DynamicProperty(json_key = "survey")
    public String survey;

 @DynamicProperty(json_key = "member_id")
    public String member;

    @DynamicProperty(json_key = "member_transaction_no")
    public String member_transaction_no;

    @DynamicProperty(json_key = "total_score")
    public String total_score;




    public SurveyQuestionChoice surveyQuestionChoice=new SurveyQuestionChoice();


    public SurveyEntry() {


    }
    public SurveyEntry(String member, String survey, String total_score) {
        this.member = member;
        this.survey = survey;
        this.total_score = total_score;


        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id= Globals.user().sid;
    }

}
