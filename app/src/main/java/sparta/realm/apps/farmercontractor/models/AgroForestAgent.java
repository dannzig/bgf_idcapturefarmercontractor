package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "agro_forest_agent")
@SyncDescription(service_id = "1", service_name = "Agro-forest agent", download_link = "/Employees/Details/RebindGridAgroforestAgent", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class AgroForestAgent extends SelectionData implements Serializable {




    @DynamicProperty(json_key = "employeeno")
    public String employee_no;
    @DynamicProperty(json_key = "country_id")
    public String country;




    public AgroForestAgent() {
        this.transaction_no = svars.getTransactionNo();

        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }

}
