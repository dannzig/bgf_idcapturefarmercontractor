package sparta.realm.apps.farmercontractor;

import android.content.Context;

import java.util.ArrayList;

import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.activities.Addendum;
import sparta.realm.apps.farmercontractor.activities.BackupReport;
import sparta.realm.apps.farmercontractor.activities.GroupRecords;
import sparta.realm.apps.farmercontractor.activities.LandMapping;
import sparta.realm.apps.farmercontractor.activities.Maintenance;
import sparta.realm.apps.farmercontractor.activities.MemberRecords;
import sparta.realm.apps.farmercontractor.activities.MemberSummary;
import sparta.realm.apps.farmercontractor.activities.ReportActivity;
import sparta.realm.apps.farmercontractor.activities.SeedlingPlanting;
import sparta.realm.apps.farmercontractor.activities.SuitabilityAssessment;
import sparta.realm.apps.farmercontractor.activities.SyncReport;
import sparta.realm.apps.farmercontractor.activities.SeedlingsProduction;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.SearchFilterItem;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.Survey;
import sparta.realm.apps.farmercontractor.models.SurveyQuestion;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.apps.farmercontractor.models.User;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.models.system.Form;
import sparta.realm.apps.farmercontractor.models.system.InputField;
import sparta.realm.apps.farmercontractor.models.system.InputGroup;
import sparta.realm.apps.farmercontractor.models.system.MultiSelectionData;
import sparta.realm.apps.farmercontractor.models.system.ValidationRules;
import sparta.realm.spartautils.app_control.models.module;


public class StaticDataLoader {


    public static ArrayList<module> sideMenuModules() {
        Context context = Realm.context;
        ArrayList<module> modules = new ArrayList<>();

        modules.add(new module(context.getDrawable(sparta.realm.R.drawable.sync_icon), "sync", "Synchronize", true));
        modules.add(new module(context.getDrawable(R.drawable.ic_baseline_dashboard_24), ReportActivity.class.getName(), "Activity report", true));
//<<<<<<< HEAD
//        modules.add(new module(context.getDrawable(R.drawable.ic_baseline_dashboard_24), ReportActivity.class.getName(), "Registration report", true));
//        modules.add(new module(context.getDrawable(R.drawable.ic_baseline_dashboard_24), "PrintHistory.class.getName()", "Suitability assessment report", true));
//        modules.add(new module(context.getDrawable(R.drawable.ic_baseline_dashboard_24), "PrintHistory.class.getName()", "Mapping report", true));
//        modules.add(new module(context.getDrawable(R.drawable.ic_baseline_dashboard_24), "PrintHistory.class.getName()", "Adendum report", true));

        modules.add(new module(context.getDrawable(sparta.realm.R.drawable.ic_fingerprint_accent), "fpc", "BT FP configuration", true));
        modules.add(new module(context.getDrawable(sparta.realm.R.drawable.ic_print), "pc", "Printer configuration", true));
        modules.add(new module(context.getDrawable(sparta.realm.R.drawable.ic_settings_grey_24dp), "c", context.getString(R.string.configuration), true));

        modules.add(new module("", "-------------------------", false));
        modules.add(new module(context.getDrawable(R.drawable.ic_baseline_sync_alt_24), SyncReport.class.getName(), "Sync report", true));
        modules.add(new module(context.getDrawable(sparta.realm.R.drawable.uploading), BackupReport.class.getName(), "Backup report", true));
        modules.add(new module(context.getDrawable(sparta.realm.R.drawable.ic_logout), "a", context.getString(R.string.logout), true));
        modules.add(new module(context.getDrawable(R.drawable.ic_s_6), "b", context.getString(R.string.exit), true));

        if (Globals.operationMode() == Globals.OperationMode.Dev) {
            modules.add(new module("", "---------DEV--------", false));
            modules.add(new module("1", "Reset Member records", true));
            modules.add(new module("2", "Reset Member Images", true));
            modules.add(new module("3", "Reset Member Fingerprints", true));
            modules.add(new module("10", "Reset Member groups", true));
            modules.add(new module("11", "Reset Member group members", true));
            modules.add(new module("4", "Reset Survey records", true));
            modules.add(new module("5", "Reset Survey question records", true));
            modules.add(new module("6", "Reset Survey question choice records", true));
            modules.add(new module("7", "Reset Survey entry record", true));
            modules.add(new module("8", "Reset Survey question entry records", true));
            modules.add(new module("12", "Reset land mapping records", true));
            modules.add(new module("13", "Reset addendum records", true));
            modules.add(new module("9", "Reset apk versions", true));

        }
        return modules;

    }

    public static ArrayList<module> mainMenuModules() {
        ArrayList<module> main_menu_modules = new ArrayList<>();
        Context context = Realm.context;

        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_new_registration), MemberRecords.class.getName(), "Farmers", true));
        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_member_suitability), SuitabilityAssessment.class.getName(), "Suitability assesment", true));

        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_member_maping), LandMapping.class.getName(), "Farm Geo-maping", true));
        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_addendum), Addendum.class.getName(), "Addendum Entry", true));

        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_group_management), GroupRecords.class.getName(), "Farmer groups", true));
        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_group_training), GroupRecords.class.getName(), "Groups Training", true));

        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_seedlings_production), SeedlingsProduction.class.getName(), "Seedling Production", true));
        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_group_distribution), GroupRecords.class.getName(), "Seedling Distribution", true));

        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_seedling_planting), SeedlingPlanting.class.getName(), "Seedling Planting", true));
        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_member_summary), MemberSummary.class.getName(), "Farmer summary", true));

        main_menu_modules.add(new module(context.getDrawable(R.drawable.menu_button_maintence_module), Maintenance.class.getName(), "Maintenance", true));

        return main_menu_modules;
    }

    public static Form sampleform() {
        Form form = new Form("1", "Farmer registration");

        form.inputGroups.add(new InputGroup("1", "farmer info", "1", null, "1"));
        form.inputGroups.add(new InputGroup("2", "Land info", "1", null, "2"));
        form.inputGroups.add(new InputGroup("3", "Actors", "1", null, "3"));
        form.inputGroups.add(new InputGroup("4", "Documents capture page", "1", null, "4"));

        ValidationRules spnValidationRules = new ValidationRules();
        spnValidationRules.mandatory = "" + ValidationRules.MandatoryStatus.Mandatory.ordinal();
        spnValidationRules.value_not_selected_error = "Please fill in this mandatory field";

        ValidationRules edtValidationRules = new ValidationRules();
        edtValidationRules.mandatory = "" + ValidationRules.MandatoryStatus.Mandatory.ordinal();
        edtValidationRules.min_text_input_length_error = "Please fill in this mandatory field";
        edtValidationRules.min_text_input_length = "5";
        edtValidationRules.max_text_input_length = "50";
        edtValidationRules.text_input_type = "" + ValidationRules.TextInputType.capWords.ordinal();

        ValidationRules phoneEdtValidationRules = new ValidationRules();
        phoneEdtValidationRules.mandatory = "" + ValidationRules.MandatoryStatus.Mandatory.ordinal();
        phoneEdtValidationRules.min_text_input_length_error = "Phone number must be 10 digits";
        phoneEdtValidationRules.min_text_input_length = "10";
        phoneEdtValidationRules.max_text_input_length = "10";
        phoneEdtValidationRules.text_input_type = "" + ValidationRules.TextInputType.numeric.ordinal();

        ValidationRules numberEdtValidationRules = new ValidationRules();
        numberEdtValidationRules.mandatory = "" + ValidationRules.MandatoryStatus.Mandatory.ordinal();
        numberEdtValidationRules.min_text_input_length_error = "Invalid input";
        numberEdtValidationRules.min_text_input_length = "1";
        numberEdtValidationRules.max_text_input_length = "10";
        numberEdtValidationRules.text_input_type = "" + ValidationRules.TextInputType.numeric.ordinal();

        form.inputGroups.get(0).inputFields.add(new InputField("1", "Country", "Search select country", Country.class.getName(), "" + InputField.InputType.Selection.ordinal(), "1", "1", spnValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("2", "County", "Search select county", County.class.getName(), "" + InputField.InputType.Selection.ordinal(), "1", "2", spnValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("3", "Location", "Search select location", Location.class.getName(), "" + InputField.InputType.Selection.ordinal(), "1", "3", spnValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("4", "Sub-Location", "Search select sub-location", SubLocation.class.getName(), "" + InputField.InputType.Selection.ordinal(), "1", "4", spnValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("5", "Village", "Search select village", Village.class.getName(), "" + InputField.InputType.Selection.ordinal(), "1", "5", spnValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("6", "Site", "Search select site", Site.class.getName(), "" + InputField.InputType.Selection.ordinal(), "1", "6", spnValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("7", "Farmer's full name", "first second name", null, "" + InputField.InputType.Text.ordinal(), "1", "7", edtValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("8", "National Id Number", "33197277", null, "" + InputField.InputType.Text.ordinal(), "1", "8", edtValidationRules));
        form.inputGroups.get(0).inputFields.add(new InputField("9", "Phone number", "0112000351", null, "" + InputField.InputType.Text.ordinal(), "1", "8", phoneEdtValidationRules));

        form.inputGroups.get(1).inputFields.add(new InputField("10", "Land in acres", "0", null, "" + InputField.InputType.Text.ordinal(), "1", "8", numberEdtValidationRules));
        form.inputGroups.get(1).inputFields.add(new InputField("11", "Total seedlings", "0", null, "" + InputField.InputType.Text.ordinal(), "1", "8", numberEdtValidationRules));
        form.inputGroups.get(1).inputFields.add(new InputField("12", "Donated seedlings", "0", null, "" + InputField.InputType.Text.ordinal(), "1", "8", numberEdtValidationRules));
        form.inputGroups.get(1).inputFields.add(new InputField("13", "Donated seedling type", "0", null, "" + InputField.InputType.Text.ordinal(), "1", "8", numberEdtValidationRules));
        form.inputGroups.get(1).inputFields.add(new InputField("14", "Years of maturity", "10", null, "" + InputField.InputType.Text.ordinal(), "1", "8", numberEdtValidationRules));
        form.inputGroups.get(1).inputFields.add(new InputField("15", "DBH at maturity", "200", null, "" + InputField.InputType.Text.ordinal(), "1", "8", numberEdtValidationRules));

        form.inputGroups.get(2).inputFields.add(new InputField("17", "Agro forestry  agent", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("24", "Group representative", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("25", "Group rep national id number", null, "contributor", "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("26", "Partner organisations", "Select organisations", "contributor", "" + InputField.InputType.MultiSelection.ordinal(), "2", "8", new ValidationRules()));


        return form;

    }

    public static Form sampleform_() {
        Form form = new Form("1", "cnam registration");


        form.inputGroups.add(new InputGroup("1", "Page 1", "1", null, "1"));
        form.inputGroups.add(new InputGroup("2", "Page 2", "1", null, "2"));
        form.inputGroups.add(new InputGroup("9", "Page 3", "1", null, "3"));
        form.inputGroups.add(new InputGroup("3", "Fingerprint page", "1", null, "4"));
        form.inputGroups.add(new InputGroup("4", "Passport photo page", "1", null, "5"));
        form.inputGroups.add(new InputGroup("5", "Documents capture page", "1", null, "6"));
        form.inputGroups.get(1).inputGroups.add(new InputGroup("6", "Marriage details", "1", "2", "7"));
        form.inputGroups.get(4).inputGroups.add(new InputGroup("7", "Id front back", "1", "5", "1"));
        form.inputGroups.get(2).inputGroups.add(new InputGroup("8", "Contributor receipt number", "1", "8", "1"));

        form.inputGroups.get(0).inputFields.add(new InputField("1", "Location of enrolment", "mkate", null, "" + InputField.InputType.Text.ordinal(), "1", "1", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("2", "civility", null, "civility", "" + InputField.InputType.Selection.ordinal(), "1", "2", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("3", "Surname", "Kulfi", null, "" + InputField.InputType.Text.ordinal(), "1", "3", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("4", "Other name", "Kulfi", null, "" + InputField.InputType.Text.ordinal(), "1", "4", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("5", "Date of birth", null, null, "" + InputField.InputType.Date.ordinal(), "1", "5", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("6", "Country of birth", null, "country", "" + InputField.InputType.Selection.ordinal(), "1", "6", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("7", "Number extract of birth", "123456789", null, "" + InputField.InputType.Text.ordinal(), "1", "7", new ValidationRules()));
        form.inputGroups.get(0).inputFields.add(new InputField("8", "Date extract of birth", null, null, "" + InputField.InputType.Date.ordinal(), "1", "8", new ValidationRules()));


        form.inputGroups.get(1).inputFields.add(new InputField("9", "Nationality", null, "country", "" + InputField.InputType.Selection.ordinal(), "2", "1", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("10", "ID type", null, "identification_type", "" + InputField.InputType.Selection.ordinal(), "2", "2", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("11", "ID number", null, null, "" + InputField.InputType.Text.ordinal(), "2", "3", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("12", "Marital status", null, "marital_status", "" + InputField.InputType.Selection.ordinal(), "2", "4", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("13", "Residence", null, "residence", "" + InputField.InputType.Selection.ordinal(), "2", "5", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("14", "Village", null, "village", "" + InputField.InputType.Selection.ordinal(), "2", "6", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("15", "Landline number", null, null, "" + InputField.InputType.Text.ordinal(), "2", "7", new ValidationRules()));
        form.inputGroups.get(1).inputFields.add(new InputField("16", "Phone number", null, null, "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));


        form.inputGroups.get(2).inputFields.add(new InputField("17", "Payer of contribution", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("24", "Other contributor", null, "contributor", "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("25", "Other contributor id number", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("26", null, null, "contributor", "" + InputField.InputType.RadioSelection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("27", "contributor insurance number", "384xxxxxxxxx", "contributor", "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("18", "Insurance type", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("28", "Other insurance type", null, "contributor", "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("19", "Collectivity", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("20", "Matrix", null, "contributor", "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("21", "Profession", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("22", "Employer organisation", null, "contributor", "" + InputField.InputType.Selection.ordinal(), "2", "8", new ValidationRules()));
        form.inputGroups.get(2).inputFields.add(new InputField("23", "Number of rights", null, "contributor", "" + InputField.InputType.Text.ordinal(), "2", "8", new ValidationRules()));


        return form;

    }

    public static Survey sampleSurvey() {

        Survey survey = new Survey("1", "Suitability assesment survey", "This survey is to determine how suitable the farmers land is sign a contract with");
        ArrayList<SurveyQuestion> surveyQuestions = new ArrayList<>();

        SurveyQuestion question1 = new SurveyQuestion("1", "1", "Quality of Farm protection: Presence of a fence around the place to be planted. Etc. ", "15", "1");
        question1.surveyQuestionChoices.add(new SurveyQuestionChoice("1", "1", "Bad", "No fence at all on the whole perimeter", "1", "0", "0", "4"));
        question1.surveyQuestionChoices.add(new SurveyQuestionChoice("2", "1", "Fair", "Signs of fence, missing sections, will not prevent animas from entering the farm", "2", "0", "5", "9"));
        question1.surveyQuestionChoices.add(new SurveyQuestionChoice("3", "1", "Good", "Presence of fence live or chain-link that can stop animals from entering the farm", "3", "0", "10", "15"));

        SurveyQuestion question2 = new SurveyQuestion("2", "1", "Afforestation efforts: Presence of planted trees in the farm e.g. fruit trees, ornamentals", "15", "2");
        question2.surveyQuestionChoices.add(new SurveyQuestionChoice("4", "2", "Bad", "No single tree in sight.In the compound or the farm", "1", "0", "0", "4"));
        question2.surveyQuestionChoices.add(new SurveyQuestionChoice("5", "2", "Fair", "A few trees that grow naturally, maybe on fence. No ornamental plants", "2", "0", "5", "9"));
        question2.surveyQuestionChoices.add(new SurveyQuestionChoice("6", "2", "Good", "Presence of many trees, most that have been planted by the farmer or family member", "3", "0", "10", "15"));

        SurveyQuestion question3 = new SurveyQuestion("3", "1", "Soil and water conservation measures: Presence of soil conservation structures e.g. terraces", "15", "3");
        question3.surveyQuestionChoices.add(new SurveyQuestionChoice("7", "3", "Bad", "No soil and water conservation structure", "1", "0", "0", "4"));
        question3.surveyQuestionChoices.add(new SurveyQuestionChoice("8", "3", "Fair", "Attempts, broken or unmaintained water retention structures", "2", "0", "5", "9"));
        question3.surveyQuestionChoices.add(new SurveyQuestionChoice("9", "3", "Good", "Presence of well-maintained water retention structures", "3", "0", "10", "15"));


        SurveyQuestion question4 = new SurveyQuestion("4", "1", "State of water catchment: Other rainwater harvesting techniques (roof harvesting, borehole, water tanks, rock water harvesting, earth dam etc)", "10", "4");
        question4.surveyQuestionChoices.add(new SurveyQuestionChoice("13", "4", "Bad", "No water harvesting techniques", "1", "0", "0", "3"));
        question4.surveyQuestionChoices.add(new SurveyQuestionChoice("14", "4", "Fair", "Presence of unmaintained water harvesting structures, probably no tank but has Iron roof and gutters", "2", "0", "4", "6"));
        question4.surveyQuestionChoices.add(new SurveyQuestionChoice("15", "4", "Good", "Presence of Tanks and well-maintained water harvesting structures", "3", "0", "7", "10"));

        SurveyQuestion question5 = new SurveyQuestion("5", "1", "Transport means: State of water transport (presence of donkeys)", "5", "5");
        question5.surveyQuestionChoices.add(new SurveyQuestionChoice("16", "5", "Bad", "No animals to aid in water transport", "1", "0", "0", "1"));
        question5.surveyQuestionChoices.add(new SurveyQuestionChoice("17", "5", "Fair", "Presence of one or two donkeys. Without a cart", "2", "0", "2", "3"));
        question5.surveyQuestionChoices.add(new SurveyQuestionChoice("18", "5", "Good", "Presence of more than two donkeys, probably availability of a donkey Cart, vehicle, motorbike or bicycle that could be used for transport", "3", "0", "4", "5"));

        surveyQuestions.add(question1);
        surveyQuestions.add(question2);
        surveyQuestions.add(question3);
        surveyQuestions.add(question4);
        surveyQuestions.add(question5);

        survey.surveyQuestions = surveyQuestions;
        return survey;

    }

    public static ArrayList<User> sampleUsers() {
        ArrayList<User> users = new ArrayList<>();

        users.add(new User("1", "Full name", "username", "password", "code1"));
        users.add(new User("2", "Full name2", "username", "password", "code1"));
        users.add(new User("3", "Full name3", "username", "password", "code1"));
        users.add(new User("4", "Full name4", "username", "password", "code1"));
        users.add(new User("5", "Full name5", "username", "password", "code1"));


        return users;

    }

    public static ArrayList<SearchFilterItem> sampleSearchFilterItems() {
        ArrayList<SearchFilterItem> searchFilterItems = new ArrayList<>();
        SearchFilterItem countrySearchFilterItem = new SearchFilterItem("country", "Country", "country", Country.class.getName());
        SearchFilterItem countySearchFilterItem = new SearchFilterItem("county", "country", "country", "County/District", "county", County.class.getName());
        SearchFilterItem locationSearchFilterItem = new SearchFilterItem("location", "county", "county", "Ward/Sub-County", "location", Location.class.getName());
        SearchFilterItem sub_locationSearchFilterItem = new SearchFilterItem("sub_location", "location", "location", "Sub-loc/Parish", "sub_location", SubLocation.class.getName());
        SearchFilterItem villageSearchFilterItem = new SearchFilterItem("village", "sub_location", "sub_location", "Village ", "village", Village.class.getName());
        SearchFilterItem siteSearchFilterItem = new SearchFilterItem("site", "country", "country", "Site ", "site", Site.class.getName());

        searchFilterItems.add(countrySearchFilterItem);
        searchFilterItems.add(countySearchFilterItem);
        searchFilterItems.add(locationSearchFilterItem);
        searchFilterItems.add(sub_locationSearchFilterItem);
        searchFilterItems.add(villageSearchFilterItem);
        searchFilterItems.add(siteSearchFilterItem);
        return searchFilterItems;
    }
    public static ArrayList<SearchFilterItem> sampleSearchGroupFilterItems() {
        ArrayList<SearchFilterItem> searchFilterItems = new ArrayList<>();
        SearchFilterItem countrySearchFilterItem = new SearchFilterItem("country", "Country", "country_id", Country.class.getName());
        SearchFilterItem countySearchFilterItem = new SearchFilterItem("county", "country", "country", "County/District", "county", County.class.getName());
        SearchFilterItem locationSearchFilterItem = new SearchFilterItem("location", "county", "county", "Ward/Sub-County", "locationId", Location.class.getName());
        SearchFilterItem sub_locationSearchFilterItem = new SearchFilterItem("sub_location", "location", "location", "Sub-loc/Parish", "sub_location", SubLocation.class.getName());
        SearchFilterItem villageSearchFilterItem = new SearchFilterItem("village", "sub_location", "sub_location", "Village ", "village", Village.class.getName());
        SearchFilterItem siteSearchFilterItem = new SearchFilterItem("site", "country", "country", "Site ", "site", Site.class.getName());

        searchFilterItems.add(countrySearchFilterItem);
        searchFilterItems.add(countySearchFilterItem);
        searchFilterItems.add(locationSearchFilterItem);
        searchFilterItems.add(sub_locationSearchFilterItem);
        searchFilterItems.add(villageSearchFilterItem);
        searchFilterItems.add(siteSearchFilterItem);
        return searchFilterItems;
    }

    public static ArrayList<MultiSelectionData> sampleMultiSelectionData() {
        ArrayList<MultiSelectionData> multiSelectionDataArrayList = new ArrayList<>();
        multiSelectionDataArrayList.add(new MultiSelectionData("1", "Member report"));
        multiSelectionDataArrayList.add(new MultiSelectionData("2", "Member group report"));
        multiSelectionDataArrayList.add(new MultiSelectionData("3", "Suitability assessment report"));
        multiSelectionDataArrayList.add(new MultiSelectionData("4", "Farm geo-mapping report"));
        multiSelectionDataArrayList.add(new MultiSelectionData("5", "Addendum report"));
        return multiSelectionDataArrayList;

    }

}
