package sparta.realm.apps.farmercontractor.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.github.lzyzsd.circleprogress.DonutProgress;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.activities.registration.Page3;
import sparta.realm.apps.farmercontractor.adapters.SurveyQuestionAdapter;
import sparta.realm.apps.farmercontractor.adapters.SurveyQuestionReportAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivitySuitabilityAssessmentBinding;
import sparta.realm.apps.farmercontractor.models.Country;
import sparta.realm.apps.farmercontractor.models.County;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Location;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberImage;
import sparta.realm.apps.farmercontractor.models.SeedlingType;
import sparta.realm.apps.farmercontractor.models.Site;
import sparta.realm.apps.farmercontractor.models.SubLocation;
import sparta.realm.apps.farmercontractor.models.SuitabilityAssessmentSurveyEntry;
import sparta.realm.apps.farmercontractor.models.Survey;
import sparta.realm.apps.farmercontractor.models.SurveyQuestion;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionEntry;
import sparta.realm.apps.farmercontractor.models.Village;
import sparta.realm.apps.farmercontractor.utils.printing.Printer;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartamodels.percent_calculation;
import sparta.realm.spartautils.s_bitmap_handler;
import sparta.realm.utils.Conversions;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

import net.sqlcipher.Cursor;

import org.json.JSONArray;
import org.json.JSONObject;

public class SuitabilityAssessment extends AppCompatActivity {

    ActivitySuitabilityAssessmentBinding binding;

    Survey survey;

    Member member;
    SuitabilityAssessmentSurveyEntry suitabilityAssessmentSurveyEntry;
    ActivityResultLauncher<Intent> activityLauncherForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();

                        member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + data.getStringExtra("sid") + "'", "sid IS NOT NULL"));
                        member.profile_photo = Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_id='" + member.sid + "'"));

                    } else {
//                        setResult(RESULT_OK);
                        finish();
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySuitabilityAssessmentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
        String member_transaction_no = getIntent().getStringExtra("transaction_no");



        if (member == null) {

            Intent intent = new Intent(this, MemberRecords.class);
            intent.putExtra("search", true);
            intent.putExtra("defaultTableFilters", new String[]{"sid not in (select member from suitability_assessment_survey_entry where is_suitability_active == 'true')"});
            activityLauncherForResult.launch(intent);

        } else {
            initUi();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (member == null) {
//            Intent intent = new Intent(this, MemberRecords.class);
//            intent.putExtra("search", true);
//            intent.putExtra("query", "select member.* from member_info_table member inner join member_leaves on member_leaves.member_id=member.sid GROUP BY member.sid");
//            activityLauncherForResult.launch(intent);
        } else {
            initUi();
        }
    }

    void initUi() {
        setupToolbar(binding.include.toolbar);
//        survey = StaticDataLoader.sampleSurvey();
        survey = Realm.databaseManager.loadObject(Survey.class, new Query().setTableFilters("sid=" + 1 + ""));
        survey.surveyQuestions = Realm.databaseManager.loadObjectArray(SurveyQuestion.class, new Query().setTableFilters("survey='" + survey.sid + "'").setLimit(50));

        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {

            surveyQuestion.surveyQuestionChoices = Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class, new Query().setTableFilters("question='" + surveyQuestion.sid + "'").addOrderFilters("choice_index", true));
        }
        binding.surveyList.setLayoutManager(new LinearLayoutManager(this));
        binding.surveyList.setAdapter(new SurveyQuestionAdapter(survey.surveyQuestions));
        binding.noRecordsLay.setVisibility(View.GONE);
        binding.submit.setOnClickListener(view -> saveSurvey());
        binding.include2.name.setText(member.full_name);
        binding.include2.info1.setText("Nat ID: " + member.nat_id);
        binding.include3.farmerNameInput.setInput(member.full_name);
        binding.include3.farmerPhoneNoInput.setInput(member.phone_no);
        binding.include3.farmerLandSizeInput.setInput(member.land_size);
        binding.include3.totalSeedlingsInput.setInput(member.total_seedlings);
        binding.include3.donatedSeedlingsInput.setInput(member.donated_seedlings);

//        suitabilityAssessmentSurveyEntry=new SuitabilityAssessmentSurveyEntry(member.sid,survey.sid,"");
    }

    boolean recordSaved = false;


    void saveSurvey() {
        if (validated()) {

            SuitabilityAssessmentSurveyEntry surveyEntry = new SuitabilityAssessmentSurveyEntry(member.sid,member.transaction_no,survey.sid, "" + totalScore());
            surveyEntry.full_name = binding.include3.farmerNameInput.getInput();
            surveyEntry.phone_no = binding.include3.farmerPhoneNoInput.getInput();
            surveyEntry.land_size = binding.include3.farmerLandSizeInput.getInput();
            surveyEntry.total_seedlings = binding.include3.totalSeedlingsInput.getInput();
            surveyEntry.donated_seedlings = binding.include3.donatedSeedlingsInput.getInput();

            DatabaseManager.database.execSQL("UPDATE land_location SET is_location_active = 'false' WHERE member='" + member.sid + "'");
            DatabaseManager.database.execSQL("UPDATE suitability_assessment_survey_entry SET is_suitability_active = 'true' WHERE member='" + member.sid + "'");

            ArrayList<SurveyQuestionEntry> surveyQuestionEntries = new ArrayList<>();
            for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
                SurveyQuestionEntry surveyQuestionEntry = new SurveyQuestionEntry(surveyEntry.transaction_no, surveyQuestion.sid, surveyQuestion.result.choice, surveyQuestion.result.marks);
                surveyQuestionEntries.add(surveyQuestionEntry);
            }
            View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_survey_confirmation, null);
            final AlertDialog ald = new AlertDialog.Builder(this)
                    .setView(aldv)
                    .show();
            ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            Button save = aldv.findViewById(R.id.save);
            save.setOnClickListener(view -> {
                if (recordSaved) {
                    new T12Printer(SuitabilityAssessment.this, new Printer.PrintingInterface() {
                        @Override
                        public void onReadyToPrint(Object... printingObjects) {
                            PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                            CanvasPrint cp = new CanvasPrint();
                            cp.init(PrinterType.TIII);
                            cp.setUseSplit(true);
                            cp.setTextAlignRight(false);
                            cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(SuitabilityAssessment.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                            FontProperty fp = new FontProperty();
                            fp.setFont(true, false, true, false, 27, null);
                            cp.setFontProperty(fp);
                            cp.drawText(100, survey.name + " survey");
                            cp.drawText("");
                            fp.setFont(true, false, false, false, 25, null);
                            cp.setFontProperty(fp);
                            cp.drawText("Transaction no: ");
                            cp.drawText(surveyEntry.transaction_no);
                            fp.setFont(true, false, false, false, 25, null);
                            cp.setFontProperty(fp);
                            cp.setTextAlignRight(false);
                            cp.drawText("");
                            cp.drawText("Farmer info");
                            fp.setFont(true, false, false, false, 20, null);
                            cp.setFontProperty(fp);
                            cp.drawText("");
                            cp.drawText("Name: " + surveyEntry.full_name);
                            cp.drawText("National ID number: " + member.nat_id);
                            cp.drawText("Phone number: " + surveyEntry.phone_no);
                            cp.drawText("Land size: " + surveyEntry.land_size + " acres");
                            cp.drawText("Total seedlings: " + surveyEntry.total_seedlings);
                            cp.drawText("Donated seedlings: " + surveyEntry.donated_seedlings);

                            cp.drawText("");
                            fp.setFont(true, false, false, false, 25, null);
                            cp.setFontProperty(fp);
                            cp.setTextAlignRight(false);
                            cp.drawText("Survey results " + totalScore() + "/" + maxScore() + " " + getScoreRating(totalScore()));
                            fp.setFont(true, false, false, false, 20, null);
                            cp.setFontProperty(fp);
                            cp.setTextAlignRight(false);
                            cp.drawText("");
                            mPrinter.printImage(cp.getCanvasImage());
                            boolean empty = true;
                            int i = 1;
                            for (SurveyQuestionEntry surveyQuestionEntry : surveyQuestionEntries) {
                                empty = false;
                                SurveyQuestion surveyQuestion = Realm.databaseManager.loadObject(SurveyQuestion.class, new Query().setTableFilters("sid='" + surveyQuestionEntry.question + "'"));
                                SurveyQuestionChoice surveyQuestionChoice = Realm.databaseManager.loadObject(SurveyQuestionChoice.class, new Query().setTableFilters("sid='" + surveyQuestionEntry.choice + "'"));
                                cp = new CanvasPrint();
                                cp.init(PrinterType.TIII);
                                fp.setFont(true, false, false, false, 20, null);
                                cp.setFontProperty(fp);
                                cp.setTextAlignRight(false);
                                cp.drawText(i + " " + surveyQuestion.question);
                                fp.setFont(false, false, false, false, 18, null);
                                cp.setFontProperty(fp);
                                cp.drawText(surveyQuestionChoice.choice + ": " + surveyQuestionChoice.choice_description);
                                fp.setFont(true, false, false, false, 20, null);
                                cp.setTextAlignRight(true);
                                cp.drawText(surveyQuestionEntry.marks + "/" + getMaxScore(surveyQuestion));
                                cp.drawLine(0, cp.getCurrentPointY(), 450, cp.getCurrentPointY());
                                mPrinter.printImage(cp.getCanvasImage());
                                i++;
                            }
                            cp = new CanvasPrint();
                            cp.init(PrinterType.TIII);
                            if (empty) {
                                fp.setFont(false, false, false, false, 18, null);
                                cp.setFontProperty(fp);
                                cp.setTextAlignRight(false);
                                cp.drawText(130, "**No records**");

                            }

                            fp.setFont(false, false, false, false, 18, null);
                            cp.setFontProperty(fp);
                            cp.setTextAlignRight(false);
                            cp.drawText("Report generated by: " + Globals.user().username);
                            cp.drawText("Report generation time: " + Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                            cp.drawText("");
                            mPrinter.printImage(cp.getCanvasImage());
                            Barcode bc = new Barcode(PDF417, 5, 300, 50, surveyEntry.transaction_no);
                            mPrinter.printBarCode(bc);
                            mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                            mPrinter.closeConnection();
                        }
                    }).print();
                } else {

                    if (Realm.databaseManager.insertObject(surveyEntry)) {
                        for (SurveyQuestionEntry surveyQuestionEntry : surveyQuestionEntries) {// new SurveyQuestionEntry(surveyEntry.transaction_no, surveyQuestion.sid, surveyQuestion.result.choice, surveyQuestion.result.marks);
                            Realm.databaseManager.insertObject(surveyQuestionEntry);
                        }
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(surveyEntry).get(0));
                        SynchronizationManager.upload_(Realm.realm.getSyncDescription(new SurveyQuestionEntry()).get(0));

                        recordSaved = true;
                        save.setText("Print");

                    }
                }

            });
            aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
                ald.dismiss();
                if (recordSaved) {

                    finish();
                }

            });
            DonutProgress donutProgress = aldv.findViewById(R.id.icon);
            TextView rating_note = aldv.findViewById(R.id.rating_note);
            TextView sub_title = aldv.findViewById(R.id.sub_title);
            TextView description = aldv.findViewById(R.id.description);
            RecyclerView recyclerView = aldv.findViewById(R.id.record_list);

            percent_calculation pc = new percent_calculation(maxScore() + "", surveyEntry.total_score);
            donutProgress.setProgress(Float.parseFloat(pc.per_balance));
//            rating_note.setText(surveyEntry.total_score+"/"+maxScore()+" "+(Float.parseFloat(pc.per_balance) > 80 ? "Good" : Float.parseFloat(pc.per_balance) > 60 ? "Average" : Float.parseFloat(pc.per_balance) > 40 ? "Poor" : "Unacceptable"));
            rating_note.setText(surveyEntry.total_score + "/" + maxScore() + " " + getScoreRating(Integer.parseInt(surveyEntry.total_score)));

            sub_title.setText(member.full_name);
            description.setText("Confirm your entries for this survey below\n");

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new SurveyQuestionReportAdapter(surveyQuestionEntries));

        }
    }


    String getScoreRating(int score) {
        int total = 0;
        HashMap<String, Integer> higest_marks_map = new HashMap<>();
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
            for (SurveyQuestionChoice surveyQuestionChoice : Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class, new Query().setTableFilters("question='" + surveyQuestion.sid + "'"))) {
                int marks_from = Integer.parseInt(surveyQuestionChoice.marks_from);
                int marks_from_hash = higest_marks_map.get(surveyQuestionChoice.choice) == null ? 0 : higest_marks_map.get(surveyQuestionChoice.choice);
                higest_marks_map.put(surveyQuestionChoice.choice, marks_from + marks_from_hash);

            }
            total += Integer.parseInt(surveyQuestion.result.marks);
        }

        int map_size = higest_marks_map.size();
        for (int i = 0; i < map_size; i++) {
            Map.Entry highest = null;
            for (Map.Entry choice_max_marks : higest_marks_map.entrySet()) {
                if (highest == null) {
                    highest = choice_max_marks;
                    continue;
                } else {
                    if ((int) choice_max_marks.getValue() > (int) highest.getValue()) {
                        highest = choice_max_marks;
                    }
                }
            }
            if (score >= (int) highest.getValue()) {
                return (String) highest.getKey();
            }
            higest_marks_map.remove(highest.getKey());
        }

        return null;
    }


    int totalScore() {
        int total = 0;
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
            total += Integer.parseInt(surveyQuestion.result.marks);
        }

        return total;
    }

    int maxScore() {
        int total = 0;
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {
            total += Integer.parseInt(getMaxScore(surveyQuestion));
        }

        return total;
    }

    String getMaxScore(SurveyQuestion surveyQuestion) {
        ArrayList<SurveyQuestionChoice> surveyQuestionChoices = Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class, new Query().setTableFilters("question='" + surveyQuestion.sid + "'"));
        SurveyQuestionChoice highestSurveyQuestionChoice = null;
        for (SurveyQuestionChoice surveyQuestionChoice : surveyQuestionChoices) {
            if (highestSurveyQuestionChoice == null) {
                highestSurveyQuestionChoice = surveyQuestionChoice;
                continue;
            }
            if (Integer.parseInt(surveyQuestionChoice.marks_to) > Integer.parseInt(highestSurveyQuestionChoice.marks_to)) {
                highestSurveyQuestionChoice = surveyQuestionChoice;

            }
        }

        return highestSurveyQuestionChoice.marks_to;
    }

    boolean validated() {
        boolean valid = true;
        if (!binding.include3.farmerNameInput.isInputValid()) {
            valid = false;

        }
        if (!binding.include3.farmerPhoneNoInput.isInputValid()) {
            valid = false;

        }
        if (!binding.include3.farmerLandSizeInput.isInputValid()) {
            valid = false;

        }
        if (!binding.include3.totalSeedlingsInput.isInputValid()) {
            valid = false;

        }
        if (!binding.include3.donatedSeedlingsInput.isInputValid()) {
            valid = false;

        }
        for (SurveyQuestion surveyQuestion : survey.surveyQuestions) {

            if (surveyQuestion.result.choice == null) {
                surveyQuestion.uiError = "Choice not selected";
                binding.surveyList.getAdapter().notifyDataSetChanged();
                valid = false;
                continue;
            }
            if (surveyQuestion.result.marks == null) {
                surveyQuestion.uiError = "Marks not awarded";
                binding.surveyList.getAdapter().notifyDataSetChanged();
                valid = false;
                continue;
            }
            try {
                if (Integer.parseInt(surveyQuestion.result.marks) < Integer.parseInt(surveyQuestion.result.surveyQuestionChoice.marks_from) || Integer.parseInt(surveyQuestion.result.marks) > Integer.parseInt(surveyQuestion.result.surveyQuestionChoice.marks_to)) {
                    surveyQuestion.uiError = "Marks not properly awarded";
                    binding.surveyList.getAdapter().notifyDataSetChanged();
                    valid = false;
                    continue;
                }
            } catch (Exception e) {
                surveyQuestion.uiError = "Marks not properly awarded";
                binding.surveyList.getAdapter().notifyDataSetChanged();
                valid = false;
                continue;
            }
            surveyQuestion.uiError = null;

        }
        return valid;
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.non_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }





    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                onBackPressed();
                break;
            case R.id.config:
                startActivity(new Intent(this, Configuration.class));
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}