package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;
import java.util.ArrayList;

import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "survey")
@SyncDescription(service_name = "Survey", download_link = "/FarmersContract/Questionnaire/getAllquestionType", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class Survey extends RealmModel implements Serializable {

    @DynamicProperty(json_key = "name")
    public String name;

    @DynamicProperty(json_key = "description")
    public String description;

    public ArrayList<SurveyQuestion> surveyQuestions=new ArrayList<>();

    public Survey() {


    }
    public Survey(String sid, String name, String description) {//dummy inserts
        this.sid = sid;
        this.name = name;
        this.description = description;


        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";

    }

}
