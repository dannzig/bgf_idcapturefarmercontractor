package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "partner_organisation")
@SyncDescription(service_id = "1", service_name = "Partner Organisation", download_link = "/Configurations/PartnerOrg/RebindGrid", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class PartnerOrganisation extends SelectionData implements Serializable {





    public PartnerOrganisation() {


    }

}
