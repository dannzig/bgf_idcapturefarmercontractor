package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

@DynamicClass(table_name = "group_trainings")
@SyncDescription(service_name = "Group Training", download_link = "/FarmersContract/Group/MobileTrainingActivitesList", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class TrainingGroup  extends RealmModel implements Serializable {
//    {
//        "$id": "6",
//            "id": 1,
//            "group_id": 125,
//            "date": "2024-02-19T00:00:00",
//            "agroforest_agent_id": 7088,
//            "training_type_id": 1,
//            "transaction_no": "202421916512",
//            "datecomparer": 17083587128603119,
//            "is_active": true,
//            "group_name": "Test 26",
//            "trainingtype": "Prunning training",
//            "worker_name": "Nelly Jerono Saina Tuwei"
//    }

    @DynamicProperty(json_key = "name")
    public String name;

    @DynamicProperty(json_key = "group_id")
    public String group_id;

    @DynamicProperty(json_key = "date")
    public String date;

    @DynamicProperty(json_key = "agroforest_agent_id")
    public String agroforest_agent_id;

    @DynamicProperty(json_key = "training_type_id")
    public String training_type_id;

    @DynamicProperty(json_key = "group_name")
    public String group_name;

    @DynamicProperty(json_key = "worker_name")
    public String worker_name;

}
