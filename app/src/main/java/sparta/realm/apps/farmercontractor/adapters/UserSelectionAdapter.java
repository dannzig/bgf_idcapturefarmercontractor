package sparta.realm.apps.farmercontractor.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.User;


public class UserSelectionAdapter extends BaseAdapter implements Filterable {

    ArrayList<User> users;
    ArrayList<User> originalUsers;

    public UserSelectionAdapter(ArrayList<User> users) {
        this.users = users;
        originalUsers=new ArrayList<>(users);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, null);
        User gf = users.get(position);
        ((TextView) convertView.findViewById(R.id.name)).setText(gf.username);
        ((TextView) convertView.findViewById(R.id.info1)).setText(gf.user_full_name);


        return convertView;
    }

    DataFilter dataFilter;

    @Override
    public Filter getFilter() {
        if (dataFilter == null) {
            dataFilter = new DataFilter();
        }
        return dataFilter;
    }

    public class DataFilter extends Filter {


        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            FilterResults results = new FilterResults();

            ArrayList<User> filtered_list = new ArrayList<User>();
            for (int i = 0; i < originalUsers.size(); i++) {
                User user = originalUsers.get(i);
                if (user.user_full_name.toLowerCase().contains(charSequence.toString().toLowerCase()) || user.username.toLowerCase().contains(charSequence.toString().toLowerCase())) {

                    filtered_list.add(user);

                }
            }
            results.count = filtered_list.size();
            results.values = filtered_list;


            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            users.clear();
            users.addAll((ArrayList<User>) filterResults.values);
            notifyDataSetChanged();
        }
    }
}
