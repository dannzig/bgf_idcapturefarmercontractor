package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "farmer_contract")
@SyncDescription(chunk_size = 100000, service_name = "Farmer contract", download_link = "/FarmersContract/Farmers/pullfarmerWithcontracts", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class FarmerContract extends RealmModel implements Serializable {
    //    http://ta.cs4africa.com:6090/FarmersContract/Farmers/pullfarmerWithcontracts
    @DynamicProperty(json_key = "contract_number")
//contract_number,generation_time,contract_url,member_id
    public String contract_no;

    @DynamicProperty(json_key = "date_time")
    public String generation_time;

    @DynamicProperty(json_key = "contract_url")
    public String contract_url;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "isgenerated")
    public String is_generated;

    @DynamicProperty(json_key = "is_contract_active")
    public String is_contract_active;

    public FarmerContract() {

    }

}
