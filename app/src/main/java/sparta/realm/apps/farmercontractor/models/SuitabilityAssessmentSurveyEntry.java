package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;


@DynamicClass(table_name = "suitability_assessment_survey_entry")

@SyncDescription(service_name = "Suitability assessment survey entry", upload_link = "/FarmersContract/Farmers/AddFarmerSuitabilityDetails", service_type = SyncDescription.service_type.Upload)
@SyncDescription(chunk_size = 100000, service_name = "Suitability assessment survey entry", download_link = "/FarmersContract/Farmers/GetAssessmentResult", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class SuitabilityAssessmentSurveyEntry extends SurveyEntry implements Serializable {

    @DynamicProperty(json_key = "full_name")
    public String full_name;

    @DynamicProperty(json_key = "phone_no")
    public String phone_no;

    @DynamicProperty(json_key = "land_size")
    public String land_size;

    @DynamicProperty(json_key = "total_seedlings")
    public String total_seedlings;

    @DynamicProperty(json_key = "donated_seedlings")
    public String donated_seedlings;

    @DynamicProperty(json_key = "longitude")
    public String longitude;

    @DynamicProperty(json_key = "latitude")
    public String latitude;

    @DynamicProperty(json_key = "is_suitability_active")
    public String is_suitability_active;






    public SurveyQuestionChoice surveyQuestionChoice = new SurveyQuestionChoice();

    public SuitabilityAssessmentSurveyEntry() {

    }

    public SuitabilityAssessmentSurveyEntry(String member,String member_transaction_no, String survey, String total_score) {
        this.member = member;
        this.member_transaction_no =member_transaction_no;
        this.survey = survey;
        this.total_score = total_score;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

    public SuitabilityAssessmentSurveyEntry(String member, String survey, String total_score) {
        this.member = member;
        this.survey = survey;
        this.total_score = total_score;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }

}
