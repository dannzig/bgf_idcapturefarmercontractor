package sparta.realm.apps.farmercontractor.activities;

import android.app.DatePickerDialog;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.StaticDataLoader;
import sparta.realm.apps.farmercontractor.adapters.ActivityReportAdapter;
import sparta.realm.apps.farmercontractor.adapters.MultiCheckAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityReportBinding;
import sparta.realm.apps.farmercontractor.models.ReportEntry;
import sparta.realm.apps.farmercontractor.models.system.MultiSelectionData;
import sparta.realm.apps.farmercontractor.utils.Pager;
import sparta.realm.apps.farmercontractor.utils.SnapHelper;
import sparta.realm.apps.farmercontractor.utils.printing.Printer;
import sparta.realm.apps.farmercontractor.utils.printing.t12.T12Printer;
import sparta.realm.spartautils.bluetooth.bt_device_connector;
import sparta.realm.spartautils.print;
import sparta.realm.spartautils.s_bitmap_handler;
import sparta.realm.spartautils.svars;
import sparta.realm.utils.Conversions;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

public class ReportActivity extends AppCompatActivity implements print.printeri {

    ActivityReportBinding binding;
    ArrayList<ReportEntry> reportEntries;
    ArrayList<MultiSelectionData> multiSelectionData;
    Pager<ReportEntry> pager;

    //members,groups,surveys,mapping,addendum
    Class t;
    Calendar fromCalendar = Calendar.getInstance();
    Calendar toCalendar = Calendar.getInstance();
    String memberQuery = "SELECT sid,sync_status,user_id,reg_time,full_name as name,\"created member\" as description  from member_info_table";
    String memberGroupQuery = "SELECT sid,sync_status,user_id,reg_time,name,\"created group\" as description from member_group";
    String surveyQuery = "SELECT sase.sid,sase.sync_status,sase.user_id,sase.reg_time,m.full_name as name,\"Did survey\" as description from suitability_assessment_survey_entry sase inner join member_info_table m on sase.member=m.sid";
    String mappingQuery = "SELECT l.sid,l.sync_status,l.user_id,l.reg_time,m.full_name as name,\"Did farm mapping\" as description from land_location l inner join member_info_table m on l.member=m.sid";
    String addendumQuery = "SELECT a.sid,a.sync_status,a.user_id,a.reg_time,m.full_name as name,\"Did a contract addendum\" as description from addendum a inner join member_info_table m on a.member_id=m.sid";
    String customQuery;
    String defaultFromTime="01-01-2011 00:00:00";
    String defaultToTime="01-01-2020 23:59:00";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityReportBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        try {
            defaultFromTime = svars.sdf_user_friendly_date.parse(defaultFromTime).getTime() + "";
            defaultFromTime = ((int) (Math.floor(Double.parseDouble(defaultFromTime) / 1000))) + "";
        } catch (ParseException e) {
            e.printStackTrace();
        }

            defaultToTime = Calendar.getInstance().getTime().getTime() + "";
            defaultToTime = ((int) (Math.floor(Double.parseDouble(defaultToTime) / 1000))) + "";


        initUi();

    }

    void initUi() {


        setupToolbar(binding.toolbarLayout.toolbar);
        reportEntries = new ArrayList<>();// Realm.databaseManager.loadObjectArray(ReportEntry.class, "SELECT sid,sync_status,user_id,reg_time,full_name as name,\"created member\" as description  from member_info_table\n" +

        customQuery = memberQuery
                + " union all " + memberGroupQuery
                + " union all " + surveyQuery
                + " union all " + mappingQuery
                + " union all " + addendumQuery;

        binding.recordList.setHasFixedSize(true);
        binding.recordList.setLayoutManager(new LinearLayoutManager(this));
        SnapHelper startSnapHelper = new SnapHelper();
        startSnapHelper.attachToRecyclerView(binding.recordList);
        binding.recordList.setAdapter(new ActivityReportAdapter(reportEntries));

        pager = new Pager(ReportEntry.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                reportEntries.clear();
                binding.recordList.getAdapter().notifyDataSetChanged();
                reportEntries.addAll((ArrayList<ReportEntry>) data);
                binding.recordList.getAdapter().notifyDataSetChanged();
                binding.noRecordsLay.setVisibility((reportEntries.size()) > 0 ? View.GONE : View.VISIBLE);


            }
        }, new EditText(this), binding.include.prev, binding.include.next, binding.include.positionIndicator, binding.include.progress, customQuery, new String[]{"CAST( strftime('%s', reg_time) as INTEGER)<" + defaultToTime, "CAST( strftime('%s', reg_time) as INTEGER)>" + defaultFromTime}, new String[]{"*", "strftime('%s', reg_time) as time"}, "name");

        binding.include.searchField.setOnClickListener(view -> setupReport(binding.include.fromEdt.getText().toString(), binding.include.toEdt.getText().toString()));
        binding.include.fromCalendarIcon.setOnClickListener(view -> pickDate(fromCalendar, binding.include.fromEdt));
        binding.include.fromEdt.setOnClickListener(view -> pickDate(fromCalendar, binding.include.fromEdt));
        binding.include.toCalendarIcon.setOnClickListener(view -> pickDate(toCalendar, binding.include.toEdt));
        binding.include.toEdt.setOnClickListener(view -> pickDate(toCalendar, binding.include.toEdt));
        binding.include.filterIcon.setOnClickListener(view -> binding.include.filterList.setVisibility(binding.include.filterList.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE));
        binding.include.filterList.setLayoutManager(new LinearLayoutManager(this));
        multiSelectionData = StaticDataLoader.sampleMultiSelectionData();
        binding.include.filterList.setAdapter(new MultiCheckAdapter(multiSelectionData, (multiSelectionData, selectedItems) -> pager.setCustomQuery(generateCustomQuery(selectedItems))));
        binding.include.filterList.postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.include.filterList.setVisibility(View.GONE);
            }
        },1000);
        binding.include.print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//    new print(sparta.realm.spartautils.print.printer_type.T12,ReportActivity.this,"Shika chini","","").print();
                new T12Printer(ReportActivity.this, new Printer.PrintingInterface() {

                    @Override
                    public void onReadyToPrint(Object... printingObjects) {
                        PrinterInstance mPrinter = (PrinterInstance) printingObjects[0];
                        CanvasPrint cp = new CanvasPrint();
                        cp.init(PrinterType.TIII);
                        cp.setUseSplit(true);
                        cp.setTextAlignRight(false);
                        cp.drawImage(0, 0, s_bitmap_handler.toGrayscale(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(ReportActivity.this.getResources(), R.drawable.bgf_logo), 400, 350, false)));
                        FontProperty fp = new FontProperty();
                        fp.setFont(true, false, true, false, 27, null);
                        cp.setFontProperty(fp);
                        cp.drawText(100, "Activity report");
                        cp.drawText("");
fp.setFont(true, false, false, false, 25, null);
                        cp.setFontProperty(fp);
                        cp.drawText( "Report data ");
                        fp.setFont(true, false, false, false, 20, null);
                        cp.setFontProperty(fp);
                        cp.drawText( 50,"From: "+Conversions.sdfUserDisplayDate.format(new Date(Long.parseLong(defaultFromTime)*1000)));
                        cp.drawText( 50,"To: "+Conversions.sdfUserDisplayDate.format(new Date(Long.parseLong(defaultToTime)*1000)));
                        cp.drawText( 50,reportEntries.size()+" records ");
                        cp.drawText("");
                        mPrinter.printImage(cp.getCanvasImage());


                        for (MultiSelectionData multiSelectionData : multiSelectionData) {
                            if (multiSelectionData.selected) {
                                cp = new CanvasPrint();
                                cp.init(PrinterType.TIII);
                                fp.setFont(true, false, false, false, 25, null);
                                cp.setFontProperty(fp);
                                cp.setTextAlignRight(false);
//    cp.drawText("Activity report for");
                                cp.drawText(multiSelectionData.name);
                                cp.drawText("");
                                mPrinter.printImage(cp.getCanvasImage());
                                boolean empty = true;
                                for (ReportEntry reportEntry : reportEntries) {
                                    if ((multiSelectionData.sid.equals("1") && reportEntry.description.equals("created member"))
                                            || (multiSelectionData.sid.equals("2") && reportEntry.description.equals("created group"))
                                            || (multiSelectionData.sid.equals("3") && reportEntry.description.equals("Did survey"))
                                            || (multiSelectionData.sid.equals("4") && reportEntry.description.equals("Did farm mapping"))
                                            || (multiSelectionData.sid.equals("5") && reportEntry.description.equals("Did a contract addendum"))

                                    ) {
                                        empty = false;
                                        cp = new CanvasPrint();
                                        cp.init(PrinterType.TIII);
                                        fp.setFont(true, false, false, false, 20, null);
                                        cp.setFontProperty(fp);
                                        cp.setTextAlignRight(false);
                                        cp.drawText(reportEntry.name);
                                        fp.setFont(false, false, false, false, 18, null);
                                        cp.setFontProperty(fp);
                                        cp.setTextAlignRight(true);
                                        cp.drawText(Conversions.getUserDisplayDateFromDBTime(reportEntry.reg_time));
                                        cp.drawLine(0, cp.getCurrentPointY(), 450, cp.getCurrentPointY());
                                        mPrinter.printImage(cp.getCanvasImage());
                                    }


                                }
                                cp = new CanvasPrint();
                                cp.init(PrinterType.TIII);
                                if (empty) {
                                    fp.setFont(false, false, false, false, 18, null);
                                    cp.setFontProperty(fp);
                                    cp.setTextAlignRight(false);
                                    cp.drawText(130, "**No records**");

                                }
                                cp.drawText("");
                                cp.drawText("");
                                mPrinter.printImage(cp.getCanvasImage());

                            }

                        }

                        cp = new CanvasPrint();
                        cp.init(PrinterType.TIII);
                        fp.setFont(false, false, false, false, 18, null);
                        cp.setFontProperty(fp);
                        cp.setTextAlignRight(false);
                        cp.drawText("Report generated by: " + Globals.user().username);
                        cp.drawText( "Report generation time: "+Conversions.sdfUserDisplayDate.format(Calendar.getInstance().getTime()));
                        cp.drawText("");
                        mPrinter.printImage(cp.getCanvasImage());
                        Barcode bc = new Barcode(PDF417, 5, 300, 50, Globals.getTransactionNo());
                        mPrinter.printBarCode(bc);
                        mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
                        mPrinter.closeConnection();
                    }
                }).print();
            }
        });
    }

    String generateCustomQuery(ArrayList<MultiSelectionData> multiSelectionData) {
        if (multiSelectionData.size() < 1) return null;
        String customQuery = "";
        for (MultiSelectionData multiSelectionData1 : multiSelectionData) {
            switch (multiSelectionData1.sid) {
                case "1":
                    customQuery += customQuery.length() > 1 ? " union all " + memberQuery : memberQuery;
                    break;
                case "2":
                    customQuery += customQuery.length() > 1 ? " union all " + memberGroupQuery : memberGroupQuery;
                    break;
                case "3":
                    customQuery += customQuery.length() > 1 ? " union all " + surveyQuery : surveyQuery;
                    break;
                case "4":
                    customQuery += customQuery.length() > 1 ? " union all " + mappingQuery : mappingQuery;
                    break;
                case "5":
                    customQuery += customQuery.length() > 1 ? " union all " + addendumQuery : addendumQuery;
                    break;
            }
        }


        return customQuery;
    }

    void getNetworkStats() {
        PackageManager packageManager = getPackageManager();

        ApplicationInfo info = null;
        try {
            info = packageManager.getApplicationInfo("sparta.realm.apps.farmercontractor", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        int packageUid = info.uid;
        NetworkStatsManager networkStatsManager = (NetworkStatsManager) getSystemService(NETWORK_STATS_SERVICE);

        NetworkStats networkStats = networkStatsManager.queryDetailsForUid(ConnectivityManager.TYPE_WIFI, "", 0, System.currentTimeMillis(), packageUid);

        long rxBytes = 0L;
        long txBytes = 0L;
        NetworkStats.Bucket bucket = new NetworkStats.Bucket();
        while (networkStats.hasNextBucket()) {
            networkStats.getNextBucket(bucket);
            rxBytes += bucket.getRxBytes();
            txBytes += bucket.getTxBytes();
        }
        networkStats.close();
        Log.e("Network stats", "RX bytes: " + rxBytes + " TX bytes: " + txBytes);
    }

    void setupReport(String from, String to) {
        from = from.replace("DD-MM-YYYY", "");
        to = to.replace("DD-MM-YYYY", "");
        from = from == null || from.length() < 2 ? "01-01-2011" : from;
        to = to == null || to.length() < 2 ? svars.sdf_user_friendly_date.format(Calendar.getInstance().getTime()) + "" : to;

        if (from == null || from.length() < 2 || to == null || to.length() < 2) {


        }

        try {
            from = from.replace(" ", "");
            from = from + " 00:00:00";
            from = svars.sdf_user_friendly_date.parse(from).getTime() + "";
            from = ((int) (Math.floor(Double.parseDouble(from) / 1000))) + "";
        } catch (Exception ex) {
        }
        try {
            to = to.replace(" ", "");
            to = to + " 23:59:00";
            to = svars.sdf_user_friendly_time.parse(to).getTime() + "";
            to = ((int) (Math.floor(Double.parseDouble(to) / 1000))) + "";

        } catch (Exception ex) {
        }
        defaultFromTime=from;
        defaultToTime=to;
        pager.setTableFilters("CAST( strftime('%s', reg_time) as INTEGER)<" + to, "CAST( strftime('%s', reg_time) as INTEGER)>" + from);

    }

    void pickDate(Calendar inputCalendar, EditText inputText) {
        Calendar cc = Calendar.getInstance();
        int mYear = cc.get(Calendar.YEAR);
        int mMonth = cc.get(Calendar.MONTH);
        int mDay = cc.get(Calendar.DAY_OF_MONTH);


        try {


            mYear = inputCalendar.get(Calendar.YEAR);
            mMonth = inputCalendar.get(Calendar.MONTH);
            mDay = inputCalendar.get(Calendar.DAY_OF_MONTH);

        } catch (Exception ex) {
        }

        // Launch Date Picker Dialog..Continuous reg
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {


                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        inputText.setText((dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "-" + ((monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : (monthOfYear + 1)) + "-" + year);


                        try {
                            inputCalendar.setTime(Conversions.sdf_user_friendly_date.parse(inputText.getText().toString()));
                        } catch (Exception ex) {
                        }


                    }
                }, mYear, mMonth, mDay);
        dpd.show();
        Calendar calendar_min = Calendar.getInstance();
        calendar_min.set(Calendar.YEAR, calendar_min.get(Calendar.YEAR));
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset_input:
                binding.include.fromEdt.setText("");
                binding.include.toEdt.setText("");
                pager.setTableFilters(new String[0]);

                break;
            case R.id.printer_config:
              new bt_device_connector(this, bt_device_connector.bt_device_type.printer).show(new bt_device_connector.device_selection_handler() {

                  @Override
                  public void on_device_paired_and_selected(BluetoothDevice device) {

                  }

                  @Override
                  public void on_device_slected(BluetoothDevice device) {

                  }

                  @Override
                  public void on_device_paired(BluetoothDevice device) {

                  }
              });


              break;
            case R.id.config:
                   startActivity(new Intent(this,Configuration.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void on_print_begun() {

    }

    @Override
    public void on_print_complete() {

    }

    @Override
    public void on_print_error() {

    }
}