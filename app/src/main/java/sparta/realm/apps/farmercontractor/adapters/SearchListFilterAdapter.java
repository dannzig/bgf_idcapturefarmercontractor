package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.atteo.evo.inflector.English;

import java.util.ArrayList;
import java.util.List;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.MemberSearchFilter;
import sparta.realm.apps.farmercontractor.models.SearchFilterItem;
import sparta.realm.apps.farmercontractor.models.system.SelectionData;
import sparta.realm.apps.farmercontractor.utils.FormTools.SearchSpinner;
import sparta.realm.spartautils.svars;


public class SearchListFilterAdapter extends RecyclerView.Adapter<SearchListFilterAdapter.view> {

    Context cntxt;
    public ArrayList<SearchFilterItem> items;
    onFilterUpdatedListener listener;
    ViewGroup parent;

    public interface onFilterUpdatedListener {

        void onFilterUpdated(SearchFilterItem searchFilterItem,ArrayList<SearchFilterItem> selectedItems, String[] tableFilter);
    }


    public SearchListFilterAdapter(ArrayList<SearchFilterItem> items, @NonNull onFilterUpdatedListener listener) {

        this.items = items;
        this.listener = listener;
        listener.onFilterUpdated(null,selectedSearchFilter(),generateTableFilter());


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        this.parent = parent;
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_search_filter_input, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public SearchSpinner searchSpinner;
        public CheckBox checkBox;

        int position;


        view(View itemView) {
            super(itemView);

            searchSpinner = itemView.findViewById(R.id.searchSpinner);
            checkBox = itemView.findViewById(R.id.searchSpinner_check);


        }

        void populate(int position) {
            this.position = position;
            SearchFilterItem searchFilterItem = items.get(position);

            SearchFilterItem savedSearchFilterItem = svars.workingObject(SearchFilterItem.class, searchFilterItem.sid);
            if (savedSearchFilterItem != null) {
                searchFilterItem.active = savedSearchFilterItem.active;
                searchFilterItem.value = savedSearchFilterItem.value;
            }


            searchSpinner.setTitle(searchFilterItem.name);
            searchSpinner.setPlaceholder("Search " + English.plural(searchFilterItem.name, 2));
            searchSpinner.setSearchTitle("Select a " + English.plural(searchFilterItem.name, 1).toLowerCase());

            try {
                searchSpinner.setDataset(Realm.databaseManager.loadObjectArray(Class.forName(searchFilterItem.dataset),
                        ((searchFilterItem.parent_column != null && searchFilterItem.parent_data_name != null)
                                ? new Query().setTableFilters(searchFilterItem.parent_column + " = "
                                + parentSearchFilerItem(searchFilterItem.parent_data_name).value)
                                : new Query())
                ), new SearchSpinner.InputListener() {
                    @Override
                    public void onInputAvailable(boolean valid, String input) {
                        if(searchFilterItem.value==input){
                            return;
                        }
                        searchFilterItem.value = input;
                        svars.setWorkingObject(searchFilterItem, searchFilterItem.sid);

                        try {
                            notifyDataSetChanged();
                        } catch (Exception ex) {
                        }
                        listener.onFilterUpdated(savedSearchFilterItem,selectedSearchFilter(),generateTableFilter());

                    }
                });
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            searchSpinner.setInput(searchFilterItem.value);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(searchFilterItem.active==b){
                        return;
                    }
                    if(searchFilterItem.value==null)
                    {
                        searchFilterItem.active=false;
                        checkBox.setChecked(false);
                        return;
                    }
                    searchFilterItem.active = b;
                    svars.setWorkingObject(searchFilterItem, searchFilterItem.sid);
                    try {
                        notifyDataSetChanged();
                    } catch (Exception ex) {
                    }
                    listener.onFilterUpdated(searchFilterItem,selectedSearchFilter(),generateTableFilter());
                }
            });
            checkBox.setChecked(searchFilterItem.active);
        }



        @Override
        public void onClick(View view) {

        }
    }
    String[] tableFilter;

    public String[] generateTableFilter() {
        int size = 0;
        List<String> filterRaw = new ArrayList<>();
        for (SearchFilterItem searchFilterItem : items) {
            SearchFilterItem savedSearchFilterItem= svars.workingObject(SearchFilterItem.class, searchFilterItem.sid);
            if (savedSearchFilterItem!=null&&savedSearchFilterItem.active) {
                filterRaw.add(searchFilterItem.column + " = '" + savedSearchFilterItem.value + "'");
            }
        }
        String[] tableFilter = new String[filterRaw.size()];
        filterRaw.toArray(tableFilter);

        return tableFilter;
    }

    ArrayList<SearchFilterItem> selectedSearchFilter() {
        ArrayList<SearchFilterItem> searchFilterItems = new ArrayList<>();
        for (SearchFilterItem searchFilterItem : items) {
            SearchFilterItem savedSearchFilterItem= svars.workingObject(SearchFilterItem.class, searchFilterItem.sid);
            if (savedSearchFilterItem!=null&&savedSearchFilterItem.active) {
                searchFilterItems.add(savedSearchFilterItem);
            }
        }

        return searchFilterItems;
    }

    SearchFilterItem parentSearchFilerItem(String parent_column) {
        for (SearchFilterItem searchFilterItem :
                items) {
            if (searchFilterItem.column.equals(parent_column)) {
                return searchFilterItem;
            }
        }

        return null;
    }




}
