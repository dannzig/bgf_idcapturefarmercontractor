package sparta.realm.apps.farmercontractor.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.vipulasri.timelineview.TimelineView;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.databinding.ActivityMemberSummaryBinding;
import sparta.realm.apps.farmercontractor.models.Addendum;
import sparta.realm.apps.farmercontractor.models.FarmerContract;
import sparta.realm.apps.farmercontractor.models.LandLocation;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberImage;
import sparta.realm.apps.farmercontractor.models.SuitabilityAssessmentSurveyEntry;
import sparta.realm.spartautils.svars;

public class MemberSummary extends AppCompatActivity {

    Member member;
    SuitabilityAssessmentSurveyEntry suitability;

    FarmerContract Contract;
    sparta.realm.apps.farmercontractor.models.Addendum addendum;
    LandLocation location;
    ActivityMemberSummaryBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMemberSummaryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        member = Realm.databaseManager.loadObject(Member.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));

        suitability = Realm.databaseManager.loadObject(SuitabilityAssessmentSurveyEntry.class, new Query().setTableFilters("member='" + getIntent().getStringExtra("sid") + "'","total_score != '" + 0 + "'"));
        Contract = Realm.databaseManager.loadObject(FarmerContract.class, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
        addendum = Realm.databaseManager.loadObject(Addendum.class, new Query().setTableFilters("member_id='" + getIntent().getStringExtra("sid") + "'"));
        location = Realm.databaseManager.loadObject(LandLocation.class, new Query().setTableFilters("member='" + getIntent().getStringExtra("sid") + "'"));


        initUi();
        binding.itemTimeline.action1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MemberSummary.this, SuitabilityAssessment.class);
                startActivity(intent);
            }
        });

        binding.itemTimeline.actionMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MemberSummary.this, LandMapping.class);
                startActivity(intent);
            }
        });

        binding.itemTimeline.action3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MemberSummary.this, sparta.realm.apps.farmercontractor.activities.Addendum.class);
                startActivity(intent);
            }
        });

    }

    @SuppressLint("ResourceAsColor")
    void initUi() {
        setupToolbar(binding.include.toolbar);
        loadMemberInfo();
        binding.itemTimeline.timeline.setStartLineColor(ContextCompat.getColor(this, R.color.purple_700), TimelineView.FOCUSABLES_ALL);
        binding.itemTimeline.timeline1.setStartLineColor(ContextCompat.getColor(this, R.color.purple_700), TimelineView.FOCUSABLES_ALL);
        binding.itemTimeline.timelineMap.setStartLineColor(ContextCompat.getColor(this, R.color.purple_700), TimelineView.FOCUSABLES_ALL);
        binding.itemTimeline.timeline2.setStartLineColor(ContextCompat.getColor(this, R.color.purple_700), TimelineView.FOCUSABLES_ALL);
        binding.itemTimeline.timeline3.setStartLineColor(ContextCompat.getColor(this, R.color.purple_700), TimelineView.FOCUSABLES_ALL);

        if (member == null) {
            binding.itemTimeline.timelines.setVisibility(View.GONE);
        } else {
            binding.itemTimeline.textTimelineDate.setText(member.reg_time);
            binding.itemTimeline.textTimelineTitle.setText("Pre-Registration Done");
        }


        if (suitability == null || suitability.total_score.equalsIgnoreCase("0")) {

            binding.itemTimeline.card1.setCardBackgroundColor(R.color.greyed);
            binding.itemTimeline.action1.setText("Suitability");
            binding.itemTimeline.textTimelineTitle1.setText("Suitability Not Done");
        } else {
            binding.itemTimeline.action1.setVisibility(View.GONE);
            binding.itemTimeline.textTimelineDate1.setText(suitability.reg_time);
            binding.itemTimeline.textTimelineTitle1.setText("Suitability Done");
        }
        if (location == null || location.latitude == null || location.latitude.equalsIgnoreCase("null")) {

            binding.itemTimeline.cardMap.setCardBackgroundColor(R.color.greyed);
            binding.itemTimeline.actionMap.setText("Geo Map");
            binding.itemTimeline.textTimelineTitleMap.setText("Geo Map Not Done");
        } else {
            binding.itemTimeline.actionMap.setVisibility(View.GONE);
            binding.itemTimeline.textTimelineDateMap.setText(location.reg_time);
            binding.itemTimeline.textTimelineTitleMap.setText("Geo Map Done");
        }

        if (Contract == null) {

            binding.itemTimeline.card2.setCardBackgroundColor(R.color.greyed);
            binding.itemTimeline.textTimelineTitle2.setText("Contract Not Done");
        } else {
            binding.itemTimeline.action2.setVisibility(View.GONE);
            binding.itemTimeline.textTimelineDate2.setText(Contract.generation_time);
            binding.itemTimeline.textTimelineTitle2.setText("Contract Done");
        }
        if (addendum == null) {


            binding.itemTimeline.card3.setCardBackgroundColor(R.color.greyed);
            binding.itemTimeline.action3.setText("Addendum");
            binding.itemTimeline.textTimelineTitle3.setText("Addendum Not Done");
        } else {
            binding.itemTimeline.action3.setVisibility(View.GONE);
            binding.itemTimeline.textTimelineDate3.setText(addendum.reg_time);
            binding.itemTimeline.textTimelineTitle3.setText("Addendum Done");
        }

    }


    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    void loadMemberInfo() {
        binding.include2.info1.setText("Nat ID: " + member.nat_id);
        binding.include2.name.setText(member.full_name);
        member.profile_photo = Realm.databaseManager.loadObject(MemberImage.class, new Query().setTableFilters("member_id='" + member.sid + "'"));
        if (member.profile_photo != null) {

            binding.include2.icon.setImageURI(null);
            binding.include2.icon.setImageURI(Uri.parse(Uri.parse(svars.current_app_config(this).file_path_employee_data) + member.profile_photo.image));

        }
    }
//    void loadSummary(Class realm_model){
//
//        Object realmModel = Realm.databaseManager.loadObject(realm_model, new Query().setTableFilters("sid='" + getIntent().getStringExtra("sid") + "'"));
//
//    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.registration_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "Clicked Menu", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.clear_all_menu:
                Toast.makeText(this, "Clicked clear all", Toast.LENGTH_LONG).show();
                break;
            case R.id.exit_registration:
                Toast.makeText(this, "Clicked Exit enrolment", Toast.LENGTH_LONG).show();
//                showQuitDialog();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}