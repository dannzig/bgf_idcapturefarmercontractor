package sparta.realm.apps.farmercontractor.activities;

import static com.android.print.sdk.PrinterConstants.BarcodeType.PDF417;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.databinding.ActivityLandMappingBinding;
import sparta.realm.apps.farmercontractor.models.Member;

import sparta.realm.spartautils.svars;

public class AddendumLandMapping extends AppCompatActivity {


    final GoogleMap[] googleMap = new GoogleMap[1];
    ActivityLandMappingBinding binding;
    boolean search;
    Member member;

    Location mypos;
    Marker currentPositionMarker;
    Boolean recordSaved = false;
    Bitmap bitmap;
    String member_fname, member_nat_id, member_sid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLandMappingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        member_fname = getIntent().getStringExtra("member_fname");
        member_nat_id = getIntent().getStringExtra("member_nat_id");
        member_sid = getIntent().getStringExtra("member_sid");

        if (!DatabaseManager.gps.canGetLocation()){
            showSettingsAlert();
        }

        initUi();
        initMap(savedInstanceState);



    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddendumLandMapping.this);
        alertDialog.setTitle("GPS settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Enable GPS in settings menu");
        alertDialog.setCancelable(false);

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(AddendumLandMapping.this, "ACCEPT APP TO ACCESS LOACTION TO CONTINUE", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                AddendumLandMapping.this.startActivity(intent);

            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (member == null) {

        } else {
            initUi();
        }
    }

    void initUi() {
        setupToolbar(binding.include.toolbar);
        loadMemberInfo();
        binding.save.setOnClickListener(view -> {

            if (mypos.getAccuracy() > 20) {
                Toast.makeText(AddendumLandMapping.this, "Location accuracy is not good,try moving to an unobstructed area (clear field)", Toast.LENGTH_LONG).show();
            } else {
//                showSaveDialog();
                captureScreen();

            }
        });

    }

    void loadMemberInfo() {
        binding.include2.info1.setText("Nat ID: " + member_nat_id);
        binding.include2.name.setText(member_fname);
    }


    public void initMap(Bundle saved_inst) {
        binding.mapView.onCreate(saved_inst);
        binding.mapView.onResume();
        binding.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap[0] = mMap;

//                MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(act, R.raw.g_map_blue_mode_1);
//                Log.e("Setting map style=>", "" + googleMap[0].setMapStyle(style));


                googleMap[0].setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {


                        mypos = location;

                        //binding.coordinates.setText("Co-ordinates: (" + location.getLatitude() + "," + location.getLongitude() + ")");
                        binding.coordinates.setText("Co-ordinates: (" + location.getLatitude() + "," + location.getLongitude() + ") Accuracy: "+ location.getAccuracy() +" ");

                        if (currentPositionMarker != null) {
                            currentPositionMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                            currentPositionMarker.setSnippet("Location accuracy is " + location.getAccuracy() + " meters");
                            currentPositionMarker.showInfoWindow();
                        } else {
                            MarkerOptions mmto = new MarkerOptions();
                            mmto.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            mmto.position(new LatLng(location.getLatitude(), location.getLongitude()));
                            mmto.title("Farm location");
                            mmto.snippet("Location accuracy is " + location.getAccuracy() + " meters");


                            currentPositionMarker = googleMap[0].addMarker(mmto);
                            currentPositionMarker.setDraggable(false);
                            currentPositionMarker.showInfoWindow();
                        }


                    }
                });
                googleMap[0].setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        try {
                            if (googleMap[0].isMyLocationEnabled()) {
                            }
                            Location myLocation = googleMap[0].getMyLocation();
                            Log.e("Patrol Check Point ", "onMapLoaded: LatLong => " + myLocation.getLatitude() + "  " + myLocation.getLongitude());
                            mypos = myLocation;
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())).zoom(17).build();
                            googleMap[0].animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        } catch (Exception ex) {

                            showSettingsAlert();

                            Toast.makeText(AddendumLandMapping.this, "UNABLE TO GET CURRENT LOCATION", Toast.LENGTH_SHORT).show();
                            ex.printStackTrace();
                        }

                    }
                });
                if (ActivityCompat.checkSelfPermission(AddendumLandMapping.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddendumLandMapping.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    // ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    // public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    // int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap[0].setMyLocationEnabled(true);
                googleMap[0].setMapType(GoogleMap.MAP_TYPE_HYBRID);


            }

        });

    }

    void showSaveDialog() {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_save_land_location_confirmation, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button save = aldv.findViewById(R.id.save);
        save.setOnClickListener(view -> {

            Intent intent = new Intent(this, Addendum.class);
            intent.putExtra("latitude", mypos.getLatitude() + "");
            intent.putExtra("longitude", mypos.getLongitude() + "");
            intent.putExtra("accuracy", mypos.getAccuracy() + "");
            intent.putExtra("mem_sid", member_sid);
            startActivity(intent);

        });
        ((TextView) aldv.findViewById(R.id.sub_title)).setText(member_fname);
        ((TextView) aldv.findViewById(R.id.description)).setText("Confirm Addendum land location details for this member"
                + "\nLand location: (" + mypos.getLatitude() + "" + " , " + mypos.getLongitude() + "" + ")"
                + "\nCo-ordinates accuracy: " + mypos.getAccuracy() + "");

        try {
            ImageView iv = aldv.findViewById(R.id.location_image);

            iv.setImageBitmap(bitmap);

        } catch (Exception ex) {

        }
        aldv.findViewById(R.id.dismiss).setOnClickListener(view -> {
            if (recordSaved) {
                finish();
            } else {
                ald.dismiss();

            }
        });
    }

    public void captureScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // TODO Auto-generated method stub
                bitmap = snapshot;

                OutputStream fout = null;

                String filePath = svars.current_app_config(Realm.context).file_path_employee_data + System.currentTimeMillis() + ".jpeg";

                try {
                    fout = new FileOutputStream(filePath);

                    // Write the string to the file
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                    fout.flush();
                    fout.close();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    Log.e("ImageCapture", "FileNotFoundException");
                    Log.e("ImageCapture", e.getMessage());
                    filePath = "";
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    Log.e("ImageCapture", "IOException");
                    Log.e("ImageCapture", e.getMessage());
                    filePath = "";
                }

//                openShareImageDialog(filePath);
                showSaveDialog();
            }
        };

        showSaveDialog();
        googleMap[0].snapshot(callback);
    }

    public void openShareImageDialog(String filePath) {
//        File file = this.getFileStreamPath(filePath);
        File file = new File(filePath);

        if (!filePath.equals("")) {
            final ContentValues values = new ContentValues(2);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
            final Uri contentUriFile = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("image/jpeg");
            intent.putExtra(android.content.Intent.EXTRA_STREAM, contentUriFile);
            startActivity(Intent.createChooser(intent, "Share Image"));
        } else {
            //This is a custom class I use to show dialogs...simply replace this with whatever you want to show an error message, Toast, etc.
            Toast.makeText(this, "Failed to share", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        // Finish the current activity
        finish();
        //onBackPressed();
        return true;
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.non_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                onBackPressed();
                break;
            case R.id.config:
                startActivity(new Intent(this, Configuration.class));

                break;
        }
        return super.onOptionsItemSelected(item);

    }
}

