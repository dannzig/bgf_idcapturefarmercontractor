package sparta.realm.apps.farmercontractor.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.realm.annotations.SyncDescription;
import com.realm.annotations.sync_service_description;
import com.realm.annotations.sync_status;

import java.text.DecimalFormat;
import java.util.ArrayList;

import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.adapters.SyncReportAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivitySyncReportBinding;
import sparta.realm.apps.farmercontractor.models.system.SyncServiceReport;

public class SyncReport extends AppCompatActivity {


    ActivitySyncReportBinding binding;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");
    double totalRecords = 0;
    double syncedRecords = 0;
    double pendingRecords = 0;
    double erroneousRecords = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySyncReportBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initUi();

    }

    void initUi() {
        setupToolbar(binding.include.toolbar);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(new SyncReportAdapter(sync_reports()));
        binding.include2.synchronizedRecordsValue.setText(((int) syncedRecords) + " of " + ((int) totalRecords));
        binding.include2.synchronizedRecordsChart.setProgress(Float.parseFloat(decimalFormat.format(((syncedRecords / totalRecords) * 100))));
        binding.include2.pendingRecordsValue.setText(((int) pendingRecords) + " of " + ((int) totalRecords));
        binding.include2.pendingRecordsChart.setProgress(Float.parseFloat(decimalFormat.format(((pendingRecords / totalRecords) * 100))));
        binding.include2.erroneousRecordsValue.setText(((int) erroneousRecords) + " of " + ((int) totalRecords));
        binding.include2.erroneousRecordsChart.setProgress(Float.parseFloat(decimalFormat.format(((erroneousRecords / totalRecords) * 100))));

    }

    ArrayList<SyncServiceReport> sync_reports() {
        totalRecords = 0;
        syncedRecords = 0;
        pendingRecords = 0;
        erroneousRecords = 0;

        ArrayList<SyncServiceReport> sync_reports = new ArrayList<>();
        for (sync_service_description ssd : Realm.realm.getSyncDescription()) {
            if (ssd.servic_type == SyncDescription.service_type.Upload) {
                SyncServiceReport ssr = new SyncServiceReport();
                ssr.service_name = ssd.service_name;
                try {
                    int temp_total = Realm.databaseManager.getRecordCount(Class.forName(ssd.object_package));
                    int temp_synced = Realm.databaseManager.getRecordCount(Class.forName(ssd.object_package), "sync_status='" + sync_status.syned.ordinal() + "'");
                    int temp_pending = Realm.databaseManager.getRecordCount(Class.forName(ssd.object_package), "sync_status='" + sync_status.pending.ordinal() + "'");
                    int temp_erroneous = Realm.databaseManager.getRecordCount(Class.forName(ssd.object_package), "sync_status='" + sync_status.pending.ordinal() + "'", "data_status='e'");
                    totalRecords += temp_total;
                    syncedRecords += temp_synced;
                    pendingRecords += temp_pending;
                    erroneousRecords += temp_erroneous;

                    ssr.total_records = +temp_total + "";
                    ssr.synchronized_records = temp_synced + "";//sd.get_record_count("member_info_table","sync_status='i'");
                    ssr.issues_records = temp_erroneous + "";//sd.get_record_count("member_info_table","sync_status='i'");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                sync_reports.add(ssr);
            }

        }
        return sync_reports;

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.registration_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "Clicked Menu", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.clear_all_menu:
                Toast.makeText(this, "Clicked clear all", Toast.LENGTH_LONG).show();
                break;
            case R.id.exit_registration:
                Toast.makeText(this, "Clicked Exit enrolment", Toast.LENGTH_LONG).show();
//                showQuitDialog();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}