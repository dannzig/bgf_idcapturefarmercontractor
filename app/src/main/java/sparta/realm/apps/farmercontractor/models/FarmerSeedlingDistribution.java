package sparta.realm.apps.farmercontractor.models;

import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;
import com.realm.annotations.SyncDescription;
import java.io.Serializable;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.spartautils.svars;

@DynamicClass(table_name = "farmer_distribution")
@SyncDescription(service_name = "Farmer Distribution", download_link = "/FarmersContract/Group/FarmerSeedlingdistributionList", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
@SyncDescription(service_name = "Farmer Distribution", upload_link = "/FarmersContract/Group/AddSeedDistributiondetails", service_type = SyncDescription.service_type.Upload)

public class FarmerSeedlingDistribution extends RealmModel implements Serializable {
//    {
//            "seedling_distribution_id":"1",
//            "member_id":"1",
//            "seedling_issued":"2000",
//            "agroforestry_agent_id":"1",
//            "Received_by":"weldon",
//            "transaction_no":"001",
//            "date":"2023-10-10"
//    }

    @DynamicProperty(json_key = "seedling_distribution_id")
    public String seedling_distribution_id;

    @DynamicProperty(json_key = "member_id")
    public String member_id;

    @DynamicProperty(json_key = "seedling_issued")
    public String seedling_issued;

    @DynamicProperty(json_key = "agroforestry_agent_id")
    public String agroforestry_agent_id;

    @DynamicProperty(json_key = "Received_by")
    public String Received_by;


    public FarmerSeedlingDistribution() {
    }

    public FarmerSeedlingDistribution(String seedling_distribution_id, String member_id, String seedling_issued, String agroforestry_agent_id, String received_by) {
        this.seedling_distribution_id = seedling_distribution_id;
        this.member_id = member_id;
        this.seedling_issued = seedling_issued;
        this.agroforestry_agent_id = agroforestry_agent_id;
        this.Received_by = received_by;

        this.transaction_no = svars.getTransactionNo();
        this.sync_status = com.realm.annotations.sync_status.pending.ordinal() + "";
        this.user_id = Globals.user().sid;
    }
}


