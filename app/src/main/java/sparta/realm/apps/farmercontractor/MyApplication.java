package sparta.realm.apps.farmercontractor;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import java.util.Calendar;

import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.models.RealmDynamics.spartaDynamics;
import sparta.realm.apps.farmercontractor.models.system.EmailConfiguration;
import sparta.realm.apps.farmercontractor.services.BackupAlarmService;
import sparta.realm.utils.AppConfig;


public class MyApplication extends Application {

    public static String logTag = "Application";
    public static SyncOverride syncOverride;
    private static Context baseContext;
    EventCheckerTimer eventCheckerTimer;
    private Intent intent;
    private PendingIntent pendingIntent;
    private AlarmManager alarmMgr;

    public static boolean isServiceRunning(Context act, Class<?> serviceClass) {
        ActivityManager manager;
        manager = (ActivityManager) act.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        baseContext = getBaseContext();



        //Demo Application
        AppConfig farmerContractorBGF = new AppConfig("https://tabiztest.cs4africa.com/fcbgf",
                null,
                "Capture Solutions",
                "TEST ACCOUNT",
                "/Authentication/Login/Submit", false

        );
//        AppConfig farmerContractorDemo = new AppConfig("http://ta.cs4africa.com:6090",
//                null,
//                "Capture Solutions",
//                "TEST ACCOUNT",
//                "/Authentication/Login/Submit", false
//
//        );
        //BGF Live Local Server
//        AppConfig farmerContractorBGF = new AppConfig("https://bgf-farmerscontract.cs4africa.com",
//                null,
//                "Capture Solutions",
//                "TEST ACCOUNT",
//                "/Authentication/Login/Submit", false
//
//        );
        //Capture Live Server
//        AppConfig farmerContractorLive = new AppConfig("http://ta.cs4africa.com:1600",
//                null,
////                "http://ta.cs4africa.com:2222/api/AppStore/LoadApp",
//                "Capture Solutions",
//                "TEST ACCOUNT",
//                "/Authentication/Login/Submit", false
//
//        );




//        farmerContractorLive.app_folder_path = Environment.getExternalStorageDirectory().toString() + "/Realm/" + BuildConfig.APPLICATION_ID + "/";
//        farmerContractorLive.file_path_db_folder = getExternalFilesDir(null).getAbsolutePath() + "/";
//        Realm.Initialize(this, new spartaDynamics(), BuildConfig.VERSION_NAME, BuildConfig.APPLICATION_ID, farmerContractorLive);

        //Creating a folder for saving apps
        farmerContractorBGF.app_folder_path = Environment.getExternalStorageDirectory().toString() + "/Realm/" + BuildConfig.APPLICATION_ID + "/";
        farmerContractorBGF.file_path_db_folder = getExternalFilesDir(null).getAbsolutePath() + "/";
        Realm.Initialize(this, new spartaDynamics(), BuildConfig.VERSION_NAME, BuildConfig.APPLICATION_ID, farmerContractorBGF);

        //Setting Text to Separate Live and Demo Apps
        //If link changes change the string in the .equalsIgnoreCase() method
        if (farmerContractorBGF.APP_MAINLINK.equalsIgnoreCase("https://tabiztest.cs4africa.com/fcbgf")){
            Globals.setOperationMode(Globals.OperationMode.Demo);

        }else if(farmerContractorBGF.APP_MAINLINK.equalsIgnoreCase("https://bgf-farmerscontract.cs4africa.com")){
            Globals.setOperationMode(Globals.OperationMode.Live);
            
        }else {
            Globals.setOperationMode(Globals.OperationMode.NONE);
        }

        eventCheckerTimer = new EventCheckerTimer(getApplicationContext());
        eventCheckerTimer.initChecks();
        EmailConfiguration ec = new EmailConfiguration();
        ec.username = "edward@capturesolutions.com";
        ec.password = "Sp@rt@!123";
        Globals.setBackupEmailConfiguration(ec);
        Globals.setBackupEmail("enoch@capturesolutions.com");
//Globals.setMemberSearchFilter(new MemberSearchFilter());
//        DatabaseManager.database.execSQL("DELETE FROM suitability_assessment_survey_entry");
//        DatabaseManager.database.execSQL("DELETE FROM farmer_contract");
//        DatabaseManager.database.execSQL("DELETE FROM village");
//        DatabaseManager.database.execSQL("DELETE FROM member_group");
//        DatabaseManager.database.execSQL("UPDATE member_image_table set image_index='3' WHERE _id=4");
        //{"$id":"1","result":{"$id":"2","isOkay":true,"message":"login successfull","result":{"$id":"3","user_id":6,"username":"demo","password":null,"account_id":1,"status":true,"user_branches":[{"$id":"4","id":5,"branch_name":"7 Forks"},{"$id":"5","id":7,"branch_name":"Dokolo"}]}},"moduleName":"Login Module"}
//        {"$id":"1","result":{"$id":"2","isOkay":true,"message":"login successfull","result":{"$id":"3","user_id":6,"username":"demo","password":null,"account_id":1,"status":true,"user_branches":[{"$id":"4","id":5,"branch_name":"7 Forks"},{"$id":"5","id":7,"branch_name":"Dokolo"}]}},"moduleName":"Login Module"}
        setupAlarms();
    }

    void setupAlarms() {
        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.MINUTE,1);
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        long delayInMillis = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        delayInMillis = System.currentTimeMillis() + delayInMillis;
        Intent myIntent = new Intent(this, BackupAlarmService.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getService(this, 60, myIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, delayInMillis, pendingIntent);
    }


}
