package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.SearchFilterItem;

import sparta.realm.apps.farmercontractor.models.system.SelectionData;


public class SelectedSearchFilterAdapter extends RecyclerView.Adapter<SelectedSearchFilterAdapter.view> {

    Context cntxt;
    public ArrayList<SearchFilterItem> items;
    onItemClickListener listener;

    public interface onItemClickListener {

        void onItemClick(SearchFilterItem mem, View view);
    }


    public SelectedSearchFilterAdapter(ArrayList<SearchFilterItem> items, onItemClickListener listener) {

        this.items = items;
        this.listener = listener;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_selected_member, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        SearchFilterItem obj = items.get(position);
        holder.icon.setVisibility(View.GONE);
        try {
            holder.name.setText(obj.name+": "+((SelectionData)Realm.databaseManager.loadObject(Class.forName(obj.dataset),new Query().setTableFilters("sid='"+obj.value+"'"))).name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(obj, holder.itemView);

            }
        });


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public String sid;
        public ImageView icon,close;


        view(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            icon = itemView.findViewById(R.id.icon);
//            icon = itemView.findViewById(R.id.icon);


        }

        @Override
        public void onClick(View view) {

        }
    }
}
