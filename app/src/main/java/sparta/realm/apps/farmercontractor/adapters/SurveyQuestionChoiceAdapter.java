package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.lzyzsd.circleprogress.ArcProgress;

import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.spartamodels.percent_calculation;


public class SurveyQuestionChoiceAdapter extends  RecyclerView.Adapter<SurveyQuestionChoiceAdapter.view> {

    Context cntxt;
    public ArrayList<SurveyQuestionChoice> items;
public String selectedChoice="";

    public interface OnChoiceListener{

        void onChoiceSelected(SurveyQuestionChoice surveyQuestionChoice);
    }
OnChoiceListener onChoiceListener;

  public SurveyQuestionChoiceAdapter(String selectedChoice,ArrayList<SurveyQuestionChoice> items,OnChoiceListener onChoiceListener)
    {
        this.selectedChoice = selectedChoice==null?"":selectedChoice;
        this.items = items;


        this.onChoiceListener = onChoiceListener;
    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt= parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_survey_question_choice, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
      holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
      public TextView choice_info;
      RadioButton check;


int position;



             view(View itemView) {
            super(itemView);

                 choice_info = itemView.findViewById(R.id.choice_info);
                 check = itemView.findViewById(R.id.check);

check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(!b){
            return;
        }
        selectedChoice=items.get(position).sid;
        onChoiceListener.onChoiceSelected(items.get(position));
        try{
            notifyDataSetChanged();
        }catch (Exception ex){

        }
    }
});

        }

       public void populate(int position){
            this.position=position;
            SurveyQuestionChoice surveyQuestionChoice=items.get(position);

            check.setText(surveyQuestionChoice.choice+": "+surveyQuestionChoice.choice_description);
           choice_info.setText("marks ("+surveyQuestionChoice.marks_from+"-"+surveyQuestionChoice.marks_to+")");
           if(surveyQuestionChoice.sid.equals(selectedChoice)){
               check.setChecked(true);
           }else {
               check.setChecked(false);

           }
           //katikati ya salama sultan amu
           //mbiini

        }
        @Override
        public void onClick(View view) {

      }
    }
}
