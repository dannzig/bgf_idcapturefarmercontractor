package sparta.realm.apps.farmercontractor.activities.registration;


import android.widget.ImageView;

import java.io.Serializable;

import sparta.realm.apps.farmercontractor.models.MemberImage;

public class ImageToCapture implements Serializable {


    public String data_field;
    public MemberImage.MemberImageType imageType;
    public ImageView iv, delete;

    public ImageToCapture(String data_field, ImageView iv, ImageView delete, MemberImage.MemberImageType imageType) {

        this.data_field = data_field;
        this.delete = delete;
        this.iv = iv;
        this.imageType = imageType;
    }

}
