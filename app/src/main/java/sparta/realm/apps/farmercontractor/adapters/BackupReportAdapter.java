package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.BackupManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.BackupEntry;
import sparta.realm.apps.farmercontractor.models.BackupUploadEntry;
import sparta.realm.utils.Conversions;


public class BackupReportAdapter extends RecyclerView.Adapter<BackupReportAdapter.view> {

    public ArrayList<BackupEntry> items;
    Context cntxt;


    public BackupReportAdapter(ArrayList<BackupEntry> items) {
        this.items = items;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_backup_report_entry, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface onItemClickListener {

        void onItemClick(BackupEntry mem);
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, info,time;
        RecyclerView recyclerView;


        int position;


        view(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            recyclerView = itemView.findViewById(R.id.backup_upload_entry_list);
            info = itemView.findViewById(R.id.info1);
            time = itemView.findViewById(R.id.time);
        }

        public void populate(int position) {
            this.position = position;
            BackupEntry backupEntry = items.get(position);
            ArrayList<BackupUploadEntry> backupUploadEntries= Realm.databaseManager.loadObjectArray(BackupUploadEntry.class,new Query().setTableFilters("backup_transaction_no='"+backupEntry.transaction_no+"'"));
            FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
            flowLayoutManager.setAutoMeasureEnabled(true);
            flowLayoutManager.setItemPrefetchEnabled(true);
            recyclerView.setLayoutManager(flowLayoutManager);
            recyclerView.setAdapter(new BackupUploadReportAdapter(backupUploadEntries));
            name.setText(backupEntry.file_name);
            info.setText("Size: "+ BackupManager.getUserDisplayBytes(backupEntry.file_size));
            time.setText("Backup time: "+ Conversions.getUserDisplayTimeFromDBTime(backupEntry.reg_time));
//            time.setText("Backup time: "+ Conversions.getUserDisplayDateFromDBTime(ssr.reg_time));


        }

        @Override
        public void onClick(View view) {

        }
    }
}
