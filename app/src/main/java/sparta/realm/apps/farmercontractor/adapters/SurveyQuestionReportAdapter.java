package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.SurveyQuestion;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionEntry;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionChoice;
import sparta.realm.apps.farmercontractor.models.SurveyQuestionEntry;


public class SurveyQuestionReportAdapter extends RecyclerView.Adapter<SurveyQuestionReportAdapter.view> {

    Context cntxt;
    public ArrayList<SurveyQuestionEntry> items;


    public interface onItemClickListener {

        void onItemClick(SurveyQuestionEntry surveyQuestionEntry);
    }


    public SurveyQuestionReportAdapter(ArrayList<SurveyQuestionEntry> items) {
        this.items = items;


    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt = parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_survey_question_entry_report, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
        holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView index, question, award, award_info;



        int position;


        view(View itemView) {
            super(itemView);

            index = itemView.findViewById(R.id.index);
            question = itemView.findViewById(R.id.question);
            award = itemView.findViewById(R.id.award);
            award_info = itemView.findViewById(R.id.award_info);

        }

        public void populate(int position) {
            this.position = position;
            SurveyQuestionEntry surveyQuestionEntry = items.get(position);
            SurveyQuestion surveyQuestion = Realm.databaseManager.loadObject(SurveyQuestion.class,new Query().setTableFilters("sid='"+surveyQuestionEntry.question+"'"));
            SurveyQuestionChoice surveyQuestionChoice = Realm.databaseManager.loadObject(SurveyQuestionChoice.class,new Query().setTableFilters("sid='"+surveyQuestionEntry.choice+"'"));

            index.setText((position + 1) + "");
            question.setText(surveyQuestion.question);
            if (surveyQuestion.surveyQuestionChoices != null){
                award.setText(surveyQuestionEntry.marks+"/"+getMaxScore(surveyQuestion));
                award_info.setText(surveyQuestionChoice  == null ? "No Choice" : surveyQuestionChoice.choice+": "+surveyQuestionChoice.choice_description);
            }


        }

        String getMaxScore(SurveyQuestion surveyQuestion){
            if (!surveyQuestion.max_marks.equalsIgnoreCase("0")){
                ArrayList<SurveyQuestionChoice> surveyQuestionChoices=Realm.databaseManager.loadObjectArray(SurveyQuestionChoice.class,new Query().setTableFilters("question='"+surveyQuestion.sid+"'"));
                SurveyQuestionChoice highestSurveyQuestionChoice=null;
                for(SurveyQuestionChoice surveyQuestionChoice:surveyQuestionChoices){
                    if(highestSurveyQuestionChoice==null){
                        highestSurveyQuestionChoice=surveyQuestionChoice;
                        continue;
                    }
                    if(Integer.parseInt(surveyQuestionChoice.marks_to)>Integer.parseInt(highestSurveyQuestionChoice.marks_to)){
                        highestSurveyQuestionChoice=surveyQuestionChoice;

                    }

                }

                return highestSurveyQuestionChoice.marks_to;
            }else{
                return "0";
            }

        }



        @Override
        public void onClick(View view) {

        }
    }
}
