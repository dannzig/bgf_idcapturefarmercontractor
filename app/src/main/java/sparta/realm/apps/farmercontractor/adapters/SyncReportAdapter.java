package sparta.realm.apps.farmercontractor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.lzyzsd.circleprogress.ArcProgress;

import java.util.ArrayList;

import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.models.system.SyncServiceReport;
import sparta.realm.spartamodels.percent_calculation;


public class SyncReportAdapter extends  RecyclerView.Adapter<SyncReportAdapter.view> {

    Context cntxt;
    public ArrayList<SyncServiceReport> items;


    public interface onItemClickListener{

        void onItemClick(SyncServiceReport mem);
    }


  public SyncReportAdapter(ArrayList<SyncServiceReport> items)
    {
        this.items = items;



    }

    @NonNull
    @Override
    public view onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        cntxt= parent.getContext();
        View view = LayoutInflater.from(cntxt).inflate(R.layout.item_service_sync_report, parent, false);

        return new view(view);
    }

    @Override
    public void onBindViewHolder(@NonNull view holder, int position) {
      holder.populate(position);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class view extends RecyclerView.ViewHolder implements View.OnClickListener {
      public TextView service_name,synchronized_records,issue_records;
      ArcProgress progress;


int position;



             view(View itemView) {
            super(itemView);

                 service_name = itemView.findViewById(R.id.service_name);
                 synchronized_records = itemView.findViewById(R.id.synchronized_records);
                 issue_records = itemView.findViewById(R.id.issue_records);
                 progress = itemView.findViewById(R.id.arc_progress);

                 progress.setUnfinishedStrokeColor(cntxt.getColor(R.color.colorAccent));
                 progress.setFinishedStrokeColor(cntxt.getColor(R.color.colorPrimaryDark));
                 progress.setTextColor(cntxt.getColor(R.color.gray));
                 progress.setSuffixText("%");




        }

       public void populate(int position){
            this.position=position;
            SyncServiceReport ssr=items.get(position);

            service_name.setText(ssr.service_name);
            synchronized_records.setText(ssr.synchronized_records+" of "+ssr.total_records);
            issue_records.setText(ssr.issues_records);
           percent_calculation pc=new percent_calculation(ssr.total_records,ssr.synchronized_records);
            progress.setProgress(Integer.parseInt(pc.per_balance));

        }
        @Override
        public void onClick(View view) {

      }
    }
}
