package sparta.realm.apps.farmercontractor.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.Services.SynchronizationManager;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.R;
import sparta.realm.apps.farmercontractor.StaticDataLoader;
import sparta.realm.apps.farmercontractor.adapters.MemberAdapter;
import sparta.realm.apps.farmercontractor.adapters.MemberGroupAdapter;
import sparta.realm.apps.farmercontractor.adapters.SearchListFilterAdapter;
import sparta.realm.apps.farmercontractor.adapters.SelectedSearchFilterAdapter;
import sparta.realm.apps.farmercontractor.adapters.TrainingGroupAdapter;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupPlannedTrainingsBinding;
import sparta.realm.apps.farmercontractor.databinding.ActivityGroupRecordsBinding;
import sparta.realm.apps.farmercontractor.models.GroupParticipant;
import sparta.realm.apps.farmercontractor.models.Member;
import sparta.realm.apps.farmercontractor.models.MemberGroup;
import sparta.realm.apps.farmercontractor.models.SearchFilterItem;
import sparta.realm.apps.farmercontractor.models.TrainingGroup;
import sparta.realm.apps.farmercontractor.utils.Pager;
import sparta.realm.apps.farmercontractor.utils.SnapHelper;
import sparta.realm.spartautils.svars;

public class GroupPlannedTrainings extends AppCompatActivity {

    ActivityGroupPlannedTrainingsBinding binding;
    ArrayList<TrainingGroup> trainingGroups;
    Pager<TrainingGroup> pager;
    boolean search;
    Activity context;
    String[] defaultTableFilters = new String[0];
    String[] tableFilters;
    String module;
    String group_transaction_no,group_sid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGroupPlannedTrainingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        context = this;
        search = getIntent().getBooleanExtra("search", false);
        defaultTableFilters = getIntent().getStringArrayExtra("defaultTableFilters");
        tableFilters = getIntent().getStringArrayExtra("defaultTableFilters");
        module = getIntent().getStringExtra("moduleName");

        group_transaction_no =getIntent().getStringExtra("transaction_no");
        group_sid =getIntent().getStringExtra("sid");

        initUI();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("GroupRecords", "OnResume: HERE HERE");
        Globals.hasGroupRep = false;
        Globals.groupRepChair = false;
        Globals.groupRepTreas = false;
        Globals.groupRepSec = false;
        //resetting selected members for training
        Globals.selectedMembersTraining = new ArrayList<>();
        //initUI();
    }

    LinearLayoutManager linearLayoutManager;

    void setTableFiler(String[] table_filter) {
        if (defaultTableFilters == null || defaultTableFilters.length < 1) {
            tableFilters = table_filter;
            if (pager != null) pager.setTableFilters(tableFilters);
        } else {
            tableFilters = DatabaseManager.concatenate(table_filter, defaultTableFilters);
            if (pager != null) pager.setTableFilters(tableFilters);
        }

    }

    void initUI() {
//        binding.include.title.setText("Member accounts");

        binding.include.back.setOnClickListener(v -> onBackPressed());
//        memberGroups = Realm.databaseManager.loadObjectArray(MemberGroup.class, new Query().setOrderFilters(true, "name").setLimit(1));
        trainingGroups = new ArrayList<>();

        binding.memberList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        binding.memberList.setLayoutManager(linearLayoutManager);
        SnapHelper startSnapHelper = new SnapHelper();
        startSnapHelper.attachToRecyclerView(binding.memberList);

        setupFilter();
        pager = new Pager(TrainingGroup.class, 1, 1000, new Pager.PagerCallback() {
            @Override
            public <RM> void onDataRefreshed(ArrayList<RM> data, int from, int to, int total) {
                trainingGroups = (ArrayList<TrainingGroup>) data;
                binding.noRecordsLay.setVisibility((trainingGroups.size()) > 0 ? View.GONE : View.VISIBLE);

                binding.memberList.setAdapter(new TrainingGroupAdapter(trainingGroups, new TrainingGroupAdapter.onItemClickListener() {

                    @Override
                    public void onItemClick(TrainingGroup mem, View view) {
                            //Intent intent = new Intent(GroupRecords.this, GroupTrainings.class);
                            Intent intent = new Intent(GroupPlannedTrainings.this, GroupTrainings.class);
                            intent.putExtra("transaction_no", mem.transaction_no);
                            intent.putExtra("sid", mem.sid);
                            intent.putExtra("reg_mode", "2");

                            intent.putExtra("group_transaction_no", group_transaction_no);
                            intent.putExtra("group_sid", group_sid);
                            intent.putExtra("reg_mode", "2");

                            startActivity(intent);

                    }
                }));

            }
        }, binding.include.searchField, binding.include.prev, binding.include.next, binding.include.positionIndicator, binding.include.progress, null, new String[0], null, "name");

        pager.setTableFilters(new String[]{"group_id='" + group_sid + "'"});

        binding.include.filterIcon.setVisibility(View.GONE);
        binding.newMember.setVisibility(View.GONE);


        binding.newMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupPlannedTrainings.this, GroupManagement.class);
                intent.putExtra("reg_mode", "1");
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    void setupFilter() {
        binding.include.filterIcon.setOnClickListener(view -> binding.include.filterList.setVisibility(binding.include.filterList.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE));
        ArrayList<SearchFilterItem> searchFilterItems = StaticDataLoader.sampleSearchGroupFilterItems();
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.setItemPrefetchEnabled(true);
        binding.include.selectedFilterList.setLayoutManager(flowLayoutManager);
        binding.include.selectedFilterList.setAdapter(new SelectedSearchFilterAdapter(new ArrayList<>(), (mem, view) -> {
            mem.active = false;
            svars.setWorkingObject(mem, mem.sid);
            if (binding.include.filterList.getVisibility() == View.VISIBLE) {
                binding.include.filterList.getAdapter().notifyDataSetChanged();
            } else {
                setTableFiler(((SearchListFilterAdapter) binding.include.filterList.getAdapter()).generateTableFilter());
//                pager.setTableFilters(((SearchListFilterAdapter) binding.include.filterList.getAdapter()).generateTableFilter());
            }
            int pos = ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.indexOf(mem);
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.remove(mem);
            binding.include.selectedFilterList.getAdapter().notifyItemRemoved(pos);
        }));
        binding.include.filterList.setLayoutManager(new LinearLayoutManager(this));
        binding.include.filterList.setAdapter(new SearchListFilterAdapter(searchFilterItems, (mem, selectedItems, tableFilter) -> {
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.clear();
            ((SelectedSearchFilterAdapter) binding.include.selectedFilterList.getAdapter()).items.addAll(selectedItems);
            binding.include.selectedFilterList.getAdapter().notifyDataSetChanged();
//            pager.setTableFilters(tableFilter);
            setTableFiler(tableFilter);
            binding.include.selectedFilterList.invalidate();
        }));
    }
    void showSaveDialog(MemberGroup mem) {

        View aldv = LayoutInflater.from(this).inflate(R.layout.dialog_group_modules, null);
        final AlertDialog ald = new AlertDialog.Builder(this)
                .setView(aldv)
                .show();
        ald.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button groupDetails = aldv.findViewById(R.id.group_details);
        groupDetails.setOnClickListener(view -> {

            Intent intent = new Intent(GroupPlannedTrainings.this, GroupManagement.class);
            intent.putExtra("transaction_no", mem.transaction_no);
            intent.putExtra("sid", mem.sid);
            intent.putExtra("reg_mode", "2");
            //startActivity(intent);

            Pair<View, String> p0 = Pair.create(view.findViewById(R.id.container), "container");
            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.name), "name");
            try {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(context, p0, p1);
                startActivity(intent, options.toBundle());
                finish();
                Log.e("GroupRecords", "showSaveDialog: HERE HERE, TRY");
            } catch (IllegalArgumentException ex) {

                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                Log.e("GroupRecords", "showSaveDialog: HERE HERE, CATCH");
                finish();
            }

            ald.dismiss();
        });

        Button groupReport = aldv.findViewById(R.id.group_comp_report);
        groupReport.setOnClickListener(view -> {

            //Toast.makeText(context, "Module Under Development", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(GroupPlannedTrainings.this, GroupCompetitionReports.class);
            intent.putExtra("transaction_no", mem.transaction_no);
            intent.putExtra("sid", mem.sid);
            intent.putExtra("reg_mode", "2");

            //startActivity(intent);

            Pair<View, String> p0 = Pair.create(view.findViewById(R.id.container), "container");
            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.name), "name");
            try {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(context, p0, p1);
                startActivity(intent, options.toBundle());
            } catch (IllegalArgumentException ex) {
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }

            ald.dismiss();
        });



    }
}