package sparta.realm.apps.farmercontractor.services;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import androidx.documentfile.provider.DocumentFile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import sparta.realm.DataManagement.Models.Query;
import sparta.realm.Realm;
import sparta.realm.Services.DatabaseManager;
import sparta.realm.apps.farmercontractor.AuthenticationManager;
import sparta.realm.apps.farmercontractor.BuildConfig;
import sparta.realm.apps.farmercontractor.Globals;
import sparta.realm.apps.farmercontractor.RequestManager;
import sparta.realm.apps.farmercontractor.models.system.AppVersion;
import sparta.realm.apps.farmercontractor.utils.StringCompare;
import sparta.realm.spartamodels.percent_calculation;
import sparta.realm.spartautils.app_control.models.sparta_app_version;
import sparta.realm.spartautils.app_control.services.App_updates;
import sparta.realm.spartautils.svars;

import static sparta.realm.spartautils.svars.current_app_config;

public class AppUpdate extends Service {

    String logTag = "AppUpdate";

    public AppUpdate() {
    }

    private final IBinder binder = new AppUpdateBinder();
    private Status currentStatus = Status.Idle;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        checkStatus();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class AppUpdateBinder extends Binder {

        public AppUpdate getService() {
            return AppUpdate.this;
        }

    }

    public enum Status {
        Idle,
        Downloading,
        CheckingForUpdates,
        ReadyToInstall,
        ReadyToCheckForUpdates,
        ReadyToDownload,
        OnTheLatestVersion,
        onDownloadFailed
    }

    public Status getCurrentStatus() {
        return currentStatus;
    }

    public interface StatusChangeListener {
        void onStatusChanged(Status status);

        void onDownloadProgress(int percentage);
    }

    StatusChangeListener statusChangeListener = new StatusChangeListener() {
        @Override
        public void onStatusChanged(Status status) {

        }

        @Override
        public void onDownloadProgress(int percentage) {

        }
    };

    public void setStatusChangeListener(StatusChangeListener statusChangeListener) {
        this.statusChangeListener = statusChangeListener;
    }

    public void checkForUpdates() {
        final JSONObject postData = new JSONObject();
        String[] app_package_name = new String[]{svars.current_app_name()};
        try {
            postData.put("RequestTime", Calendar.getInstance().getTime().toString());
            postData.put("RequestData", new JSONArray(app_package_name));
            Log.e("Post data", "" + postData.toString());
        } catch (Exception ex) {
            return;

        }
        statusChangeListener.onStatusChanged(Status.CheckingForUpdates);
        new RequestManager().requestPost("http://ta.cs4africa.com:2222/api/AppStore/LoadApp", null, postData, new RequestManager.RequestCallback() {

            @Override
            public void onApiConnectionFailed() {

            }

            @Override
            public void OnRequestSuccessfully(JSONObject response) {
                try {
                    if (response.getInt("StatusCode") == 1) {


                        JSONObject app_j = response.getJSONObject("Response");
                        JSONArray app_versions = app_j.getJSONArray("application_versions");

                        for (int l = 0; l < app_versions.length(); l++) {
                            JSONObject appcateg_j = app_versions.getJSONObject(l);

                            if (Realm.databaseManager.loadObject(AppVersion.class, new Query().setTableFilters("sid='" + appcateg_j.getString("Version_id") + "'")) == null) {
                                appcateg_j.put("id", appcateg_j.getString("Version_id"));
                               save_versions(appcateg_j);


                            }
                        }
//                        String time_now = svars.gett_time();
//                        svars.set_update_check_time(AppUpdate.this, time_now);
                        Globals.setUpdateCheckTime(System.currentTimeMillis());

                        checkStatus();


                    } else {
//                    checking_for_updates = false;
                        //030562
                        //191846

                        Log.e("Load apps4_categs =>", "NO SUCCESS");
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void OnRequestFailed() {
                checkStatus();

            }
        });


    }

    long updateChackDelay = 1000 * 60 * 10;
    public void save_versions(JSONObject json_sav) {
        try {

                ContentValues cv = new ContentValues();

                cv.put("sid", json_sav.getString("Version_id"));
                cv.put("version_code", json_sav.getString("Version_code"));
                cv.put("version_name", json_sav.getString("Version_name"));
                cv.put("release_notes", json_sav.getString("Release_notes"));
                cv.put("release_name", json_sav.getString("Release_name"));
                cv.put("creation_time", json_sav.getString("creation_time"));
                cv.put("download_link", json_sav.getString("Landing_page_url"));
//cv.put("sid",appcateg_j.getString("icon"));

                Log.e("Apk version ", "About to insert ");

                //;
                Log.e("Apk version ", "Inserted " + DatabaseManager.database.insert("app_versions_table", null, cv));


        } catch (Exception exx) {
            Log.e("Version insert error :", " " + exx.getMessage());
        }

    }

    public void checkStatus() {
        if (System.currentTimeMillis() - Globals.updateCheckTime() > updateChackDelay) {
            statusChangeListener.onStatusChanged(Status.ReadyToCheckForUpdates);

        } else {

        }
        {

            AppVersion appVersion = Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false).setLimit(1));
            if (appVersion == null) {

                statusChangeListener.onStatusChanged(Status.ReadyToCheckForUpdates);
                return;
            }
            if (/*Integer.parseInt(appVersion.version_code) > BuildConfig.VERSION_CODE || */new StringCompare(appVersion.version_name).compareTo(new StringCompare(BuildConfig.VERSION_NAME)) > 0) {
                if (appVersion.data_status == null || !appVersion.data_status.equals("d")) {//Check if the latest app is downloaded
                    statusChangeListener.onStatusChanged(Status.ReadyToDownload);
                    downloadUpdates(Realm.databaseManager.loadObject(AppVersion.class, new Query().setColumns("app_versions_table.*", "cast(sid as INTEGER) as sid2").addOrderFilters("sid2", false)));
                } else {
                    statusChangeListener.onStatusChanged(Status.ReadyToInstall);
                }

            } else {
                statusChangeListener.onStatusChanged(Status.OnTheLatestVersion);
//
            }

        }


    }

    public void downloadUpdates(AppVersion appVersion) {

        statusChangeListener.onStatusChanged(Status.ReadyToDownload);
        //new RequestManager().download(appVersion.download_link, current_app_config(Realm.context).app_folder_path+"/"+Globals.getTransactionNo()+".apk", new RequestManager.DownloadCallback();
        new RequestManager().download(appVersion.download_link, current_app_config(Realm.context).app_folder_path, new RequestManager.DownloadCallback() {
            @Override
            public void onDownloadComplete(File file) {

                statusChangeListener.onStatusChanged(Status.ReadyToInstall);
                //updating the local path
                Log.e("AppUpdate", "onDownloadComplete: " + file.getAbsolutePath());
                Realm.databaseManager.update_downloaded_versions(appVersion.sid,file.getAbsolutePath());
            }

            @Override
            public void onApiConnectionStatusUpdated(int status) {
                switch (status){

                    case 200:
                     statusChangeListener.onStatusChanged(Status.Downloading);

                     break;

                    default:
                        statusChangeListener.onStatusChanged(Status.onDownloadFailed);

                        break;
                }

            }

            @Override
            public void onDownloadProgress(long total, long downloaded) {
                percent_calculation pc = new percent_calculation(total + "", downloaded + "");
                statusChangeListener.onDownloadProgress(Integer.parseInt(pc.per_balance));
            }
        });


    }
    //1436

    public void installApp(Activity activity, AppVersion appVersion) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                builder.detectFileUriExposure();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                installAfterQ(Uri.fromFile(new File(appVersion.local_path)));
            }else{
         /**/   final Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.fromFile(new File(current_app_config(Realm.context).file_path_app_downloads + "/" + appVersion.download_link.split("/")[appVersion.download_link.split("/").length - 1])), "application/vnd.android.package-archive");
            intent.setDataAndType(Uri.fromFile(new File(appVersion.local_path)), "application/vnd.android.package-archive");
//            intent.setDataAndType(Uri.fromFile(new File(appVersion.local_path), "application/vnd.android.package-archive"));

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
                }
        } catch (Exception ex) {
            Log.e(logTag, "Error installing :" + ex.getMessage());

        }
    }

    public void installAfterQ(Uri uri) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                InputStream inputStream = getApplication().getContentResolver().openInputStream(uri);

                long length = DocumentFile.fromSingleUri(getApplication(), uri).length();
                PackageInstaller installer = getApplication().getPackageManager().getPackageInstaller();
                PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
                int sessionId = installer.createSession(params);
                PackageInstaller.Session session = installer.openSession(sessionId);

                File file = new File(uri.getPath());
                OutputStream outputStream = session.openWrite(file.getName(), 0, length);

                byte[] buf = new byte[1024*16];
                int len;
                while ((len = inputStream.read(buf)) > 0) {
                    outputStream.write(buf, 0, (int) len);
                }


                session.fsync(outputStream);

                outputStream.close();
                inputStream.close();

                Intent intent = new Intent(getApplication(), UpdateReceiver.class);
//                intent.addFlags(FLAG_IM)
                PendingIntent pi = PendingIntent.getBroadcast(getApplication(), 3439, intent, PendingIntent.FLAG_MUTABLE);
                session.commit(pi.getIntentSender());
                session.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}