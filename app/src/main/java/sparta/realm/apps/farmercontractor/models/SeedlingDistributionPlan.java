package sparta.realm.apps.farmercontractor.models;


        import com.realm.annotations.DynamicClass;
        import com.realm.annotations.DynamicProperty;
        import com.realm.annotations.RealmModel;
        import com.realm.annotations.SyncDescription;

        import java.io.Serializable;

@DynamicClass(table_name = "distribution_plans")
@SyncDescription(service_name = "Seedling Distribution Plan", download_link = "/FarmersContract/Group/seedlingDistributionMobile", is_ok_position = "JO:isOkay", download_array_position = "JO:result;JO:result", service_type = SyncDescription.service_type.Download)
public class SeedlingDistributionPlan  extends RealmModel implements Serializable {
    {
//        {
//            "$id": "7",
//                "id": 2,
//                "nursery_name": "new ffggh",
//                "nursery_type_id": 3,
//                "total_seedling": 200,
//                "seedling_issued": null,
//                "transport_means": "man power",
//                "issued_by": 6884,
//                "received_by": 7082,
//                "destination": "den",
//                "group_id": 1,
//                "agroforest_agent_id": 7082,
//                "slo_resposible": "2333",
//                "distribution_date": "2024-02-23T00:00:00",
//                "is_active": true,
//                "datecomparer": 17087050221462935,
//                "syncdate": "2024-02-23T16:17:02",
//                "transaction_no": "202422316172",
//                "user_id": null,
//                "nursery_type": null,
//                "issued_name": null,
//                "receive_name": null,
//                "group_name": null,
//                "agro_name": null
//        }
    }

    @DynamicProperty(json_key = "nursery_name")
    public String name;

    @DynamicProperty(json_key = "nursery_type_id")
    public String nursery_type_id;

    @DynamicProperty(json_key = "total_seedling")
    public String total_seedling;

    @DynamicProperty(json_key = "seedling_issued")
    public String seedling_issued;

    @DynamicProperty(json_key = "transport_means")
    public String transport_means;

    @DynamicProperty(json_key = "issued_by")
    public String issued_by;

    @DynamicProperty(json_key = "received_by")
    public String received_by;

    @DynamicProperty(json_key = "destination")
    public String destination;

    @DynamicProperty(json_key = "group_id")
    public String group_id;

    @DynamicProperty(json_key = "agroforest_agent_id")
    public String agroforest_agent_id;

    @DynamicProperty(json_key = "slo_resposible")
    public String slo_resposible;

    @DynamicProperty(json_key = "distribution_date")
    public String distribution_date;

    @DynamicProperty(json_key = "syncdate")
    public String syncdate;

}

