package sparta.realm.apps.farmercontractor.models;


import com.realm.annotations.DynamicClass;
import com.realm.annotations.DynamicProperty;
import com.realm.annotations.RealmModel;

import java.io.Serializable;


@DynamicClass(table_name = "member_search_filter")
public class SearchFilterItem extends RealmModel implements Serializable {


    @DynamicProperty(json_key = "column")
    public String column;

    public String parent_data_name;
    public String parent_column;

    @DynamicProperty(json_key = "name")
    public String name;

    @DynamicProperty(json_key = "dataset")
    public String dataset;


    @DynamicProperty(json_key = "value")
    public String value;

    public boolean active;

    public SearchFilterItem(String sid,String name, String column, String dataset) {
        this.sid = sid;
        this.name = name;
        this.column = column;
        this.dataset = dataset;
    }


    public SearchFilterItem(String name, String column, String dataset) {
        this.name = name;
        this.column = column;
        this.dataset = dataset;
    }

    public SearchFilterItem(String sid,String parent_data_name, String parent_column, String name, String column, String dataset) {
        this.sid = sid;
        this.parent_data_name = parent_data_name;
        this.parent_column = parent_column;
        this.name = name;
        this.column = column;
        this.dataset = dataset;
    }

    public SearchFilterItem() {


    }

}
